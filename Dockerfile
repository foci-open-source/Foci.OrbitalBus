FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln ./

COPY OrbitalBus/Foci.Orbital.Agent/Foci.Orbital.Agent.csproj ./OrbitalBus/Foci.Orbital.Agent/

# Copy everything else and build
COPY . ./
RUN dotnet publish OrbitalBus/Foci.Orbital.Agent/Foci.Orbital.Agent.csproj -c Release -o /app/out

RUN mkdir -p /app/out/Configs

# Build runtime image
FROM microsoft/dotnet:sdk
WORKDIR /app
COPY --from=build-env /app/out .

VOLUME /Configs
VOLUME /logs

RUN chmod -R u+w /app
ENTRYPOINT ["dotnet", "Foci.Orbital.Agent.dll"]