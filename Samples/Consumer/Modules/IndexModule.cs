using Nancy;

namespace Consumer.Modules
{
    /// <summary>
    /// Index Module with default constructor for nancy.
    /// </summary>
    public class IndexModule : NancyModule
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IndexModule()
        {
        }
    }
}