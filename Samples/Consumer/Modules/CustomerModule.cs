using Bogus;
using Consumer.Models;
using Nancy;
using Nancy.ModelBinding;
using System;

namespace Consumer.Modules
{
    /// <summary>
    /// Class to define the customer's HTTP endpoints in the Nancy consumer.
    /// </summary>
    public class CustomerModule : NancyModule
    {
        private readonly Faker faker = new Faker(); 
        /// <summary>
        /// Instance of customer module.
        /// </summary>
        public CustomerModule() : base("/customer")
        {
            // Receive customer and return success string
            Post("/", args =>
            {
                var customer = this.Bind<Customer>();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Post[/]: Received Customer Information: {0}\n", customer.ToString());
                Console.ResetColor();

                return "Success!";
            });

            // Return a randomly generated customer 
            Get("/", args =>
            {
                var id = Request.Query.Id;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Get[/]: Received customer ID: {0}\n", id);
                Console.ResetColor();

                var customer = new Customer()
                {
                    Id = id,
                    Name = new Name()
                    {
                        FirstName = faker.Name.FirstName(),
                        MiddleName = faker.Name.FirstName(),
                        LastName = faker.Name.LastName()
                    },
                    Address = new Address()
                    {
                        AddressLine1 = faker.Address.StreetAddress(),
                        AddressLine2 = "",
                        City = faker.Address.City(),
                        ProvinceState = faker.Address.StateAbbr(),
                        Country = faker.Address.Country(),
                        PostalZipCode = faker.Address.ZipCode()
                    }
                };

                return Response.AsJson(customer);
            });

            // Return http status code: NotFound
            Post("/StatusCodeError", args =>
            {

                var body = this.Bind<HttpStatusCodeError>();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Post[/StatusCodeError]: Received Message: {0}", body.ArbitraryString);
                Console.ResetColor();

                return HttpStatusCode.NotFound;
            });

            // Return an unserializable type.
            Post("/WrongType", args =>
            {
                var body = this.Bind<HttpStatusCodeError>();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Post[/WrongType]: Received Message: {0}", body.ArbitraryString);
                Console.ResetColor();

                var wrongObject = new WrongObjectResponse() { ArbitraryString = body.ArbitraryString };

                return Response.AsJson(wrongObject);
            });
        }
    }
}