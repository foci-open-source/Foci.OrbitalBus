﻿using Consumer.Models.Configurations;
using Newtonsoft.Json;
using NLog;
using System;
using System.IO;
using System.Reflection;

namespace Consumer.App_Start
{
    /// <summary>
    /// Reads a JSON file containing the properties necessary to set up a consumer and connect with the Agent.
    /// </summary>
    public static class ConfigurationInitialization
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        public static ConsumerConfiguration Config;

        /// <summary>
        /// Reads from the configuration file and deserializes to the configuration model.
        /// </summary>
        /// <param name="fileName">String representation of the configuration file's name.</param>
        public static void Configure(string fileName)
        {
            var filePath = Path.Combine(GetExecutingAssemblyLocation(), "Configs", fileName);
            if (File.Exists(filePath))
            {
                string json = File.ReadAllText(filePath);
                if (string.IsNullOrWhiteSpace(json))
                {
                    log.Warn("Unable to read configuration file: {0}, using default configuration instead.", fileName);
                    Config = new ConsumerConfiguration();
                    return;
                }

                Config = JsonConvert.DeserializeObject<ConsumerConfiguration>(json);
            }
            else
            {
                log.Warn("Unable to find configuration file: {0}, using default configuration instead.", fileName);
                Config = new ConsumerConfiguration();
            }
        }

        /// <summary>
        /// Gets the directory of the executing assembly
        /// </summary>
        /// <returns>String representation of the directory path.</returns>
        private static string GetExecutingAssemblyLocation()
        {
            var fileUri = new Uri(Assembly.GetExecutingAssembly().Location);
            var directory = new Uri(fileUri, ".");
            return directory.LocalPath;
        }
    }
}
