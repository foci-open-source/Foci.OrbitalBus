namespace Consumer.Models.Configurations
{
    /// <summary>
    /// Model for configuration properties.
    /// </summary>
    public class ConsumerConfiguration
    {
        /// <summary>
        /// String representation of the Nancy IP address to be used.
        /// </summary>
        public string NancyIp { get; set; } = "localhost";

        /// <summary>
        /// String representation of the Nancy port to be used.
        /// </summary>
        public string NancyPort { get; set; } = "3580";
    }
}
