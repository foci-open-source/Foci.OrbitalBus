﻿namespace Consumer.Models
{
    /// <summary>
    /// Class containing a string to represent a wrong object.
    /// </summary>
    public class WrongObjectResponse
    {
        /// <summary>
        /// String representation to be used for testing.
        /// </summary>
        public string ArbitraryString { get; set; }
    }
}
