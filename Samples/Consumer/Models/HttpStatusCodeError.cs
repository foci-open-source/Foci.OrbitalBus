﻿namespace Consumer.Models
{
    /// <summary>
    /// Place holder with a string representing an HTTP status code.
    /// </summary>
    public class HttpStatusCodeError
    {
        public string ArbitraryString { get; set; }
    }
}
