﻿namespace Consumer.Models
{
    /// <summary>
    /// Response type object for customer. 
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Customer()
        {
            this.Name = new Name();
            this.Address = new Address();
        }
        
        public int Id { get; set; }
        public Name Name { get; set; }
        public Address Address { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, First Name: {1}, Last Name: {2}, Address Line 1: {3}", Id.ToString(), Name.FirstName, Name.LastName, Address.AddressLine1);
        }
    }
}
