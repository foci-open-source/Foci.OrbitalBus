﻿namespace Consumer.Models
{
    /// <summary>
    /// Model for Customer's Name.
    /// </summary>
    public class Name
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}
