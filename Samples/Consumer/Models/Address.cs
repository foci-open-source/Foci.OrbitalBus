﻿namespace Consumer.Models
{
    /// <summary>
    /// Model for Customer's Address.
    /// </summary>
    public class Address
    {
        public int StreetNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string ProvinceState { get; set; }
        public string Country { get; set; }
        public string PostalZipCode { get; set; }
    }
}
