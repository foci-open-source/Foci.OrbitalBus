﻿using Consumer.App_Start;
using Nancy.Hosting.Self;
using System;
using System.Threading;

namespace Consumer
{
    class Program
    {
        private const string defaultConfigurationFile = "orbital.json";

        /// <summary>
        /// Main that takes in user's configuration file name.
        /// </summary>
        /// <param name="args">String of arrays representing all the parameters entered by the user.</param>
        static void Main(string[] args)
        {
            Console.Title = "Consumer";
            ConfigurationInitialization.Configure((args == null || args.Length == 0) ? defaultConfigurationFile : args[0]);

            var ip = ConfigurationInitialization.Config.NancyIp;
            var port = ConfigurationInitialization.Config.NancyPort;
     
            var uri = new Uri(string.Format("http://{0}:{1}", ip, port));
            using (var host1 = new NancyHost(uri))
            {
                host1.Start();

                Console.WriteLine("Your application is running on " + uri);
                Thread.Sleep(Timeout.Infinite);
            }
            Console.WriteLine("No longer listening.");
        }
    }
}
