﻿using Nancy;

namespace Consumer
{
    public class IndexModule : NancyModule
    {
        public IndexModule()
        {
            Get("/", parameters =>
            {
                return View["index"];
            });
        }
    }
}