﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceUploader.OrbitalEasyNetQ
{
    internal static class TypeNameLookup
    {
        public const string updateservice = "Foci.Orbital.Agent.Models.UpdateService:Foci.Orbital.Agent";
        public const string topicupdateservice = "ServiceUploader.Models.UpdateService:ServiceUploader";
    }
}
