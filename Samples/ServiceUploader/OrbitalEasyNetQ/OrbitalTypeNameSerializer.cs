﻿using EasyNetQ;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace ServiceUploader.OrbitalEasyNetQ
{
    /// <summary>
    /// Replace easynetq component with our custom type name serializer.
    /// </summary>
    public class OrbitalTypeNameSerializer : ITypeNameSerializer
    {
        private readonly ConcurrentDictionary<string, Type> deserializedTypes = new ConcurrentDictionary<string, Type>();
        private readonly ConcurrentDictionary<Type, string> serializedTypes = new ConcurrentDictionary<Type, string>();


        /// <summary>
        /// Takes a string representating a type name and returns a C# type.
        /// </summary>
        /// <param name="typeName">String representation of the Type object name to be matched.</param>
        /// <returns>The Type object matched with the paramater value of typeName/</returns>
        public Type DeSerialize(string typeName)
        {
            return deserializedTypes.GetOrAdd(typeName, t =>
            {
                var types = t.Split(',').ToList();
                if (types.Count == 1)
                {
                    var type = GetTypeWithOnePart(types[0]);

                    return type;
                }
                else
                {
                    var type = GetTypeWithMoreThanOnePart(types, t);

                    return type;
                }
            });
        }

        /// <summary>
        /// A helper method to perform work if the there is more than one type embedded in the string. Ie. type with generics.
        /// </summary>
        /// <param name="types">collection of strings representing the Type of object names.</param>
        /// <param name="t">String representation of Type of object name</param>
        /// <returns>Type of object</returns>
        private static Type GetTypeWithMoreThanOnePart(List<string> types, string t)
        {
            var nameParts = types[0].Split(':');

            if (nameParts.Length != 2)
            {
                throw new EasyNetQException(
                    "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
            }

            var baseType = Type.GetType(nameParts[0] + ", " + nameParts[1]);

            var genericTypes = new List<Type>();

            foreach (var genericTypeString in types.Skip(1))
            {
                var genericNameParts = genericTypeString.Split(':');
                if (genericNameParts.Length != 2)
                {
                    throw new EasyNetQException(
                        "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
                }

                var genericType = Type.GetType(genericNameParts[0] + ", " + genericNameParts[1]);
                genericTypes.Add(genericType);
            }

            var type = baseType.MakeGenericType(genericTypes.ToArray());

            if (type == null)
            {
                throw new EasyNetQException("Cannot find type {0}", t);
            }
            return type;
        }

        /// <summary>
        /// A helper method to perform work if the there is only one type embedded in the string. Ie. type with no generics.
        /// </summary>
        /// <param name="t">String representation of Type of object name</param>
        /// <returns>Type of object</returns>
        private static Type GetTypeWithOnePart(string t)
        {
            var nameParts = t.Split(':');
            if (nameParts.Length != 2)
            {
                throw new EasyNetQException(
                    "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
            }

            var type = Type.GetType(nameParts[0] + ", " + nameParts[1]);
            if (type == null)
            {
                throw new EasyNetQException("Cannot find type {0}", t);
            }
            return type;
        }

        /// <summary>
        /// Serializes a C# type to a string representation.
        /// </summary>
        /// <param name="type">Type object to be matched to a TypeNameLookup string representation of the Type object's name.<see cref="System.Console.Write(Foci.Orbital.OrbitalConnector.Lookups.TypeNameLookup)"/></param>
        /// <returns>String representation of the Type object's name.</returns>
        public string Serialize(Type type)
        {
            var typeName = type.FullName + ":" + type.Assembly.GetName().Name;
            switch (typeName)
            {
                case TypeNameLookup.topicupdateservice:
                    return TypeNameLookup.updateservice;
                default:
                    throw new Exception("Serialize type does not exist in ServiceUploader");
            }
        }
    }
}
