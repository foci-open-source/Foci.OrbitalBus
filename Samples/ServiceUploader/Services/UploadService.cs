﻿using EasyNetQ;
using RestSharp;
using ServiceUploader.Models;
using ServiceUploader.Models.Configurations;
using ServiceUploader.Models.Consul;
using ServiceUploader.Models.Service;
using ServiceUploader.OrbitalEasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceUploader.Services
{
    public class UploadService
    {
        private readonly IRestClient client;
        public readonly ConsulCallInfo setKeyValue;
        private readonly HostConfiguration host;

        /// <summary>
        /// Constructor that will setup the rest client
        /// </summary>
        /// <param name="configuration">configuration containing Consul URL</param>
        /// <param name="client">Rest client object</param>
        public UploadService(UploaderConfiguration configuration, IRestClient client)
        {
            this.client = client;
            this.client.BaseUrl = new Uri(string.Format("http://{0}:{1}", configuration.ConsulIP, configuration.ConsulPort));
            setKeyValue = new ConsulCallInfo
            {
                Endpoint = "v1/kv/{key}",
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
            this.host = new HostConfiguration
            {
                Host = configuration.RabbitIP,
                Port = configuration.RabbitPort
            };
        }

        /// <summary>
        /// Upload service definition to Consul
        /// </summary>
        /// <param name="serviceDefinition">Object containing the service definition</param>
        public bool Upload(ServiceDefinition serviceDefinition)
        {
            var key = string.Format("orbital.{0}.definition", serviceDefinition.serviceName);
            var urlSegments = new Dictionary<string, string> { { "key", key } };
            var request = CreatePutRequestWithParameters(setKeyValue, serviceDefinition, urlSegments);
            var result = client.Execute<bool>(request);

            if (result.ErrorException != null)
            {
                return false;
            }

            this.NotifyAgent();
            return result.IsSuccessful;
        }

        #region Private Methods

        /// <summary>
        /// Notifies the Agent that a service was uploaded.
        /// </summary>
        private void NotifyAgent()
        {
            var connection1 = new ConnectionConfiguration();
            
            connection1.Hosts = new List<HostConfiguration> { this.host };

            var bus = RabbitHutch.CreateBus(connection1, serviceProvider =>
            {
                serviceProvider.Register<ITypeNameSerializer>(serviceCreator => new OrbitalTypeNameSerializer());
            });

            bus.Publish<UpdateService>(new UpdateService() { ServiceName = "ConsumerService" }, "orbital.service.publish");
            bus.Dispose();
        }
        /// <summary>
        /// Create a PUT request with consul info.  URL parameters are injected into the endpoint.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="body">The already serialized string.</param>
        /// <param name="urlParameters">The parameters to inject into the endpoint.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreatePutRequestWithParameters(ConsulCallInfo consulCallInfo, ServiceDefinition body, IDictionary<string, string> urlParameters)
        {
            var request = CreatePutRequest(consulCallInfo, body);

            //Create URL parameters
            if (urlParameters != null && urlParameters.Count > 0)
            {
                urlParameters.ToList().ForEach(kvp => request.AddUrlSegment(kvp.Key, kvp.Value));
            }

            return request;
        }

        /// <summary>
        /// Create a PUT request with consul info.  No URL parameters, just a fixed endpoint with headers and a body.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="body">The body to serialize into JSON.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreatePutRequest(ConsulCallInfo consulCallInfo, ServiceDefinition body)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.PUT);

            //Create header
            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }

            //Create body
            if (body != null)
            {
                request.AddJsonBody(body);
            }

            return request;
        }
        #endregion
    }
}
