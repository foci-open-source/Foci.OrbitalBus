﻿using System;
using System.IO;
using System.Reflection;

namespace ServiceUploader.Services
{
    public class FileService
    {
        /// <summary>
        /// Returns the runtime assembly directory
        /// </summary>
        private string AssemblyLocation
        {
            get
            {
                UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
                return Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            }
        }

        /// <summary>
        /// Read text from the given file at the assembly location
        /// </summary>
        /// <param name="fileName">Name of the file</param>
        /// <returns>Content of the file</returns>
        public string ReadFile(string fileName)
        {
            string path = Path.Combine(AssemblyLocation, fileName);
            return File.ReadAllText(path);
        }

        /// <summary>
        /// Check if the given file exists at the assembly location
        /// </summary>
        /// <param name="fileName">Name of the file</param>
        /// <returns>True if file exist, false otherwise</returns>
        public bool FileExists(string fileName)
        {
            string path = Path.Combine(AssemblyLocation, fileName);
            return File.Exists(path);
        }
    }
}
