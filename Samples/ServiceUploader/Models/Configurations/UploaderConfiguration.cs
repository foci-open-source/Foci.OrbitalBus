﻿using Newtonsoft.Json;

namespace ServiceUploader.Models.Configurations
{
    public class UploaderConfiguration
    {
        [JsonProperty("consulIP")]
        public string ConsulIP { get; internal set; } = "localhost";
        [JsonProperty("consulPort")]
        public ushort ConsulPort { get; internal set; } = 8500;
        [JsonProperty("rabbitIP")]
        public string RabbitIP { get; internal set; } = "localhost";
        [JsonProperty("rabbitPort")]
        public ushort RabbitPort { get; internal set; } = 5672;
    }
}
