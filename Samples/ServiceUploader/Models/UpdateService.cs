﻿using EasyNetQ;
using Newtonsoft.Json;

namespace ServiceUploader.Models
{
    [Queue("UpdateServiceQueue", ExchangeName = "orbital.service.publish")]
    public class UpdateService
    {
        /// <summary>
        /// String representation of the service name to be updated.
        /// </summary>
        [JsonProperty("servicename")]
        public string ServiceName { get; internal set; }
    }
}
