﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationDefinition
    {
        /// <summary>
        /// String representation of operation name
        /// </summary>
        [JsonProperty("name")]
        public string name { get; internal set; }

        /// <summary>
        /// True if this operation is sync, false if this operation is async
        /// </summary>
        [JsonProperty("sync")]
        public bool sync { get; internal set; }

        /// <summary>
        /// Request and/or response schema for this operation
        /// </summary>
        [JsonProperty("schemas")]
        public OperationSchemas schemas { get; internal set; }

        /// <summary>
        /// String representation of the Javascript translation
        /// </summary>
        [JsonProperty("translation")]
        public string translation { get; internal set; }

        /// <summary>
        /// Object that contains adapter type and adapter configuration
        /// </summary>
        [JsonProperty("adapter")]
        public AdapterConfiguration adapter { get; internal set; }

        /// <summary>
        /// Object that contains flag to determine if request/response need validation
        /// </summary>
        [JsonProperty("validate")]
        public OperationValidation validate { get; set; }

        /// <summary>
        /// Object that contains http configuration
        /// </summary>
        [JsonProperty("http")]
        public HttpConfiguration http { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public OperationDefinition()
        {
            schemas = new OperationSchemas();
            adapter = new AdapterConfiguration();
            validate = new OperationValidation();
            http = new HttpConfiguration();
        }
    }
}
