﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationValidation
    {
        /// <summary>
        /// True to validate request message, false otherwise
        /// </summary>
        [JsonProperty("request")]
        public bool request { get; internal set; }

        /// <summary>
        /// True to validate response message, false otherwise
        /// </summary>
        [JsonProperty("response")]
        public bool response { get; internal set; }
    }
}
