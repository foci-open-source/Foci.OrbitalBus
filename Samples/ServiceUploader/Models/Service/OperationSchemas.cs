﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationSchemas
    {
        /// <summary>
        /// String representation of the request schema
        /// </summary>
        [JsonProperty("request")]
        public string request { get; internal set; }

        /// <summary>
        /// String representation of the response schema
        /// </summary>
        [JsonProperty("response")]
        public string response { get; internal set; }
    }
}
