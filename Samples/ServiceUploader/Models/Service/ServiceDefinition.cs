﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class ServiceDefinition
    {
        /// <summary>
        /// String representation of service name
        /// </summary>
        [JsonProperty("serviceName")]
        public string serviceName { get; internal set; }

        /// <summary>
        /// List of operations that are available
        /// </summary>
        [JsonProperty("operations")]
        public IEnumerable<OperationDefinition> operations { get; internal set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public ServiceDefinition()
        {
            operations = new List<OperationDefinition>();
        }
    }
}
