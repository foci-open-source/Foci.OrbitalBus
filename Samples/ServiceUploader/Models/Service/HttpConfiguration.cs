﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class HttpConfiguration
    {
        /// <summary>
        /// True if http endpoint is enabled for the operation, false otherwise
        /// </summary>
        [JsonProperty("enabled")]
        public bool enabled { get; internal set; }

        /// <summary>
        /// String representation of the operation's http endpoint verb
        /// </summary>
        [JsonProperty("verb")]
        public string verb { get; internal set; }

        /// <summary>
        /// True if this operation can be access from the root endpoint using the verb, false otherwise
        /// </summary>
        [JsonProperty("verbOnly")]
        public bool verbOnly { get; internal set; }
    }
}
