﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace ServiceUploader.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class AdapterConfiguration
    {
        /// <summary>
        /// String representation of adapter type
        /// </summary>
        [JsonProperty("type")]
        public string type { get; internal set; }

        /// <summary>
        /// String representation of adapter configuration
        /// </summary>
        [JsonProperty("configuration")]
        public string configuration { get; internal set; }
    }
}
