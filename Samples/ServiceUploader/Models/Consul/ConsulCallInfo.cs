﻿using System.Collections.Generic;

namespace ServiceUploader.Models.Consul
{
    /// <summary>
    /// Class containing the endpoint to hit and headers necessary for the requests.
    /// </summary>
    public class ConsulCallInfo
    {
        public string Endpoint { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}
