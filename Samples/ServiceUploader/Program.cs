﻿using Newtonsoft.Json;
using RestSharp;
using ServiceUploader.Models.Configurations;
using ServiceUploader.Models.Service;
using ServiceUploader.Services;
using System;

namespace ServiceUploader
{
    public class Program
    {
        private const string serviceDefinitionFile = "ServiceDefinition.json";

        public static int Main(string[] args)
        {
            try
            {
                FileService fileService = new FileService();

                //Try to read configuration from file, if not fond, use default
                UploaderConfiguration configuration = new UploaderConfiguration();
                if ((args != null && args.Length > 0) && fileService.FileExists(args[0]))
                {
                    string json = fileService.ReadFile(args[0]);
                    if (!string.IsNullOrWhiteSpace(json))
                    {
                        Console.WriteLine("Configuring the service uploader using configurations from {0}", args[0]);
                        configuration = JsonConvert.DeserializeObject<UploaderConfiguration>(json);
                    }
                }

                //Get service definition from file and upload it to Consul
                UploadService uploader = new UploadService(configuration, new RestClient());

                string definitionJson = fileService.ReadFile(serviceDefinitionFile);
                if (string.IsNullOrWhiteSpace(definitionJson))
                {
                    throw new Exception("The provided file (" + serviceDefinitionFile + ") cannot be empty");
                }
                ServiceDefinition serviceDefinition = JsonConvert.DeserializeObject<ServiceDefinition>(definitionJson);

                Console.WriteLine("Uploading service definition in {0}", serviceDefinitionFile);
                if (uploader.Upload(serviceDefinition))
                {
                    Console.WriteLine("Successfully uploaded the service definition to Consul");
                }
                else
                {
                    Console.WriteLine("Unable to upload the service definition to Consul");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to upload the service definition due to {0}: {1}", e.GetType(), e.Message);
                return -1;
            }

            return 0;
        }
    }
}
