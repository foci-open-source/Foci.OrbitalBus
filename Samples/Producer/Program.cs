﻿using Bogus;
using Newtonsoft.Json;
using Producer.Models;
using Producer.Models.Configurations;
using Producer.Services;
using System;
using System.IO;
using System.Threading;

namespace Producer
{
    public class Program
    {
        private const string configFile = "ProducerConfiguration.json";
        private const int invalidCustomerID = 47;

        //// <summary>
        /// Producer's Main.
        /// </summary>
        /// <param name="args">String of arrays representing all the parameters entered by the user.</param>
        public static void Main(string[] args)
        {
            Console.Title = "Producer";
            Faker faker = new Faker();

            ProducerConfiguration config = new ProducerConfiguration();
            try
            {
                if (File.Exists(configFile))
                {
                    string json = File.ReadAllText(configFile);
                    if (!string.IsNullOrWhiteSpace(json))
                    {
                        config = JsonConvert.DeserializeObject<ProducerConfiguration>(json);
                    }
                }
            }
            catch (IOException) { }

            var customerService = new CustomerService(config);

            Thread.Sleep(2000);
            while (true)
            {
                Console.WriteLine("Enter 1 for Add Customer, 2 for Get Customer, 3 for Business Fault, 4 for Runtime fault, 5 for Translation Business Fault.");
                var selection = Console.ReadLine();

                switch (selection)
                {
                    case "1":
                        var customer = new Customer()
                        {
                            Id = faker.Random.UShort(invalidCustomerID + 1),
                            Name = new Name()
                            {
                                FirstName = faker.Name.FirstName(),
                                MiddleName = faker.Name.FirstName(),
                                LastName = faker.Name.LastName()
                            },
                            Address = new Address()
                            {
                                AddressLine1 = faker.Address.StreetAddress(),
                                AddressLine2 = "",
                                City = faker.Address.City(),
                                ProvinceState = faker.Address.StateAbbr(),
                                Country = faker.Address.Country(),
                                PostalZipCode = faker.Address.ZipCode()
                            }
                        };
                        var newCustomer = customerService.CreateCustomer(customer);
                        break;
                    case "2":
                        var customerIdentifier = new CustomerIdentifier(faker.Random.UShort(invalidCustomerID + 1, 100));
                        var returnedCustomer = customerService.GetCustomerByIdentifier(customerIdentifier);
                        break;
                    case "3":
                        var faultedCall = customerService.GetHttpStatusCodeFault();
                        break;
                    case "4":
                        var wrongType = customerService.GetWrongObjectType();
                        break;
                    case "5":
                        var badCustomerIdentifier = new CustomerIdentifier(invalidCustomerID);
                        var returnedBadCustomer = customerService.GetCustomerByIdentifier(badCustomerIdentifier);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
