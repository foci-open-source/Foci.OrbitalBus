﻿using Foci.Orbital.OrbitalConnector.Factories;
using Foci.Orbital.OrbitalConnector.Services;
using Newtonsoft.Json;
using Producer.Models;
using Producer.Models.Configurations;
using System;

namespace Producer.Services
{

    public class CustomerService
    {
        private readonly OrbitalConnectionService sender;
        private readonly ProducerConfiguration configuration;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CustomerService(ProducerConfiguration configuration)
        {
            this.configuration = configuration;
            new OrbitalLaunchPad().UseConsulAddress(configuration.ConsulIP)
                .UseConsulPort(configuration.ConsulPort)
                .AddNewService(configuration.ServiceName)
                .WithUserNamePassword(configuration.RabbitMqUsername, configuration.RabbitMqPassword);
            sender = new OrbitalConnectionService();
        }

        /// <summary>
        /// Send an asynchronous request to create a customer.
        /// </summary>
        /// <param name="customer">The customer to create.</param>
        /// <returns>The customer JSON.</returns>
        public Customer CreateCustomer(Customer customer)
        {
            sender.SendAsync(configuration.ServiceName, configuration.AddCustomer, customer);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Customer created: {0}", JsonConvert.SerializeObject(customer));
            Console.ResetColor();
            return customer;
        }

        /// <summary>
        /// Send a synchronous request to retrieve a customer.
        /// </summary>
        /// <param name="customerIdentifier">The ID of the customer to retrieve.</param>
        /// <returns>The customer.</returns>
        public Customer GetCustomerByIdentifier(CustomerIdentifier customerIdentifier)
        {
            try
            {
                var response = sender.Send(configuration.ServiceName, configuration.GetCustomer, customerIdentifier);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Customer retrieved: {0}", response);
                Console.ResetColor();
                return GetResponse<Customer>(response);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred: {0}", e);
                return null;
            }

        }

        /// <summary>
        /// Send a synchronous request to get a HttpStatusCode Fault
        /// </summary>
        /// <param name="customerIdentifier">The ID of the customer to retrieve.</param>
        /// <returns>The customer.</returns>
        public Customer GetHttpStatusCodeFault()
        {
            var message = new HttpStatusCodeErrorCustomer() {ArbitraryString = "Arbitrary"};
            try
            {
                var response = sender.Send(configuration.ServiceName, configuration.GetStatusCodeError, message);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Customer retrieved: {0}", response);
                Console.ResetColor();
                return GetResponse<Customer>(response);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred: {0}", e);
                return null;
            }

        }

        /// <summary>
        /// Send a synchronous request to get the wrong data type to generate a runtime error.
        /// </summary>
        /// <returns>The customer.</returns>
        public Customer GetWrongObjectType()
        {
            var message = new WrongObjectRequest() { ArbitraryString = "Arbitrary" };
            try
            {
                var response = sender.Send(configuration.ServiceName, configuration.GetWrongObject, message);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Customer retrieved: {0}", response);
                Console.ResetColor();
                return GetResponse<Customer>(response);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occurred: {0}", e);
                return null;
            }

        }

        /// <summary>
        /// Deserializes a message response from the Orbital.
        /// </summary>
        /// <typeparam name="T">The type of response desired</typeparam>
        /// <param name="toDeserialize">The Orbital response.</param>
        /// <returns>The response.</returns>
        private T GetResponse<T>(string toDeserialize)
        {
            return JsonConvert.DeserializeObject<T>(toDeserialize);
        }

        
    }
}
