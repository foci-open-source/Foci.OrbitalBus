﻿using Newtonsoft.Json;

namespace Producer.Models
{

    public class Address
    {
        [JsonProperty("streetNumber")]
        public int StreetNumber { get; set; }
        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }
        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("provinceState")]
        public string ProvinceState { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("postalZipCode")]
        public string PostalZipCode { get; set; }
    }
}
