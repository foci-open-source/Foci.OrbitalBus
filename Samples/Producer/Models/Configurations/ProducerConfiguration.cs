﻿using Newtonsoft.Json;

namespace Producer.Models.Configurations
{
    public class ProducerConfiguration
    {
        [JsonProperty("consulIP")]
        public string ConsulIP { get; internal set; } = "localhost";
        [JsonProperty("consulPort")]
        public ushort ConsulPort { get; internal set; } = 8500;

        [JsonProperty("rabbitMqUsername")]
        public string RabbitMqUsername { get; internal set; } = "guest";
        [JsonProperty("rabbitMqPassword")]
        public string RabbitMqPassword { get; internal set; } = "guest";

        [JsonProperty("serviceName")]
        public string ServiceName { get; internal set; } = "ConsumerService";

        [JsonProperty("addCustomer")]
        public string AddCustomer { get; internal set; } = "AddCustomer";
        [JsonProperty("getCustomer")]
        public string GetCustomer { get; internal set; } = "GetCustomer";
        [JsonProperty("getStatusCodeError")]
        public string GetStatusCodeError { get; internal set; } = "GetHttpStatusCodeError";
        [JsonProperty("getWrongObject")]
        public string GetWrongObject { get; internal set; } = "GetWrongObject";
    }
}
