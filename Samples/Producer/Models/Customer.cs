﻿using Newtonsoft.Json;

namespace Producer.Models
{
    public class Customer
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Customer()
        {
            this.Name = new Name();
            this.Address = new Address();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public Name Name { get; set; }
        [JsonProperty("address")]
        public Address Address { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, First Name: {1}, Last Name: {2}, Address Line 1: {3}", Id.ToString(), Name.FirstName, Name.LastName, Address.AddressLine1);
        }
    }
}
