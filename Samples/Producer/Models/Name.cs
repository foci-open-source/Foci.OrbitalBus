﻿using Newtonsoft.Json;

namespace Producer.Models
{
    public class Name
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("middleName")]
        public string MiddleName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
