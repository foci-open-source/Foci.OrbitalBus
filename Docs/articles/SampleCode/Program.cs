﻿using SampleProducer.Models;
using SampleProducer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleProducer
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter 1 to get a user(GET) : ");
                var selection = Console.ReadLine();
                var service = new ProducerService();

                switch (selection)
                {
                    case "1":
                        var address = new Address() { city = "", street = "", zipcode = "" };
                        var userIdentifier = new UserIdentifier() { id = 1, name="",address = address,username="" }; //simple model
                        var returnedUser = service.GetUserById(userIdentifier);
                        Console.WriteLine("The user returned is: \n ID : {0} \n Name: {1} \n Username: {2} \n Address: \n Street: {3} \n City : {4} \n Zip Code: {5}",
                            returnedUser.id, returnedUser.name, returnedUser.username, 
                            returnedUser.address.street, returnedUser.address.city, returnedUser.address.zipcode);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
