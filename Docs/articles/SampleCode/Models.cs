public class Address
    {
        public string street { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
    }

public class GenericPost
    {
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public int userId { get; set; }
    }

public class UserIdentifier
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public Address address { get; set; }
    }