﻿using SampleProducer.Models;
using SampleProducer.OrbitalConnector;

namespace SampleProducer.Services
{
    public class ProducerService
    {
        private string GetUserOperation = "GetUserOperation";


        public UserIdentifier GetUserById(UserIdentifier userId) {
            var user = new UserIdentifier() { id = 1};
            var response = JsonHolder.GetSyncMessage(userId, GetUserOperation);
            var userToReturn = OrbitalConnectorHelper.GetResponse<UserIdentifier>(response);
            return userToReturn;
        }
        
    }
}
