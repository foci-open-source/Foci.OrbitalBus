﻿using Newtonsoft.Json;

namespace SampleProducer.OrbitalConnector
{
    public static class OrbitalConnectorHelper
    {
        /// <summary>
        /// Deserializes a message response from the Orbital.
        /// </summary>
        /// <typeparam name="T">The type of response desired</typeparam>
        /// <param name="toDeserialize">The Orbital response.</param>
        /// <returns>The response.</returns>
        public static T GetResponse<T>(string toDeserialize)
        {
            return JsonConvert.DeserializeObject<T>(toDeserialize);
        }
    }
}
