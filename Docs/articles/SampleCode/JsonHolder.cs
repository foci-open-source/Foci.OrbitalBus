﻿using Foci.Orbital.OrbitalConnector.Factories;
using Foci.Orbital.OrbitalConnector.Factories.Interfaces;
using Foci.Orbital.OrbitalConnector.Models;
using Foci.Orbital.OrbitalConnector.Services.Interfaces;
using Newtonsoft.Json;

namespace SampleProducer.OrbitalConnector
{
    public static class JsonHolder
    {
        #region Constants
        private const string ServiceId = "JsonHolderLibrary";
        #endregion

        #region OrbitalConnector  
        public static readonly OrbitalServiceLaunchPad launchPad;
        public static readonly OrbitalConnection Sender;
        #endregion

        #region Static Constructor
        /// <summary>
        /// Static Constructor 
        /// </summary>
        static JsonHolder()
        {
            launchPad = new OrbitalLaunchPad();
            launchPad.UseConsulAddress("localhost").UseConsulPort(8500).AddNewService(ServiceId).WithUserNamePassword("guest","guest");
            Sender = new OrbitalConnection();
        }
        #endregion

        /// <summary>
        /// Serializes a message for communication to the Orbital.
        /// </summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="toBeSerialized">The object ot be serialized.</param>
        /// <param name="operationId">The operation to be called on the consumer.</param>
        /// <returns>A composed request.</returns>
        public static string GetSyncMessage<T>(T toBeSerialized, string operationId)
        {
            var message = new SyncronousMessageRequest { ServiceId = ServiceId, OperationId = operationId };
            message.Message = JsonConvert.SerializeObject(toBeSerialized);
            var response = Sender.SendRaw(ServiceId,operationId,message);
            return response;
        }
    }
}
