## Understanding the Architecture

So you want to know how Orbital works? Let’s start by taking a look at the high-level components:

[![A simplified representation of the Orbital message flow.](../images/FlowOverview.png)](../images/FlowOverview.png)

* **Client**
The client application is the message sender. A client application might have endpoints for receiving messages too, but in our architecture the client application is an application that wants to send messages out. That means using the *Orbital Connector*.
* **Orbital Connector**
There isn’t one Orbital Connector. The Orbital Connector is the term we use to refer to the integration library that allows client applications to send messages to *Agents*. It communicates to Consul to find designated services and operations, and uses RabbitMQ to send the message along. Over time more Orbital Connectors will appear for more languages. Don’t see an Orbital Connector for your programming language of choice? Check out [how to get involved](#how-to-get-involved). 
* **Agent**
The "heavy lifting" of Orbital is done here, if it can be called heavy. The Agent listens for messages, executes transformations, and sends the data on to the consumer.
* **Service Definition**
The service definition delineates the schemas, operations, adapters to use per operation, translation and the Agent manipulates them to create the appropriate requests to send to the consumer and responses received from the consumer.

* **Adapter**
Similar to the case with the Orbital Connector, Orbital can implement multiple adapters. The adapters allow the Agent to communicate to consumers over different protocols. 

Want to allow for communication over different protocols or to different databases or file systems? Take a look at our section on [how to get involved](#how-to-get-involved).
* **Endpoint**
The end of the line for your message, the consumer can be any web service. Consumers don’t have to be coded any differently than normal. The work done by the Agent takes care of that.  


## Sample Message Flow
* The client application composes a message and uses the *Orbital Connector* to validate the message and send it to the RabbitMQ queue with the name of the operation where the *Agent* is listening to.
* The *Agent* picks the message off the queue and performs any necessary translations and passes the message along with the adapter configuration information to the *adapter*.
* The *adapter* calls out from the *Agent* to the consumer.
* If the message is synchronous, the response is passed long the reverse path all the way to the client application.

## The Orbital Bus Handshake

An end-to-end transmission with Orbital Bus encompasses many interactions amongst the various components. We've put together a diagram to help describe how a message passes through the system.
[![The Orbital Bus handshake from end to end.](../images/OrbitalBusHandshake.png)](../images/OrbitalBusHandshake.png)
This diagram covers a number of points where more complex logic can find its way into the system. The translations, for instance, have the potential to add many layers of complexity. The adapters are also subject to change. We have based this diagram on our bundled REST adapter. Additional adapters might provide added layers of functionality.
