# How to Configure the Orbital Agent

The Orbital Agent is designed to be highly configurable. As a result, the configuration file contains a lot of data which should be understood in order to properly configure your environment.

The Orbital Agent configuration file is a simple JSON file, and thus it must follow the correct JSON syntax.

This article will explain the purpose of each configuration property and how to set your new configuration for the Orbital Agent.

## Configuration Properties

Below we have detailed descriptions of each properties to describe the expected values.

#### Service Static Parameters

The `ServiceStaticParameters` property is a list of key-value pairs which can be used by the Orbital Agent to inject values into the adapter configuration. For instance, the following is used to inject IP and port into the adapter configuration:

```json
"ServiceStaticParameters": {
  "ServiceIp": "localhost",
  "ServicePort": "3580"
}
```

#### Broker Configuration

`BrokerConfiguration` object holds properties relevant to establishing a connection with RabbitMQ.

- **BusHostIp** designates the IP of RabbitMQ that Agent will listen to.

- **BusHostPort** indicates the port of RabbitMQ that the Agent will listen to. It will be used in conjunction with the `BusHostIp`.

- **Username** is required in order to establish a valid connection to RabbitMQ at all times. The username will have to be configured in RabbitMQ separately.

- **Password** is required in order to establish a valid connection to RabbitMQ at all times. The password will have to be configured in RabbitMQ separately.

- **SslEnabled** is set to indicate if communication to and from RabbitMQ should be secured.

The following is an example `RabbitMQConfiguration` used by the Agent:

```json
"BrokerConfiguration": {
  "BusHostIp": "localhost",
  "BusHostPort": 5672,
  "Username": "guest",
  "Password": "guest",
  "SslEnabled": "false"
},
```

#### Consul Configuration

The `ConsulConfiguration` contains information used to establish both secure and unsecure communication to and from Consul.

- **HostIp** contains the IP address of the Consul host on the node's machine.

- **HostPort** corresponds with the Consul host port on the node's machine. It will be used in conjunction with the `HostIp`.

- **SslPolicyCNerror** is used to avoid errors being thrown if the certificate common name (CN) does not match when securing communication with Consul (which might happen with self-signed certificates). When set to `false`, this parameter will throw errors during certificate authentication, and should always be the case when using SSL with Consul in production. It should be set to `true` in development instances when testing with self-signed certificates. 

- **SslEnabledConsul:** is used indicate if communication with Consul should be secured.

The following is an example of `ConsulConfiguration` used by the Agent:

```json
"ConsulConfiguration": {
  "HostIp": "localhost",
  "HostPort": 8500,
  "SslPolicyCNerror": "false",
  "SslEnabledConsul": "false"
}
```

#### Public Configuration

The `PublicConfiguration` contains information for the public addresses you can use to reach the broker and the Orbital Agent.

- **HttpIp** contains the public IP address of the Orbital Agent's web server to send http requests.

- **HostPort** corresponds with the Orbital Agent's web server port. It will be used in conjunction with the `HttpIp`.

- **BrokerIp** designates the public IP of RabbitMQ that Agent will listen to.

- **BrokerPort:** indicates the public port of RabbitMQ that Agent will listen to. It will be used in conjunction with the `BrokerIp`.

The following is an example of `PublicConfiguration` used by the Agent:

```json
"PublicConfiguration": {
    "HttpIp": "localhost",
    "HttpPort": 3000,
    "BrokerIp": "localhost",
    "BrokerPort": 5672
}
```
#### Registered Service Names

If there are already services in Consul, we can declare them in `RegisteredServiceNames` so when the Orbital Agent starts loading, it can register the necessary information to RabbitMQ and Consul. You can list as many services as you want.

```
"RegisteredServiceNames": [ "ConsumerService", "AnotherService" ]
```

#### Sample Configuration

The following is a sample configuration that contains all the properties described in the previous section

```json
{
  "ServiceStaticParameters": {
    "ServiceIp": "localhost",
    "ServicePort": "3580"
  },
  "BrokerConfiguration": {
    "BusHostIp": "localhost",
    "BusHostPort": 5672,
    "Username": "guest",
    "Password": "guest",
    "SslEnabled": "false"
  },
  "ConsulConfiguration": {
    "HostIp": "localhost",
    "HostPort": 8500,
    "SslPolicyCNerror": "false",
    "SslEnabledConsul": "false"
  },
  "PublicConfiguration": {
    "HttpIp": "localhost",
    "HttpPort": 3000,
    "BrokerIp": "localhost",
    "BrokerPort": 5672
  },
  "RegisteredServiceNames": [ "ConsumerService" ]
}

```

## Set the configuration properties in the Orbital Agent

There are different ways to set the configuration properties in the Orbital Agent:

* Setting an environment variable
* Creating a configuration file
* Use default values

The Orbital Agent looks first into the environment variable, if the environment variable is undefined, it will look into a configuration file called `orbital.config` or the configuration name defined as an argument when you run the Agent. In the rest of this section, the json defined in the `Sample Configuration` section will be used to configure the Orbital Agent.

### Environment Variable

If you are using docker to run an Orbital instance, you can add the environment variable in your docker-compose file:

```
orbital:
  environment:
    - orbital.agent.configuration={"ServiceStaticParameters":{"ServiceIp":"localhost","ServicePort":"3580"},"BrokerConfiguration":{"BusHostIp":"localhost","BusHostPort":5672,"Username":"guest","Password":"guest","SslEnabled":"false"},"ConsulConfiguration":{"HostIp":"localhost","HostPort":8500,"SslPolicyCNerror":"false","SslEnabledConsul":"false"},"PublicConfiguration":{"HttpIp":"localhost","HttpPort":3000,"BrokerIp":"localhost","BrokerPort":5672},"RegisteredServiceNames":["ConsumerService"]}	

```
For other ways to set an environment variable in docker, please refer to their [documentation](https://docs.docker.com/compose/environment-variables/#pass-environment-variables-to-containers).

If you are not using docker, open a terminal and set an environment variable called `orbital.agent.configuration` and set the json in the `Sample Configuration` as the value.

After setting the environment variable, simply start an orbital instance through docker by running `docker-compose up` or if you are not using docker, run the following command `dotnet Foci.Orbital.Agent` or start the application `./Foci.Orbital.Agent`.

### Configuration File

At the same location where your Orbital Agent application, or the directory dedicated to the Orbital docker image is, create a folder called `Configs`. Inside this new folder, you will create a file called `orbital.config`. In this file you will add the json in the `Sample Configuration`. If you prefer to name the file differently, you can do so. If you have named your configuration file different to `orbital.config`, you will need to specify the name as an argument when you run your Orbital Agent. I.E. `./Foci.Orbital.Agent myConfigFile`.

Now that there is a configuration file inside the `Configs` folder, you can now run your Orbital Agent instance. If you are using docker, do not forget to set the volume in your docker-compose file like this:

```
volumes:
      - "./Configs:/app/Configs"
```


### Default values

If an environment variable or a configuration file is not set and you start an Orbital Agent instance, the Orbital Agent will load the following configuration values:

```
{
  "ServiceStaticParameters": {
    "ServiceIp": "localhost",
    "ServicePort": "3580"
  },
  "BrokerConfiguration": {
    "BusHostIp": "localhost",
    "BusHostPort": 5672,
    "Username": "guest",
    "Password": "guest",
    "SslEnabled": "false"
  },
  "ConsulConfiguration": {
    "HostIp": "localhost",
    "HostPort": 8500,
    "SslPolicyCNerror": "false",
    "SslEnabledConsul": "false"
  },
  "PublicConfiguration": {
    "HttpIp": "localhost",
    "HttpPort": 3000,
    "BrokerIp": "localhost",
    "BrokerPort": 5672
  },
  "RegisteredServiceNames": [ "ConsumerService" ]
}
```
