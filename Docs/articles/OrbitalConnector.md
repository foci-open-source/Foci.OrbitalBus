# Orbital Connector

One of the essential components for the Orbital, are Orbital Connectors. Orbital Connector is a term we use to refer to the integration library used to send messages to the Agent. The Connector also handles any response, fault, or timeout received in return as well as any validation and caching of your information. Currently we have a C# Orbital Connector available. If Orbital Connector doesn't have your information, it will fetch it by accessing Consul to get it. Over time more Orbital Connectors will appear for more languages.

*Connector library available through [Nuget](https://www.nuget.org/packages/Foci.Orbital.OrbitalConnector/)*

## Configuration

In your application, to set up the Orbital Connector you need to configure the Connector utilizing the `OrbitalLaunchPad`. Below, you can see we are using the methods in the `OrbitalLaunchPad` to set up the different properties.

```csharp
new OrbitalLaunchPad()
  .UseConsulAddress("localhost") // To setup IP address of Consul
  .UseConsulPort(8500)  // To setup the port Consul communicates through
  .UseSslEnabledForConsul()  // *OPTIONAL* Enable SSL connection for Consul 
  .AddNewService("ServiceName") // Sets the Service that will be used
  .WithUserNamePassword("RabbitMQ_Username","RabbitMQ_Password") // Handles RabbitMQ credentials
  .WithSsLEnabledForService() // *OPTIONAL* Enable SSL connection for RabbitMQ
  .WithOperationTimeout("OperationName", 10) // *OPTIONAL* To setup time out the given operation
```

## Message Passing

The `OrbitalConnectionService` class hosts all the methods used for message passing. You can send an `object` or a `JSON string` as a message. To send a `JSON string`, you will use a method with the RAW keyword, `SendRaw()` or `SendAsyncRaw()`. To send an `object`, use the methods that don't contain the RAW keyword, `Send()` or `SendAsync()`, it will deserialize the `object` to a `JSON string` then pass it along.

The following is an example of sending a message using the methods.

```csharp
OrbitalConnectionService Sender = new OrbitalConnectionService();

// String variables representing the name of the Service and the Operations it offers
string ServiceName = "ServiceName";
string OperationName = "OperationName";

// Sending an object and expecting a response
Message message = new Message { Content = "Message content" };
string response = Sender.Send(ServiceName, OperationName, message);

// Sending an object and NOT expecting a response
Message message = new Message { Content = "Message content" };
Sender.SendAsync(ServiceName, OperationName, message);

// Sending JSON string and expecting a response
string message = "{\"Content\": \"Message content\"}"
string response = Sender.SendRaw(ServiceName, OperationName, message);

// Sending JSON string and NOT expecting a response
string message = "{\"Content\": \"Message content\"}"
Sender.SendAsyncRaw(ServiceName, OperationName, message);
```
