# Orbital Adapters

One of the essential components for the Orbital is the Adapter. The adapters allow the Orbital Agent to communicate to consumers over different protocols, databases, or file systems. Over time more Orbital Adapters will appear for more protocols, databases, and file systems.

**Current Orbital Adapters:**

* REST
* Mock
* Database


## How Orbital Agent implements an Adapter

The Orbital Agent is responsible for passing all the necessary information to the adapter in order to communicate with a consumer. The adapter is the point of contact from the Orbital, to and from the consumer, and it houses the code which will render the proper protocol to communicate with the consumer.

## REST Adapter

The purpose of this adapter is to make REST calls to the `uri` defined in the `adapter configuration`. If any response comes back from the consumer, the adapter will wrap it appropriately and return it to the Orbital Agent. The following is an example of what an adapter configuration for the REST adapter looks like:

```
{
    "type": "Adapters.Rest",
    "configuration": 
    {
        "uri": "",
        "method": "GET",
        "headers": [
              {
                "key": "Content-Type",
                "value": "application/json"
              }
          ]
    }

}
```
* **type** Defines the type of adapter that is going to be used for this configuration.
* **configuration** This pertains to the actual configuration to be used by the adapter
    * **uri** This is the REST endpoint to be hit with the message sent through the Orbital Agent.
    * **method** The HTTP verb to be used when constructing your REST request.
    * **headers** These headers will be added to the REST request's headers.
  
you can modify the properties to fit what is needed to make a successful REST call.

## Mock Adapter

In order to help developers quickly create services and test them without needing a third party endpoint, a mock adapter was created. The developer should be able to configure it to either get a response or exception depending on the condition met when a message is sent. The adapter will wrap the response or exception appropriately and return it to the Orbital Agent.

The following is an example of what a Mock Adapter configuration should look like:

```
{
    "type": "Adapters.Mock",
    "configuration": {
        "selector": "$.Id",
        "conditions": [
            {
                "operator": "==",
                "compareTo": "48",
                "exception": "This is an exception message"
            },
            {
                "operator": "==",
                "compareTo": "112",
                "response": "{\"id\":112,\"name\":{\"firstName\":\"John\",\"middleName\":\"Jane\",\"lastName\":\"Doe\"},\"address\":{\"addressLine1\":\"5 City Street\",\"addressLine2\":\"\",\"city\":\"Ottawa\",\"provinceState\":\"ON\",\"country\":\"CA\",\"postalZipCode\":\"5A5A5A\"}}"
            }
        ],
        "default": {
                "response": "{'response': 'Default Response'}"
        }
    }
}

```

* **type** Defines the type of adapter that is going to be used for this configuration.
* **configuration** This pertains to the actual configuration to be used by the adapter
    * **selector** This is the value that your message should contain in order to compare it against the conditions provided and return the `response` or `exception` defined in the conditions. The value we get using the selector can only be a number, string or a boolean.
    * **conditions** This is a group of conditions defined by the developer to test how your microservice should behave. it is required to define an `operator`, `compareTo`, and `exception` or `response` property.
        * **operator** You can define one operator per condition. At the moment we support the following operators: ==,>=,>,<=,<, and !=. This is used to compare the `selector` against the `compareTo` value defined.
        * **compareTo** This will be the value to be compared against the `selector`. This can only be a number
        * **response** If a `selector` meets the condition, this string will be returned so the adapter can wrap it and send it back to the Orbital Agent.
        * **exception** If a `selector` meets the condition, this string will be returned so the adapter can wrap it as a fault and send it back to the Orbital Agent.
    * * **default** this should be defined in case that none of the conditions are met, so a `response` or `exception` of your choice is returned.

## Database Adapter

This adapter will help developers access databases to perform sql statements through orbital. The developer will provide the connection string, the database to use and the sql statement. The adapter will read the developer's configuration and perform the statement to the database provided in the connection string and response with any issues or successes wrapped in a Payload.

The following is an example of what a Database Adapter configuration should look like:

```
{
    "type": "Adapters.Database",
    "configuration": {
        "statement": "EXECUTE",
        "type": "MYSQL",
        "sql": "INSERT INTO Customer ( CustomerId, LastName, FirstName ) VALUES ( @id, @lastName, @firstName );",
        "connection": "Server=localhost;Database=Customer;Uid=root;Pwd=example;",
        "parameters": {
            "id": "1",
            "lastName": "Goodman",
            "firstName": "Saul"
        }
    }
}

```

* **type** Defines the type of adapter that is going to be used for this configuration.
* **configuration** This pertains to the actual configuration to be used by the adapter
    * **statement** There are two ways to differentiate statements. If the developer wants to insert data to the database you will define this as **"EXECUTE"**. In the other hand, if the developer wants to retrieve data from the database, **"QUERY"** should be defined in this property.
    * **type** Define the database provider you need to access the database. The following are the supported databases:
                * MYSQL
                * SQLITE
                * MSSQL
                * POSTGRESQL
                * ORACLE
    * **sql** The statement to be executed against the database should be defined in this property. The developer has the option to define parameter names with "@" at the beginning of the parameter name as demonstrated in the above example. It is important to note that the sql statement cannot have mustache templates "{{}}"
    * **connection** This is the property that the developer has to define the data source, address and port to reach the database, username, and password. The database Adapter will use this to try to reach the database.
    * **parameters** As mentioned in **sql**, parameters can be defined and here is where the value is assigned to any parameter names mentioned in the **sql** property. If any extra parameters are defined in this property and not in the **sql** property, these will be ignored. When the adapter is about to execute the statement, it will replace all the "@parametername" with the actual value defined.