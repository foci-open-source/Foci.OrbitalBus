# Orbital CLI Commands

The Orbital CLI is a command-line interface that helps you create and configure a service for the Orbital Agent. This article is meant to explain all the commands and flags.

## Install Orbital CLI

To install Orbital CLI through npm, open a terminal/console window and run the following:

```
npm i @foci-solutions/orbital-cli
```

To confirm the CLI was successfully installed, run the following command:

```
orbital -v
```

The Orbital CLI version will be displayed in the format below:

```
@foci-solutions/orbital-cli/3.0.11 win32-x64 node-v8.12.0
```

## Basic Workflow

To start create services, choose a directory location dedicated to the services you are going to create with the Orbital CLI. If you want to publish the following service, be sure to have Consul and RabbitMQ up and running.

Run the `create:service` command to start building your mock service. We will add an operation as well in this command. Like this:

```
orbital create:service ConsumerService -o GetCustomer -s true -a Adapters.Mock --validateRequest false --validateResponse false --httpEnabled GET --verbOnly true
```

After your service is created successfully, go inside the folder `ConsumerService`. You will now build your service by running:

```
orbital build
```

The `ConsumerService` should successfully build. Finally we publish the new service:

```
orbital publish
```

You will get notified that the service was uploaded to Consul and the update was sent to RabbitMQ.

## Orbital CLI folder structure

The folder structure of a valid and complete service looks like this:

```

ServiceName
    |___ OperationName
    |          |__adapterConfigurations
    |                     |__adapterConfig.json
    |          |__schemas
    |                |__request.json
    |                |__response.json
    |          |__translations
    |                |__translation.json
    |          |__operationConfiguration.json
    |
    |___.orbital
    |___bin
          |__serviceDefinition.json
```

## CLI command-language syntax

The command syntax will look like this:

**orbital** *commandName argument* **[options]**

* Some commands have required arguments.
* Most options have aliases.
* You are required to define some options because they depend on each other.

### Command Overview

| Command          | Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| [create:service](#createservice)   |               | Creates the initial service folder structure|
| [create:operation](#createoperation) |               | Creates an asynchronous or synchronous operation for a service. This command should be run inside the service's folder |
| [build](#build) |      |  Creates the service definition based on the operations defined with its adapter configuration, schemas, translation and other configurations if applicable. This command should be run inside the service's folder|
| [publish](#publish)   |               | Publishes the service definition to Consul and sends a message to RabbitMQ so any Orbital Agent can update to the new service. This command should be run inside the service's folder |

### Flag Overview

| Flag          | Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| --help  |       -h        | Help menu to get descriptions and examples on how to use a command or flag|
| --operation |     -o          | indicates the name of the operation you want to create along with the service. This flag is only applicable when running the command `create:service` and the rest of the flags available depend on this flag if you are running `create:service`. |
| --sync |   -s   |  Indicates if the operation will be synchronous or not. If flag is set to true, the operation will be synchronous.|
| --adapter   |         -a      | Publishes the service definition to Consul and sends a message to RabbitMQ so any Orbital Agent can update to the new service. This command should be run inside the service's folder |
| --validateRequest   |           | If the message request has to be validated this flag has to be set to true. A schema to define the validation will be created. |
| --validateResponse  |           | If the message response has to be validated this flag has to be set to true. A schema to define the validation will be created. |
| --httpEnabled  |           | If the operation should be reached by HTTP requests, it has to be configured when creating the service. This flag will indicate the verb to be used by the HTTP request. |
| --verbOnly  |           | If the operation should be reached by HTTP requests, you can specify that this can also be reached by just specifying the HTTP verb. This can only be specified one time per http verb.  |

### create:service

Creates the initial service folder structure.

```
orbital create:service SERVICENAME  [options]
```

#### Description

Creates the initial service folder structure with an `.orbital` file. The `.orbital` file contains the information to communicate to Consul and the RabbitMQ broker. If you have different ports and IP addresses from the default values found in the `.orbital` file, you can change them.

#### Argument

| Argument          | Description  |
| :-------------:    | :-----:|
| SERVICENAME   |  name of the service to create|

#### Options

| Option     |   Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| --operation=operation   | -o |Optional operation name to create.|
| --adapterType= AdapterType.Mock, AdapterType.REST |  -a  |  Flag to indicate the adapter to use with the operation.  **Depends on:** --operation |
| --sync=true, false    | -s | Flag to indicate if operation is synchronous or asynchronous. **Depends on:** --operation |
| --httpEnabled=HTTPVerb.DELETE, HTTPVerb.GET, HTTPVerb.NONE, HTTPVerb.PATCH, HTTPVerb.POST, HTTPVerb.PUT |  |  Flag to indicate if the operation will be reached by HTTP requests. A HTTP verb has to be specified. **Depends on:** --operation |
| --verbOnly=true, false    |  | Flag to indicate if operation can be reached by only specifying the HTTP verb in the HTTP request. **Depends on:** --httpEnabled |
| --validateRequest=true, false    |  |Flag to indicate if validation for the operation request message is needed.**Depends on:** --operation |
| --validateResponse=true, false    |  |Flag to indicate if validation for the operation response message is needed.**Depends on:** --operation |
| --help   | -h | show CLI command and options descriptions. |

### create:operation

Creates  a folder for the operation defined.

```
orbital create:operation OPERATIONNAME  [options]
```

#### Description

Creates a folder for the operation defined. Inside the operation folder, 3 folders will be created:

* adapterConfigurations
* schemas
* translations

All these folders will contain files that the user will customize to handle the messages. Additionally, an `operationConfiguration.json` file will be created. This file should not be modified at all.

#### Argument

| Argument          | Description  |
| :-------------:    | :-----:|
| OPERATIONNAME   |  operation name to create|

#### Options

| Option     |   Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| --adapterType= AdapterType.Mock, AdapterType.REST |  -a  |  Flag to indicate the adapter to use with the operation.  **Depends on:** --operation |
| --sync=true, false    | -s | Flag to indicate if operation is synchronous or asynchronous. **Depends on:** --operation |
| --httpEnabled=HTTPVerb.DELETE, HTTPVerb.GET, HTTPVerb.NONE, HTTPVerb.PATCH, HTTPVerb.POST, HTTPVerb.PUT |  |  Flag to indicate if the operation will be reached by HTTP requests. A http verb has to be specified. **Depends on:** --operation |
| --verbOnly=true, false    |  | Flag to indicate if operation can be reached by only specifying the HTTP verb in the HTTP request. **Depends on:** --httpEnabled |
| --validateRequest=true, false    |  |Flag to indicate if validation for the operation request message is needed.**Depends on:** --operation |
| --validateResponse=true, false    |  |Flag to indicate if validation for the operation response message is needed.**Depends on:** --operation |
| --help   | -h | show CLI command and options descriptions. |

### build

Builds the service with its operations and outputs a service definition file.

```
orbital build
```

#### Description

Builds the service with its operations and outputs a service definition file. A folder called `bin` will be created under the service folder. This folder will contain a `service definition` file that will contain all the information from the rest of the folder condensed in a single file. This cannot be altered under any circumstance.

#### Options
| Option          | Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| --help   | -h | show CLI command and options descriptions.|

### publish

Publishes service definition file to Consul and an update to RabbitMQ.

```
orbital publish
```

#### Description

Based on the addresses and IPs defined in the `.orbital` file, the Orbital CLI will publish the service definition file found in the `bin` folder. If the service already exists, the orbital CLI will prompt a warning about the service being already up in Consul and confirm if you want to proceed to overwrite it.

#### Options
| Option          | Alias         | Description  |
| :-------------:    |:-------------:| :-----:|
| --help   | -h | show CLI command and options descriptions.|