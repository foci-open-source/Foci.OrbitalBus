# HTTP Endpoints

One of the two ways you can send messages to the Orbital Agent is through HTTP requests. This feature offers the user the ability to send messages to the Orbital Agent either programatically or through the REST tool of your choice. 

At the moment this feature supports the following verbs:

* GET
* PUT
* POST
* PATCH
* DELETE


## How To Send An HTTP Request

To be able to create your HTTP request, you need to know:

* the service name I.E `ConsumerService`
* the operation name I.E `AddCustomer`
* if the operation is configured to be reached only by the HTTP verb I.E. `PUT`

It's important to note that at the moment, you can create HTTP request with the following content type:

* text/plain
* application/json

When you create a service with the Orbital CLI, you can define if an operation can be reached by only specifying the HTTP verb and service name. To set this, you need to specify the flag `--verbonly` as true (to know more about the CLI flags please go to the Orbital CLI [article](OrbitalCLICommands.md)). Is it important to note that **only one operation per verb** can be reached by verb only. This will make your HTTP request look like this:

```
http://localhost:3000/ConsumerService
```

On the other hand, if you have not specify `--verbonly` as false, you will have to define the name of the operation in your request. I.E.

```
http://localhost:3000/ConsumerService/AddCustomer
```

If you are expecting a response back, you will get an `Orbital Payload` that will contain either a response or a fault.