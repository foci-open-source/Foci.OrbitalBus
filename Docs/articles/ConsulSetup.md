# Consul Installation and Setup

This article is intended to help you get Consul up and running in the manner expected by Orbital. If you're a Consul pro and can handle the installation and basic configuration yourself, feel free to skip to the [Configuration of Orbital](#configuration-of-orbital-bus) section.

## Table of Contents
* [Installation on Windows](#installation-on-windows)
* [Configure a service on Linux with Systemd](#configure-a-service-on-linux-with-systemd)
* [Configure a service on Linux with Upstart](#configure-a-service-on-linux-with-upstart)
* [Configuration of Orbital](#configuration-of-orbital-bus)
* [Troubleshooting](#troubleshooting)

## Installation on Windows
Orbital has been tested with Consul version 1.0.0. It can be downloaded from the Consul website here: [https://releases.hashicorp.com/consul/1.0.0/](https://releases.hashicorp.com/consul/1.0.0/).  

If you're setting up a development environment on Windows, we recommend the following steps:  
* Download from the Consul website.
* Drop the unzipped executable somewhere you want it kept.
* Add the location of the executable to the PATH variable.
  * Go to "Edit the system environment variables".
  * In the "Advanced" tab, click on "Environment ...".
  * Select the PATH variable and click "Edit..."
  * Add the location of the Consul executable.
  * Click "OK" on every window to back out.

## Configure a service on Linux with Systemd
For the purposes of this guide we'll presume that all the work will be done via terminal and not through a GUI. Also, it is expected that you will have sudo access. Let's begin!
1. You'll need an unzip program. We're setting ours up on a Ubuntu server, so we'll use `apt-get`.
[!code-bash[GetUnzip](SampleCode/getUnzip.sh "apt-get unzip")]  
2. We're going to place the Consul executable in the `/usr/local/bin` directory to make it accessible via command. We'll need this setup for our scripts to work. The following block lists the commands to download and unzip that executable.
[!code-bash[GetConsulExecutable](SampleCode/consulExecutable.sh "Download and unzip Consul.")]
3. The service will run under its own user, so we'll want to create that user and a directory for the Consul data.  
[!code-bash[AddUser](SampleCode/addUser.sh "Create a Consul user and data directory.")]
4. Consul can accept command-line parameters or configuration files. For services it's best to use configuration files. Thankfully we've included some template files in the `DevelopmentScripts/Consul/Configs` folder. We'll need a place to store these files. (For more information, check out our [section on configuring Consul for Orbital](#configuration-of-orbital-bus).)
[!code-bash[CreateScripts](SampleCode/scriptsLocation.sh "Create a location and add in the scripts.")]
5. We need an systemd script to activate our Consul as a service. We've included a sample script in the `DevelopmentScripts/Consul/Systemd` folder.  
[!code-bash[CreateSystemd](SampleCode/createSystemd.sh "Create a new systemd script.")]
The content of the script is really just a call to the Consul command with parameters to point to our config file. In the example below we're using the configuration to start up a server instance. This script will allow you to control the local instance with the commands `systemctl start consul` and `systemctl stop consul`. It will also start consul on server start, so you don't have to worry about bringing it up on restart.  
[!code-bash[SampleSystemd](SampleCode/consul.service "A sample systemd script.")]

## Configure a service on Linux with Upstart
For the purposes of this guide we'll presume that all the work will be done via terminal and not through a GUI. Also, it is expected that you will have sudo access. Let's begin!
1. You'll need an unzip program. We're setting ours up on a Ubuntu server, so we'll use `apt-get`.
[!code-bash[GetUnzip](SampleCode/getUnzip.sh "apt-get unzip")]  
2. We're going to place the Consul executable in the `/usr/local/bin` directory to make it accessible via command. We'll need this setup for our scripts to work. The following block lists the commands to download and unzip that executable.
[!code-bash[GetConsulExecutable](SampleCode/consulExecutable.sh "Download and unzip Consul.")]
3. The service will run under its own user, so we'll want to create that user and a directory for the Consul data.  
[!code-bash[AddUser](SampleCode/addUser.sh "Create a Consul user and data directory.")]
4. Consul can accept command-line parameters or configuration files. For services it's best to use configuration files. Thankfully we've included some template files in the `DevelopmentScripts/Consul/Configs` folder. We'll need a place to store these files. (For more information, check out our [section on configuring Consul for Orbital](#configuration-of-orbital-bus).)
[!code-bash[CreateScripts](SampleCode/scriptsLocation.sh "Create a location and add in the scripts.")]
5. We need an upstart script to activate our Consul as a service. We've included a sample script in the `DevelopmentScripts/Consul/Upstart` folder.  
[!code-bash[CreateUpstart](SampleCode/createUpstart.sh "Create a new upstart script.")]
The content of the upstart script is really just a call to the Consul command with parameters to point to our config file. In the example below we're using the configuration to start up a server instance. This script will allow you to control the local instance with the commands `start consul` and `stop consul`. It will also start consul on server start, so you don't have to worry about bringing it up on restart.  
[!code-bash[SampleUpstart](SampleCode/consul.conf "A sample upstart script.")]


## Configuration of Orbital
In the implementation of Orbital we have a few assumptions about how Consul is configured. Some of these are best practices while others are particular to Orbital. We think they're all important enough to bring to your attention.  
* **Orbital should run with at least three raft members.** While all rafts must begin with a single member, a production instance should run with a minimum of three agents on separate machines. Hashicorp recommends three or five agents per datacenter. A fairly complete explanation thereof can be found [here](https://www.consul.io/docs/internals/consensus.html).  
* **The first agent should start with bootstrap.** In the Linux setup sections there are three configuration files referred to under the `DevelopmentScripts/Consul/Configs` folder: bootstrap, client, and server. The server and client configurations will be the most common in your raft. The bootstrap configuration will start a Consul agent in bootstrap mode to force it as the leader. This configuration is recommended for the first agent being spun up. Once the raft is up and running this agent can be restarted as a regular server or client. The raft as a whole can then manage the leadership going forward. When your production environments have started you should have a plan for how to manage a bootstrapped agent to bring up your environments in the case of an outage.  
* **Each agent should be configured with the client address set.** The client address can be set either via the `-client` command-line option or the `client_addr` configuration file property. This property specifies the IP address that Consul will listen on. If this property is not set, Consul listens on the loopback at `127.0.0.1`. Loopback might be okay for your configuration if all communication is performed with agents on localhost or `127.0.0.1`, but to access a Consul agent by the IP address of its machine requires this property be set.

## Troubleshooting

Some of the common scenarios that might prevent Consul from starting: 

* If you are working with VirtualBox or similar app, you might encounter 

```

Failed to get advertise address: Multiple private IPs found. Please configure one.

```

You will have to disable the network connections from Virtual Box. 

In Windows, go to Control Panel -> Network and Internet -> Network Connections and disable Virtual Box's network connection.

* Check if the ports are free to use and are not occupied by other resources.

* If Consul fails to elect a leader, you will have to locate the folder you chose to contain Consul's data and delete all of it. Unfortunately that means that you will have to register all the services again. 