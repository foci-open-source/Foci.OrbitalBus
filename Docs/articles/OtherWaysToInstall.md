# Other Ways To Install Orbital

The purpose of this article is to guide developers, who are not familiar with Docker, to set up Orbital. This guide includes how to install the supporting component `RabbitMQ` if you decide to use the `Orbital Connector` to send messages and if you want the Agent to receive updates when you create and publish new services using the Orbital CLI. If you want to use HTTP requests to send messages, you can skip the RabbitMQ step.

We first start with the supporting components: Consul and RabbitMQ.

## Install Supporting Components

### Consul

Orbital has been tested with Consul version 1.0.0. This version can be found at their website here: https://releases.hashicorp.com/consul/1.0.0/.
For information regarding installation and configuration, please refer to our [Consul Setup Guide](ConsulSetup.md).

Be sure to have Consul up and running before continuing.

### RabbitMQ

You can install RabbitMQ on its own. You can find it here: https://www.rabbitmq.com/download.html. One of the ways to install RabbitMQ is to use Chocolatey on Windows since it manages the process for us and is a great tool for all sorts of packages.

Install Chocolatey (https://chocolatey.org/install).
Run Powershell as administrator.
Run the following command: choco install rabbitmq

RabbitMQ should be installed.

Be sure to have RabbitMQ up and running before continuing.

## Install Orbital Agent

To be able to get the Orbital Agent, you can either get the source code from our [repo](https://gitlab.com/foci-open-source/Foci.OrbitalBus) or download your preferred standalone Orbital Agent version [here](https://gitlab.com/foci-open-source/Foci.OrbitalBus/tags) without installing .NET core.

If you decided to fork the Orbital Agent, you will have to compile it using the dotnet cli. Open a terminal window and go to the source code folder. Run the following command:

```
dotnet build
```
The source code should compile successfully. Navigate to the `OrbitalBus\Foci.Orbital.Agent` folder and run the command:

```
dotnet run
``` 
An instance of Orbital Agent should be up and running now. 

In the other hand if you have downloaded the standalone version, you can go ahead and extract the contents to a folder of your choice. Using a terminal window, you can go ahead and run the Orbital Agent application.

To learn how to configure the Agent, check out the article [How to Configure the Orbital Agent](ConfigurationDescription.md).