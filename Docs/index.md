# Welcome to Orbital!

Orbital is a tool to quickly help the developer build and test services. When developing for microservices, there are a number of reasons that mocking might be important.  Perhaps a service has yet to be completed.  Perhaps there is no development instance of a service or the development responses are not sufficient for testing.  Whatever the reason, Orbital along with the adapters can replace an absentee service, letting a developer quickly mock out a service with expected responses to a variety of requests.  The Orbital Agent can also be used to front data persistence or other services.  Thanks to its adapter layer, the Agent can simulate work done by other services using its translations without requiring the construction of a whole new service.

For more information check out our [Getting Started](articles/README.html). Also be sure to check out our [repository](https://gitlab.com/foci-open-source/Foci.OrbitalBus).

**Check out our v4.0 release [here](https://gitlab.com/foci-open-source/Foci.OrbitalBus/tags/4.0.592)**

# License

Orbital is licensed under the 3-Clause BSD license for open-source software. Please see the [LICENSE](LICENSE.md) file for more information.  


# Contact Information
For inquiries about the project, please e-mail us at [opensource@focisolutions.com](mailto:opensource@focisolutions.com).