{% if grains['id'] == 'salt' %}
passwordHash: $6$9QaLrw9s$fQwuHcClVOvxi.VRdoJfjkFPmKcjUtOwGp9k/eBigeuK79w.qNoUyWn3GzYXb8ihX8pRRR3NGFoT5XLLgF8jd/
{% elif grains['id'] == 'minion1' %}
passwordHash: $6$mh3tVMW0$JM.VHrxQl.Cgs1usqGSrZ0rGPzW8ODVR.SsCYtVulVfcu5uh7PASwPjZa08Y.PgONeKPR7bIflb67qk6DRLVs.
{% elif grains['id'] == 'minion2' %}
passwordHash: $6$YeTIGq6V$RMyIVmFqaW.mW/52Y1tEA3dR5p9upDYn9aJKTQJrImL/boGdGK3ueQuVeDv7Kt5TECKxJGVqwF5t9A9i/EMAg1
{% endif %}
