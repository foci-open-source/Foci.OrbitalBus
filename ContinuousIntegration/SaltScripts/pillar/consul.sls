consul_location: https://releases.hashicorp.com/consul/0.6.4/consul_0.6.4_linux_amd64.zip
consul_address: 10.0.1.13
{% if grains['id'] == 'salt' %}
consul_confLocation: salt://configs/consul-boot.conf
{% else %}
consul_confLocation: salt://configs/consul-conf.conf
{% endif %}
