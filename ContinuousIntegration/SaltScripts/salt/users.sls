saltMinion:
  user.present:
    - home: /home/saltMinion
    - groups:
      - sudo
teamCity:
  user.present:
    - home: /home/teamCity
    - password: {{pillar['passwordHash']}}
    - shell: /bin/bash
    - groups:
      - adm
      - dip
      - plugdev
      - sambashare
