{% if grains['master'] == grains['host'] %}
start_agent_with_bootstrap:
  cmd.run:
    - name: "consul agent -server -bootstrap-expect 1 -data-dir='/usr/share/consul' &"
{% else %}
start_agent_without_bootstrap:
  cmd.run:
    - name: "consul agent -server -data-dir='/usr/share/consul' -join 10.0.1.13 &"
{% endif %}
