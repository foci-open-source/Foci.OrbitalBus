check_for_prereqs:
  pkg.installed:
    - pkgs:
      - curl
      - unzip
/var/lib/consul:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
del_consul_data:
  file.absent:
    - name: "/usr/share/consul"
/usr/share/consul:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
/etc/consul/conf.d:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
    - makedirs: True
get_consul:
  cmd.run:
    - name: "curl -OL https://dl.bintray.com/mitchellh/consul/0.5.2_linux_amd64.zip"
unzip_consul:
  cmd.run:
    - name: "unzip 0.5.2_linux_amd64.zip"
del_consul:
  cmd.run:
    - name: "rm /usr/local/bin/consul"
move_consul:
  cmd.run:
    - name: "mv consul /usr/local/bin/consul"
#include:
#  - activateAgent
