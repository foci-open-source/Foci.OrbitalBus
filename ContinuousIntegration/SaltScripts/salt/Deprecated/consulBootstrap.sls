include:
  - installMono
  - installRabbitmq
check_for_prereqs:
  pkg.installed:
    - pkgs:
      - curl
      - unzip
/var/lib/consul:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
/usr/share/consul:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
    - clean: True
/etc/consul/conf.d:
  file.directory:
    - user: saltMinion
    - group: sudo
    - dir_mode: 755
    - makedirs: True
get_consul:
  cmd.run:
    - name: "wget https://releases.hashicorp.com/consul/0.6.4/consul_0.6.4_linux_amd64.zip"
unzip_consul:
  cmd.run:
    - name: "unzip consul_0.6.4_linux_amd64.zip"
del_consul:
  cmd.run:
    - name: "rm /usr/local/bin/consul"
move_consul:
  cmd.run:
    - name: "mv consul /usr/local/bin/consul"
start_agent_with_bootstrap:
  cmd.run:
    - name: "nohup consul agent -server --bootstrap-expect 1 -data-dir='/usr/share/consul' &"
pause:
  cmd.run:
    - name: "sleep 30"
