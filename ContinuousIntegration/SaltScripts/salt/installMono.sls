install_curl_and_update:
  pkg.installed:
    - name: curl
    - refresh: True
remove_lists:
  file.directory:
    - name: /var/lib/apt/lists
    - clean: True
add_repo:
  cmd.run:
    - name: "apt-key adv --keyserver pgp.mit.edu --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF"
add_to_lists:
  file.append:
    - name: /etc/apt/sources.list.d/mono-xamarin.list
    - text: "deb http://download.mono-project.com/repo/debian wheezy main"
install_package:
  pkg.installed:
    - pkgs:
      - mono-devel
      - ca-certificates-mono
      - fsharp
      - mono-vbnc
      - nuget
    - refresh: True
cleanup_lists:
  file.directory:
    - name: /var/lib/apt/lists
    - clean: True
updgrade_mono:
  cmd.run:
    - name: "apt-get upgrade mono-complete"
