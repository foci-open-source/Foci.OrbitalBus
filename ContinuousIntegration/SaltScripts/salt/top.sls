base:
  '*':
    - firewall
    - users
  'salt':
    - order: 1
    - consul
  'minion*':
    - order: 2
    - consul
    - vsftpd
