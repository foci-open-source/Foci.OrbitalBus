consul:
  user.present:
    - fullname: Consul User
    - shell: /bin/bash
    - home: /home/consul
/etc/consul.d/bootstrap:
  file.directory:
    - dir_mode: 755
    - file_mode: 755
    - makedirs: True
/etc/consul.d/server:
  file.directory:
    - dir_mode: 755
    - file_mode: 755
    - makedirs: True
/etc/consul.d/client:
  file.directory:
    - dir_mode: 755
    - file_mode: 755
    - makedirs: True
/var/consul:
  file.directory:
    - dir_mode: 755
    - file_mode: 755
    - user: consul
    - group: consul
place_bootstrap_config:
  file.managed:
    - name: /etc/consul.d/bootstrap/config.json
    - source: salt://configs/consul-bootstrap.json
    - replace: True
place_server_config:
  file.managed:
    - name: /etc/consul.d/server/config.json
    - source: salt://configs/consul-server.json
    - replace: True
place_upstart_script:
  file.managed:
    - name: /etc/init/consul.conf
    - source: {{pillar['consul_confLocation']}}
    - replace: True
