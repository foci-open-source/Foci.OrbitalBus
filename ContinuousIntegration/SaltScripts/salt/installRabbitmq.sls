install_rabbitmq:
  pkg.installed:
    - name: rabbitmq-server
    - refresh: True
start_service:
  service.running:
    - name: rabbitmq-server
enable_web_ui:
  cmd.run:
    - name: "rabbitmq-plugins enable rabbitmq_management"
restart_service:
  service.running:
    - name: rabbitmq-server
    - reload: True
