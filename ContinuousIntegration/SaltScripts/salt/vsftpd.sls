vsftpd:
  pkg:
   - installed
clear_old_sender_data:
  file.absent:
    - name: /home/saltMinion/sender
clear_old_backendClient_data:
  file.absent:
    - name: /home/saltMinion/backendClient
clear_old_messageReceiver_data:
  file.absent:
    - name: /home/saltMinion/messageReceiver
watch_service_config:
  service.running:
    - name: vsftpd
    - watch:
      - file: /etc/vsftpd.conf
replace_configuration_file:
  file.managed:
    - name: /etc/vsftpd.conf
    - source: salt://configs/vsftpd.conf
    - replace: True
