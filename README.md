# Getting Started

Orbital is a tool to quickly help the developer build and test services. 


This article will show you how to quickly have Orbital up, publish services to it with the help of the Orbital CLI and send http requests to prove the service is working. These steps should take you less than 30 minutes. 

## Prerequisites
   
### NPM package manager

We need the Orbital CLI to create services. In order to get it, we need to have npm installed. If you do not have it, click [here](https://www.npmjs.com/get-npm).

### Docker

We need to install Docker in order to download the Orbital image. If you don't have it installed, follow this [link](https://docs.docker.com/install/overview/) and choose your preferred docker.

## Install Orbital CLI

To install Orbital CLI through npm, open a terminal/console window and run the following:

```
npm i @foci-solutions/orbital-cli
```

To confirm the CLI was successfully installed, run the following command:

```
orbital -v
```

The Orbital CLI version will be displayed in the format below:

```
@foci-solutions/orbital-cli/3.0.11 win32-x64 node-v8.12.0
```

## Run Orbital

### Pull the docker image

To get an instance of Orbital, we need to download the image found [here](https://hub.docker.com/r/focisolutions/orbital/). You can run the following to get the image:

```
docker pull focisolutions/orbital
```
### Configure the Orbital Agent

Create a directory called Orbital anywhere you like in your file system. Inside it, create a `Configs` folder. Inside this folder we will create a configuration file called `orbital.config` to configure the `Orbital Agent` like this:

```
{
  "ServiceStaticParameters": {
    "ServiceIp": "integration",
    "ServicePort": "3580"
  },
  "BrokerConfiguration": {
    "BusHostIp": "rabbitmq",
    "BusHostPort": 5672,
    "Username": "guest",
    "Password": "guest",
    "SslEnabled": "false"
  },
  "ConsulConfiguration": {
    "HostIp": "consul",
    "HostPort": 8500,
    "SslPolicyCNerror": "false",
    "SslEnabledConsul": "false"
  },
  "PublicConfiguration": {
    "HttpIp": "orbital",
    "HttpPort": 80,
    "BrokerIp": "rabbitmq",
    "BrokerPort": 5672
  },
  "RegisteredServiceNames": [ ]
}
```
For more information on how to configure the Orbital Agent, refer to this [article](ConfigurationDescription.md).

### Create the docker-compose file

Inside your `Orbital` folder, create a `docker-compose.yaml` file that will contain the following:

```
version: '3' 

services: 
  rabbitmq: 
    image: "rabbitmq:3-management" 
    hostname: "rabbitmq" 
    environment: 
      RABBITMQ_ERLANG_COOKIE: "SWQOKODSQALRPCLNMEQG" 
      RABBITMQ_DEFAULT_USER: "guest" 
      RABBITMQ_DEFAULT_PASS: "guest" 
      RABBITMQ_DEFAULT_VHOST: "/" 
    ports: 
      - "15672:15672" 
      - "5672:5672" 
    labels: 
      NAME: "rabbitmq" 

  consul: 
    image: consul:latest 
    hostname: "consul" 
    ports: 
    - "8400:8400" 
    - "8500:8500" 
    - "8600:8600" 
    labels: 
      NAME: "consul" 
  
  orbital:
    image: focisolutions/orbital:latest
    hostname: "orbital"
    volumes:
      - "./Configs:/app/Configs"

```

Optional: if you want to see the Orbital Agent's logs, add a folder called `logs` in your `Orbital` folder and under the `volumes` section in the docker-compose file add `- "./logs:/app/logs"`.

### Start the Orbital Agent

Open a terminal in your `Orbital` folder and run `docker-compose up -d`. Orbital should be up and running. You can check the logs (if you configured the folder and volume previously mentioned) to confirm the Orbital Agent is up.

## Create your first Service

Open a terminal in your `Orbital` folder and run the following command with the Orbital CLI:

```
orbital create:service ConsumerService -o GetCustomer -s true -a Adapters.Mock --validateRequest false --validateResponse false --httpEnabled GET --verbOnly true
```

This will create a `ConsumerService` folder. Go into the folder through the terminal. You will find a couple of folders, if you want to know about it, go to the [Orbital CLI article](OrbitalCLICommands.md). For now ignore them and run the command:

```
orbital build
```

Finally, run the command:

```
orbital publish
```

This will publish your new service to the Orbital Agent.

## Send a message

With the tool of your choice, to make HTTP requests, create a GET request like the following:

![An http request sample](../images/HttpRequest.PNG)

And you will receive the following response:

```
{
    "left": "{\"id\":112,\"name\":{\"firstName\":\"John\",\"middleName\":\"Jane\",\"lastName\":\"Doe\"},\"address\":{\"addressLine1\":\"5 City Street\",\"addressLine2\":\"\",\"city\":\"Ottawa\",\"provinceState\":\"ON\",\"country\":\"CA\",\"postalZipCode\":\"5A5A5A\"}}",
    "right": null,
    "headers": {}
}
```

Congratulations! You have successfully created your first service.

To know more about Orbital, refer to the rest of the articles in this website.