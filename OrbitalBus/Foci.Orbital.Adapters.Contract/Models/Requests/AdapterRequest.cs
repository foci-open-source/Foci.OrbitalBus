﻿using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Contract.Models.Requests
{
    /// <summary>
    /// Class containing information needed for sending request to an adapter
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class AdapterRequest
    {
        /// <summary>
        /// String representation of adapter configuration
        /// </summary>
        public string AdapterConfiguration { get; }

        /// <summary>
        /// String representation of translated message
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Dictionary of dynamic and static parameters
        /// </summary>
        public ReadOnlyDictionary<string, string> Parameters { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="adapterConfiguration">String representation of adapter configuration</param>
        /// <param name="message">String representation of translated message</param>
        /// <param name="parameters">Dictionary of dynamic and static parameters</param>
        public AdapterRequest(string adapterConfiguration, string message, ReadOnlyDictionary<string, string> parameters)
        {
            AdapterConfiguration = adapterConfiguration;
            Message = message;
            Parameters = parameters;
        }
    }
}
