﻿using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Contract.Models.Payloads;

namespace Foci.Orbital.Adapters.Contract.Models.Interfaces
{
    /// <summary>
    /// Public interface for adapters to send messages.
    /// </summary>
    public interface IAdapter
    {
        /// <summary>
        /// Send asynchronous request to adapter (with or without authentication credentials from the client)
        /// </summary>
        /// <param name="request">The object containing request information</param>
        void SendAsync(AdapterRequest request);

        /// <summary>
        /// Send synchronous request to adapter (with or without authentication credentials from the client)
        /// </summary>
        /// <param name="request">The object containing request information</param>
        /// <returns>Deserialized response as a payload with and adapter response containing all the necessary information.</returns>
        Payload SendSync(AdapterRequest request);
    }
}
