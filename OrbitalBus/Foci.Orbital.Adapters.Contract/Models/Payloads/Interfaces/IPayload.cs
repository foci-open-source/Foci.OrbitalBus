﻿using Foci.Orbital.Adapters.Contract.Faults;
using System;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Contract.Models.Payloads.Interfaces
{
    /// <summary>
    /// The interface exposing what a payload must have.
    /// </summary>
    public interface IPayload
    {
        /// <summary>
        /// The headers of a payload. read-only.
        /// </summary>
        PayloadHeaders Headers { get; }

        /// <summary>
        /// A payload must implement a Match function. This is a non generic match function returning an object.
        /// </summary>
        /// <param name="leftMatch">Function to be used if the either contains a left value.</param>
        /// <param name="rightMatch">Function to be used if the either contains a right value.</param>
        /// <returns>Both functions must return a type of object.</returns>
        object Match(Func<string, object> leftMatch, Func<IEnumerable<Fault>, object> rightMatch);
    }
}
