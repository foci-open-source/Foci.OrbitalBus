﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads.Converter;
using Foci.Orbital.Adapters.Contract.Models.Payloads.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Contract.Models.Payloads
{
    /// <summary>
    /// Class to wrap the response from RestSharper request.
    /// </summary>
    /// <typeparam name="T">Type of response from RestSharper request.</typeparam>
    public class Payload : Either<string, IEnumerable<Fault>>, IPayload
    {
        /// <summary>
        /// The headers of a payload. Will generally contain communication data.
        /// </summary>
        [JsonConverter(typeof(OrbitalImmutableDictionaryConverter))]
        public PayloadHeaders Headers { get; } = new PayloadHeaders();

        /// <summary>
        /// Constructor that sets the headers to handle.
        /// </summary>
        /// <param name="left">Type of response.</param>
        /// <param name="right">Type of fault.</param>
        /// <param name="Headers">wrapped http headers in PayloadHeaders.</param>
        [JsonConstructor]
        private Payload(string left, IEnumerable<Fault> right, PayloadHeaders Headers) : base(left, right)
        {
            this.Headers = Headers ?? new PayloadHeaders();
        }

        /// <summary>
        /// Wraps type of response into a Payload object.
        /// </summary>
        /// <typeparam name="T">Type of response from RestSharper request.</typeparam>
        /// <param name="message">Type of response from RestSharper request.</param>
        /// <returns>Wrapped response in a Payload object.</returns>
        public static Payload Create(string message)
        {
            return new Payload(message);
        }

        /// <summary>
        /// Wraps type of response with headers into a Payload object.
        /// </summary>
        /// <typeparam name="T">Type of response from RestSharper request.</typeparam>
        /// <param name="message">Type of response from RestSharper request.</param>
        /// <param name="headers">wrapped HTTP headers in PayloadHeaders object. </param>
        /// <returns>Wrapped response with headers in a Payload object.</returns>
        public static Payload Create(string message, PayloadHeaders headers)
        {
            return new Payload(message, headers);
        }

        /// <summary>
        /// Wraps type of fault into a Payload object.
        /// </summary>
        /// <typeparam name="T">Type of fault.</typeparam>
        /// <param name="fault">fault object.</param>
        /// <returns>Wrapped fault in a Payload object.</returns>
        public static Payload Create(IEnumerable<Fault> faults)
        {
            return new Payload(faults);
        }

        /// <summary>
        /// Wraps type of fault with headers into a Payload object.
        /// </summary>
        /// <typeparam name="T">Type of fault.</typeparam>
        /// <param name="fault">fault object.</param>
        /// <param name="headers">wrapped HTTP headers in PayloadHeaders object.</param>
        /// <returns>Wrapped fault with headers in a Payload object.</returns>
        public static Payload Create(IEnumerable<Fault> faults, PayloadHeaders headers)
        {
            return new Payload(faults, headers);
        }

        /// <inheritdoc />
        internal Payload(string value) : base(value)
        {

        }

        /// <inheritdoc />
        internal Payload(IEnumerable<Fault> faults) : base(faults)
        {

        }

        /// <inheritdoc />
        internal Payload(string value, PayloadHeaders headers) : this(value)
        {
            this.Headers = Headers ?? new PayloadHeaders();
        }

        /// <inheritdoc />
        internal Payload(IEnumerable<Fault> faults, PayloadHeaders headers) : base(faults)
        {
            this.Headers = Headers ?? new PayloadHeaders();
        }

        /// <inheritdoc />
        public object Match(Func<string, object> leftMatch, Func<IEnumerable<Fault>, object> rightMatch)
        {
            return Match<object>(leftMatch, rightMatch);
        }
    }
}
