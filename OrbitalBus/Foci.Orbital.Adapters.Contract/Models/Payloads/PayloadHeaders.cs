﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;

namespace Foci.Orbital.Adapters.Contract.Models.Payloads
{
    /// <summary>
    /// Class that will store headers associated with a payload.
    /// </summary>
    public class PayloadHeaders : IEnumerable<KeyValuePair<string, string>>
    {
        private readonly ImmutableDictionary<string, string> headers;
        private readonly IEqualityComparer<string> comparer = StringComparer.Create(new CultureInfo("en"), true);

        /// <summary>
        /// Default constructor that creates empty ImmutableDictionary.
        /// </summary>
        public PayloadHeaders()
        {
            this.headers = ImmutableDictionary.Create(comparer, comparer);
        }

        /// <summary>
        /// Constructor that forces IEnumerable to ImmutableDictionary
        /// </summary>
        /// <param name="headers">Enumerator containing http headers.</param>
        public PayloadHeaders(IEnumerable<KeyValuePair<string, string>> headers)
        {
            this.headers = headers.ToImmutableDictionary();
        }

        /// <summary>
        /// Constructor that forces ICollection to ImmutableDictionary.
        /// </summary>
        /// <param name="headers">Collection containing http headers.</param>
        public PayloadHeaders(ICollection<KeyValuePair<string, string>> headers)
        {
            this.headers = headers.ToImmutableDictionary();
        }

        /// <summary>
        /// Constructor that forces dictionary to immutable dictionary.
        /// </summary>
        /// <param name="headers">Dictionary containing http headers.</param>
        public PayloadHeaders(IDictionary<string, string> headers)
        {
            this.headers = headers.ToImmutableDictionary();
        }

        /// <summary>
        /// Gets the value of a key based on input string.
        /// </summary>
        /// <param name="key">String representation of the name related to a header value.</param>
        /// <returns>String representation of the header value.</returns>
        public string Value(string key)
        {
            return this.headers[key];
        }

        /// <summary>
        /// Gets all the keys of the header.
        /// </summary>
        /// <returns>Enumerator of string representing the name of the headers.</returns>
        public IEnumerable<string> GetAllKeys()
        {
            return this.headers.Keys;
        }

        /// <summary>
        /// Determines whether the headers have key.
        /// </summary>
        /// <param name="key">string representation of the name of a header.</param>
        /// <returns>Flag indicating true if the key exists in the immutable dictionary or false if it does not exist.</returns>
        public bool ContainsKey(string key)
        {
            return this.headers.ContainsKey(key);
        }

        #region IEnumerable
        /// <summary>
        /// Returns an enumerator from a dictionary of headers.
        /// </summary>
        /// <returns>Enumerator from a dictionary of headers.</returns>
        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return this.headers.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator from a dictionary of headers.
        /// </summary>
        /// <returns>Enumerator from a dictionary of headers.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.headers.GetEnumerator();
        }
        #endregion
    }
}
