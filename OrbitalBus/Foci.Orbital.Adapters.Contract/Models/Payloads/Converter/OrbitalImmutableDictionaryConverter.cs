﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Foci.Orbital.Adapters.Contract.Models.Payloads.Converter
{
    /// <summary>
    /// The json converter used to serialize and deserialize an ImmutableDictionary to Json and back
    /// </summary>
    internal class OrbitalImmutableDictionaryConverter : JsonConverter
    {
        /// <summary>
        /// Overridden property that determines whether this converter can write json.
        /// </summary>
        public override bool CanWrite { get; } = true;

        /// <summary>
        /// Overridden property that determines whether this convert can read json.
        /// </summary>
        public override bool CanRead { get; } = true;

        /// <summary>
        /// Overridden that determines how an object is written to json.
        /// </summary>
        /// <param name="writer">JsonWriter object that will write the value object into json.</param>
        /// <param name="value">Dictionary object to be written to an json object.</param>
        /// <param name="serializer">Serializer to write object to json format.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();

            // Write dictionary key-value pairs.
            var dictionary = value as PayloadHeaders;
            var dictionaryKeys = dictionary.GetAllKeys();
            foreach (var kvp in dictionaryKeys)
            {
                writer.WritePropertyName(kvp);
                serializer.Serialize(writer, dictionary.Value(kvp));
            }

            writer.WriteEndObject();
        }

        /// <summary>
        /// Overridden function that determines how an object type is read from json data.
        /// </summary>
        /// <param name="reader">JsonReader object containing header information to be turned to a dictionary for a PayloadHeaders object.</param>
        /// <param name="objectType">Type: System.Type. Type of the object. Not used in this implementation.</param>
        /// <param name="existingValue">Type: System.Type. The existing value of object being read. Not used in this implementation.</param>
        /// <param name="serializer">Type: Newtonsoft.Json.JsonSerializer. The calling serializer. Not used in this implementation.</param>
        /// <returns>New PayloadHeaders with dictionary information.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            try
            {
                var item = JObject.Load(reader);

                var dictionary = item.ToObject<Dictionary<string, string>>();

                return new PayloadHeaders(dictionary);
            }
            catch (Exception)
            {
                return new PayloadHeaders();
            }
        }

        /// <summary>
        /// Overridden method that returns whether a type can be converted.
        /// </summary>
        /// <param name="objectType">Type object to be compared.</param>
        /// <returns>True if equal.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(PayloadHeaders);
        }
    }
}
