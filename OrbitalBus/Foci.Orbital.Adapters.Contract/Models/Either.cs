﻿using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Contract.Models
{
    /// <summary>
    /// An either class. Can store two types and return one or the other.
    /// </summary>
    /// <typeparam name="TLeft">Type of object.</typeparam>
    /// <typeparam name="TRight">Type of object</typeparam>
    [ExcludeFromCodeCoverage]
    public class Either<TLeft, TRight>
        where TLeft : class
        where TRight : class
    {
        private readonly int tag;
        [JsonProperty]
        private readonly TLeft left = null;
        [JsonProperty]
        private readonly TRight right = null;

        /// <summary>
        /// If the constructor fault value is null we know to use the value route as there is no fault. Otherwise, if the fault is not null we must
        /// set the value to null and use the fault.
        /// </summary>
        /// <param name="left">Type of object</param>
        /// <param name="right">Type of object</param>
        protected Either(TLeft left, TRight right)
        {
            if (left == default(TLeft))
            {
                this.tag = 1;
                this.right = right;
            }
            else
            {
                this.tag = 0;
                this.left = left;
            }
        }

        /// <summary>
        /// Building the left side of a fault.
        /// </summary>
        /// <param name="left">Type of object</param>
        public Either(TLeft left) : this(left, default(TRight))
        {

        }

        /// <summary>
        /// Building the right side of a fault.
        /// </summary>
        /// <param name="right">Type of object</param>
        public Either(TRight right) : this(default(TLeft), right)
        {

        }

        /// <summary>
        /// Applies the proper function depending on what value is set.
        /// </summary>
        /// <typeparam name="TReturn">The return type</typeparam>
        /// <param name="leftMatch"> Function to execute if the left value was set.</param>
        /// <param name="rightMatch"> Function to execute if the right value was set.</param>
        /// <returns> The generic return value of the two functions.</returns>
        public TReturn Match<TReturn>(Func<TLeft, TReturn> leftMatch, Func<TRight, TReturn> rightMatch)
        {
            switch (tag)
            {
                case 0: return leftMatch(this.left);
                case 1: return rightMatch(this.right);
                // Will never run.
                default: throw new Exception("Unrecognized tag value: " + tag);
            }
        }
    }
}
