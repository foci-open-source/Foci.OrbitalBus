﻿using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Newtonsoft.Json;
using Nustache.Core;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Foci.Orbital.Adapters.Contract
{
    /// <summary>
    /// The abstract foundation to contain protected and internal code.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public abstract class BaseAdapter : IAdapter
    {
        /// <inheritdoc />
        public abstract void SendAsync(AdapterRequest request);

        /// <inheritdoc />
        public abstract Payload SendSync(AdapterRequest request);

        /// <summary>
        /// Loads parameters into the service configuration.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <param name="config">The path to the service configuration file.</param>
        /// <param name="parameters">The parameters to inject into the service configuration. Keys will be matched against keys, values inserted in their places.</param>
        /// <returns>The service configuration with the injected parameters deserialized into the type requested.</returns>
        protected T LoadParamsToConfig<T>(string config, ReadOnlyDictionary<string, string> parameters)
        {
            var configAsJson = parameters.Any()
                ? Render.StringToString(config, parameters)
                : config;

            var serviceConfiguration = JsonConvert.DeserializeObject<T>(configAsJson);
            return serviceConfiguration;
        }
    }
}
