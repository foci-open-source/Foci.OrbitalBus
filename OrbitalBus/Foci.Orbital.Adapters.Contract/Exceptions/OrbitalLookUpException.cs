﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Adapters.Contract.Exceptions
{
    /// <summary>
    /// A look up exception has occurred in the Orbital.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class OrbitalLookUpException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalLookUpException()
        { }

        /// <inheritdoc />
        public OrbitalLookUpException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalLookUpException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalLookUpException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

    }
}
