﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Adapters.Contract.Exceptions
{
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalAdapterException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalAdapterException()
        { }

        /// <inheritdoc />
        public OrbitalAdapterException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalAdapterException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalAdapterException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
