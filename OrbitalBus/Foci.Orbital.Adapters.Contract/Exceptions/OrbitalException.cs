﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.Serialization;

namespace Foci.Orbital.Adapters.Contract.Exceptions
{
    /// <summary>
    /// The root OrbitalException. All Orbital exceptions must inherit from this type.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public abstract class OrbitalException : Exception
    {

        #region Constants
        private const string headersPropertyName = "Headers";
        #endregion

        /// <summary>
        /// Specifies the original source of the exception.
        /// </summary>
        public string OriginalSource { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalException()
        { }

        /// <summary>
        /// Default overridden exception constructor
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        public OrbitalException(string message)
        : base(message)
        { }

        /// <summary>
        /// Stores the inner exception source into the OrbitalException original source.
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        /// <param name="innerException">Exception object containing additional information regarding the exception occurred.</param>
        public OrbitalException(string message, Exception innerException)
            : base(message, innerException)
        {
            OriginalSource = innerException.Source;
        }

        /// <summary>
        /// Default overridden exception constructor.
        /// </summary>
        /// <param name="info">Serialized data with information to include in Exception object.</param>
        /// <param name="context">Describes source and destination of the parameter info value.</param>
        protected OrbitalException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

        public bool HasHeaders()
        {
            return this != null && this.GetType().GetProperty(headersPropertyName) != default(PropertyInfo);
        }

        /// <summary>
        /// Get the Headers property of the exception.
        /// </summary>
        /// <param name="exception">The exception to derive the headers dictionary from.</param>
        /// <returns>The headers dictionary.</returns>
        public Dictionary<string, string> GetHeaders()
        {
            return (Dictionary<string, string>)this.GetType().GetProperty(headersPropertyName).GetValue(this);
        }
    }
}
