﻿using Foci.Orbital.Adapters.Contract.Faults.Lookups;
using System;

namespace Foci.Orbital.Adapters.Contract.Faults.Factories
{
    /// <summary>
    /// A factory that creates all runtime faults.
    /// </summary>
    public class FaultFactory
    {
        private readonly FaultLookup faultLookup;

        /// <summary>
        /// Default constructor
        /// </summary>
        public FaultFactory() : this(new FaultLookup()) { }

        /// <summary>
        /// Constructor to pass in the correct fault look up used to retrieve fault code
        /// </summary>
        /// <param name="faultLookup">The fault look up used to retrieve fault code</param>
        protected FaultFactory(FaultLookup faultLookup)
        {
            this.faultLookup = faultLookup;
        }

        /// <summary>
        /// Method to translate a Exception into a fault.
        /// </summary>
        /// <param name="exception">The exception to translate</param>
        /// <returns>Translated fault</returns>
        public Fault CreateFault(Exception exception)
        {
            return CreateFault(faultLookup, exception);
        }

        /// <summary>
        /// Method to translate a Exception into a fault.
        /// </summary>
        /// <param name="faultLookup">The fault look up used to retrieve fault code</param>
        /// <param name="exception">The exception to translate</param>
        /// <returns>Translated fault</returns>
        private Fault CreateFault(FaultLookup faultLookup, Exception exception)
        {
            if (exception == null)
            {
                throw new ArgumentNullException(nameof(exception));
            }

            var faultCode = faultLookup.GetFaultCode(exception);

            return Fault.Create(exception.Message, faultCode);
        }
    }
}
