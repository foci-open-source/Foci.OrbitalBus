﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Adapters.Contract.Faults.Lookups
{
    /// <summary>
    /// Based on enumeration value, a Error Description will be found.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal static class ErrorDescriptionLookUp
    {
        /// <summary>
        /// Static method to get error description string.
        /// </summary>
        /// <param name="errorCode">enum representing an error code</param>
        /// <returns>String representation of the error code matched. If the error code is not matched, throws an exception.</returns>
        /// <exception cref="OrbitalLookUpException"/>
        public static string GetErrorDescription(OrbitalFaultCode errorCode)
        {
            if (errorCodeLookup.TryGetValue(errorCode, out string errorDescription))
            {
                return errorDescription;
            }
            else
            {
                throw new OrbitalLookUpException();
            }
        }

        /// <summary>
        /// The look up dictionary
        /// </summary>
        private static readonly Dictionary<OrbitalFaultCode, string> errorCodeLookup = new Dictionary<OrbitalFaultCode, string>()
        {
            { OrbitalFaultCode.Orbital_UnhandledFault, "Unhandled fault has occurred. View the details for more information." },
            { OrbitalFaultCode.Orbital_BusinessFault_010, "A Business fault has occurred. View the details for more information." },
            { OrbitalFaultCode.Orbital_Business_Common_011, "This is a common error code. The fault details should have more information." },
            { OrbitalFaultCode.Orbital_Business_Adapter_Error_012, "There was an error in the adapter. See the details and headers for more information on the error." },
            { OrbitalFaultCode.Orbital_Business_Translation_Error_013, "An error was thrown by the translation code. See the message and headers for information provided by the translation." },
            { OrbitalFaultCode.Orbital_Runtime_CommunicationFault_001, "A communication fault occurred. View the details for more information." },
            { OrbitalFaultCode.Orbital_Runtime_TranslationFault_002, "A translation fault has occurred. View the details for more information." },
            { OrbitalFaultCode.Orbital_Runtime_InternalFault_003, "A Orbital internal fault has occurred. Please contact your administrator for help." },
            { OrbitalFaultCode.Orbital_Runtime_AggregateFault_004, "An aggregate fault has occurred. There are multiple faults, please view details to learn more." },
            { OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005, "An argument fault has occurred. View details for more information." },
            { OrbitalFaultCode.Orbital_Runtime_FileFault_008, "A File error has occurred. View details for more information." },
            { OrbitalFaultCode.Orbital_Runtime_TargetInvocationFault_009, "A reflection invocation error has occurred. View details for more information." },
            { OrbitalFaultCode.Orbital_Operation_Handler_Error_014, "An operation handling fault occurred while attempting to invoke the pipeline." },
            { OrbitalFaultCode.Orbital_Business_Validation_Error_015, "A Validation error has occurred while validating incoming request and/or outgoing response." }
        };
    }
}
