﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace Foci.Orbital.Adapters.Contract.Faults.Lookups
{
    /// <summary>
    /// A class to store the relationship between exceptions and faults.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class FaultLookup
    {
        /// <summary>
        /// The dictionary storing a type to type relationship from exception to fault.
        /// </summary>
        protected readonly Dictionary<Type, OrbitalFaultCode> faultLookup = new Dictionary<Type, OrbitalFaultCode>()
        {
            { typeof(AggregateException), OrbitalFaultCode.Orbital_Runtime_AggregateFault_004  },
            { typeof(ArgumentException), OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005 },
            { typeof(TargetInvocationException), OrbitalFaultCode.Orbital_Runtime_TargetInvocationFault_009 },
            { typeof(NotImplementedException), OrbitalFaultCode.Orbital_Runtime_InternalFault_003 },
            { typeof(ArgumentNullException), OrbitalFaultCode.Orbital_Runtime_InternalFault_003 },
            { typeof(OrbitalAdapterException),OrbitalFaultCode.Orbital_Business_Adapter_Error_012}
        };

        /// <summary>
        /// Looks up the exception and returns a fault type.
        /// </summary>
        /// <param name="exception">Exception object to be match to custom types.</param>
        /// <returns>Type of fault matched with the exception provided.</returns>
        public OrbitalFaultCode GetFaultCode(Exception exception)
        {
            if (exception == null)
            {
                return OrbitalFaultCode.Orbital_UnhandledFault;
            }


            if (faultLookup.TryGetValue(exception.GetType(), out OrbitalFaultCode faultCode))
            {
                return faultCode;
            }
            else
            {
                return OrbitalFaultCode.Orbital_UnhandledFault;
            }
        }
    }
}
