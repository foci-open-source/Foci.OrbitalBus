﻿using Foci.Orbital.Adapters.Contract.Faults.Lookups;
using Newtonsoft.Json;
using System;

namespace Foci.Orbital.Adapters.Contract.Faults
{
    /// <summary>
    /// Enum representing potential fault codes. All faults are assigned a fault code and description.
    /// </summary>
    public enum OrbitalFaultCode
    {
        /// <summary>
        /// All unhandled faults resolve to the Unhandled Fault
        /// </summary>
        Orbital_UnhandledFault = 0,
        /// <summary>
        /// Communication fault
        /// </summary>
        Orbital_Runtime_CommunicationFault_001,
        /// <summary>
        /// Translation fault.
        /// </summary>
        Orbital_Runtime_TranslationFault_002,
        /// <summary>
        /// Internal fault.
        /// </summary>
        Orbital_Runtime_InternalFault_003,
        /// <summary>
        /// Aggregate Fault.
        /// </summary>
        Orbital_Runtime_AggregateFault_004,
        /// <summary>
        /// Argument fault.
        /// </summary>
        Orbital_Runtime_ArgumentFault_005,
        /// <summary>
        /// File Fault 
        /// </summary>
        Orbital_Runtime_FileFault_008,
        /// <summary>
        /// Reflection Fault 
        /// </summary>
        Orbital_Runtime_TargetInvocationFault_009,
        /// <summary>
        /// Reflection Fault 
        /// </summary>
        Orbital_BusinessFault_010,
        /// <summary>
        /// A common fault. If no other fault matches.
        /// </summary>
        Orbital_Business_Common_011,
        /// <summary>
        /// Adapter code fault.
        /// </summary>
        Orbital_Business_Adapter_Error_012,
        /// <summary>
        /// An error was thrown by the translation script.
        /// </summary>
        Orbital_Business_Translation_Error_013,
        /// <summary>
        /// An error was thrown by the OperationHandler.
        /// </summary>
        Orbital_Operation_Handler_Error_014,
        /// <summary>
        /// An error was thrown when validating request/response.
        /// </summary>
        Orbital_Business_Validation_Error_015
    }

    /// <summary>
    /// The base fault class that all other faults are derived from.
    /// </summary>
    public class Fault
    {
        public readonly OrbitalFaultCode OrbitalFaultCode;
        /// <summary>
        /// The string representation of an error code unique to this fault.
        /// </summary>
        public readonly string OrbitalFaultName;
        /// <summary>
        /// The string representation of the error description unique to this fault.
        /// </summary>
        public readonly string ErrorDescription;
        /// <summary>
        /// The fault details.
        /// </summary>
        public readonly string Details;

        /// <summary>
        /// The constructor used to instantiate all properties of the fault. Child classes use this method as their
        /// json constructor.
        /// </summary>
        /// <param name="details">String representation of the fault details.</param>
        /// <param name="errorCode">String representation of an error code unique to this fault.</param>
        /// <param name="errorDescription">String representation of the error description unique to this fault.</param>
        /// <param name="typeSource">The json serialization/deserialization string used between OrbitalConnector and common.</param>
        [JsonConstructor]
        public Fault(string details, string errorCode, string errorDescription, OrbitalFaultCode faultCode)
        {
            this.Details = details ?? string.Empty;
            this.OrbitalFaultName = string.IsNullOrEmpty(errorCode) ? GetEnumName(faultCode) : errorCode;
            this.ErrorDescription = errorDescription ?? GetErrorDescription(faultCode);
            this.OrbitalFaultCode = faultCode;
        }

        /// <summary>
        /// The basic data needed to create a fault.
        /// </summary>
        /// <param name="details">The details of a fault.</param>
        /// <param name="faultCode">Used to look up the Error Description and generate the fault code string.</param>
        public static Fault Create(string details, OrbitalFaultCode faultCode)
        {
            return new Fault(details, GetEnumName(faultCode), GetErrorDescription(faultCode), faultCode);
        }

        /// <summary>
        /// Calls the lookup table to get the error description.
        /// </summary>
        /// <param name="errorCode">Used to look up the Error Description and generate the Error code string.</param>
        /// <returns>String representation of the error code description.</returns>
        private static string GetErrorDescription(OrbitalFaultCode errorCode)
        {
            return ErrorDescriptionLookUp.GetErrorDescription(errorCode);
        }

        /// <summary>
        /// Generates the string form of the error code.
        /// </summary>
        /// <param name="errorCode">Used to look up the error name and generate the Error code string.</param>
        /// <returns>String representation of the error code name.</returns>
        private static string GetEnumName(Enum errorCode)
        {
            var enumType = errorCode.GetType();

            return Enum.GetName(enumType, errorCode);
        }
    }
}
