﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("FAE04EC0-301F-11D3-BF4B-00C04F79EFBC")]

[assembly: InternalsVisibleTo("Foci.Orbital.Integration.Tests")]
[assembly: InternalsVisibleTo("Foci.Orbital.Adapters.Contract.Tests")]
