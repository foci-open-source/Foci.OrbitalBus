﻿using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.App_Start;
using Foci.Orbital.Agent.Factories;
using Foci.Orbital.Agent.HttpApi.Startup;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Pipelines.Generic.Models.Interfaces;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Web;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using Unity;
using Unity.Microsoft.DependencyInjection;

namespace Foci.Orbital.Agent
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        private const string defaultConfigurationFile = "orbital.config";
        private static NLog.ILogger log = LogManager.GetCurrentClassLogger();
        private static readonly AutoResetEvent closing = new AutoResetEvent(false);

        public static int Main(string[] args)
        {
            Console.Title = "Orbital Agent";
            Console.CancelKeyPress += new ConsoleCancelEventHandler((sender, eventArgs) =>
            {
                DependencyInjector.Container.Dispose();
                closing.Set();
            });
            DependencyInjector.RegisterDependencies();

            //If needed, create any required folders
            var fileChecker = DependencyInjector.Container.Resolve<FileSystemReadiness>();
            fileChecker.CheckFileSystem();

            // Configure global settings.
            var configurationService = DependencyInjector.Container.Resolve<IConfigurationService>();
            var configurationLoader = new ConfigurationLoader((args == null || args.Length == 0) ? defaultConfigurationFile : args[0]);

            var config = configurationLoader.GetConfiguration();
            configurationService.Initialize(config);

            //start pipelines
            var syncMessageProcessor = DependencyInjector.Container.Resolve<IPipeline<MessageProcessorInput, Payload>>();
            syncMessageProcessor.Start();
            var asyncMessageProcessor = DependencyInjector.Container.Resolve<IPipeline<MessageProcessorInput>>();
            asyncMessageProcessor.Start();

            //set cache
            DependencyInjector.Container.Resolve<ServiceCache>();
            ServiceCache.StartPolicyCache();

            var webhost = BuildWebHost(configurationService.GetConfigurationProperties().PublicConfiguration);

            try
            {
                log.Info("Initialization process completed. Starting Agent now.");

                var Agent = DependencyInjector.Container.Resolve<Agent>();
                Agent.Start(webhost);
            }
            catch (Exception e)
            {
                log.Error(e.ToString());

                webhost.StopAsync();
                syncMessageProcessor.Stop();
                asyncMessageProcessor.Stop();

                return 10;
            }

            closing.WaitOne();

            log.Info("Closing Application");
            webhost.StopAsync();
            //stop pipelines
            syncMessageProcessor.Stop();
            asyncMessageProcessor.Stop();

            log.Info("All processing completed.");
            return 0;
        }

        // Build the web host using provided url
        private static IWebHost BuildWebHost(PublicConfiguration publicConfig)
        {
            var url = string.Format("http://{0}:{1}", publicConfig.HttpIp, publicConfig.HttpPort);

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(url)
                .UseUnityServiceProvider(DependencyInjector.Container)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseNLog()
                .Build();

            return host;
        }
    }
}
