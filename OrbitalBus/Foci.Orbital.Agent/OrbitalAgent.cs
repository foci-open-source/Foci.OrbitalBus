﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using NLog;
using System.Linq;

namespace Foci.Orbital.Agent
{
    /// <summary>
    /// Serves to load the service definitions.
    /// </summary>
    internal class Agent
    {
        private readonly IConfigurationService config;
        private readonly IConsulService consulService;
        private readonly IServiceDefinitionService serviceDefinitionService;
        private ILogger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor first composes the container, then adds the subscriptions to a bus, and lastly register services to consul.
        /// </summary>
        /// <param name="config">IConfigurationService used to get configuration values</param>
        /// <param name="consulService">IConsulService used for register services to consul</param>
        /// <param name="keystoreService">IKeystoreService used to get RabbitMQ credential</param>
        public Agent(IConfigurationService config,
                     IConsulService consulService,
                     IServiceDefinitionService serviceDefinitionService)
        {
            this.consulService = consulService;
            this.config = config;
            this.serviceDefinitionService = serviceDefinitionService;
        }

        /// <summary>
        /// Starts the registration of services and subscriptions to RabbitMQ.
        /// </summary>
        /// <returns>An overall status of the services and successful registrations.</returns>
        public AgentStartStatus Start(IWebHost webHost)
        {
            var serviceResults = this.serviceDefinitionService.RegisterAndBootsrapServices();
            this.serviceDefinitionService.SubscribeToServiceUpdate();
            var serviceRegistrationResults = new ServiceRegistrationResults(serviceResults);
            var consulResults = this.consulService.RegisterPublishedServices(serviceResults).ToList();

            log.Info("Broker is listening to the loaded Agents on {0}", config.GetConfigurationProperties().BrokerConfiguration.BusHostIp);

            var publicConfig = config.GetConfigurationProperties().PublicConfiguration;
            string httpUrl = string.Format("http://{0}:{1}", publicConfig.HttpIp, publicConfig.HttpPort);

            webHost.RunAsync();

            return new AgentStartStatus(serviceRegistrationResults, consulResults);
        }
    }
}
