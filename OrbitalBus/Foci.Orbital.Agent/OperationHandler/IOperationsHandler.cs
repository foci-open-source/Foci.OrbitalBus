﻿using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Requests;
using Foci.Orbital.Agent.Models.Service;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.OperationHandler
{
    public interface IOperationsHandler
    {
        /// <summary>
        /// Get the callback functions for subscriptions and return a SubscriptionDetails.
        /// </summary>
        /// <param name="configService">Configuration Services containing properties necessary for dependencies to work correctly.</param>
        /// <returns></returns>
        IEnumerable<SubscriberDetails> GetSubscriberDetails(ServiceDefinition serviceDefinition);

        /// <summary>
        /// A generic async operation that takes a request and builds a description to invoke the pipeline
        /// </summary>
        /// <param name="request">The Orbital Request model</param>
        void InvokeAsyncOperation(OrbitalRequest request);

        /// <summary>
        /// A generic sync operation that takes a request and builds a description to invoke the pipeline
        /// </summary>
        /// <param name="request">The Orbital Request model</param>
        Payload InvokeSyncOperation(OrbitalRequest request);
    }
}
