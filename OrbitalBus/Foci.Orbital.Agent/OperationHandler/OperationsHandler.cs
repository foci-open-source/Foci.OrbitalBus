﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Requests;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Pipelines.Generic.Models.Interfaces;
using Foci.Orbital.Agent.Policies.Cache;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Foci.Orbital.Agent.OperationHandler
{
    /// <summary>
    /// This class sets the translation service, the configuration service needed to create an Adapter pipeline invoker that will send the messages.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class OperationsHandler : IOperationsHandler
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        private readonly IServiceDefinitionRepository serviceDefinitionRepository;
        private readonly IPipeline<MessageProcessorInput, Payload> syncMessageProcessor;
        private readonly IPipeline<MessageProcessorInput> asyncMessageProcessor;

        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        /// <summary>
        /// Injected constructor.
        /// </summary>
        /// <param name="memCacheService">The service definition service</param>
        /// <param name="syncMessageProcessor"></param>
        /// <param name="asyncMessageProcessor"></param>
        public OperationsHandler(IServiceDefinitionRepository serviceDefinitionRepository,
            IPipeline<MessageProcessorInput, Payload> syncMessageProcessor,
            IPipeline<MessageProcessorInput> asyncMessageProcessor)
        {
            this.serviceDefinitionRepository = serviceDefinitionRepository ?? throw new ArgumentNullException(nameof(serviceDefinitionRepository));
            this.syncMessageProcessor = syncMessageProcessor ?? throw new ArgumentNullException(nameof(syncMessageProcessor));
            this.asyncMessageProcessor = asyncMessageProcessor ?? throw new ArgumentNullException(nameof(asyncMessageProcessor));
        }

        /// <inheritdoc />
        public IEnumerable<SubscriberDetails> GetSubscriberDetails(ServiceDefinition serviceDefinition)
        {

            var subscriberDetails = new List<SubscriberDetails>();

            foreach (var operation in serviceDefinition.Operations)
            {
                SubscriberDetails detail;
                if (operation.IsSync)
                {
                    detail = SubscriberDetails.Create<OrbitalRequest, Payload>(InvokeSyncOperation,
                        string.Format("{0}.{1}", serviceDefinition.ServiceName, operation.Name));
                }
                else
                {
                    detail = SubscriberDetails.Create<OrbitalRequest>(InvokeAsyncOperation,
                        string.Format("{0}.{1}", serviceDefinition.ServiceName, operation.Name));
                }
                subscriberDetails.Add(detail);
            }

            return subscriberDetails;
        }

        /// <inheritdoc />
        public void InvokeAsyncOperation(OrbitalRequest request)
        {
            var serviceName = request.GetValue(AgentConstants.SERVICE_NAME_KEY);
            var operationName = request.GetValue(AgentConstants.OPERATION_NAME_KEY);

            var serviceDefinition = ServiceCachePolicy.Get((context) => serviceDefinitionRepository.GetServiceDefinitionByName(serviceName), serviceName);
            var operation = serviceDefinition.Operations.FirstOrDefault(o => o.Name == operationName);
            if (operation == null)
            {
                logger.Error("OperationsHandler :: InvokeAsyncOperation: Operation {0} does not exist in ServiceDefinition {1}",
                    operationName, serviceName);
                return;
            }

            asyncMessageProcessor.Push(new MessageProcessorInput(operation, request.Body));
        }

        /// <inheritdoc />
        public Payload InvokeSyncOperation(OrbitalRequest request)
        {
            var serviceName = request.GetValue(AgentConstants.SERVICE_NAME_KEY);
            var operationName = request.GetValue(AgentConstants.OPERATION_NAME_KEY);

            var serviceDefinition = ServiceCachePolicy.Get((context) => serviceDefinitionRepository.GetServiceDefinitionByName(serviceName), serviceName);
            var operation = serviceDefinition.Operations.FirstOrDefault(o => o.Name == operationName);
            if (operation == null)
            {
                var error = string.Format("Operation {0} does not exist in ServiceDefinition {1}", operationName, serviceName);
                logger.Error("OperationsHandler :: InvokeSyncOperation: {0}", error);

                return Payload.Create(new List<Fault>() {
                    faultFactory.CreateFault(new OrbitalOperationHandlerException(error))
                });
            }

            return syncMessageProcessor.Push(new MessageProcessorInput(operation, request.Body));
        }
    }
}
