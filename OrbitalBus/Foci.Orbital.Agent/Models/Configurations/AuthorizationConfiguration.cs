﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Configurations
{
    /// <summary>
    /// Model to store authorization configurations
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class AuthorizationConfiguration
    {
        /// <summary>
        /// String representation of the certificate
        /// </summary>
        public string PlainText { get; set; }
    }
}
