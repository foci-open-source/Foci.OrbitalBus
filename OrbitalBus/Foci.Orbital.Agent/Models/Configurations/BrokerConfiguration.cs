﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Configurations
{
    /// <summary>
    /// RabbitMQ configuration class. Stores properties necessary for RabbitMQ configuration.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class BrokerConfiguration
    {
        /// <summary>
        /// String representation of RabbitMQ's IP address.
        /// </summary>
        public string BusHostIp { get; set; } = "localhost";

        /// <summary>
        /// Int representation of RabbitMQ's port.
        /// </summary>
        public int BusHostPort { get; set; } = 5672;

        /// <summary>
        /// String representation of RabbitMQ username
        /// </summary>
        public string Username { get; set; } = "guest";

        /// <summary>
        /// String representation of RabbitMQ Password
        /// </summary>
        public string Password { get; set; } = "guest";

        /// <summary>
        /// Flag to enable secure communication with RabbitMQ.
        /// </summary>
        public bool SslEnabled { get; set; } = false;
    }
}
