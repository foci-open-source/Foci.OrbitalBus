﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Configurations
{
    /// <summary>
    /// Orbital Configuration for Agent.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalConfiguration
    {
        [JsonProperty("ServiceStaticParameters")]
        private Dictionary<string, string> _serviceStaticParameters;

        /// <summary>
        /// Representation of broker configuration.
        /// </summary>
        public BrokerConfiguration BrokerConfiguration { get; set; } = new BrokerConfiguration();

        /// <summary>
        /// Representation of Consul configuration.
        /// </summary>
        public ConsulConfiguration ConsulConfiguration { get; set; } = new ConsulConfiguration();

        /// <summary>
        /// Representation of public configuration
        /// </summary>
        public PublicConfiguration PublicConfiguration { get; set; } = new PublicConfiguration();

        /// <summary>
        /// Representation of the service static parameters.
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, string> ServiceStaticParameters
        {
            get
            {
                if (_serviceStaticParameters == null)
                {
                    _serviceStaticParameters = new Dictionary<string, string>() { { "ServiceIp", "localhost" }, { "ServicePort", "3580" } };
                }
                return _serviceStaticParameters;
            }
            set => _serviceStaticParameters = value;
        }

        /// <summary>
        /// List of registered service names
        /// </summary>
        [JsonProperty("RegisteredServiceNames")]
        public IEnumerable<string> RegisteredServiceNames { get; set; } = new List<string>();
    }
}
