﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Configurations
{

    /// <summary>
    /// Consul configuration class. Stores properties necessary for consul configuration.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulConfiguration
    {
        /// <summary>
        /// String representation of Consul's IP address.
        /// </summary>
        public string HostIp { get; set; } = "localhost";

        /// <summary>
        /// String representation of Consul's Port.
        /// </summary>
        public int HostPort { get; set; } = 8500;

        /// <summary>
        /// Authorization object that contains certificates
        /// </summary>
        public AuthorizationConfiguration Authorization { get; set; }

        /// <summary>
        /// Flag to avoid mismatch with the CN in a certificate. Do not enable in production.
        /// </summary>
        public bool SslPolicyCNError { get; set; } = false;

        /// <summary>
        /// Flag to enable secure communication with Consul.
        /// </summary>
        public bool SslEnabledConsul { get; set; } = false;
    }
}
