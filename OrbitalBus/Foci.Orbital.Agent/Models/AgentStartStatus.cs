﻿using Foci.Orbital.Agent.Models.Consul;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models
{
    [ExcludeFromCodeCoverage]
    internal class AgentStartStatus
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public AgentStartStatus() : this(new ServiceRegistrationResults(), new List<ConsulRegistrationResult>())
        {

        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AgentStartStatus(ServiceRegistrationResults pluginRegistrationResults,
                                   IEnumerable<ConsulRegistrationResult> consulRegistrationResults)
        {

            this.PluginRegistrationResults = pluginRegistrationResults;
            this.ConsulRegistrationResults = consulRegistrationResults;
        }

        public ServiceRegistrationResults PluginRegistrationResults { get; set; }
        public IEnumerable<ConsulRegistrationResult> ConsulRegistrationResults { get; set; }
    }
}
