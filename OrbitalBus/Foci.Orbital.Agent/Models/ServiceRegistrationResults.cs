﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models
{
    /// <summary>
    /// Stores service registration results
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class ServiceRegistrationResults
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ServiceRegistrationResults() : this(new List<RegisteredServiceDetails>())
        {

        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="registeredServiceDetails">List of registered service details</param>
        public ServiceRegistrationResults(IEnumerable<RegisteredServiceDetails> registeredServiceDetails)
        {
            this.RegisteredServices = registeredServiceDetails;
        }

        public IEnumerable<RegisteredServiceDetails> RegisteredServices { get; private set; }
    }

    /// <summary>
    /// Stores registered service details
    /// </summary>
    public class RegisteredServiceDetails
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public RegisteredServiceDetails() : this(string.Empty, new List<SubscriberDetails>(), string.Empty)
        {

        }

        /// <summary>
        /// Constructor that initialize registered service details
        /// </summary>
        /// <param name="serviceId">Service ID</param>
        /// <param name="registeredServices">List of subscriber details</param>
        /// <param name="serviceContract">JSON string representation of service description</param>
        internal RegisteredServiceDetails(string serviceName, IEnumerable<SubscriberDetails> registeredServices, string serviceContract)
        {
            this.ServiceName = serviceName;
            this.RegisteredServices = registeredServices;
            this.ServiceContract = serviceContract;
        }

        internal string ServiceName { get; private set; }
        internal IEnumerable<SubscriberDetails> RegisteredServices { get; private set; }
        internal string ServiceContract { get; private set; }
    }
}
