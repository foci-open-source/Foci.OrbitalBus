﻿using Foci.Orbital.Agent.Models.Configurations;
using NLog;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Foci.Orbital.Agent.Models
{
    [ExcludeFromCodeCoverage]
    public class BusConnectionDetails
    {
        /// <summary>
        /// These classes are used only inside of the BusConnectionDetails.
        /// </summary>
        #region Protected Classes
        protected abstract class ConnectionDetail
        {
            /// <summary>
            /// Gets the Property in the required string format i.e. host=localhost
            /// </summary>
            /// <returns>string</returns>
            public abstract string GetPropertyString();

            /// <summary>
            /// Gets the value stored in the object as an object instead of Typed.
            /// </summary>
            /// <returns>string</returns>
            public abstract object GetValue();
        }

        protected class ConnectionDetail<T> : ConnectionDetail
        {
            /// <summary>
            /// The format string for the property.
            /// This will always be a ConnectionDetail constant
            /// </summary>
            private readonly string formatString;

            /// <summary>
            /// The default value of the property.
            /// This will always be a ConnectionDetail constant
            /// </summary>
            private readonly T defaultValue;

            /// <summary>
            /// The value set by the user
            /// </summary>
            private readonly T setValue;

            /// <summary>
            /// Constructor for a generic connection detail.
            /// </summary>
            /// <param name="formatString">The format string for the property.</param>
            /// <param name="defaultValue">The default value of the property.</param>
            /// <param name="setValue">The value set by the user.</param>
            public ConnectionDetail(string formatString, T defaultValue, T setValue)
            {
                this.formatString = formatString;
                this.defaultValue = defaultValue;
                this.setValue = setValue;
            }

            /// <summary>
            /// Grabs the appropriate value, either user specific or default and applied the format string
            /// </summary>
            /// <returns>string</returns>
            public override string GetPropertyString()
            {
                var value = defaultValue.Equals(setValue) ? this.defaultValue : this.setValue;

                return string.Format(this.formatString, value);
            }

            /// <summary>
            /// Gets the value, either the user specific of default.
            /// </summary>
            /// <returns></returns>
            public override object GetValue()
            {
                return defaultValue.Equals(setValue) ? this.defaultValue : setValue;
            }

            /// <summary>
            /// The Connection detail can be identified by it's format string.
            /// There should only ever be one value per format string.
            /// </summary>
            /// <param name="obj">the companion object</param>
            /// <returns>bool</returns>
            public override bool Equals(object obj)
            {
                return this.formatString.Equals(obj);
            }

            public override int GetHashCode()
            {
                return this.formatString.GetHashCode();
            }
        }
        #endregion

        #region Constants
        // Format Strings
        private const string HostPropertyName = "host={0}";
        private const string PortPropertyName = "port={0}";
        private const string VirtualHostPropertyName = "virtualHost={0}";
        private const string UsernamePropertyName = "username={0}";
        private const string PasswordPropertyName = "password={0}";
        private const string RequestedHeartbeatPropertyName = "requestedHeartbeat={0}";
        private const string PrefetchcountHeartbeatPropertyName = "prefetchcount={0}";
        private const string PublisherConfirmsPropertyName = "publisherConfirms={0}";
        private const string PersistentMessagesPropertyName = "persistentMessages={0}";
        private const string ProductPropertyName = "product={0}";
        private const string PlatformPropertyName = "platform={0}";
        private const string TimeoutPropertyName = "timeout={0}";
        private const string CertificatePropertyName = "certificate={0}";
        private const string SslEnabledPropertyName = "sslEnabled={0}";

        // Default Values
        private const string HostDefaultValue = "localhost";
        private const string PortDefaultValue = "5672";
        private const string VirtualHostDefaultValue = "/";
        private const string UsernameDefaultValue = "guest";
        private const string PasswordDefaultValue = "guest";
        private const int RequestedHeartbeatDefaultValue = 10;
        private const int PrefetchCountDefaultValue = 50;
        private const bool PublisherConfirmsDefaultValue = false;
        private const bool PersistentMessagesDefaultValue = false;
        private const string ProductDefaultValue = "";
        private const string PlatformDefaultValue = "";
        private const int TimeoutDefaultValue = 10;
        private const bool SslEnabledDefaultValue = false;

        #endregion

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// List to store all the connection details that will be used to build the ConnectionString
        /// </summary>
        private readonly IList<ConnectionDetail> properties;

        /// <summary>
        /// Constructor with only the host name as input.
        /// </summary>
        /// <param name="host">The host value set by the user</param>
        #region Constructors
        public BusConnectionDetails(string host)
        {
            this.properties = new List<ConnectionDetail>
            {
                new ConnectionDetail<string>(HostPropertyName, HostDefaultValue, host)
            };
        }

        /// <summary>
        /// Constructor that takes a configuration service to create bus details for the Agent.
        /// </summary>
        /// <param name="hostIp">The IP address for the bus host.</param>
        /// <param name="portNumber">The port number for the bus host.</param>
        /// <param name="configService">The configuration service to pull properties from.</param>
        public BusConnectionDetails(string hostIp, string portNumber, string username, string password)
        {
            this.properties = new List<ConnectionDetail>
            {
                //The host and port depend if this is for the Agent.
                new ConnectionDetail<string>(HostPropertyName, HostDefaultValue, hostIp),
                new ConnectionDetail<string>(PortPropertyName, PortDefaultValue, portNumber),
                new ConnectionDetail<string>(UsernamePropertyName, UsernameDefaultValue, username),
                new ConnectionDetail<string>(PasswordPropertyName, PasswordDefaultValue, password)
            };
        }

        public BusConnectionDetails(BrokerConfiguration configuration)
        {
            this.properties = new List<ConnectionDetail>
            {
                //The host and port depend if this is for the Agent.
                new ConnectionDetail<string>(HostPropertyName, HostDefaultValue, configuration.BusHostIp),
                new ConnectionDetail<string>(PortPropertyName, PortDefaultValue, configuration.BusHostPort.ToString()),
                new ConnectionDetail<bool>(SslEnabledPropertyName,SslEnabledDefaultValue,configuration.SslEnabled),
                new ConnectionDetail<string>(UsernamePropertyName, UsernameDefaultValue, configuration.Username),
                new ConnectionDetail<string>(PasswordPropertyName, PasswordDefaultValue, configuration.Password)
            };
        }
        #endregion

        #region Public Property Accessors
        /// <summary>
        /// The host i.e. localhost
        /// </summary>
        public string Host => this.GetValueOrDefaultFromConnectionDetail(HostPropertyName, HostDefaultValue);

        /// <summary>
        /// The port.
        /// </summary>
        public string Port => this.GetValueOrDefaultFromConnectionDetail(PortPropertyName, PortDefaultValue);

        /// <summary>
        /// The username i.e. guest
        /// </summary>
        public string Username => this.GetValueOrDefaultFromConnectionDetail(UsernamePropertyName, UsernameDefaultValue);
        /// <summary>
        /// The password i.e. guest
        /// </summary>
        public string Password => this.GetValueOrDefaultFromConnectionDetail(PasswordPropertyName, PasswordDefaultValue);
        /// <summary>
        /// The security status i.e. true
        /// </summary>
        public bool SslEnabled => this.GetValueOrDefaultFromConnectionDetail(SslEnabledPropertyName, SslEnabledDefaultValue);

        /// <summary>
        /// Gets an EasyNetQ connection string.
        /// </summary>
        /// <returns>Formatted EasyNetQ connection string.</returns>
        public string GetConnectionString()
        {
            var propertyArray = this.properties.Select(p => p.GetPropertyString()).ToArray();
            return string.Join(";", propertyArray);
        }
        #endregion

        /// <summary>
        /// Gets the right value for the given format string.
        /// </summary>
        /// <typeparam name="T"> The return type</typeparam>
        /// <param name="formatString"> the format string to get a value for</param>
        /// <param name="defaultValue"> the default value for the connection string.</param>
        /// <returns>the default or saved property.</returns>
        private T GetValueOrDefaultFromConnectionDetail<T>(string formatString, T defaultValue)
        {
            var property = this.properties.FirstOrDefault(x => x.Equals(formatString));
            return property == null ? defaultValue : (T)property.GetValue();
        }

        /// <summary>
        /// Override to formatted string representing all the connection detail properties.
        /// </summary>
        /// <returns>Formatted string representing all the connection detail properties.</returns>
        public override string ToString()
        {
            var connectionDetails = new StringBuilder();
            foreach (var detail in properties)
            {
                connectionDetails.AppendFormat("{0}; ", detail.GetPropertyString());
            }
            return connectionDetails.ToString();
        }
    }
}
