﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models
{
    [ExcludeFromCodeCoverage]
    public class AgentConstants
    {
        public const string CONFIG_PATH = "Configs";
        public const string OPERATION_NAME_KEY = "operationName";
        public const string SERVICE_NAME_KEY = "serviceName";
        public const string MEDIA_TYPE_TEXT_PLAIN = "text/plain";
        public const string MEDIA_TYPE_APP_JSON = "application/json";
        public const string CONFIG_SCHEMA = "{'type':'object','properties':{'ServiceStaticParameters':{'type':'object'},'BrokerConfiguration':{'type':'object','required':['BusHostIp','BusHostPort','Username','Password','SslEnabled'],'properties':{'BusHostIp':{'type':'string'},'BusHostPort':{'type':'integer'},'Username':{'type':'string'},'Password':{'type':'string'},'SslEnabled':{'type':'string'}}},'ConsulConfiguration':{'type':'object','required':['HostIp','HostPort','SslPolicyCNerror','SslEnabledConsul'],'properties':{'HostIp':{'type':'string'},'HostPort':{'type':'integer'},'SslPolicyCNerror':{'type':'string'},'SslEnabledConsul':{'type':'string'},'Authorization':{'type':'object','required':['PlainText'],'properties':{'PlainText':{'type':'string'}}}}},'PublicConfiguration':{'type':'object','required':['HttpIp','HttpPort','BrokerIp','BrokerPort'],'properties':{'HttpIp':{'type':'string'},'HttpPort':{'type':'integer'},'BrokerIp':{'type':'string'},'BrokerPort':{'type':'integer'}}},'RegisteredServiceNames':{'type':'array','items':{'type':'string'}}},'required':['BrokerConfiguration','ConsulConfiguration','PublicConfiguration']}";
    }
}
