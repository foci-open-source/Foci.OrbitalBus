﻿using System.Collections.Generic;

namespace Foci.Orbital.Agent.Models.Consul
{
    /// <summary>
    /// Class containing constants for consul REST requests.
    /// </summary>
    public static class ConsulConstants
    {
        public static ConsulCallInfo GetServiceInfo = new ConsulCallInfo
        {
            Endpoint = "v1/catalog/service/{ServiceName}",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetServiceInfoWithDataCenter = new ConsulCallInfo
        {
            Endpoint = "v1/catalog/service/{ServiceName}?dc={DataCenter}",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetLocalServices = new ConsulCallInfo
        {
            Endpoint = "v1/agent/services",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo RegisterService = new ConsulCallInfo
        {
            Endpoint = "v1/agent/service/register",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
        public static ConsulCallInfo DeregisterService = new ConsulCallInfo
        {
            Endpoint = "v1/agent/service/deregister/{ServiceName}",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
        public static ConsulCallInfo GetLocalChecks = new ConsulCallInfo
        {
            Endpoint = "v1/agent/checks",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetWanMembers = new ConsulCallInfo
        {
            Endpoint = "v1/agent/members?wan=1",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetLanMembers = new ConsulCallInfo
        {
            Endpoint = "v1/agent/members",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo ShowSelf = new ConsulCallInfo
        {
            Endpoint = "v1/agent/self",
            Headers = new Dictionary<string, string> { { "Accept", "application/json" } }
        };
        public static ConsulCallInfo GetKeyValue = new ConsulCallInfo
        {
            Endpoint = "v1/kv/{key}?recurse",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
        public static ConsulCallInfo SetKeyValue = new ConsulCallInfo
        {
            Endpoint = "v1/kv/{key}",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
        public static ConsulCallInfo DeleteKeyValue = new ConsulCallInfo
        {
            Endpoint = "v1/kv/{key}",
            Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
        };
    }

    /// <summary>
    /// Class containing the endpoint to hit and headers necessary for the requests.
    /// </summary>
    public class ConsulCallInfo
    {
        public string Endpoint { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}
