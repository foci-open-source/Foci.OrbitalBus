﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Foci.Orbital.Agent.Models.Consul.Responses
{
    /// <summary>
    /// Model containing the properties from consul's response for service information request.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class MemberResponse
    {
        /// <summary>
        /// String representation of the members's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// String representation of the member's IP address.
        /// </summary>
        [JsonProperty(PropertyName = "Addr")]
        public string Address { get; set; }

        /// <summary>
        /// String representation of the member's port.
        /// </summary>
        public ushort Port { get; set; }

        /// <summary>
        /// Dictionary collection containing member's tags.
        /// </summary>
        public Dictionary<string, string> Tags { get; set; }

        /// <summary>
        /// Integer representation of the member's status
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Integer representation of the member's minimum protocol.
        /// </summary>
        public int ProtocolMin { get; set; }

        /// <summary>
        /// Integer representation of the member's maximum protocol.
        /// </summary>
        public int ProtocolMax { get; set; }

        /// <summary>
        /// Integer representation of the member's cur protocol.
        /// </summary>
        public int ProtocolCur { get; set; }

        /// <summary>
        /// Integer representation of the member's minimum delegate.
        /// </summary>
        public int DelegateMin { get; set; }

        /// <summary>
        /// Integer representation of the member's maximum delegate.
        /// </summary>
        public int DelegateMax { get; set; }

        /// <summary>
        /// Integer representation of the member's cur delegate.
        /// </summary>
        public int DelegateCur { get; set; }

        /// <summary>
        /// Override return formatted string with the member's information.
        /// </summary>
        /// <returns>Formatted string with the member's information.</returns>
        public override string ToString()
        {
            var response = new StringBuilder();
            response.AppendFormat("Name:{0}, Address:{1}, Port:{2}, Status:{3}, ProtocolMin:{4}, ProtocolMax:{5}, ProtocolCur:{6}, DelegateMin:{7}, DelegateMax:{8}, DelegateCur:{9}, Tags(key:value):",
                    Name, Address, Port, Status, ProtocolMin, ProtocolMax, ProtocolCur, DelegateMin, DelegateMax, DelegateCur);
            foreach (var keyvalue in Tags)
            {
                response.AppendFormat("{0}:{1};", keyvalue.Key, keyvalue.Value);
            }
            return response.ToString();
        }

    }
}
