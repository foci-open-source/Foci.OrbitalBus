﻿using Foci.Orbital.Agent.Models.Consul.Common;

namespace Foci.Orbital.Agent.Models.Consul.Responses
{
    /// <summary>
    /// Model for Consul's check response.
    /// </summary>
    public class CheckResponse
    {
        /// <summary>
        /// String representation of the node's name.
        /// </summary>
        public string Node { get; set; }

        /// <summary>
        /// String representation of the check's ID.
        /// </summary>
        public string CheckId { get; set; }

        /// <summary>
        /// String representation of the check's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// CheckStatus representation of the check's status.
        /// </summary>
        public CheckStatus Status { get; set; }

        /// <summary>
        /// String representation of the check's notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// String representation of the check's output.
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// String representation of the service's ID.
        /// </summary>
        public string ServiceID { get; set; }

        /// <summary>
        /// String representation of the service's Name.
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Override returns a formatted string with the service's information and it's check information.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Node:{0}, CheckId:{1}, Name:{2}, Status:{3}, Notes:{4}, Output:{5}, ServiceID:{6}, ServiceName:{7}",
                    Node, CheckId, Name, Status, Notes, Output, ServiceID, ServiceName);
        }
    }
}
