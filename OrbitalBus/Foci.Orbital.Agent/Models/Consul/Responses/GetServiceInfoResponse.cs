﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Foci.Orbital.Agent.Models.Consul.Responses
{
    /// <summary>
    /// Model containing the properties from consul's response for service information request.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class GetServiceInfoResponse
    {
        /// <summary>
        /// String representation of the node's name.
        /// </summary>
        [JsonProperty(PropertyName = "Node")]
        public string NodeName { get; set; }

        /// <summary>
        /// String representation of the node's IP address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// String representation of the service's ID.
        /// </summary>
        [JsonProperty(PropertyName = "ServiceID")]
        public string Id { get; set; }

        /// <summary>
        /// String representation of the service's name.
        /// </summary>
        [JsonProperty(PropertyName = "ServiceName")]
        public string Name { get; set; }

        /// <summary>
        /// Collection of string representations of the service's tags.
        /// </summary>
        [JsonProperty(PropertyName = "ServiceTags")]
        public List<string> Tags { get; set; }

        /// <summary>
        /// String representation of the service's IP address.
        /// </summary>
        public string ServiceAddress { get; set; }

        /// <summary>
        /// Integer representation of the service's IP address.
        /// </summary>
        [JsonProperty(PropertyName = "ServicePort")]
        public int Port { get; set; }

        /// <summary>
        /// Override return formatted string with the service's information.
        /// </summary>
        /// <returns>Formatted string with the service's information.</returns>
        public override string ToString()
        {
            var response = new StringBuilder();
            response.AppendFormat("NodeName:{0}, Address:{1}, Id:{2}, Name:{3}, ServiceAddress:{4}, Port:{5}, Tags:",
                    NodeName, Address, Id, Name, ServiceAddress, Port);
            if (Tags != null && Tags.Count > 0)
            {
                Tags.ForEach(tag => response.AppendFormat("{0};", tag));
            }
            return response.ToString();
        }
    }
}
