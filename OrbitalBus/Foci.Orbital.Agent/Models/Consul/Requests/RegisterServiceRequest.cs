﻿using Foci.Orbital.Agent.Models.Consul.Common;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Foci.Orbital.Agent.Models.Consul.Requests
{
    /// <summary>
    /// Model for Services to be registered in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RegisterServiceRequest : ServiceDetails
    {
        /// <inheritdoc />
        public RegisterServiceRequest(string id, string name, string address, ushort port)
            : base(id, name, address, port)
        {
            Checks = new List<RegisterCheckRequest>();
        }

        public List<RegisterCheckRequest> Checks { get; set; }

        /// <summary>
        /// Override to return a formatted string representation of the register request checks.
        /// </summary>
        /// <returns>Formatted string representation of the register request checks.</returns>
        public override string ToString()
        {
            var request = new StringBuilder();
            request.AppendFormat("{0}, Checks:", base.ToString());
            Checks.ForEach(check => request.AppendFormat("{0};", check));
            return request.ToString();
        }
    }
}
