﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Consul.Requests
{
    /// <summary>
    /// Abstract class containing the name and ID related to the service in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public abstract class RegisterCheckRequest
    {
        /// <summary>
        /// String representation of the service ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// String representation of the service name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Override to return a formatted string containing the service ID and name.
        /// </summary>
        /// <returns>Formatted string containing the service ID and name.</returns>
        public override string ToString()
        {
            return string.Format("Id: {0}, Name: {1}", Id, Name);
        }
    }

    /// <summary>
    /// Class containing the TTL related to the service in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RegisterTtlCheckRequest : RegisterCheckRequest
    {
        /// <summary>
        /// String representation of the service's TTL.
        /// </summary>
        [JsonProperty("TTL")]
        public string Ttl { get; set; }

        /// <summary>
        /// Override to return a formatted string containing the service TTL.
        /// </summary>
        /// <returns>Formatted string containing the service TTL.</returns>
        public override string ToString()
        {
            return string.Format("{0}, Ttl: {1}", base.ToString(), Ttl);
        }
    }

    /// <summary>
    /// Class containing the Interval related to the service in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public abstract class RegisterIntervalCheckRequest : RegisterCheckRequest
    {
        /// <summary>
        /// String representation of the service's Interval. 
        /// </summary>
        public string Interval { get; set; }

        /// <summary>
        /// Override to return a formatted string containing the service Interval.
        /// </summary>
        /// <returns>Formatted string containing the service Interval.</returns>
        public override string ToString()
        {
            return string.Format("{0}, Interval: {1}", base.ToString(), Interval);
        }
    }

    /// <summary>
    /// Class containing the HTTPS related to the service in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RegisterHttpCheckRequest : RegisterIntervalCheckRequest
    {
        /// <summary>
        /// String representation of the service HTTPS GET request. 
        /// </summary>
        [JsonProperty("HTTPS")]
        public string Https { get; set; }

        /// <summary>
        /// Override to return a formatted string containing the service HTTPS GET request.
        /// </summary>
        /// <returns>Formatted string containing the service HTTPS GET request.</returns>
        public override string ToString()
        {
            return string.Format("{0}, Https: {1}", base.ToString(), Https);
        }
    }

    /// <summary>
    /// Class containing the script related to the service in Consul.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RegisterScriptCheckRequest : RegisterIntervalCheckRequest
    {
        /// <summary>
        /// String representation of the service script. 
        /// </summary>
        public string Script { get; set; }

        /// <summary>
        /// Override to return a formatted string containing the service script.
        /// </summary>
        /// <returns>Formatted string containing the service script.</returns>
        public override string ToString()
        {
            return string.Format("{0}, Script: {1}", base.ToString(), Script);
        }
    }
}
