﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Consul
{

    /// <summary>
    /// This class is used to show the result of registering a service with consul
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulRegistrationResult
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ConsulRegistrationResult()
        {

        }

        /// <summary>
        /// Constructor that initialize service ID and registration status
        /// </summary>
        /// <param name="serviceId">Service ID</param>
        /// <param name="registrationSuccessful">OperationsRegistration status</param>
        public ConsulRegistrationResult(string serviceId, bool registrationSuccessful)
        {
            this.ServiceId = serviceId;
            this.RegistrationSuccessful = registrationSuccessful;
        }

        public string ServiceId { get; private set; }
        public bool RegistrationSuccessful { get; private set; }
    }
}
