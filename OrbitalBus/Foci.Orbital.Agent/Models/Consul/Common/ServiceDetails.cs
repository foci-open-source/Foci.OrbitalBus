﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;

namespace Foci.Orbital.Agent.Models.Consul.Common
{
    /// <summary>
    /// A class to represent the parameters of a Service.
    /// </summary>
    public class ServiceDetails
    {
        [JsonConstructor]
        private ServiceDetails()
        {
            Tags = new List<string>();
        }


        /// <summary>
        /// The default constructor required to set the properties that are necessary for
        /// a unique service.
        /// </summary>
        /// <param name="id">String representation of the service ID</param>
        /// <param name="name">String representation of the service name.</param>
        /// <param name="address">String representation of the service IP address.</param>
        /// <param name="port">Ushort representation of the service port.</param>
        public ServiceDetails(string id, string name, string address, ushort port)
            : this()
        {
            ID = id;
            Name = name;
            Address = address;
            Port = port;
        }

        //Required Fields
        /// <summary>
        /// String representation of the service name.
        /// </summary>
        [JsonProperty(PropertyName = "Service")]
        public string Name { get; set; }

        /// <summary>
        /// String representation of the service IP address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Ushort representation of the service port.
        /// </summary>
        public ushort Port { get; set; }

        /// <summary>
        /// An optional field.
        /// If not specified, will be the same as Name. Has to be specified and unique if duplicate names are used.
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// A list of string based tags associated with a service. 
        /// </summary>
        public List<string> Tags { get; set; }

        /// <summary>
        /// Override of ToString(). Prints the properties of the Service Details.
        /// </summary>
        /// <returns>Formatted string representing the property details of a service.</returns>
        public override string ToString()
        {
            var details = new StringBuilder();
            details.AppendFormat("Name:{0}, Address:{1}, Port:{2}, ID:{3}, Tags:", Name, Address, Port, ID);
            Tags.ForEach(tag => details.AppendFormat("{0};", tag));
            return details.ToString();
        }
    }
}
