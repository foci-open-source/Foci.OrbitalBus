﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Consul.Common
{
    /// <summary>
    /// Enumeration to store possible check status values.
    /// </summary>
    public enum CheckStatusValue
    {
        any = 1,
        unknown,
        passing,
        warning,
        critical
    }

    /// <summary>
    /// A class used to represent a check status value. Check status can only hold one of
    /// the values of CheckStatusValues.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public sealed class CheckStatus
    {
        private readonly string name;
        private readonly CheckStatusValue value;
        private static readonly Dictionary<string, CheckStatus> instance = new Dictionary<string, CheckStatus>();

        public static readonly CheckStatus ANY = new CheckStatus(CheckStatusValue.any, "any");
        public static readonly CheckStatus UNKNOWN = new CheckStatus(CheckStatusValue.unknown, "unknown");
        public static readonly CheckStatus PASSING = new CheckStatus(CheckStatusValue.passing, "passing");
        public static readonly CheckStatus WARNING = new CheckStatus(CheckStatusValue.warning, "warning");
        public static readonly CheckStatus CRITICAL = new CheckStatus(CheckStatusValue.critical, "critical");

        /// <summary>
        /// Constructor taking in a status value and name.
        /// </summary>
        /// <param name="value">Enum representation of a status value.</param>
        /// <param name="name">String representation of the name of the status value.</param>
        private CheckStatus(CheckStatusValue value, string name)
        {
            this.name = name;
            this.value = value;
            instance[name] = this;
        }

        /// <summary>
        /// Override to return the name of the Check status.
        /// </summary>
        /// <returns>string representation of the check status name.</returns>
        public override string ToString()
        {
            return name;
        }

        /// <summary>
        /// Explicit operator for converting a string to a CheckStatus object.
        /// The string must match the name of one of CheckStatusValue enum types.
        /// </summary>
        /// <param name="str">string representation of the check status value enum.</param>
        public static explicit operator CheckStatus(string str)
        {
            CheckStatus result;
            if (instance.TryGetValue(str, out result))
            {
                return result;
            }
            else
            {
                throw new InvalidCastException();
            }
        }

        /// <summary>
        /// Operator to convert a CheckStatus object to its native CheckStatusValue.
        /// </summary>
        /// <param name="status">CheckStatus object.</param>
        public static implicit operator CheckStatusValue(CheckStatus status)
        {
            return status.value;
        }
    }
}
