﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models
{
    /// <summary>
    /// A maybe is a representation of a class that can have a value of None or Some.
    /// Use the methods of the Maybe to unwrap the value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [ExcludeFromCodeCoverage]
    internal struct Maybe<T>
    {
        /// <summary>
        /// Static value to represent None
        /// </summary>
        public static Maybe<T> None = new Maybe<T>(default(T));

        /// <summary>
        /// Backer of the Some value
        /// </summary>
        public T Value { get; set; }

        /// <summary>
        /// Shows if the Maybe is a Some of None
        /// </summary>
        public bool HasValue => this.Value != null && !this.Value.Equals(default(T));

        /// <summary>
        /// Constructor of a Maybe.
        /// </summary>
        /// <param name="value"></param>
        public Maybe(T value)
        {
            Value = value;
        }
    }

    /// <summary>
    /// Extensions to Maybe
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal static class MaybeExtensions
    {
        /// <summary>
        /// Allow any value to become a Maybe value
        /// </summary>
        /// <typeparam name="T">Type parameter for the Some</typeparam>
        /// <param name="input">Value of the Some</param>
        /// <returns>Maybe of Some</returns>
        public static Maybe<T> ToMaybe<T>(this T input)
        {
            return new Maybe<T>(input);
        }

        /// <summary>
        /// Bind function to chain Maybe calls together
        /// </summary>
        /// <typeparam name="TInput">The value of the maybe type</typeparam>
        /// <typeparam name="TResult"> The result of type of the Maybe</typeparam>
        /// <param name="maybe">The extended Maybe</param>
        /// <param name="func">The function to apply to the Maybe. Must return a Maybe</param>
        /// <returns>Maybe of new Some type</returns>
        public static Maybe<TResult> Get<TInput, TResult>(this Maybe<TInput> maybe, Func<TInput, TResult> func)
        {
            return maybe.HasValue ? new Maybe<TResult>(func(maybe.Value)) : Maybe<TResult>.None;
        }

        /// <summary>
        /// Extension to Get to allow for old linq style syntax
        /// </summary>
        /// <typeparam name="TInput">The value of the maybe type</typeparam>
        /// <typeparam name="TResult"> The result of type of the Maybe</typeparam>
        /// <param name="maybe">The extended Maybe</param>
        /// <param name="func">The function to apply to the Maybe. Must return a Maybe</param>
        /// <returns>Maybe of new Some type</returns>
        public static Maybe<TResult> SelectMany<TInput, TResult>(this Maybe<TInput> maybe, Func<TInput, TResult> func)
        {
            return maybe.Get(func);
        }

        /// <summary>
        /// Apply a boolean function to the Some if the maybe is a Some
        /// Otherwise return a None
        /// </summary>
        /// <typeparam name="TInput">Some type of the Maybe</typeparam>
        /// <param name="maybe">The extended Maybe</param>
        /// <param name="func">Function used to check truth to the Some</param>
        /// <returns>Either a None or the extended Maybe</returns>
        public static Maybe<TInput> If<TInput>(this Maybe<TInput> maybe, Func<TInput, bool> func)
        {
            return (maybe.HasValue && func(maybe.Value)) ? maybe : Maybe<TInput>.None;
        }

        /// <summary>
        /// Gets Some from the Maybe unless the Maybe is a None.
        /// If Maybe is a None the default value is returned
        /// </summary>
        /// <typeparam name="TInput">Some type of the Maybe</typeparam>
        /// <typeparam name="TResult">return result type</typeparam>
        /// <param name="maybe">The extended maybe</param>
        /// <param name="func">Function to apply to the Some</param>
        /// <param name="defaultValue">Value to return if the Maybe is None</param>
        /// <returns></returns>
        public static TResult GetValueOrDefault<TInput, TResult>(this Maybe<TInput> maybe, Func<TInput, TResult> func, TResult defaultValue)
        {
            return maybe.HasValue ? func(maybe.Value) : defaultValue;
        }
    }
}
