﻿using EasyNetQ.FluentConfiguration;
using EasyNetQ.Producer;
using System;

namespace Foci.Orbital.Agent.Models
{
    /// <summary>
    /// Abstract class to generate subscriber details.
    /// </summary>
    public abstract class SubscriberDetails
    {
        protected Type RequestType;
        protected Type ResponseType;
        protected Action<object> AsyncMessageHandler;
        protected Func<object, object> SyncronousMessageHandler;
        protected Action<ISubscriptionConfiguration> SubscriptionConfiguration;
        protected Action<IResponderConfiguration> ResponderConfiguration;

        /// <summary>
        /// A FlatMap that is passed a pair of functions. If the calling SubscriberDetails is async we shall call the first functioon. If not
        /// we choose the second function and call it.
        /// </summary>
        /// <typeparam name="T">The type we shall return.</typeparam>
        /// <param name="asyncDetails">A function that takes a subscriber detail as input and returns type T. Called when DetailsIsAsync</param>
        /// <param name="syncDetails">A function that takes a subscriber detail as input and returns type T.</param>
        /// <returns>Type of object.</returns>
        public T FlatMap<T>(
            Func<SubscriberDetails, T> asyncDetails,
            Func<SubscriberDetails, T> syncDetails)
        {
            return IsSubscriberAsyncDetails()
                ? asyncDetails.Invoke(this)
                : syncDetails.Invoke(this);
        }

        /// <summary>
        /// Checks if the SubscriberDetails are Async via reflection.
        /// </summary>
        /// <returns>True if an instance of SubscriberAsyncDetails.</returns>
        public bool IsSubscriberAsyncDetails()
        {
            return this.GetType().GetGenericTypeDefinition() == typeof(SubscriberAsyncDetails<>);
        }

        /// <summary>
        /// We create an async Subscriber Details whom we pass a messageHandler action to perform work for us.
        /// </summary>
        /// <typeparam name="T">The subscriber request type.</typeparam>
        /// <param name="asyncMessageHandler">The method that performs work on type T when received.</param>
        /// <returns>The created async subscriber details.</returns>
        public static SubscriberAsyncDetails<T> Create<T>(Action<T> asyncMessageHandler, string subscriptionConfiguration) where T : class
        {
            return SubscriberAsyncDetails<T>.Create(asyncMessageHandler, subscriptionConfiguration) as SubscriberAsyncDetails<T>;
        }

        /// <summary>
        /// We create an async Subscriber Details whom we pass a messageHandler action to perform work for us.
        /// </summary>
        /// <typeparam name="T">The subscriber request type.</typeparam>
        /// <typeparam name="T1">The subscriber return type.</typeparam>
        /// <param name="syncMessageHandler">The function that does work for us on the request type and returns an object of type T1</param>
        /// <returns>The created sync subscriber details.</returns>
        public static SubscriberSynchronousDetails<T, T1> Create<T, T1>(Func<T, T1> syncMessageHandler, string queueName)
            where T : class
            where T1 : class
        {
            return SubscriberSynchronousDetails<T, T1>.Create(syncMessageHandler, queueName) as SubscriberSynchronousDetails<T, T1>;
        }

        public override string ToString()
        {
            return string.Format("SubscriberDetails-RequestType({0}), ResponseType({1})",
                    RequestType != null ? RequestType.ToString() : "NULL", ResponseType != null ? ResponseType.ToString() : "NULL");
        }
    }

    /// <summary>
    /// Class to generate asynchronus subscriber details.
    /// </summary>
    /// <typeparam name="T">Asynchronous request type</typeparam>
    public class SubscriberAsyncDetails<T> : SubscriberDetails where T : class
    {
        public Action<ISubscriptionConfiguration> GetQueueName => this.SubscriptionConfiguration;

        /// <summary>
        /// Sets our request type and stores our async message handler.
        /// </summary>
        /// <param name="asyncMessageHandler"></param>
        private SubscriberAsyncDetails(Action<T> asyncMessageHandler, string queueName)
        {
            this.RequestType = typeof(T);
            this.AsyncMessageHandler = (o => asyncMessageHandler((T)o));
            this.SubscriptionConfiguration = new Action<ISubscriptionConfiguration>(o => o.WithQueueName(queueName));
        }

        /// <summary>
        /// Returns our request type.
        /// </summary>
        public Type Type => this.RequestType;

        /// <summary>
        /// Returns our asynchronous message handler.
        /// </summary>
        public Action<T> GetMessageHandler => this.AsyncMessageHandler;

        /// <summary>
        /// Calls our private constructor. Allows subscriber details base class to create this type.
        /// </summary>
        /// <param name="asyncMessageHandler">The message handler to store.</param>
        /// <returns>A new async subscriber details</returns>
        public static SubscriberDetails Create(Action<T> asyncMessageHandler, string queueName)
        {
            return new SubscriberAsyncDetails<T>(asyncMessageHandler, queueName);
        }
    }

    /// <summary>
    /// Class to generate synchronus subscriber details.
    /// </summary>
    /// <typeparam name="T">The subscriber request type.</typeparam>
    /// <typeparam name="T1">The subscriber return type.</typeparam>
    public class SubscriberSynchronousDetails<T, T1> : SubscriberDetails where T : class where T1 : class
    {

        public Action<IResponderConfiguration> GetQueueResponse => this.ResponderConfiguration;

        /// <summary>
        /// Assigns our request and return types and stores our synchronous message handler.
        /// </summary>
        /// <param name="syncMessageHandler">Function Handler for synchronous message.</param>
        private SubscriberSynchronousDetails(Func<T, T1> syncMessageHandler, string queueName)
        {
            this.RequestType = typeof(T);
            this.ResponseType = typeof(T1);
            this.SyncronousMessageHandler = o => syncMessageHandler((T)o);
            this.ResponderConfiguration = new Action<IResponderConfiguration>(o => o.WithQueueName(queueName));
        }

        /// <summary>
        /// Returns the request type.
        /// </summary>
        public Type GetRequestType => this.RequestType;

        /// <summary>
        /// Returns the response type.
        /// </summary>
        public Type GetResponseType => this.ResponseType;

        /// <summary>
        /// Gets the synchronous message handler.
        /// </summary>
        public Func<T, T1> GetMessageHandler => o => (T1)this.SyncronousMessageHandler(o);

        /// <summary>
        /// Calls the private constructor of this type. A static method to be called in subscriber details base class.
        /// </summary>
        /// <param name="syncMessageHandler">The message handler to be stored.</param>
        /// <returns>The new Synchronous subscriber details.</returns>
        public static SubscriberDetails Create(Func<T, T1> syncMessageHandler, string queueName)
        {
            return new SubscriberSynchronousDetails<T, T1>(syncMessageHandler, queueName);
        }
    }
}
