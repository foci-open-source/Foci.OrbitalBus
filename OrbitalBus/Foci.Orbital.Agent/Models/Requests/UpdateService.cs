﻿using EasyNetQ;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models
{
    /// <summary>
    /// Request to be received from the Service Update queue
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Queue("UpdateServiceQueue", ExchangeName = "orbital.service.publish")]
    public class UpdateService
    {
        /// <summary>
        /// String representation of the service name to be updated.
        /// </summary>
        [JsonProperty("servicename")]
        public string ServiceName { get; internal set; }
    }
}
