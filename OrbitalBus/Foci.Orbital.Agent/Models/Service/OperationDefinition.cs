﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationDefinition
    {
        /// <summary>
        /// String representation of operation name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }

        /// <summary>
        /// True if this operation is sync, false if this operation is async
        /// </summary>
        [JsonProperty("sync")]
        public bool IsSync { get; internal set; }

        /// <summary>
        /// Request and/or response schema for this operation
        /// </summary>
        [JsonProperty("schemas")]
        public OperationSchemas Schemas { get; internal set; }

        /// <summary>
        /// String representation of the Javascript translation
        /// </summary>
        [JsonProperty("translation")]
        public string Translation { get; internal set; }

        /// <summary>
        /// Object that contains adapter type and adapter configuration
        /// </summary>
        [JsonProperty("adapter")]
        public AdapterConfiguration Adapter { get; internal set; }

        /// <summary>
        /// Object that contains flag to determine if request/response need validation
        /// </summary>
        [JsonProperty("validate")]
        public OperationValidation Validate { get; set; }

        /// <summary>
        /// Object that contains http configuration
        /// </summary>
        [JsonProperty("http")]
        public HttpConfiguration Http { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public OperationDefinition()
        {
            Schemas = new OperationSchemas();
            Adapter = new AdapterConfiguration();
            Validate = new OperationValidation();
            Http = new HttpConfiguration();
        }
    }
}
