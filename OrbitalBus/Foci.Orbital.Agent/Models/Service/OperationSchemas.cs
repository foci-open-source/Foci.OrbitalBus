﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationSchemas
    {
        /// <summary>
        /// String representation of the request schema
        /// </summary>
        [JsonProperty("request")]
        public string Request { get; internal set; }

        /// <summary>
        /// String representation of the response schema
        /// </summary>
        [JsonProperty("response")]
        public string Response { get; internal set; }
    }
}
