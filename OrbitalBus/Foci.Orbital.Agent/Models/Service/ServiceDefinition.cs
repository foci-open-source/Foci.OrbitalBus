﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class ServiceDefinition
    {
        /// <summary>
        /// String representation of service name
        /// </summary>
        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        /// <summary>
        /// List of operations that are available
        /// </summary>
        [JsonProperty("operations")]
        public IEnumerable<OperationDefinition> Operations { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public ServiceDefinition()
        {
            Operations = new List<OperationDefinition>();
        }

    }
}
