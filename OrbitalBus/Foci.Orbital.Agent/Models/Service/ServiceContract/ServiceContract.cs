﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service.ServiceContract
{
    /// <summary>
    /// The service contract model.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ServiceContract
    {
        /// <summary>
        /// String representation of the service's name.
        /// </summary>
        [JsonProperty(PropertyName = "serviceName")]
        public string ServiceName { get; set; }

        /// <summary>
        /// Type of object for the request.
        /// </summary>
        [JsonProperty(PropertyName = "operations")]
        public IList<OperationDescription> Operations { get; set; }
    }
}
