﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service.ServiceContract

{
    [ExcludeFromCodeCoverage]
    public class OperationDescription
    {
        /// <summary>
        /// Flag to indicate if the operation is asynchronous.
        /// </summary>
        [JsonProperty(PropertyName = "isAsync")]
        public bool IsAsync { get; set; }

        /// <summary>
        /// String representation of the operation's name.
        /// </summary>
        [JsonProperty(PropertyName = "operationName")]
        public string Name { get; set; }

        /// <summary>
        /// Type of object for the request.
        /// </summary>
        [JsonProperty(PropertyName = "requestSchema")]
        public string RequestSchema { get; set; }

        /// <summary>
        /// Type of object for the response.
        /// </summary>
        [JsonProperty(PropertyName = "responseSchema")]
        public string ResponseSchema { get; set; }
    }
}
