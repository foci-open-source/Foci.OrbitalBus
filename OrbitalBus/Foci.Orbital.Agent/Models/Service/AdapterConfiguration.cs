﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class AdapterConfiguration
    {
        /// <summary>
        /// String representation of adapter type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; internal set; }

        /// <summary>
        /// String representation of adapter configuration
        /// </summary>
        [JsonProperty("configuration")]
        public string Configuration { get; internal set; }
    }
}
