﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Models.Service
{
    [ExcludeFromCodeCoverage]
    public class OperationValidation
    {
        /// <summary>
        /// True to validate request message, false otherwise
        /// </summary>
        [JsonProperty("request")]
        public bool Request { get; internal set; }

        /// <summary>
        /// True to validate response message, false otherwise
        /// </summary>
        [JsonProperty("response")]
        public bool Response { get; internal set; }
    }
}
