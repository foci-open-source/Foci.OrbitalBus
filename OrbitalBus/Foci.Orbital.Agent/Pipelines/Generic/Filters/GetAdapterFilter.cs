﻿using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using System;
using Unity;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class GetAdapterFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, IAdapterDescriptionPort, IAdapterPort
    {
        private readonly IUnityContainer container;

        /// <summary>
        /// Default constructor to pass in dependency injector container
        /// </summary>
        /// <param name="container">The dependency injector container</param>
        public GetAdapterFilter(IUnityContainer container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container), "The dependency container cannot be null");
        }

        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port))
            {
                return port;
            }

            var type = port.AdapterType;
            if (string.IsNullOrWhiteSpace(type))
            {
                var error = "Adapter type cannot be null or empty";
                logger.Warn("GetAdapterFilter :: Process: {0}", error);
                return (T)port.AppendFault(new OrbitalArgumentException(error));
            }

            //Try retrieve adapter
            if (container.IsRegistered<IAdapter>(type))
            {
                port.Adapter = container.Resolve<IAdapter>(type);
            }
            else
            {
                var error = "Adapter matching the given adapter type is not registered with Orbital Agent";
                logger.Warn("GetAdapterFilter :: Process: {0}", error);
                port = (T)port.AppendFault(new OrbitalArgumentException(error));
            }

            return port;
        }
    }
}
