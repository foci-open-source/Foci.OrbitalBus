﻿using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Services.Interfaces;
using Jint.Runtime;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class GetAdapterParametersFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, ITranslationPort, IOrbitalRequestBodyPort, IAdapterParametersPort
    {
        private readonly IJsonTranslationService translationService;
        private readonly IConfigurationService configurationService;

        /// <summary>
        /// Default constructor to take in needed services
        /// </summary>
        /// <param name="translationService">JSON translation service to execute translation</param>
        /// <param name="configurationService">Configuration service to get configuration</param>
        public GetAdapterParametersFilter(IJsonTranslationService translationService, IConfigurationService configurationService)
        {
            this.translationService = translationService ?? throw new ArgumentNullException(nameof(translationService));
            this.configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
        }


        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port))
            {
                return port;
            }

            var translation = port.Translation;
            var message = port.OrbitalRequestBody;
            var parameters = configurationService.GetConfigurationProperties()?.ServiceStaticParameters ?? new Dictionary<string, string>();
            try
            {
                // If GetParam exist in translation, get the dynamic parameters
                var translationFunction = JsonTranslationFunction.GetParams;
                if (translationService.FunctionExist(translation, translationFunction))
                {
                    var result = translationService.ExecuteTranslationFunction(message, translation, translationFunction) ?? string.Empty;
                    var dynamicParam = JsonConvert.DeserializeObject<Dictionary<string, string>>(result) ?? new Dictionary<string, string>();

                    parameters = parameters.Union(dynamicParam).ToDictionary(key => key.Key, value => value.Value);
                }
            }
            catch (Exception e) when (e is JavaScriptException
            || e is OrbitalTranslationException
            || e is OrbitalUnknownEnumException
            || e is OrbitalArgumentException
            || e is ArgumentNullException
            || e is ArgumentException
            || e is OrbitalBusinessException)
            {
                logger.Warn(e, "GetAdapterParametersFilter :: Process: {0}", e.Message);
                return (T)port.AppendFault(e);
            }

            port.Parameters = parameters;
            return port;
        }
    }
}
