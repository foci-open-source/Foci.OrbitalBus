﻿using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using System.Collections.ObjectModel;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class CreateAdapterRequestFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, IAdapterRequestPort, ITranslatedMessagePort, IAdapterParametersPort, IAdapterDescriptionPort
    {
        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port))
            {
                return port;
            }

            var adapterConfig = port.AdapterConfiguration;
            var message = port.TranslatedMessage;
            var parameters = port.Parameters;
            if (parameters == null)
            {
                var error = "Adapter parameters cannot be null";
                logger.Warn("CreateAdapterRequestFilter :: Process: {0}", error);
                return (T)port.AppendFault(new OrbitalArgumentException(error));
            }

            port.AdapterRequest = new AdapterRequest(adapterConfig, message, new ReadOnlyDictionary<string, string>(parameters));
            return port;
        }
    }
}
