﻿using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Services.Interfaces;
using Jint.Runtime;
using System;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class TranslateOrbitalRequestBodyFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, ITranslationPort, IOrbitalRequestBodyPort, ITranslatedMessagePort
    {
        private readonly IJsonTranslationService translationService;

        /// <summary>
        /// Default constructor to take in needed services
        /// </summary>
        /// <param name="translationService">JSON translation service to execute translation</param>
        public TranslateOrbitalRequestBodyFilter(IJsonTranslationService translationService)
        {
            this.translationService = translationService ?? throw new ArgumentNullException(nameof(translationService));
        }

        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port))
            {
                return port;
            }

            var translation = port.Translation;
            var message = port.OrbitalRequestBody;
            try
            {
                // If Inbound function exist in translation, use it to translate the request body
                var translationFunction = JsonTranslationFunction.Inbound;
                if (this.translationService.FunctionExist(translation, translationFunction))
                {
                    message = this.translationService.ExecuteTranslationFunction(message, translation, translationFunction) ?? string.Empty;
                }
            }
            catch (Exception e) when (e is JavaScriptException
            || e is OrbitalTranslationException
            || e is OrbitalUnknownEnumException
            || e is OrbitalArgumentException
            || e is ArgumentNullException
            || e is ArgumentException
            || e is OrbitalBusinessException)
            {
                this.logger.Warn(e, "TranslateOrbitalRequestBodyFilter :: Process: {0}", e.Message);
                return (T)port.AppendFault(e);
            }

            port.TranslatedMessage = message;
            return port;
        }
    }
}
