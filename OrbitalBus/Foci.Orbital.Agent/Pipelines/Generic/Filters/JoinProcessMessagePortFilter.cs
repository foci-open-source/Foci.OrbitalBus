﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class JoinProcessMessagePortFilter
    {
        protected readonly ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Process data using content provided by given port
        /// </summary>
        /// <param name="ports">ports that needed to be joined</param>
        /// <returns>Port containing processed data</returns>
        public ProcessMessagePort ProcessThree(Tuple<ProcessMessagePort, ProcessMessagePort, ProcessMessagePort> ports)
        {

            var newPort = Process(Tuple.Create(ports.Item1, ports.Item2));
            return Process(Tuple.Create(newPort, ports.Item3));
        }

        /// <summary>
        /// Process data using content provided by given port
        /// </summary>
        /// <param name="ports">ports that needed to be joined</param>
        /// <returns>Port containing processed data</returns>
        public ProcessMessagePort Process(Tuple<ProcessMessagePort, ProcessMessagePort> ports)
        {
            var port1 = ports.Item1;
            var port2 = ports.Item2;
            if (!IsPortValid(port1, out port1) | !IsPortValid(port2, out port2))
            {
                var faults = port1.Faults ?? new List<Fault>();
                faults = faults.Union(port2.Faults ?? new List<Fault>()).ToList();
                port1.Faults = faults;
                return port1;
            }

            port1.Parameters = port1.Parameters ?? new Dictionary<string, string>();
            port2.Parameters = port2.Parameters ?? new Dictionary<string, string>();

            return new ProcessMessagePort
            {
                Translation = port1.Translation ?? port2.Translation,
                OrbitalRequestBody = port1.OrbitalRequestBody ?? port2.OrbitalRequestBody,
                AdapterType = port1.AdapterType ?? port2.AdapterType,
                AdapterConfiguration = port1.AdapterConfiguration ?? port2.AdapterConfiguration,
                Payload = port1.Payload ?? port2.Payload,
                Parameters = port1.Parameters.Union(port2.Parameters).ToDictionary(key => key.Key, value => value.Value),
                TranslatedMessage = port1.TranslatedMessage ?? port2.TranslatedMessage,
                Adapter = port1.Adapter ?? port2.Adapter,
                AdapterRequest = port1.AdapterRequest ?? port2.AdapterRequest,
                ValidateRequest = port1.ValidateRequest || port2.ValidateRequest,
                ValidateResponse = port1.ValidateResponse || port2.ValidateResponse,
                RequestSchema = port1.RequestSchema ?? port2.RequestSchema,
                ResponseSchema = port1.ResponseSchema ?? port2.ResponseSchema
            };
        }

        /// <summary>
        /// If the given port is null, create a new port with appropriate fault, pass the new port to outPort and return false;
        /// If the given port is faulted, pass the provided port to outPort and return false;
        /// Otherwise, pass the provided port to outPort and return true
        /// </summary>
        /// <param name="inPort">The port to check if it is valid</param>
        /// <param name="outPort">Message processor port</param>
        /// <returns>True if port is valid, false otherwise</returns>
        private bool IsPortValid(ProcessMessagePort inPort, out ProcessMessagePort outPort)
        {
            if (inPort == null)
            {
                var error = "Pipeline port cannot be null";
                logger.Warn("JoinProcessMessagePortFilter :: IsPortValid: {0}", error);
                outPort = (ProcessMessagePort)new ProcessMessagePort().AppendFault(new OrbitalArgumentException(error));
                return false;
            }

            outPort = inPort;

            return !inPort.IsFaulted;
        }
    }
}
