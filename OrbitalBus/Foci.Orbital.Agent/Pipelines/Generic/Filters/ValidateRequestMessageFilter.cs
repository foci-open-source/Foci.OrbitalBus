﻿using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Repositories.Extensions;
using System.Linq;

namespace Foci.Orbital.Agent.Pipelines.Generic.Filters
{
    internal class ValidateRequestMessageFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, ISchemaPort, IOrbitalRequestBodyPort
    {
        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port) || !port.ValidateRequest)
            {
                return port;
            }

            var message = port.OrbitalRequestBody;
            if (message == null)
            {
                var error = "Request message body cannot be null";
                logger.Warn("ValidateRequestMessageFilter :: Process: {0}", error);
                return (T)port.AppendFault(new OrbitalArgumentException(error));
            }

            var schema = port.RequestSchema;
            if (schema == null)
            {
                var error = "Request schema cannot be null";
                logger.Warn("ValidateRequestMessageFilter :: Process: {0}", error);
                return (T)port.AppendFault(new OrbitalArgumentException(error));
            }

            var result = schema.ValidateMessage(message);
            if (result.Any())
            {
                logger.Warn("ValidateRequestMessageFilter :: Process: Request message body does not match the provided schema");
                result.ForEach(r => port = (T)port.AppendFault(r));
            }

            return port;
        }
    }
}
