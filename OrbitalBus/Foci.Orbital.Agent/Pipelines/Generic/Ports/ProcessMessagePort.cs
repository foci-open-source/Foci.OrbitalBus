﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using NJsonSchema;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports
{
    /// <summary>
    /// Model class representing a port for message processor pipelines
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class ProcessMessagePort : ITranslationPort, IOrbitalRequestBodyPort, IAdapterDescriptionPort, IPayloadPort,
        IAdapterParametersPort, ITranslatedMessagePort, IAdapterPort, IAdapterRequestPort, IFaultablePort, ISchemaPort
    {
        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        //ITranslationPort
        /// <inheritdoc />
        public string Translation { get; set; }

        //IOrbitalRequestBodyPort
        /// <inheritdoc />
        public string OrbitalRequestBody { get; set; }

        //IAdapterDescriptionPort
        /// <inheritdoc />
        public string AdapterType { get; set; }
        /// <inheritdoc />
        public string AdapterConfiguration { get; set; }

        //IPayloadPort
        /// <inheritdoc />
        public Payload Payload { get; set; }

        //IAdapterParametersPort
        /// <inheritdoc />
        public IDictionary<string, string> Parameters { get; set; }

        //ITranslatedMessagePort
        /// <inheritdoc />
        public string TranslatedMessage { get; set; }

        //IAdapterPort
        /// <inheritdoc />
        public IAdapter Adapter { get; set; }

        //IAdapterRequestPort
        /// <inheritdoc />
        public AdapterRequest AdapterRequest { get; set; }

        //IFaultablePort
        /// <inheritdoc />
        public ICollection<Fault> Faults { get; set; }
        /// <inheritdoc />
        public bool IsFaulted => Faults != null && Faults.Any();

        //ISchemaPort
        /// <inheritdoc />
        public JsonSchema4 RequestSchema { get; set; }
        /// <inheritdoc />
        public JsonSchema4 ResponseSchema { get; set; }
        /// <inheritdoc />
        public bool ValidateRequest { get; set; }
        /// <inheritdoc />
        public bool ValidateResponse { get; set; }

        /// <inheritdoc />
        public IFaultablePort AppendFault(Exception e)
        {
            var fault = faultFactory.CreateFault(e);

            Faults = Faults ?? new List<Fault>();
            Faults.Add(fault);

            return this;
        }
    }
}
