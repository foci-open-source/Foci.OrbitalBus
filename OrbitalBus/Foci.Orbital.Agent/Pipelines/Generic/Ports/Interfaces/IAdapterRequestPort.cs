﻿using Foci.Orbital.Adapters.Contract.Models.Requests;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains adapter request;
    /// 
    /// Note: this is a port that contains intermediate result
    /// </summary>
    internal interface IAdapterRequestPort
    {
        /// <summary>
        /// Object containing information needed for sending request to an adapter
        /// </summary>
        AdapterRequest AdapterRequest { get; set; }
    }
}
