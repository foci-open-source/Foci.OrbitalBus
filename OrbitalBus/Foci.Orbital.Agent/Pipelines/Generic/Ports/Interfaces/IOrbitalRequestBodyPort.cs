﻿namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains a orbital request body
    /// </summary>
    internal interface IOrbitalRequestBodyPort
    {
        /// <summary>
        /// String representation of message body from Orbital Connector
        /// </summary>
        string OrbitalRequestBody { get; set; }
    }
}
