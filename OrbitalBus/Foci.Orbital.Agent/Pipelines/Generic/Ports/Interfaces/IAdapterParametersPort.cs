﻿using System.Collections.Generic;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains adapter parameters
    /// </summary>
    internal interface IAdapterParametersPort
    {
        /// <summary>
        /// Dictionary containing the adapter parameters
        /// </summary>
        IDictionary<string, string> Parameters { get; set; }
    }
}
