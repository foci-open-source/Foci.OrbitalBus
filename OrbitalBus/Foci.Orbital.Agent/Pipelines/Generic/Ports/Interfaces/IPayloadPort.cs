﻿using Foci.Orbital.Adapters.Contract.Models.Payloads;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains a payload
    /// </summary>
    internal interface IPayloadPort
    {
        /// <summary>
        /// Object representation of a payload
        /// </summary>
        Payload Payload { get; set; }
    }
}
