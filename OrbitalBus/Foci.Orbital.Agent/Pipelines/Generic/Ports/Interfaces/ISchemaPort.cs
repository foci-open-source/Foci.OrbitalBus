﻿using NJsonSchema;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains JSON schema
    /// </summary>
    internal interface ISchemaPort
    {
        /// <summary>
        /// True if request message need to be validated, false otherwise
        /// </summary>
        bool ValidateRequest { get; set; }

        /// <summary>
        /// True if response message need to be validated, false otherwise
        /// </summary>
        bool ValidateResponse { get; set; }

        /// <summary>
        /// String representation of the request schema
        /// </summary>
        JsonSchema4 RequestSchema { get; set; }

        /// <summary>
        /// String representation of the response schema
        /// </summary>
        JsonSchema4 ResponseSchema { get; set; }
    }
}
