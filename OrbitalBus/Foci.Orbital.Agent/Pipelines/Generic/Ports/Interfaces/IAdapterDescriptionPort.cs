﻿namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains adapter type and adapter configuration
    /// </summary>
    internal interface IAdapterDescriptionPort
    {
        /// <summary>
        /// String representation of adapter type
        /// </summary>
        string AdapterType { get; set; }

        /// <summary>
        /// String representation of adapter configuration
        /// </summary>
        string AdapterConfiguration { get; set; }
    }
}
