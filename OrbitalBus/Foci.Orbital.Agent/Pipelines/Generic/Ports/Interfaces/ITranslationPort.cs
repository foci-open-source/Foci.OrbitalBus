﻿namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains a translation
    /// </summary>
    internal interface ITranslationPort
    {
        /// <summary>
        /// String representation of JavaScript translation
        /// </summary>
        string Translation { get; set; }
    }
}
