﻿namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains translated message
    /// </summary>
    internal interface ITranslatedMessagePort
    {
        /// <summary>
        /// String representation of a translated message, using JavaScript translation
        /// </summary>
        string TranslatedMessage { get; set; }
    }
}
