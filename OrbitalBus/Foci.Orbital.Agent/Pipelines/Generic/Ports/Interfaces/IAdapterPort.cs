﻿using Foci.Orbital.Adapters.Contract.Models.Interfaces;

namespace Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces
{
    /// <summary>
    /// Interface representation of a port that contains IAdapter
    /// </summary>
    internal interface IAdapterPort
    {
        /// <summary>
        /// Object representation of an adapter
        /// </summary>
        IAdapter Adapter { get; set; }
    }
}
