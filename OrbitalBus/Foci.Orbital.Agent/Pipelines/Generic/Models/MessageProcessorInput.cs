﻿using Foci.Orbital.Agent.Models.Service;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Pipelines.Generic.Models
{
    [ExcludeFromCodeCoverage]
    internal class MessageProcessorInput
    {
        /// <summary>
        /// Object containing operation definition
        /// </summary>
        public OperationDefinition OperationDefinition { get; }

        /// <summary>
        /// String representation of message body from Orbital Connector
        /// </summary>
        public string OrbitalRequestBody { get; }

        /// <summary>
        /// Constructor to take in necessary data
        /// </summary>
        /// <param name="operationDefinition">Operation definition containing translation, adapter type, and adapter config</param>
        /// <param name="orbitalRequestBody">String representation of message body from Orbital Connector</param>
        public MessageProcessorInput(OperationDefinition operationDefinition, string orbitalRequestBody)
        {
            OperationDefinition = operationDefinition;
            OrbitalRequestBody = orbitalRequestBody;
        }
    }
}
