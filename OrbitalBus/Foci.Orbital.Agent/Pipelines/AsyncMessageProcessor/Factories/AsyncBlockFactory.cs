﻿using Foci.Orbital.Agent.Pipelines.Generic.Envelopes.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Factories.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks.Dataflow;

namespace Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Factories
{
    [ExcludeFromCodeCoverage]
    internal class AsyncBlockFactory : BaseBlockFactory<ProcessMessagePort>
    {
        /// <inheritdoc />
        internal override TransformBlock<Tuple<IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>>, IEnvelope<ProcessMessagePort>> CreateJoinTransformBlock(Func<Tuple<ProcessMessagePort, ProcessMessagePort>, ProcessMessagePort> func)
        {
            return new TransformBlock<Tuple<IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>>, IEnvelope<ProcessMessagePort>>(envelopes =>
            {
                var envelope1 = envelopes.Item1;
                var envelope2 = envelopes.Item2;

                var result = func.Invoke(Tuple.Create(envelope1.Data, envelope2.Data));
                return envelope1.Transform(result);
            });
        }

        /// <inheritdoc />
        internal override TransformBlock<Tuple<IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>>, IEnvelope<ProcessMessagePort>> CreateJoinTransformBlock(Func<Tuple<ProcessMessagePort, ProcessMessagePort, ProcessMessagePort>, ProcessMessagePort> func)
        {
            return new TransformBlock<Tuple<IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>, IEnvelope<ProcessMessagePort>>, IEnvelope<ProcessMessagePort>>(envelopes =>
            {
                var envelope1 = envelopes.Item1;
                var envelope2 = envelopes.Item2;
                var envelope3 = envelopes.Item3;

                var result = func.Invoke(Tuple.Create(envelope1.Data, envelope2.Data, envelope3.Data));
                return envelope1.Transform(result);
            });
        }

        /// <summary>
        /// Create ActionBlock that invoke the given action
        /// </summary>
        /// <param name="action">The action to be invoked by the block</param>
        /// <returns>ActionBlock that will invoke the given action</returns>
        internal ActionBlock<IEnvelope<ProcessMessagePort>> CreateActionBlock(Action<ProcessMessagePort> action)
        {
            return CreateActionBlock(envelope => action(envelope.Data));
        }
    }
}
