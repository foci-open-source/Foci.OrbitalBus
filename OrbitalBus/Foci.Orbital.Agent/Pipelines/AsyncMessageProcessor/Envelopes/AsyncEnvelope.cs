﻿using Foci.Orbital.Agent.Pipelines.Generic.Envelopes.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Envelopes
{
    [ExcludeFromCodeCoverage]
    internal class AsyncEnvelope : IEnvelope<ProcessMessagePort>
    {
        /// <inheritdoc />
        public ProcessMessagePort Data { get; }

        /// <summary>
        /// Default constructor to take in data
        /// </summary>
        /// <param name="data">Data stored in the current envelope</param>
        public AsyncEnvelope(ProcessMessagePort data)
        {
            Data = data;
        }

        /// <inheritdoc />
        public IEnvelope<ProcessMessagePort> Transform(ProcessMessagePort item)
        {
            return new AsyncEnvelope(item);
        }

        /// <inheritdoc />
        public IEnvelope<ProcessMessagePort> Transform(Func<ProcessMessagePort, ProcessMessagePort> func)
        {
            return new AsyncEnvelope(func.Invoke(Data));
        }
    }
}
