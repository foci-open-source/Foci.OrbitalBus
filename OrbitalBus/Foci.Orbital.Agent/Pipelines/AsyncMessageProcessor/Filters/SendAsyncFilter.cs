﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using System;

namespace Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Filters
{
    internal class SendAsyncFilter : FaultableBaseFilter<ProcessMessagePort>
    {
        /// <inheritdoc />
        public override ProcessMessagePort Process(ProcessMessagePort port)
        {
            if (!IsPortValid(port, out port))
            {
                return port;
            }

            var adapter = port.Adapter;
            if (adapter == null)
            {
                var error = "An adapter is needed to be able to send message";
                logger.Warn("SendAsyncFilter :: Process: {0}", error);
                return (ProcessMessagePort)port.AppendFault(new OrbitalAdapterException(error));
            }

            var adapterRequest = port.AdapterRequest;
            if (adapterRequest == null)
            {
                var error = "An adapter request is needed to be able to send message";
                logger.Warn("SendAsyncFilter :: Process: {0}", error);
                return (ProcessMessagePort)port.AppendFault(new OrbitalAdapterException(error));
            }

            try
            {
                logger.Info("SendAsyncFilter :: Process: Sending message to {0} asynchronously", port.AdapterType);
                adapter.SendAsync(adapterRequest);
            }
            catch (Exception e) when (e is OrbitalAdapterException)
            {
                logger.Warn(e, "SendAsyncFilter :: Process: {0}", e.Message);
                return (ProcessMessagePort)port.AppendFault(e);
            }

            logger.Debug("SendAsyncFilter :: Process: Successfully send message to {0} asynchronously", port.AdapterType);
            return port;
        }
    }
}
