﻿using Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Envelopes;
using Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Factories;
using Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Envelopes.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Pipelines.Generic.Models.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Services.Interfaces;
using NJsonSchema;
using NLog;
using System;
using System.Threading.Tasks.Dataflow;
using Unity;

namespace Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor
{
    internal class AsyncMessageProcessor : IPipeline<MessageProcessorInput>
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly AsyncBlockFactory blockFactory;

        private readonly GetAdapterFilter<ProcessMessagePort> getAdapterFilter;
        private readonly GetAdapterParametersFilter<ProcessMessagePort> getAdapterParametersFilter;
        private readonly ValidateRequestMessageFilter<ProcessMessagePort> validateRequestMessageFilter;
        private readonly TranslateOrbitalRequestBodyFilter<ProcessMessagePort> translateOrbitalRequestBodyFilter;
        private readonly CreateAdapterRequestFilter<ProcessMessagePort> createAdapterRequestFilter;
        private readonly JoinProcessMessagePortFilter joinProcessMessagePortFilter;
        private readonly SendAsyncFilter sendAsyncFilter;

        private BroadcastBlock<IEnvelope<ProcessMessagePort>> startBlock;
        private ActionBlock<IEnvelope<ProcessMessagePort>> endBlock;

        /// <summary>
        /// Constructor to take in needed services
        /// </summary>
        /// <param name="unityContainer">The dependency injector container</param>
        /// <param name="translationService">JSON translation service to execute translation</param>
        /// <param name="configurationService">Configuration service to get configuration</param>
        public AsyncMessageProcessor(IUnityContainer unityContainer, IJsonTranslationService translationService, IConfigurationService configurationService)
            : this(new GetAdapterFilter<ProcessMessagePort>(unityContainer),
                  new GetAdapterParametersFilter<ProcessMessagePort>(translationService, configurationService),
                  new ValidateRequestMessageFilter<ProcessMessagePort>(),
                  new TranslateOrbitalRequestBodyFilter<ProcessMessagePort>(translationService),
                  new CreateAdapterRequestFilter<ProcessMessagePort>(),
                  new JoinProcessMessagePortFilter(),
                  new SendAsyncFilter())
        {
        }

        /// <summary>
        /// Constructor that takes in needed filters
        /// </summary>
        /// <param name="getAdapterFilter">Filter to get IAdapter</param>
        /// <param name="getAdapterParametersFilter">Filter to get adapter parameters</param>
        /// <param name="translateOrbitalRequestBodyFilter">Filter to translate orbital request body</param>
        /// <param name="createAdapterRequestFilter">Filter to create adapter request</param>
        /// <param name="joinProcessMessagePortFilter">Filter to join multiple ports</param>
        /// <param name="sendAsyncFilter">Filter to send async request</param>
        public AsyncMessageProcessor(GetAdapterFilter<ProcessMessagePort> getAdapterFilter,
            GetAdapterParametersFilter<ProcessMessagePort> getAdapterParametersFilter,
            ValidateRequestMessageFilter<ProcessMessagePort> validateRequestMessageFilter,
            TranslateOrbitalRequestBodyFilter<ProcessMessagePort> translateOrbitalRequestBodyFilter,
            CreateAdapterRequestFilter<ProcessMessagePort> createAdapterRequestFilter,
            JoinProcessMessagePortFilter joinProcessMessagePortFilter,
            SendAsyncFilter sendAsyncFilter)
        {
            this.getAdapterFilter = getAdapterFilter;
            this.getAdapterParametersFilter = getAdapterParametersFilter;
            this.translateOrbitalRequestBodyFilter = translateOrbitalRequestBodyFilter;
            this.createAdapterRequestFilter = createAdapterRequestFilter;
            this.validateRequestMessageFilter = validateRequestMessageFilter;
            this.joinProcessMessagePortFilter = joinProcessMessagePortFilter;
            this.sendAsyncFilter = sendAsyncFilter;

            this.blockFactory = new AsyncBlockFactory();
        }

        /// <inheritdoc />
        public void Start()
        {
            var linkOptions = new DataflowLinkOptions { PropagateCompletion = true };

            //Initialize blocks
            this.startBlock = this.blockFactory.CreateBroadcastBlock(envelope => envelope);

            var getAdapterBlock = this.blockFactory.CreateTransformBlock(this.getAdapterFilter.Process);
            var getParameterBlock = this.blockFactory.CreateTransformBlock(this.getAdapterParametersFilter.Process);
            var validateBlock = this.blockFactory.CreateTransformBlock(this.validateRequestMessageFilter.Process);
            var translateMessageBlock = this.blockFactory.CreateTransformBlock(this.translateOrbitalRequestBodyFilter.Process);
            var joinRequestPartsBlock = this.blockFactory.CreateJoinThreeBlock(new GroupingDataflowBlockOptions() { Greedy = false });
            var mergePartsBlock = this.blockFactory.CreateJoinTransformBlock(this.joinProcessMessagePortFilter.ProcessThree);
            var createRequestBlock = this.blockFactory.CreateTransformBlock(this.createAdapterRequestFilter.Process);

            this.endBlock = this.blockFactory.CreateActionBlock(FinalAction);

            // Diagram can be found https://gitlab.com/foci-open-source/Foci.OrbitalBus/wikis/Async-Pipeline-Diagram
            //Broadcast incoming request to all getter blocks
            this.startBlock.LinkTo(getAdapterBlock, linkOptions);
            this.startBlock.LinkTo(getParameterBlock, linkOptions);
            this.startBlock.LinkTo(validateBlock, linkOptions);

            //Join result from all getter blocks
            getAdapterBlock.LinkTo(joinRequestPartsBlock.Target1, linkOptions);
            getParameterBlock.LinkTo(joinRequestPartsBlock.Target2, linkOptions);

            validateBlock.LinkTo(translateMessageBlock, linkOptions);
            translateMessageBlock.LinkTo(joinRequestPartsBlock.Target3, linkOptions);
            joinRequestPartsBlock.LinkTo(mergePartsBlock, linkOptions);

            //Create adapter request and send async
            mergePartsBlock.LinkTo(createRequestBlock, linkOptions);
            createRequestBlock.LinkTo(this.endBlock, linkOptions);

            this.logger.Debug("AsyncMessageProcessor :: Start: Initialization complete, ready to process message");
        }

        /// <inheritdoc />
        public void Push(MessageProcessorInput input)
        {
            if (input == null ||
                input.OperationDefinition == null ||
                input.OperationDefinition.Adapter == null ||
                input.OperationDefinition.Validate == null ||
                input.OperationDefinition.Schemas == null)
            {
                this.logger.Warn("AsyncMessageProcessor :: Push: Input cannot be null");
                return;
            }

            var port = new ProcessMessagePort()
            {
                Translation = input.OperationDefinition.Translation,
                AdapterType = input.OperationDefinition.Adapter.Type,
                AdapterConfiguration = input.OperationDefinition.Adapter.Configuration,
                OrbitalRequestBody = input.OrbitalRequestBody,
                ValidateRequest = input.OperationDefinition.Validate.Request,
                ValidateResponse = input.OperationDefinition.Validate.Response,
                RequestSchema = JsonSchema4.FromJsonAsync(input.OperationDefinition.Schemas.Request ?? string.Empty).Result,
                ResponseSchema = JsonSchema4.FromJsonAsync(input.OperationDefinition.Schemas.Response ?? string.Empty).Result
            };

            var envelope = new AsyncEnvelope(port);

            this.startBlock.Post(envelope);
        }

        /// <inheritdoc />
        public bool Stop()
        {
            try
            {
                if (this.startBlock != null)
                {
                    this.startBlock.Complete();
                    if (this.endBlock != null)
                    {
                        this.endBlock.Completion.Wait();
                    }
                }
            }
            catch (AggregateException ae)
            {
                this.logger.Warn(ae, "AsyncMessageProcessor :: Stop: Unable to shutdown gracefully");
                return false;
            }
            this.logger.Debug("AsyncMessageProcessor :: Stop: Successfully shutdown");
            return true;
        }

        //Log warning if port is null, otherwise send the message
        private void FinalAction(ProcessMessagePort port)
        {
            if (port == null)
            {
                this.logger.Warn("AsyncMessageProcessor :: FinalAction: Pipeline port cannot be null");
            }
            else
            {
                this.sendAsyncFilter.Process(port);
            }
        }
    }
}
