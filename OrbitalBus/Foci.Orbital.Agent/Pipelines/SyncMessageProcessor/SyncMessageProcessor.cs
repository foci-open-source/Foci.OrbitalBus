﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Envelopes.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Pipelines.Generic.Models.Interfaces;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Envelopes;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Factories;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters;
using Foci.Orbital.Agent.Services.Interfaces;
using NJsonSchema;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Unity;

namespace Foci.Orbital.Agent.Pipelines.SyncMessageProcessor
{
    internal class SyncMessageProcessor : IPipeline<MessageProcessorInput, Payload>
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly SyncBlockFactory blockFactory;
        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        private readonly GetAdapterFilter<ProcessMessagePort> getAdapterFilter;
        private readonly GetAdapterParametersFilter<ProcessMessagePort> getAdapterParametersFilter;
        private readonly ValidateRequestMessageFilter<ProcessMessagePort> validateRequestMessageFilter;
        private readonly TranslateOrbitalRequestBodyFilter<ProcessMessagePort> translateOrbitalRequestBodyFilter;
        private readonly CreateAdapterRequestFilter<ProcessMessagePort> createAdapterRequestFilter;
        private readonly JoinProcessMessagePortFilter joinProcessMessagePortFilter;
        private readonly SendSyncFilter sendSyncFilter;
        private readonly TranslateResponseMessageFilter translateResponseMessageFilter;
        private readonly ValidateResponseMessageFilter<ProcessMessagePort> validateResponseMessageFilter;

        private BroadcastBlock<IEnvelope<ProcessMessagePort>> startBlock;
        private ActionBlock<IEnvelope<ProcessMessagePort>> endBlock;

        /// <summary>
        /// Constructor to take in needed services
        /// </summary>
        /// <param name="unityContainer">The dependency injector container</param>
        /// <param name="translationService">JSON translation service to execute translation</param>
        /// <param name="configurationService">Configuration service to get configuration</param>
        public SyncMessageProcessor(IUnityContainer unityContainer, IJsonTranslationService translationService, IConfigurationService configurationService)
            : this(new GetAdapterFilter<ProcessMessagePort>(unityContainer),
                  new GetAdapterParametersFilter<ProcessMessagePort>(translationService, configurationService),
                  new ValidateRequestMessageFilter<ProcessMessagePort>(),
                  new TranslateOrbitalRequestBodyFilter<ProcessMessagePort>(translationService),
                  new CreateAdapterRequestFilter<ProcessMessagePort>(),
                  new JoinProcessMessagePortFilter(),
                  new SendSyncFilter(),
                  new TranslateResponseMessageFilter(translationService),
                  new ValidateResponseMessageFilter<ProcessMessagePort>())
        {
        }

        /// <summary>
        /// Constructor that takes in needed filters
        /// </summary>
        /// <param name="getAdapterFilter">Filter to get IAdapter</param>
        /// <param name="getAdapterParametersFilter">Filter to get adapter parameters</param>
        /// <param name="translateOrbitalRequestBodyFilter">Filter to translate orbital request body</param>
        /// <param name="createAdapterRequestFilter">Filter to create adapter request</param>
        /// <param name="joinProcessMessagePortFilter">Filter to join multiple ports</param>
        /// <param name="sendSyncFilter">Filter to send sync request</param>
        /// <param name="translateResponseMessageFilter">Filter to translate adapter response message</param>
        public SyncMessageProcessor(GetAdapterFilter<ProcessMessagePort> getAdapterFilter,
            GetAdapterParametersFilter<ProcessMessagePort> getAdapterParametersFilter,
            ValidateRequestMessageFilter<ProcessMessagePort> validateRequestMessageFilter,
            TranslateOrbitalRequestBodyFilter<ProcessMessagePort> translateOrbitalRequestBodyFilter,
            CreateAdapterRequestFilter<ProcessMessagePort> createAdapterRequestFilter,
            JoinProcessMessagePortFilter joinProcessMessagePortFilter,
            SendSyncFilter sendSyncFilter,
            TranslateResponseMessageFilter translateResponseMessageFilter,
            ValidateResponseMessageFilter<ProcessMessagePort> validateResponseMessageFilter)
        {
            this.getAdapterFilter = getAdapterFilter;
            this.getAdapterParametersFilter = getAdapterParametersFilter;
            this.translateOrbitalRequestBodyFilter = translateOrbitalRequestBodyFilter;
            this.createAdapterRequestFilter = createAdapterRequestFilter;
            this.validateRequestMessageFilter = validateRequestMessageFilter;
            this.joinProcessMessagePortFilter = joinProcessMessagePortFilter;
            this.sendSyncFilter = sendSyncFilter;
            this.translateResponseMessageFilter = translateResponseMessageFilter;
            this.validateResponseMessageFilter = validateResponseMessageFilter;

            this.blockFactory = new SyncBlockFactory();
        }

        /// <inheritdoc />
        public void Start()
        {
            var linkOptions = new DataflowLinkOptions { PropagateCompletion = true };

            //Initialize blocks
            this.startBlock = this.blockFactory.CreateBroadcastBlock(envelope => envelope);

            var getAdapterBlock = this.blockFactory.CreateTransformBlock(this.getAdapterFilter.Process);
            var getParameterBlock = this.blockFactory.CreateTransformBlock(this.getAdapterParametersFilter.Process);
            var validateRequestBlock = this.blockFactory.CreateTransformBlock(this.validateRequestMessageFilter.Process);
            var translateMessageBlock = this.blockFactory.CreateTransformBlock(this.translateOrbitalRequestBodyFilter.Process);

            var joinRequestPartsBlock = this.blockFactory.CreateJoinThreeBlock(new GroupingDataflowBlockOptions() { Greedy = false });
            var mergePartsBlock = this.blockFactory.CreateJoinTransformBlock(this.joinProcessMessagePortFilter.ProcessThree);

            var createRequestBlock = this.blockFactory.CreateTransformBlock(this.createAdapterRequestFilter.Process);

            var sendSyncBlock = this.blockFactory.CreateTransformBlock(this.sendSyncFilter.Process);
            var translateResponseBlock = this.blockFactory.CreateTransformBlock(this.translateResponseMessageFilter.Process);
            var validateResponseBlock = this.blockFactory.CreateTransformBlock(this.validateResponseMessageFilter.Process);

            this.endBlock = this.blockFactory.CreateFinalBlock();

            //Diagram can be found https://gitlab.com/foci-open-source/Foci.OrbitalBus/wikis/Sync-Pipeline-Diagram
            //Broadcast incoming request to all getter blocks
            this.startBlock.LinkTo(getAdapterBlock, linkOptions);
            this.startBlock.LinkTo(getParameterBlock, linkOptions);
            this.startBlock.LinkTo(validateRequestBlock, linkOptions);

            //Join result from all getter blocks
            getAdapterBlock.LinkTo(joinRequestPartsBlock.Target1, linkOptions);
            getParameterBlock.LinkTo(joinRequestPartsBlock.Target2, linkOptions);

            validateRequestBlock.LinkTo(translateMessageBlock, linkOptions);
            translateMessageBlock.LinkTo(joinRequestPartsBlock.Target3, linkOptions);
            joinRequestPartsBlock.LinkTo(mergePartsBlock, linkOptions);

            //Create adapter request and send sync
            mergePartsBlock.LinkTo(createRequestBlock, linkOptions);
            createRequestBlock.LinkTo(sendSyncBlock, linkOptions);


            //Translate adapter response and package all faults in the port as a payload
            sendSyncBlock.LinkTo(translateResponseBlock, linkOptions);
            translateResponseBlock.LinkTo(validateResponseBlock, linkOptions);
            validateResponseBlock.LinkTo(this.endBlock, linkOptions);

            this.logger.Debug("SyncMessageProcessor :: Start: Initialization complete, ready to process message");
        }

        /// <inheritdoc />
        public Payload Push(MessageProcessorInput input)
        {
            if (input == null ||
                input.OperationDefinition == null ||
                input.OperationDefinition.Adapter == null ||
                input.OperationDefinition.Validate == null ||
                input.OperationDefinition.Schemas == null)
            {
                var error = "Pipeline input cannot be null";
                this.logger.Warn("SyncMessageProcessor :: Push: {0}", error);
                return CreateFaultPayload(error);
            }

            var port = new ProcessMessagePort()
            {
                Translation = input.OperationDefinition.Translation,
                AdapterType = input.OperationDefinition.Adapter.Type,
                AdapterConfiguration = input.OperationDefinition.Adapter.Configuration,
                OrbitalRequestBody = input.OrbitalRequestBody,
                ValidateRequest = input.OperationDefinition.Validate.Request,
                ValidateResponse = input.OperationDefinition.Validate.Response,
                RequestSchema = JsonSchema4.FromJsonAsync(input.OperationDefinition.Schemas.Request ?? string.Empty).Result,
                ResponseSchema = JsonSchema4.FromJsonAsync(input.OperationDefinition.Schemas.Response ?? string.Empty).Result
            };

            var completionSource = new TaskCompletionSource<ProcessMessagePort>();

            var envelope = new SyncEnvelope(completionSource, port);

            this.startBlock.Post(envelope);

            completionSource.Task.Wait();
            port = completionSource.Task.Result;
            if (port == null)
            {
                var error = "Pipeline port cannot be null";
                this.logger.Warn("TranslateResponseMessageFilter :: Process: {0}", error);
                return CreateFaultPayload(error);
            }

            return port.Payload;
        }

        /// <inheritdoc />
        public bool Stop()
        {
            try
            {
                if (this.startBlock != null)
                {
                    this.startBlock.Complete();
                    if (this.endBlock != null)
                    {
                        this.endBlock.Completion.Wait();
                    }
                }
            }
            catch (AggregateException ae)
            {
                this.logger.Warn(ae, "SyncMessageProcessor :: Stop: Unable to shutdown gracefully");
                return false;
            }
            this.logger.Debug("SyncMessageProcessor :: Stop: Successfully shutdown");
            return true;
        }

        //create payload containing OrbitalArgumentException
        private Payload CreateFaultPayload(string error)
        {
            var faults = new List<Fault>() { this.faultFactory.CreateFault(new OrbitalArgumentException(error)) };
            return Payload.Create(faults);
        }
    }
}
