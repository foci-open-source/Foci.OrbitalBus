﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Repositories.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters
{
    internal class ValidateResponseMessageFilter<T> : FaultableBaseFilter<T>
        where T : IFaultablePort, ISchemaPort, IPayloadPort
    {
        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        /// <inheritdoc />
        public override T Process(T port)
        {
            if (!IsPortValid(port, out port) || !port.ValidateResponse)
            {
                return port;
            }

            var payload = port.Payload;
            if (payload == null)
            {
                var error = "Payload cannot be null";
                logger.Warn("ValidateResponseMessageFilter :: Process: {0}", error);
                port = (T)port.AppendFault(new OrbitalArgumentException(error));
                port.Payload = CreateFaultPayload(port.Faults);
                return port;
            }

            var message = (string)payload.Match(value => value ?? string.Empty, fault => null);
            if (message == null)
            {
                return port;
            }

            var schema = port.ResponseSchema;
            if (schema == null)
            {
                var error = "Response schema cannot be null";
                logger.Warn("ValidateResponseMessageFilter :: Process: {0}", error);
                port = (T)port.AppendFault(new OrbitalArgumentException(error));
                port.Payload = CreateFaultPayload(port.Faults);
                return port;
            }

            var result = schema.ValidateMessage(message);
            if (result.Any())
            {
                logger.Warn("ValidateResponseMessageFilter :: Process: Adapter response message does not match the provided schema");
                result.ForEach(r => port = (T)port.AppendFault(r));
                port.Payload = CreateFaultPayload(port.Faults);
            }

            return port;
        }

        //Create payload containing given exception
        private Payload CreateFaultPayload(Exception e, PayloadHeaders headers = null)
        {
            var faults = new List<Fault>() { faultFactory.CreateFault(e) };
            return CreateFaultPayload(faults, headers);
        }

        //create payload containing given collection of faults
        private Payload CreateFaultPayload(ICollection<Fault> faults, PayloadHeaders headers = null)
        {
            var payload = (headers == null) ? Payload.Create(faults) : Payload.Create(faults, headers);
            return payload;
        }
    }
}
