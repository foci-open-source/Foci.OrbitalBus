﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters.Bases;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Services.Interfaces;
using Jint.Runtime;
using System;
using System.Collections.Generic;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters
{
    internal class TranslateResponseMessageFilter : FaultableBaseFilter<ProcessMessagePort>
    {
        private readonly IJsonTranslationService translationService;
        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        /// <summary>
        /// Default constructor to take in needed services
        /// </summary>
        /// <param name="translationService">JSON translation service to execute translation</param>
        public TranslateResponseMessageFilter(IJsonTranslationService translationService)
        {
            this.translationService = translationService ?? throw new ArgumentNullException(nameof(translationService));
        }

        /// <inheritdoc />
        public override ProcessMessagePort Process(ProcessMessagePort port)
        {
            if (port == null)
            {
                var error = "Pipeline port cannot be null";
                logger.Warn("TranslateResponseMessageFilter :: Process: {0}", error);
                port = (ProcessMessagePort)new ProcessMessagePort().AppendFault(new OrbitalArgumentException(error));
                port.Payload = CreateFaultPayload(port.Faults);
                return port;
            }
            else if (port.IsFaulted)
            {
                port.Payload = CreateFaultPayload(port.Faults);
                return port;
            }

            var translation = port.Translation;

            var payload = port.Payload;
            if (payload == null)
            {
                var error = "Adapter response cannot be null";
                logger.Warn("TranslateResponseMessageFilter :: Process: {0}", error);
                port = (ProcessMessagePort)port.AppendFault(new OrbitalAdapterException(error));
                port.Payload = CreateFaultPayload(port.Faults);
                return port;
            }

            var headers = port.Payload.Headers;
            var message = (string)port.Payload.Match(value => value ?? string.Empty, fault => null);
            if (message == null)
            {
                logger.Debug("TranslateResponseMessageFilter :: Process: Successfully receive message from {0} synchronously", port.AdapterType);
                return port;
            }

            try
            {
                // If outbound function exist in translation, use it to translate the response
                var translationFunction = JsonTranslationFunction.Outbound;
                if (translationService.FunctionExist(translation, translationFunction))
                {
                    message = translationService.ExecuteTranslationFunction(message, translation, translationFunction);
                }
            }
            catch (Exception e) when (e is JavaScriptException
            || e is OrbitalTranslationException
            || e is OrbitalUnknownEnumException
            || e is OrbitalArgumentException
            || e is ArgumentNullException
            || e is ArgumentException
            || e is OrbitalBusinessException)
            {
                logger.Warn(e, "TranslateResponseMessageFilter :: Process: {0}", e.Message);
                port.Payload = CreateFaultPayload(e, headers);
                return port;
            }

            logger.Debug("TranslateResponseMessageFilter :: Process: Successfully receive message from {0} synchronously", port.AdapterType);
            port.Payload = Payload.Create(message, headers);
            return port;
        }

        //Create payload containing given exception
        private Payload CreateFaultPayload(Exception e, PayloadHeaders headers = null)
        {
            var faults = new List<Fault>() { faultFactory.CreateFault(e) };
            return CreateFaultPayload(faults, headers);
        }

        //create payload containing given collection of faults
        private Payload CreateFaultPayload(ICollection<Fault> faults, PayloadHeaders headers = null)
        {
            var payload = (headers == null) ? Payload.Create(faults) : Payload.Create(faults, headers);
            return payload;
        }
    }
}
