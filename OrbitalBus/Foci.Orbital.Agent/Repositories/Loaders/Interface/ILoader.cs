﻿namespace Foci.Orbital.Agent.Repositories.Loaders.Interface
{
    /// <summary>
    /// Interface for Configuration Loaders that will load the configuration from different locations.
    /// </summary>
    public interface ILoader
    {
        /// <summary>
        /// Check if the configuration exists in the current location.
        /// </summary>
        /// <returns>Flags true if it exists, false otherwise.</returns>
        bool Exists();

        /// <summary>
        /// Loads configuration from the current location.
        /// </summary>
        /// <returns>json string representation of the configuration</returns>
        string LoadConfiguration();
    }
}
