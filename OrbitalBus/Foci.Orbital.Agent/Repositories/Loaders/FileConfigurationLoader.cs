﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Repositories.Loaders.Interface;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace Foci.Orbital.Agent.Repositories.Loaders
{
    /// <summary>
    /// Configuration Loader to load the configuration from file system.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class FileConfigurationLoader : ILoader
    {
        private readonly string fileName;

        /// <summary>
        /// Constructor that takes in the name of the file and file repository
        /// </summary>
        /// <param name="fileName">string representation of the file name where the configuration is located.</param>
        /// <param name="fileRepository">FileRepository instance</param>
        public FileConfigurationLoader(string fileName)
        {
            this.fileName = string.IsNullOrWhiteSpace(fileName) ? "orbital.config" : fileName;
        }

        /// <inheritdoc />
        public bool Exists()
        {
            var filePath = Path.Combine(AssemblyDirectory, AgentConstants.CONFIG_PATH, fileName);
            return File.Exists(filePath);
        }

        /// <inheritdoc />
        public string LoadConfiguration()
        {
            var filePath = Path.Combine(AssemblyDirectory, AgentConstants.CONFIG_PATH, fileName);
            try
            {
                return File.ReadAllText(filePath);
            }
            catch (IOException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns the runtime assembly directory
        /// </summary>
        private string AssemblyDirectory
        {
            get
            {
                UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
                return Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            }
        }
    }
}
