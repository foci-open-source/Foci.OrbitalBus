﻿using Foci.Orbital.Agent.Repositories.Loaders.Interface;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Repositories.Loaders
{
    /// <summary>
    /// Configuration Loader to load the configuration from environment variable
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class EnvVarConfigurationLoader : ILoader
    {
        private const string environmentVariableKey = "orbital.agent.configuration";
        private readonly string config;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EnvVarConfigurationLoader()
        {
            config = Environment.GetEnvironmentVariable(environmentVariableKey);
        }

        /// <inheritdoc />
        public bool Exists()
        {
            return !string.IsNullOrWhiteSpace(config);
        }

        /// <inheritdoc />
        public string LoadConfiguration()
        {
            return config;
        }
    }
}
