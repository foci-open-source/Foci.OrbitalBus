﻿using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul;
using Foci.Orbital.Agent.Models.Consul.Requests;
using Foci.Orbital.Agent.Models.Consul.Responses;
using Foci.Orbital.Agent.Repositories.Extensions;
using Foci.Orbital.Agent.Repositories.Interface;
using Newtonsoft.Json;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Unity.Attributes;

namespace Foci.Orbital.Agent.Repositories
{
    /// <summary>
    /// Interacts directly with consul.
    /// </summary>
    internal class ConsulRepository : IConsulRepository
    {
        private static ILogger log = LogManager.GetCurrentClassLogger();
        private readonly IRestClient request;
        private readonly string serviceDefinitionKey = "orbital.{0}.definition";

        [InjectionConstructor]
        public ConsulRepository() : this(new RestClient())
        {
        }

        public ConsulRepository(IRestClient client)
        {
            this.request = client ?? throw new ArgumentNullException(nameof(client));
        }

        /// <inheritdoc />
        public bool RegisterService(RegisterServiceRequest registerServiceRequest, string consulHostAddress, Maybe<X509Certificate> cert)
        {
            if (registerServiceRequest == null)
            {
                throw new ArgumentNullException(nameof(registerServiceRequest));
            }

            if (string.IsNullOrWhiteSpace(consulHostAddress))
            {
                throw new ArgumentException("Consul IP address cannot be null or empty", nameof(consulHostAddress));
            }

            var client = ConfigureRestClient(consulHostAddress, cert);
            var request = CreatePutRequest(ConsulConstants.RegisterService, registerServiceRequest);
            var response = client.Execute(request);
            if (response.ErrorException != null)
            {
                throw (response.ErrorException);
            }
            return response.IsSuccessful();
        }

        /// <inheritdoc />
        public bool SetKeyValue(string key, string value, string consulHostAddress, Maybe<X509Certificate> cert)
        {
            if (string.IsNullOrWhiteSpace(consulHostAddress))
            {
                log.Warn("SetKeyValue: Consul IP address cannot be null or empty");
                return false;
            }

            var client = ConfigureRestClient(consulHostAddress, cert);

            var urlSegments = new Dictionary<string, string> { { "key", key } };
            var request = CreatePutRequestWithParameters(ConsulConstants.SetKeyValue, value, urlSegments);
            var response = client.Execute<bool>(request);
            if (response.ErrorException != null)
            {
                throw (response.ErrorException);
            }
            return response.IsSuccessful();
        }

        /// <inheritdoc />
        public string GetKeyValue(string key, string consulHostAddress, Maybe<X509Certificate> cert)
        {
            if (string.IsNullOrWhiteSpace(consulHostAddress))
            {
                log.Warn("GetKeyValue: Consul IP address cannot be null or empty");
                return string.Empty;
            }

            var client = ConfigureRestClient(consulHostAddress, cert);
            var serviceKey = string.Format(serviceDefinitionKey, key);
            var urlSegments = new Dictionary<string, string> { { "key", serviceKey } };
            var request = CreateGetRequestWithParameters(ConsulConstants.GetKeyValue, urlSegments);
            var response = client.Execute<List<GetKeyValueResponse>>(request);

            if (response.ErrorException != null)
            {
                throw (response.ErrorException);
            }

            if (response.Content == string.Empty)
            {
                log.Warn("GetKeyValue: Could not find a value with key {}", serviceKey);
                return string.Empty;
            }
            return EncodeRestResponse(response);
        }


        #region Private methods
        /// <summary>
        /// Create REST Client with or without a certificate
        /// </summary>
        /// <param name="consulHostAddress">Consul Address.</param>
        /// <param name="cert">Certificate to add to REST client if certificate has been provided</param>
        /// <returns>A request ready to execute.</returns>
        private IRestClient ConfigureRestClient(string consulHostAddress, Maybe<X509Certificate> cert)
        {
            this.request.BaseUrl = new Uri(consulHostAddress);

            if (cert.HasValue)
            {
                var collection = new X509CertificateCollection
                {
                    cert.Value
                };
                request.ClientCertificates = collection;
            }
            return request;
        }

        /// <summary>
        /// Create a GET request with consul info.  No URL parameters, just headers and a fixed endpoint.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and any headers to use.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateGetRequest(ConsulCallInfo consulCallInfo)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.GET);

            #region Headers
            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }
            #endregion

            return request;
        }


        /// <summary>
        /// Create a GET request with consul info and parameters injected into the URL.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="urlParameters">The parameters to inject into the URL.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateGetRequestWithParameters(ConsulCallInfo consulCallInfo, IDictionary<string, string> urlParameters)
        {
            var request = CreateGetRequest(consulCallInfo);

            #region Url Parameters
            if (urlParameters != null && urlParameters.Count > 0)
            {
                urlParameters.ToList().ForEach(kvp => request.AddUrlSegment(kvp.Key, kvp.Value));
            }
            #endregion

            return request;
        }

        /// <summary>
        /// Create a PUT request with consul info.  No URL parameters, just a fixed endpoint with headers and a body.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="body">The body to serialize into JSON.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreatePutRequest(ConsulCallInfo consulCallInfo, object body)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.PUT);

            #region Headers
            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }

            #endregion

            #region Body
            if (body != null)
            {
                request.AddJsonBody(body);
            }
            #endregion

            return request;
        }

        /// <summary>
        /// Create a PUT request with consul info.  No URL parameters, just a fixed endpoint with headers and a body.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="body">The body to serialize into JSON.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreatePutRequest(ConsulCallInfo consulCallInfo, string jsonBody)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.PUT);

            #region Headers
            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }

            #endregion

            #region Body
            if (!string.IsNullOrWhiteSpace(jsonBody))
            {
                request.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            #endregion

            return request;
        }

        /// <summary>
        /// Create a PUT request with consul info.  URL parameters are injected into the endpoint.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="body">The already serialized string.</param>
        /// <param name="urlParameters">The parameters to inject into the endpoint.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreatePutRequestWithParameters(ConsulCallInfo consulCallInfo, string body, IDictionary<string, string> urlParameters)
        {
            var request = CreatePutRequest(consulCallInfo, body);

            #region Url Parameters
            if (urlParameters != null && urlParameters.Count > 0)
            {
                urlParameters.ToList().ForEach(kvp => request.AddUrlSegment(kvp.Key, kvp.Value));
            }
            #endregion

            return request;
        }

        /// <summary>
        /// Create a DELETE request with consul info.  No URL parameters, just a fixed endpoint with headers and a body.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateDeleteRequest(ConsulCallInfo consulCallInfo)
        {
            var request = new RestRequest(consulCallInfo.Endpoint, Method.DELETE);

            #region Headers

            if (consulCallInfo.Headers != null && consulCallInfo.Headers.Count > 0)
            {
                consulCallInfo.Headers.ToList().ForEach(kvp => request.AddHeader(kvp.Key, kvp.Value));
            }

            #endregion

            return request;
        }

        /// <summary>
        /// Create a DELETE request with consul info. URL parameters are injected into the endpoint.
        /// </summary>
        /// <param name="consulCallInfo">The endpoint and header information.</param>
        /// <param name="urlParameters">The parameters to inject into the endpoint.</param>
        /// <returns>A request ready to execute.</returns>
        private RestRequest CreateDeleteRequestWithParameters(ConsulCallInfo consulCallInfo, IDictionary<string, string> urlParameters)
        {
            var request = CreateDeleteRequest(consulCallInfo);

            #region Url Parameters

            if (urlParameters != null && urlParameters.Count > 0)
            {
                urlParameters.ToList().ForEach(kvp => request.AddUrlSegment(kvp.Key, kvp.Value));
            }

            #endregion

            return request;
        }

        /// <summary>
        /// The generic pattern for making a call against consul without parameters or a body.
        /// </summary>
        /// <typeparam name="T">The return type expected from the call.</typeparam>
        /// <param name="hostAddress">The host address for consul.</param>
        /// <param name="constants">The ConsulCallInfo to provide the endpoint and header information.</param>
        /// <returns>T</returns>
        private T GetWithoutParams<T>(string hostAddress, ConsulCallInfo constants, Maybe<X509Certificate> cert) where T : new()
        {
            var client = ConfigureRestClient(hostAddress, cert);
            var request = CreateGetRequest(constants);
            var response = client.Execute(request);
            return DeserializeRestResponse<T>(response);
        }

        /// <summary>
        /// Deserializes the response from RestSharp and manages any error response.
        /// </summary>
        /// <typeparam name="T">The type of the expected response object.</typeparam>
        /// <param name="response">The response from RestSharp.</param>
        /// <returns>The data if successful.</returns>
        private T DeserializeRestResponse<T>(IRestResponse response) where T : new()
        {
            if (response.IsSuccessful())
            {
                T restResponseValue = JsonConvert.DeserializeObject<T>(response.Content);
                return restResponseValue;
            }
            else if (response.ErrorException != null)
            {
                log.Error(response.ErrorException, "The response came back with the following error: " + response.ErrorException.Message);
                return default(T);
            }
            // A business fault is located here. Our business will have to be created by without the use of the pipeline as they are validation faults, meaning
            // they do not throw exceptions. Business behavior we wish to catch will have to generate proper fault through if/else as this example shows.
            else
            {
                var exception = new OrbitalCommunicationException(string.Format("There was a problem communicating with Consul. Https code {0}. Status code description {1}.", response.StatusCode.ToString(), response.StatusDescription));
                log.Error(exception.Message);
                log.Debug(exception);
                return default(T);
            }
        }

        /// <summary>
        /// Decode the string value of a rest response
        /// </summary>
        /// <param name="encodedData">The encoded string</param>
        /// <returns>The decoded string</returns>
        private string EncodeRestResponse(IRestResponse<List<GetKeyValueResponse>> encodedData)
        {
            var desrlizedResponse = DeserializeRestResponse<List<GetKeyValueResponse>>(encodedData);
            var base64EncodedBytes = Convert.FromBase64String(desrlizedResponse.FirstOrDefault().Value);
            return Encoding.UTF8.GetString(base64EncodedBytes);

        }

        #endregion
    }
}
