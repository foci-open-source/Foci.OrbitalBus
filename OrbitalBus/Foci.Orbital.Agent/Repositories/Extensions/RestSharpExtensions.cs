﻿using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.Repositories.Extensions
{
    /// <summary>
    /// Retrieves information from a response and returns it in a appropriate format.
    /// </summary>
    public static class RestSharpExtensions
    {
        /// <summary>
        /// Returns RestSharp response properties values as a Dictionary for the creation of a successful or failure response payload.
        /// </summary>
        /// <param name="response">RestSharp response to be interpreted.</param>
        /// <returns>Dictionary with RestSharp response properties values.</returns>
        public static Dictionary<string, string> ToDictionary(this IRestResponse response)
        {
            if (response == null)
            {
                throw new System.ArgumentNullException(nameof(response));
            }

            return response.GetType().GetProperties().ToDictionary
                (
                    property => property.Name,
                    property => property.GetValue(response) != null
                                            ? property.Name != "Content"
                                                   ? property.GetValue(response).ToString()
                                                   : "See payload value or fault"
                                            : "null"
                );
        }

        /// <summary>
        /// Interprets the status code from the RestSharp response and returns a flag indicating if the RestSharp response is a success or failure.
        /// </summary>
        /// <param name="response">RestSharp response to be interpreted.</param>
        /// <returns>Flag indicating if the RestSharp response is a success or failure.</returns>
        public static bool IsSuccessfulStatusCode(this IRestResponse response)
        {
            if (response == null)
            {
                throw new System.ArgumentNullException(nameof(response));
            }

            int numericResponse = (int)response.StatusCode;
            return numericResponse >= 200
                && numericResponse <= 399;
        }

        /// <summary>
        /// Interprets if the response status code is a success and complete, and returns a flag indicating if the RestSharp response is a success or failure.
        /// </summary>
        /// <param name="response">RestSharp response to be interpreted.</param>
        /// <returns>Flag indicating if the RestSharp response is a success or failure.</returns>
        public static bool IsSuccessful(this IRestResponse response)
        {
            if (response == null)
            {
                throw new System.ArgumentNullException(nameof(response));
            }

            return response.IsSuccessfulStatusCode()
                && response.ResponseStatus == ResponseStatus.Completed;
        }
    }
}
