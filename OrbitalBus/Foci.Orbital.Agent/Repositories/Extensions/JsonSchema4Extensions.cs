﻿using Foci.Orbital.Agent.Exceptions;
using Newtonsoft.Json;
using NJsonSchema;
using NJsonSchema.Validation;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.Repositories.Extensions
{
    /// <summary>
    /// Extensions for the JsonSchema4 class.
    /// </summary>
    public static class JsonSchema4Extensions
    {
        /// <summary>
        /// Validates a json message against a schema.
        /// </summary>
        /// <param name="schema">The schema to use for validation</param>
        /// <param name="msg">The Json to validate</param>
        /// <returns>True if message matches the schema, false otherwise</returns>
        internal static List<Exception> ValidateMessage(this JsonSchema4 schema, string msg)
        {
            ILogger logger = LogManager.GetCurrentClassLogger();
            if (msg == null)
            {
                var error = "In order to perform validation, the message cannot be null";
                logger.Warn("ValidateMessage : {0}", error);
                return new List<Exception>() { new OrbitalValidationException(error) };
            }

            logger.Info("ValidateMessage : Validating JSON message against schema");
            try
            {
                var result = schema.Validate(msg) ?? new List<ValidationError>();
                if (result.Any())
                {
                    logger.Warn("ValidateMessage : Message does not match the provided schema");
                    return result.Select(error => (Exception)new OrbitalValidationException(error.ToString())).ToList();
                }
            }
            catch (JsonReaderException jre)
            {
                logger.Warn("ValidateMessage : {0}", jre.Message);
                return new List<Exception>() { new OrbitalValidationException(jre.Message) };
            }

            return new List<Exception>();
        }
    }
}
