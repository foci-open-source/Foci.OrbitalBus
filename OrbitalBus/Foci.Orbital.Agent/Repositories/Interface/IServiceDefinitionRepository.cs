﻿using Foci.Orbital.Agent.Models.Service;

namespace Foci.Orbital.Agent.Repositories.Interface
{
    /// <summary>
    /// Responsible for handling any ServiceDefinition functionalities
    /// </summary>
    public interface IServiceDefinitionRepository
    {
        /// <summary>
        /// Get a service definition model by service name.
        /// </summary>
        /// <param name="serviceName">the unique service name</param>
        /// <returns></returns>
        ServiceDefinition GetServiceDefinitionByName(string serviceName);

        void RegisterToServiceDiscovery(string serviceName);
    }
}
