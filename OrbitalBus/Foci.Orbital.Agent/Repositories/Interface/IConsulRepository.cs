﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul.Requests;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.Agent.Repositories.Interface
{
    /// <summary>
    /// Interacts directly with consul.
    /// </summary>
    internal interface IConsulRepository
    {
        /// <summary>
        /// Registers a service with Consul.
        /// </summary>
        /// <param name="consulHostAddress">The address of Consul, including host and port.</param>
        /// <param name="cert">Certificate to be used if Consul is secure.</param>
        /// <param name="registerServiceRequest">The service information to register with Consul.</param>
        /// <returns>Flag indicating success or failure result</returns>
        bool RegisterService(RegisterServiceRequest registerServiceRequest, string consulHostAddress, Maybe<X509Certificate> cert);

        /// <summary>
        /// Add a key value to the Consul key-value store.
        /// </summary>
        /// <param name="key">The key against which to store the value.</param>
        /// <param name="value">The value to store.</param>
        /// <param name="consulHostAddress">The address of Consul, including host and port.</param>
        /// <param name="cert">Certificate to be used if Consul is secure.</param>
        /// <returns>True if successful.</returns>
        bool SetKeyValue(string key, string value, string consulHostAddress, Maybe<X509Certificate> cert);

        /// <summary>
        /// Get a value from Consul key-value store using a key.
        /// </summary>
        /// <param name="key">The key against which to store the value.</param>
        /// <param name="consulHostAddress">The address of Consul, including host and port.</param>
        /// <param name="cert">Certificate to be used if Consul is secure.</param>
        /// <returns>True if successful.</returns>
        string GetKeyValue(string key, string consulHostAddress, Maybe<X509Certificate> cert);
    }
}
