﻿using EasyNetQ;
using Foci.Orbital.Agent.Models;

namespace Foci.Orbital.Agent.Repositories.Interface
{
    public interface IEasyNetQRepository
    {
        /// <summary>
        /// Create a bus using EasyNetQ RabbitHutch static methods.
        /// </summary>
        /// <param name="connectionDetails">The required EasyNetQ connection details.</param>
        /// <returns>A new IBus</returns>
        IBus CreateBus(BusConnectionDetails connectionDetails);
    }
}
