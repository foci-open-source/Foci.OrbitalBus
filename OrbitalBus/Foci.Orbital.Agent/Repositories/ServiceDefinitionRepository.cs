﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul.Requests;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Models.Service.ServiceContract;
using Foci.Orbital.Agent.Policies.Consul;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interfaces;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Foci.Orbital.Agent.Repositories
{
    [ExcludeFromCodeCoverage]
    internal class ServiceDefinitionRepository : IServiceDefinitionRepository
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IConsulRepository consulRepository;
        private readonly IConfigurationService config;
        private const string Contract_Key = "orbital.{0}.contract";
        private const string BROKER_ID = "orbital.{0}.broker";
        private const string BROKER_PRIVATE_ID = "orbital.{0}.broker.private";
        private const string HTTP_ID = "orbital.{0}.http";

        public ServiceDefinitionRepository() : this(new ConsulRepository(), new ConfigurationService()) { }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ServiceDefinitionRepository(IConsulRepository consulRepository, IConfigurationService config)
        {
            this.consulRepository = consulRepository ?? throw new ArgumentNullException(nameof(consulRepository));
            this.config = config ?? throw new ArgumentNullException(nameof(config));
        }
        /// <inheritdoc />       
        public ServiceDefinition GetServiceDefinitionByName(string serviceName)
        {
            var json = string.Empty;
            if (string.IsNullOrEmpty(serviceName))
            {
                logger.Warn("ServiceDefinitionRepository:: GetServiceDefinitionByName: service name shouldn't be null or empty");
            }
            else
            {
                json = consulRepository.GetKeyValue(serviceName, GetConsulHostAddress(), GetX509Certificate());
            }
            return this.LoadFromJSON(json);
        }

        public void RegisterToServiceDiscovery(string serviceName)
        {
            logger.Info("ServiceDefinitionRepository:: Registering all {0} addresses", serviceName);
            var serviceDefinition = GetServiceDefinitionByName(serviceName);

            //Get Service Contract
            var serviceContract = ExtractServiceContract(serviceDefinition);
            var serviceContractName = string.Format(Contract_Key, serviceName);
            SetKeyValue(serviceContractName, serviceContract);

            //Register Services Addresses
            RegisterAllServicesAddresses(serviceName);

        }

        #region private 

        private void RegisterAllServicesAddresses(string serviceName)
        {
            var publicConfig = config.GetConfigurationProperties().PublicConfiguration;
            var rabbitConfiguration = config.GetConfigurationProperties().BrokerConfiguration;

            var brokerIP = publicConfig.BrokerIp;
            var brokerPort = (ushort)publicConfig.BrokerPort;
            var privateBrokerIP = rabbitConfiguration.BusHostIp;
            var privateBrokerPort = (ushort)rabbitConfiguration.BusHostPort;
            var httpIP = publicConfig.HttpIp;
            var httpPort = (ushort)publicConfig.HttpPort;

            RegisterService(BROKER_ID, serviceName, brokerIP, brokerPort);
            RegisterService(BROKER_PRIVATE_ID, serviceName, privateBrokerIP, privateBrokerPort);
            RegisterService(HTTP_ID, serviceName, httpIP, httpPort);

        }

        /// <summary>
        /// Register provided service to Consul
        /// </summary>
        /// <param name="idFormat">Format string for the ID</param>
        /// <param name="serviceName">String representation of service name</param>
        /// <param name="ip">String representation of ip address</param>
        /// <param name="port">ushort representation of port</param>
        /// <returns>Consul registration result</returns>
        private void RegisterService(string idFormat, string serviceName, string ip, ushort port)
        {
            var id = string.Format(idFormat, serviceName);
            var registerService = new RegisterServiceRequest(id, serviceName, ip, port);
            var isSuccessful = ConsulPolicy.Execute(c => this.consulRepository.RegisterService(registerService, GetConsulHostAddress(), GetX509Certificate()),
                                                                 new ConsulData("RegisterService", "Register a service with Consul"));
            if (!isSuccessful)
            {
                this.logger.Warn("{0} was not registered with {1}:{2} on the Consul host {3}",
                registerService.Name,
                registerService.Address,
                registerService.Port,
                config.GetConfigurationProperties().ConsulConfiguration.HostIp);
            }
            else
            {
                this.logger.Info("{0} was successfully registered with {1}:{2} on the Consul host {3}",
                registerService.Name,
                registerService.Address,
                registerService.Port,
                config.GetConfigurationProperties().ConsulConfiguration.HostIp);
            }
            
        }
        private bool SetKeyValue(string key, string value)
        {
            if (string.IsNullOrEmpty(key))
            {
                logger.Warn("SetKeyValue: Key shouldn't be null or empty");
                return false;
            }

            if (string.IsNullOrEmpty(value))
            {
                logger.Warn("SetKeyValue: Value shouldn't be null or empty");
                return false;
            }

            return ConsulPolicy.Execute(c => this.consulRepository.SetKeyValue(key, value, GetConsulHostAddress(), GetX509Certificate()),
                                                                 new ConsulData("SetKeyValue", "Add an entry to Consul key value store"));
        }
        /// <summary>
        /// This method extracts the service contract json string from the ServiceDefinition object
        /// </summary>
        /// <param name="service">The service definition object</param>
        /// <returns>The service contract json string</returns>
        private string ExtractServiceContract(ServiceDefinition service)
        {
            var contract = new ServiceContract
            {
                Operations = new List<OperationDescription>(),
                ServiceName = service.ServiceName
            };
            foreach (var operation in service.Operations)
            {
                contract.Operations.Add(new OperationDescription()
                {
                    Name = operation.Name,
                    IsAsync = operation.IsSync,
                    RequestSchema = operation.Schemas.Request,
                    ResponseSchema = operation.Schemas.Response
                });
            }

            return JsonConvert.SerializeObject(contract);
        }

        //Load configuration from json
        private ServiceDefinition LoadFromJSON(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                throw new ArgumentException("JSON string cannot be empty or null.");
            }

            return JsonConvert.DeserializeObject<ServiceDefinition>(json);
        }

        // Get Consul host address from configuration
        private string GetConsulHostAddress()
        {
            var configurationProperties = config.GetConfigurationProperties();

            return configurationProperties.ConsulConfiguration.SslEnabledConsul
                ? string.Format("https://{0}:{1}", configurationProperties.ConsulConfiguration.HostIp, configurationProperties.ConsulConfiguration.HostPort.ToString())
                : string.Format("http://{0}:{1}", configurationProperties.ConsulConfiguration.HostIp, configurationProperties.ConsulConfiguration.HostPort.ToString());
        }

        // Get certificate from configuration
        private Maybe<X509Certificate> GetX509Certificate()
        {
            string certString = config.GetConfigurationProperties().ConsulConfiguration.Authorization != null ?
                config.GetConfigurationProperties().ConsulConfiguration.Authorization.PlainText :
                string.Empty;
            if (string.IsNullOrWhiteSpace(certString))
            {
                return Maybe<X509Certificate>.None;
            }

            try
            {
                byte[] rawCert = Encoding.ASCII.GetBytes(certString);
                X509Certificate cert = new X509Certificate(rawCert);
                return cert.ToMaybe();
            }
            catch (Exception)
            {
                return Maybe<X509Certificate>.None;
            }
        }
        #endregion
    }
}
