﻿using EasyNetQ;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.OrbitalEasyNetQ;
using Foci.Orbital.Agent.Repositories.Interface;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net.Security;

namespace Foci.Orbital.Agent.Repositories
{
    [ExcludeFromCodeCoverage]
    public class EasyNetQRepository : IEasyNetQRepository
    {
        private ILogger log = LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Create a bus using EasyNetQ RabbitHutch static methods.
        /// </summary>
        /// <param name="connectionDetails">The required EasyNetQ connection string.</param>
        /// <returns>A new IBus</returns>
        public IBus CreateBus(BusConnectionDetails connectionDetails)
        {
            var connection = new ConnectionConfiguration();


            var host1 = new HostConfiguration
            {
                Host = connectionDetails.Host,
                Port = ushort.Parse(connectionDetails.Port)
            };

            if (connectionDetails.SslEnabled)
            {
                connection.UserName = connectionDetails.Username;
                connection.Password = connectionDetails.Password;
                host1.Ssl.CertPath = String.Empty; //Place Holder. After implementing certificate security in RabbitMQ, replace with the appropriate properties.
                host1.Ssl.CertPassphrase = String.Empty;//Place Holder. After implementing certificate security in RabbitMQ, replace with the appropriate properties.
                host1.Ssl.ServerName = String.Empty;//Place Holder. After implementing certificate security in RabbitMQ, replace with the appropriate properties.
                host1.Ssl.Enabled = connectionDetails.SslEnabled;
                host1.Ssl.AcceptablePolicyErrors = SslPolicyErrors.RemoteCertificateNotAvailable |
                    SslPolicyErrors.RemoteCertificateNameMismatch |
                   SslPolicyErrors.RemoteCertificateChainErrors;
            }
            connection.Hosts = new List<HostConfiguration> { host1 };

            var bus = RabbitHutch.CreateBus(connection,

                serviceProvider =>
                {
                    serviceProvider.Register<ITypeNameSerializer>(serviceCreator => new OrbitalTypeNameSerializer());
                    serviceProvider.Register<ISerializer>(serviceCreator => new OrbitalJsonSerializer(serviceCreator.Resolve<ITypeNameSerializer>()));
                }
                );

            if (!bus.IsConnected)
            {
                log.Warn("Bus created successfully but is not connected to the RabbitMQ broker");
            }
            return bus;
        }
    }
}
