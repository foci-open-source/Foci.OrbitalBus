﻿using Foci.Orbital.Agent.Models;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace Foci.Orbital.Agent.App_Start
{
    [ExcludeFromCodeCoverage]
    public class FileSystemReadiness
    {
        private readonly string assemblyDirectory;

        public FileSystemReadiness()
        {
            UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
            assemblyDirectory = Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
        }

        /// <summary>
        /// Check if all required folders exist, and create any that doesn't exist
        /// </summary>
        /// <returns>True if successfully created the folders or no action needed, false otherwise</returns>
        public bool CheckFileSystem()
        {
            try
            {
                CreateDirectory(AgentConstants.CONFIG_PATH);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        //If the given directory doesn't exist, create it
        private void CreateDirectory(string path)
        {
            string dir = Path.Combine(assemblyDirectory, path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }
    }
}
