﻿using EasyNetQ;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Database.Services;
using Foci.Orbital.Adapters.Mock.Services;
using Foci.Orbital.Adapters.Rest.Services;
using Foci.Orbital.Agent.App_Start;
using Foci.Orbital.Agent.HttpApi.Services;
using Foci.Orbital.Agent.HttpApi.Services.Interfaces;
using Foci.Orbital.Agent.OperationHandler;
using Foci.Orbital.Agent.OrbitalEasyNetQ;
using Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Pipelines.Generic.Models.Interfaces;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using Foci.Orbital.Agent.Repositories;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using Unity;
using Unity.Interception.ContainerIntegration;
using Unity.Lifetime;

namespace Foci.Orbital.Agent
{
    /// <summary>
    /// DependencyInjector class is responsible for registering and injecting the Agent's discrepancies. 
    /// </summary>
    public static class DependencyInjector
    {
        public static IUnityContainer Container = new UnityContainer();

        public static void RegisterDependencies()
        {
            //Each interface requires the registration of the interceptor. This allows attribute handlers to work.
            //Container implementation starts here
            Container.AddNewExtension<Interception>();

            //App start
            Container.RegisterType<Agent>();
            Container.RegisterType<FileSystemReadiness>();//TODO remove it later

            //Adapters
            Container.RegisterType<IAdapter, RestAdapter>("Adapters.Rest");
            Container.RegisterType<IAdapter, MockAdapter>("Adapters.Mock");
            Container.RegisterType<IAdapter, DatabaseAdapter>("Adapters.Database");

            //Repositories
            Container.RegisterType<IConsulRepository, ConsulRepository>();
            Container.RegisterType<IServiceDefinitionRepository, ServiceDefinitionRepository>();
            Container.RegisterType<IEasyNetQRepository, EasyNetQRepository>();

            //Services
            Container.RegisterType<IConsulService, ConsulService>();
            Container.RegisterType<IBusManager, BusManager>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IJsonTranslationService, JsonTranslationService>();
            Container.RegisterType<IBusService, BusService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IServiceDefinitionService, ServiceDefinitionService>();
            Container.RegisterType<IPathCreator, RequestPathService>();
            Container.RegisterType<IServiceValidator, RequestPathService>();

            //Singleton services
            Container.RegisterType<IConfigurationService, ConfigurationService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ServiceCache>();

            //Handler
            Container.RegisterType<IOperationsHandler, OperationsHandler>();

            //Singleton pipelines
            Container.RegisterType<IPipeline<MessageProcessorInput>, AsyncMessageProcessor>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IPipeline<MessageProcessorInput, Payload>, SyncMessageProcessor>(new ContainerControlledLifetimeManager());

            //Serializers
            Container.RegisterType<ITypeNameSerializer, OrbitalTypeNameSerializer>();
            Container.RegisterType<ISerializer, OrbitalJsonSerializer>();
        }
    }
}