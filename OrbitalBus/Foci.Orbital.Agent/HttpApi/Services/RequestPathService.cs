﻿using Foci.Orbital.Agent.HttpApi.Services.Interfaces;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Policies.Cache;
using Foci.Orbital.Agent.Repositories.Interface;
using Microsoft.AspNetCore.Http;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.HttpApi.Services
{
    public class RequestPathService : IServiceValidator, IPathCreator
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IServiceDefinitionRepository serviceDefinitionRepository;

        /// <summary>
        /// Default constructor that takes necessary services
        /// </summary>
        /// <param name="memCacheService">Memory cache service to get the service definition</param>
        public RequestPathService(IServiceDefinitionRepository serviceDefinitionRepository)
        {
            this.serviceDefinitionRepository = serviceDefinitionRepository ?? throw new ArgumentNullException(nameof(serviceDefinitionRepository));
        }
        /// <inheritdoc />
        public PathString GetAdditionalPath(PathString path, string verb)
        {
            var paths = ToPathArray(path);
            var service = GetService(paths[0]);
            var operation = GetOperation(service, GetOperationName(paths), verb);

            return (paths.Length > 1) ? new PathString($"/{operation.IsSync}") : new PathString($"/{operation.Name}/{operation.IsSync}");
        }

        /// <inheritdoc />
        public IEnumerable<string> Validate(PathString path, string verb)
        {
            var result = new List<string>();
            ServiceDefinition service;
            string operationName;
            try
            {
                var paths = ToPathArray(path);
                service = GetService(paths[0]);
                operationName = GetOperationName(paths);
            }
            catch (ArgumentException ae)
            {
                result.Add(ae.Message);
                return result;
            }

            return (operationName == null) ? ValidateVerbOnlyOperation(service, verb) : ValidateOperation(service, operationName, verb);
        }

        //Validate that the request operations exists and is available, return a list of error messages if its not found
        private IEnumerable<string> ValidateOperation(ServiceDefinition service, string operationName, string verb)
        {
            var result = new List<string>();
            logger.Info("RequestPathService :: ValidateOperation: Attempt to find operation {0} for {1} using {2}.", operationName, service.ServiceName, verb);

            try
            {
                var operation = GetOperation(service, operationName, verb);
                if (!operation.Http.Verb.Equals(verb, StringComparison.InvariantCultureIgnoreCase))
                {
                    var error = string.Format("{0} operation does not support {1} method", operationName, verb);
                    logger.Warn("RequestPathService :: ValidateOperation: {0}.", error);
                    result.Add(error);
                }
            }
            catch (ArgumentException ae)
            {
                result.Add(ae.Message);
            }

            return result;
        }

        //Get the operation name of the verb only method, return list of error messages if not found
        private IEnumerable<string> ValidateVerbOnlyOperation(ServiceDefinition service, string verb)
        {
            var result = new List<string>();
            logger.Info("RequestPathService :: ValidateVerbOnlyOperation: Operation name isn't provided for {0}, check verb only operations using {1}.", service.ServiceName, verb);
            try
            {
                GetOperation(service, null, verb);
            }
            catch (ArgumentException ae)
            {
                result.Add(ae.Message);
            }

            return result;
        }

        //Convert path string to a string array
        private string[] ToPathArray(PathString path)
        {
            var paths = path.Value.Split('/');
            if (paths.Length < 2)
            {
                var error = "Service name is needed in order to process the request";
                logger.Warn("RequestPathService :: ExtractServices: {0}.", error);
                throw new ArgumentException(error);
            }

            paths = paths.Skip(1).ToArray();// Remove empty string at the beginning of the array
            if (paths.Length > 2)
            {
                var error = "The request path cannot have more than two URL segments";
                logger.Warn("RequestPathService :: ExtractServices: {0}.", error);
                throw new ArgumentException(error);
            }

            return paths;
        }

        // Get service using the provided service name
        private ServiceDefinition GetService(string serviceName)
        {
            ServiceDefinition service;
            try
            {
                service = ServiceCachePolicy.Get((context) => serviceDefinitionRepository.GetServiceDefinitionByName(serviceName), serviceName);
            }
            catch (ArgumentException ae)
            {
                var error = string.Format("Unable to retrieve the service definition due to {0}", ae.Message);
                logger.Warn("RequestPathService :: GetService: {0}.", error);
                throw new ArgumentException(error);
            }

            if (service == null)
            {
                var error = string.Format("Unable to retrieve the service definition due to service isn't registered with Consul");
                logger.Warn("RequestPathService :: GetService: {0}.", error);
                throw new ArgumentException(error);
            }

            return service;
        }

        // Extract operation name from path array
        private string GetOperationName(string[] paths)
        {
            return (paths.Length > 1) ? paths[1] : null;
        }

        // Get operation definition for the given operation name;
        // If operation name is null, this method will use the provided verb to find matching verb only operation.
        private OperationDefinition GetOperation(ServiceDefinition service, string operationName, string verb)
        {
            if (operationName == null)
            {
                var operations = service.Operations
                   .Where(o => o.Http.IsEnabled && o.Http.VerbOnly && o.Http.Verb.Equals(verb, StringComparison.InvariantCultureIgnoreCase))
                   .ToList();
                if (!operations.Any() || operations.Count() > 1)
                {
                    var error = string.Format("A service should have one verb only operation for {0}", verb);
                    logger.Warn("RequestPathService :: GetOperation: {0}.", error);
                    throw new ArgumentException(error);
                }
                return operations.First();
            }

            var operation = service.Operations.FirstOrDefault(o => o.Http.IsEnabled && o.Name == operationName);
            if (operation == null)
            {
                var error = string.Format("Unable to find any operation that matches the given conditions");
                logger.Warn("RequestPathService :: GetOperation: {0}.", error);
                throw new ArgumentException(error);
            }

            return operation;
        }
    }
}
