﻿using Microsoft.AspNetCore.Http;

namespace Foci.Orbital.Agent.HttpApi.Services.Interfaces
{
    public interface IPathCreator
    {
        /// <summary>
        /// If the operation is a verb only, this will return the path containing the operation name and is sync;
        /// Otherwise, this returns the path and indicates if the operation is sync or not.
        /// </summary>
        /// <returns>Path string</returns>
        PathString GetAdditionalPath(PathString path, string verb);
    }
}
