﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.HttpApi.Services.Interfaces
{
    public interface IServiceValidator
    {
        /// <summary>
        /// Validate if the requested service and operation is valid (i.e., exist and available)
        /// </summary>
        /// <param name="path">Path string containing the request header</param>
        /// <param name="verb">Verb of the request</param>
        /// <returns>List of error messages if invalid; Empty list if valid</returns>
        IEnumerable<string> Validate(PathString path, string verb);
    }
}
