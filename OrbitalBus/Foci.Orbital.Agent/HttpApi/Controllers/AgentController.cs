﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Requests;
using Foci.Orbital.Agent.OperationHandler;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.HttpApi.Controllers
{
    [ApiController]
    public class AgentController : ControllerBase
    {
        private readonly IOperationsHandler operationsHandler;

        /// <summary>
        /// Default constructor to take in operation handler
        /// </summary>
        /// <param name="operationsHandler">Operation handler for messaging processing</param>
        public AgentController(IOperationsHandler operationsHandler)
        {
            this.operationsHandler = operationsHandler;
        }

        /// <summary>
        /// Method for receiving body from query string
        /// </summary>
        /// <param name="serviceName">String representation of service name</param>
        /// <param name="operationName">String representation of operation name</param>
        /// <param name="isSync">True if the operation is sync, false otherwise</param>
        /// <param name="body">Request message body</param>
        /// <returns>Result from pipeline with proper status code</returns>
        [HttpGet("{serviceName}/{operationName}/{isSync}")]
        [HttpDelete("{serviceName}/{operationName}/{isSync}")]
        public IActionResult Get(string serviceName, string operationName, bool isSync, [FromQuery]string body)
        {
            return PerformInvoke(serviceName, operationName, isSync, body);
        }

        /// <summary>
        /// Method for receiving body from http body
        /// </summary>
        /// <param name="serviceName">String representation of service name</param>
        /// <param name="operationName">String representation of operation name</param>
        /// <param name="isSync">True if the operation is sync, false otherwise</param>
        /// <param name="body">Request message body</param>
        /// <returns>Result from pipeline with proper status code</returns>
        [HttpPut("{serviceName}/{operationName}/{isSync}")]
        [HttpPost("{serviceName}/{operationName}/{isSync}")]
        [HttpPatch("{serviceName}/{operationName}/{isSync}")]
        public IActionResult Post(string serviceName, string operationName, bool isSync, [FromBody]string body)
        {
            return PerformInvoke(serviceName, operationName, isSync, body);
        }

        //Perform invoke operation using the provided request
        private IActionResult PerformInvoke(string serviceName, string operationName, bool isSync, string body)
        {
            var request = CreateRequest(serviceName, operationName, body);
            if (isSync)
            {
                var result = operationsHandler.InvokeSyncOperation(request);
                if (result.Match<bool>(r => false, e => true))
                {
                    return BadRequest(result);
                }
                return Ok(result);
            }
            operationsHandler.InvokeAsyncOperation(request);
            return Ok("Success!");
        }

        //Create request using the given service name, operation name, and message
        private OrbitalRequest CreateRequest(string serviceName, string operationName, string message)
        {
            return new OrbitalRequest()
            {
                Headers = new Dictionary<string, string>(){
                    { AgentConstants.SERVICE_NAME_KEY, serviceName },
                    { AgentConstants.OPERATION_NAME_KEY, operationName }
                },
                Body = message
            };
        }
    }
}
