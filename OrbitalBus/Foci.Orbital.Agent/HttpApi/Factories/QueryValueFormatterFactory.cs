﻿using Foci.Orbital.Agent.HttpApi.Middlewares.Formatters;
using Foci.Orbital.Agent.Policies.Cache;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Foci.Orbital.Agent.HttpApi.Factories
{
    [ExcludeFromCodeCoverage]
    public class QueryValueFormatterFactory : IValueProviderFactory
    {
        private readonly IServiceDefinitionRepository serviceDefinitionRepository;

        public QueryValueFormatterFactory(IServiceDefinitionRepository serviceDefinitionRepository)
        {
            this.serviceDefinitionRepository = serviceDefinitionRepository;
        }

        /// <inheritdoc />
        public Task CreateValueProviderAsync(ValueProviderFactoryContext context)
        {
            var request = context.ActionContext.HttpContext.Request;

            var paths = request.Path.Value.Split('/').Skip(1).SkipLast(1).ToArray();
            var service = ServiceCachePolicy.Get((contexts) => serviceDefinitionRepository.GetServiceDefinitionByName(paths[0]), paths[0]);
            var operation = service.Operations.FirstOrDefault(o => o.Name == paths[1]);

            context.ValueProviders.Insert(0, new QueryValueFormatter(BindingSource.Query,
                operation, request.Query,
                CultureInfo.InvariantCulture));
            return Task.CompletedTask;
        }
    }
}
