﻿using Foci.Orbital.Agent.HttpApi.Factories;
using Foci.Orbital.Agent.HttpApi.Middlewares;
using Foci.Orbital.Agent.HttpApi.Middlewares.Formatters;
using Foci.Orbital.Agent.Repositories.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.CodeAnalysis;
using Unity;

namespace Foci.Orbital.Agent.HttpApi.Startup
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore(options =>
            {
                options.InputFormatters.Insert(0, new RequestBodyFormatter());
                options.ValueProviderFactories.Insert(0, new QueryValueFormatterFactory(DependencyInjector.Container.Resolve<IServiceDefinitionRepository>()));
            })
                .AddJsonFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Map("/favicon.ico", delegate { })
               .UseMiddleware<RequestPathMiddleware>()
               .UseMvc();
        }
    }
}
