﻿using Foci.Orbital.Agent.Models.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using NJsonSchema;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Foci.Orbital.Agent.HttpApi.Middlewares.Formatters
{
    public class QueryValueFormatter : QueryStringValueProvider
    {
        private readonly IQueryCollection queries;
        private readonly IReadOnlyDictionary<string, JsonProperty> schema;

        /// <summary>
        /// Default constructor to take in query collection
        /// </summary>
        /// <param name="bindingSource">Required by base class</param>
        /// <param name="values">The query collection</param>
        /// <param name="culture">Required by base class</param>
        public QueryValueFormatter(BindingSource bindingSource, OperationDefinition operation, IQueryCollection values, CultureInfo culture) : base(bindingSource, values, culture)
        {
            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            var schema = JsonSchema4.FromJsonAsync(operation.Schemas.Request ?? string.Empty).Result;
            this.schema = schema.ActualProperties;
            queries = values;
        }

        /// <inheritdoc />
        public override ValueProviderResult GetValue(string key)
        {
            var map = new Dictionary<string, object>();
            foreach (var q in queries)
            {
                var propertyName = q.Key;
                object value = q.Value.ToString();
                if (schema.TryGetValue(propertyName, out var property))
                {
                    value = Cast(property.Type, q.Value);
                }
                map.Add(propertyName, value);
            }

            var result = JsonConvert.SerializeObject(map);
            return new ValueProviderResult(result);
        }

        // Cast the given value to the given type
        private object Cast(JsonObjectType type, string value)
        {
            switch (type)
            {
                case JsonObjectType.Integer:
                case JsonObjectType.Number:
                    if (double.TryParse(value, out double dvalue))
                    {
                        //If double is int, it as int
                        if ((dvalue % 1) == 0)
                        {
                            return (int)dvalue;
                        }
                        return dvalue;
                    }
                    break;
                case JsonObjectType.Boolean:
                    if (bool.TryParse(value, out bool bvalue))
                    {
                        return bvalue;
                    }
                    break;
            }
            return value;
        }
    }
}
