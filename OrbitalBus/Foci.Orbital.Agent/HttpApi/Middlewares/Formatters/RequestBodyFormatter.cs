﻿using Foci.Orbital.Agent.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Threading.Tasks;

namespace Foci.Orbital.Agent.HttpApi.Middlewares.Formatters
{
    /// <summary>
    /// This formatter takes the request body stream from http context 
    /// then extract all contents from the stream and return it as string
    /// 
    /// Note: this formatter only accepts text/plain, application/json, or no content type
    /// </summary>
    public class RequestBodyFormatter : InputFormatter
    {
        /// <summary>
        /// Formatter that allows content of type text/plain and application/json
        /// or no content type to be parsed to raw data.
        /// </summary>
        public RequestBodyFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(AgentConstants.MEDIA_TYPE_TEXT_PLAIN));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(AgentConstants.MEDIA_TYPE_APP_JSON));
        }

        /// <inheritdoc />
        public override bool CanRead(InputFormatterContext context)
        {
            if (context == null)
            {
                return false;
            }

            var contentType = context.HttpContext.Request.ContentType;

            return string.IsNullOrEmpty(contentType) || contentType == AgentConstants.MEDIA_TYPE_TEXT_PLAIN ||
                contentType == AgentConstants.MEDIA_TYPE_APP_JSON;
        }

        /// <inheritdoc />
        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            var request = context.HttpContext.Request;
            using (var reader = new StreamReader(request.Body))
            {
                var content = await reader.ReadToEndAsync();
                return await InputFormatterResult.SuccessAsync(content);
            }
        }
    }
}
