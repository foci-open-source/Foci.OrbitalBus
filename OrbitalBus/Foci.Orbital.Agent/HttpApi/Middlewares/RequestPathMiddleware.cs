﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.HttpApi.Services.Interfaces;
using Foci.Orbital.Agent.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foci.Orbital.Agent.HttpApi.Middlewares
{
    /// <summary>
    /// The middleware that validates if the path is valid; If valid, this class will add additional segments to the path
    /// </summary>
    public class RequestPathMiddleware
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly AgentFaultFactory faultFactory = new AgentFaultFactory();

        private readonly IPathCreator pathCreator;
        private readonly IServiceValidator serviceValidator;
        private readonly RequestDelegate next;

        /// <summary>
        /// Default constructor that takes in the necessary services
        /// </summary>
        public RequestPathMiddleware(IPathCreator pathCreator, IServiceValidator serviceValidator, RequestDelegate next)
        {
            this.pathCreator = pathCreator ?? throw new ArgumentNullException(nameof(pathCreator));
            this.serviceValidator = serviceValidator ?? throw new ArgumentNullException(nameof(serviceValidator));
            this.next = next ?? throw new ArgumentNullException(nameof(next));
        }

        /// <summary>
        /// Invoke the validation and path transform. 
        /// If the service and operation is valid, continue; Otherwise, return 400 error
        /// </summary>
        /// <param name="context">The HTTP context containing the request</param>
        /// <returns>next task or 400 error</returns>
        public Task Invoke(HttpContext context)
        {
            var verb = context.Request.Method;
            var path = context.Request.Path;

            var errors = serviceValidator.Validate(path, verb);
            if (errors.Any())
            {
                return SendError(context, errors.Select(e => (Exception)new OrbitalValidationException(e)).ToArray());
            }

            try
            {
                var additionalPath = pathCreator.GetAdditionalPath(path, verb);
                context.Request.Path = context.Request.Path.Add(additionalPath);
            }
            catch (ArgumentException ae)
            {
                return SendError(context, ae);
            }

            return next(context);
        }

        //Parse the given exceptions as payload and send that payload as 400 body
        private async Task SendError(HttpContext context, params Exception[] exceptions)
        {
            var faults = new List<Fault>();
            foreach (var e in exceptions)
            {
                faults.Add(faultFactory.CreateFault(e));
            }

            var result = JsonConvert.SerializeObject(Payload.Create(faults));
            context.Response.ContentType = AgentConstants.MEDIA_TYPE_APP_JSON;
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsync(result);
        }
    }
}
