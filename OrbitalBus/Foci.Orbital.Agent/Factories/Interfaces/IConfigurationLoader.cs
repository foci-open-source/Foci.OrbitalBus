﻿namespace Foci.Orbital.Agent.Factories.Interfaces
{
    public interface IConfigurationLoader
    {
        /// <summary>
        /// Gets the configuration as a json string
        /// </summary>
        /// <returns>json string representation of the configuration</returns>
       string GetConfiguration();

    }
}
