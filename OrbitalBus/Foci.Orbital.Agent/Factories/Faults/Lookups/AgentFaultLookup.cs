﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Lookups;
using Foci.Orbital.Agent.Exceptions;
using Jint.Runtime;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Factories.Faults.Lookups
{
    /// <inheritdoc/>
    [ExcludeFromCodeCoverage]
    internal class AgentFaultLookup : FaultLookup
    {
        /// <summary>
        /// Constructor to initialize agent fault look up with more exceptions
        /// </summary>
        public AgentFaultLookup()
        {
            faultLookup.Add(typeof(OrbitalCommunicationException), OrbitalFaultCode.Orbital_Runtime_CommunicationFault_001);
            faultLookup.Add(typeof(OrbitalTranslationException), OrbitalFaultCode.Orbital_Runtime_TranslationFault_002);
            faultLookup.Add(typeof(OrbitalUnknownEnumException), OrbitalFaultCode.Orbital_Runtime_InternalFault_003);
            faultLookup.Add(typeof(OrbitalLookUpException), OrbitalFaultCode.Orbital_Runtime_InternalFault_003);
            faultLookup.Add(typeof(OrbitalUnhandledException), OrbitalFaultCode.Orbital_UnhandledFault);
            faultLookup.Add(typeof(OrbitalAggregateException), OrbitalFaultCode.Orbital_Runtime_AggregateFault_004);
            faultLookup.Add(typeof(OrbitalArgumentException), OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005);
            faultLookup.Add(typeof(OrbitalBusinessException), OrbitalFaultCode.Orbital_BusinessFault_010);
            faultLookup.Add(typeof(OrbitalFileException), OrbitalFaultCode.Orbital_Runtime_FileFault_008);
            faultLookup.Add(typeof(JavaScriptException), OrbitalFaultCode.Orbital_Business_Translation_Error_013);
            faultLookup.Add(typeof(OrbitalOperationHandlerException), OrbitalFaultCode.Orbital_Operation_Handler_Error_014);
            faultLookup.Add(typeof(OrbitalValidationException), OrbitalFaultCode.Orbital_Business_Validation_Error_015);
        }
    }
}
