﻿using Foci.Orbital.Adapters.Contract.Faults.Factories;
using Foci.Orbital.Agent.Factories.Faults.Lookups;

namespace Foci.Orbital.Agent.Factories.Faults
{
    /// <summary>
    /// A factory that creates all runtime faults.
    /// </summary>
    internal class AgentFaultFactory : FaultFactory
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public AgentFaultFactory() : base(new AgentFaultLookup()) { }
    }
}
