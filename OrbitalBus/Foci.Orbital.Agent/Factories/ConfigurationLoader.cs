﻿using Foci.Orbital.Agent.Factories.Interfaces;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Repositories.Extensions;
using Foci.Orbital.Agent.Repositories.Loaders;
using Foci.Orbital.Agent.Repositories.Loaders.Interface;
using NJsonSchema;
using NLog;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Foci.Orbital.Agent.Factories
{
    /// <summary>
    /// Retrieves the json string configuration. Implements IConfigurationLoader.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConfigurationLoader : IConfigurationLoader
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
        private readonly IEnumerable<ILoader> loaders;
        private readonly JsonSchema4 schema;

        /// <summary>
        /// Default Constructor that takes a file repository and optional filename. Initializes the loader list.
        /// </summary>
        /// <param name="fileRepository">file repository instance</param>
        /// <param name="filename">optional filename</param>
        public ConfigurationLoader(string filename = "orbital.config")
        {
            schema = JsonSchema4.FromJsonAsync(AgentConstants.CONFIG_SCHEMA).Result;
            loaders = new List<ILoader>()
            {
                new EnvVarConfigurationLoader(),
                new FileConfigurationLoader(filename)
            };
        }

        /// <inheritdoc />
        public string GetConfiguration()
        {
            foreach (var loader in loaders)
            {
                if (loader.Exists())
                {
                    string config = loader.LoadConfiguration();
                    var errors = schema.ValidateMessage(config);
                    if (errors.Any())
                    {
                        var message = string.Join(",", errors.Select(e => e.Message).ToArray());
                        logger.Warn("ConfigurationLoader :: GetConfiguration: Failed to retrieve configuration from {0} due to {1}", loader.GetType().Name, message);
                    }
                    else
                    {
                        return config;
                    }
                }
            }
            return string.Empty;
        }
    }
}
