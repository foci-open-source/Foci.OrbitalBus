﻿using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Policies.Configuration.Scenarios;
using NLog;
using Polly;
using System;

namespace Foci.Orbital.Agent.Policies.Configuration
{
    /// <summary>
    ///  Executes the action within the policy
    /// </summary>
    public class ConfigurationDeserializationPolicy
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Executes the policy with the action defined with the custom data.
        /// </summary>
        /// <param name="action">Action to be executed</param>
        /// <param name="data">custom data to pass as Context.</param>
        /// <returns>OrbitalConfiguration object</returns>
        public static OrbitalConfiguration Execute(Func<Context, OrbitalConfiguration> action, ConfigurationDeserializationData data)
        {
            var finalPolicy = ConfigurationDeserialization.Get();
            var result = finalPolicy.Execute(action, data.ToDictionary());

            return result;
        }
    }
}
