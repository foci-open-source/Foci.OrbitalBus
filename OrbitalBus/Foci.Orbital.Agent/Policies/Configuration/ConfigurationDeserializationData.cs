﻿using Polly;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Policies.Configuration
{
    /// <summary>
    /// Custom data to be pass to the policy.
    /// </summary>
    public struct ConfigurationDeserializationData
    {
        private const string methodName = "methodName";
        private const string methodPurpose = "methodPurpose";
        private const string configJson = "configJson";

        public string MethodName { get; }
        public string MethodPurpose { get; }
        public string ConfigJson { get; }

        /// <summary>
        /// Constructor to create the custom data.
        /// </summary>
        /// <param name="methodName">String representation of the method name</param>
        /// <param name="methodPurpose">String representation of the purpose of the action</param>
        /// <param name="configJson">String representation of the json in the configuration file</param>
        public ConfigurationDeserializationData(string methodName, string methodPurpose, string configJson)
        {
            MethodName = methodName;
            MethodPurpose = methodPurpose;
            ConfigJson = configJson;
        }

        /// <summary>
        /// Gets the Custom data to  Dictionary
        /// </summary>
        /// <returns>Gets the custom data defined to a dictionary</returns>
        public IDictionary<string, object> ToDictionary()
        {
            return new Dictionary<string, object>()
            {
                { methodName, this.MethodName },
                { methodPurpose, this.MethodPurpose },
                { configJson, this.ConfigJson }
            };
        }

        /// <summary>
        /// Gets the policy content into a ConfigurationDeserializationData object
        /// </summary>
        /// <param name="context">The policy's context with custom data.</param>
        /// <returns>ConfigurationDeserializationData object with custom data.</returns>
        public static ConfigurationDeserializationData FromContext(Context context)
        {
            var method = context[methodName]?.ToString() ?? "No method defined";
            var purpose = context[methodPurpose]?.ToString() ?? "No purpose defined";
            var function = context[configJson]?.ToString() ?? "No config json defined";
            return new ConfigurationDeserializationData(method, purpose, function);
        }
    }
}
