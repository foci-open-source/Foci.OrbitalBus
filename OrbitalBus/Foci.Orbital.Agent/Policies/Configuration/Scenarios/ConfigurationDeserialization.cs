﻿using Foci.Orbital.Agent.Models.Configurations;
using Newtonsoft.Json;
using NLog;
using Polly;
using System.Threading;

namespace Foci.Orbital.Agent.Policies.Configuration.Scenarios
{
    /// <summary>
    /// Policy that handles a JsonSerializationException thrown by the ConfigurationService
    /// </summary>
    public class ConfigurationDeserialization
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Returns the Policy needed to be executed.
        /// </summary>
        /// <returns>Policy that returns a Payload, as a result, if it's triggered</returns>
        public static Policy<OrbitalConfiguration> Get()
        {
            return Policy<OrbitalConfiguration>.Handle<JsonSerializationException>().Fallback(FallbackValue, OnFallback);
        }

        /// <summary>
        /// On Fallback action to be performed if the policy is triggered and after FallbackValue()  is complete.
        /// </summary>
        /// <param name="result">The data resulting from the action that has triggered the policy</param>
        /// <param name="context">Custom data containing descriptive data</param>
        private static void OnFallback(DelegateResult<OrbitalConfiguration> result, Context context)
        {
            var data = ConfigurationDeserializationData.FromContext(context);
            logger.Error("Failed to deserialize {0}.The contents of the configuration JSON string are not complete or properly formatted. A default config was returned.", data.ConfigJson);
            logger.Debug("{0} method with the purpose of {1} failed. The contents of the configuration JSON string are not complete or properly formatted. A default config was returned.", data.MethodName, data.MethodPurpose);

        }

        /// <summary>
        /// Action to be performed when the policy is triggered.
        /// </summary>
        /// <param name="result">The data resulting from the action that has triggered the policy</param>
        /// <param name="context">Custom data containing descriptive data</param>
        /// <param name="token">N/A</param>
        /// <returns>Payload result</returns>
        private static OrbitalConfiguration FallbackValue(DelegateResult<OrbitalConfiguration> result, Context context, CancellationToken token)
        {

            var configurationProperties = new OrbitalConfiguration();

            return configurationProperties;
        }
    }
}
