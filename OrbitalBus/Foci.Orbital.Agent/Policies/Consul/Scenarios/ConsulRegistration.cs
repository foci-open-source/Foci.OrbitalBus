﻿using NLog;
using Polly;
using Polly.Retry;
using System;
using System.Net;
using System.Net.Sockets;
using System.Reflection;

namespace Foci.Orbital.Agent.Policies.Consul.Scenarios
{
    /// <summary>
    /// This Consul policy provides a WaitAndRetry policy for service registration.
    /// </summary>
    public static class ConsulRegistration
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Get returns a policy that handles Consul registrations. 
        /// </summary>
        /// <returns>RetryPolicy</returns>
        public static RetryPolicy Get()
        {
            return Policy.Handle<WebException>()
                         .OrInner<SocketException>()
                            .WaitAndRetry(new[]
                              {
                                TimeSpan.FromMilliseconds(500),
                                TimeSpan.FromSeconds(2),
                                TimeSpan.FromSeconds(3)
                              }, (exception, timeSpan, context) => {
                                  var data = ConsulData.FromContext(context);
                                  logger.Debug("Retrying method: {0} in {1} seconds", data.MethodName, timeSpan);
                                  logger.Warn("Unable to register the service on Consul. Trying again in {0} seconds.", timeSpan);
                              });
        }
    }
}
