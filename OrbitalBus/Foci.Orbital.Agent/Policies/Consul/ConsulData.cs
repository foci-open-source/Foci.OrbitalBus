﻿using Polly;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Policies.Consul
{
    /// <summary>
    /// ConsulData contains information about the calling method and purpose of executing 
    /// a Consul policy.
    /// </summary>
    public struct ConsulData
    {
        private const string methodName = "methodName";
        private const string methodPurpose = "methodPurpose";

        /// <summary>
        /// Name of the calling method applying a policy.
        /// </summary>
        public string MethodName { get; }


        /// <summary>
        /// Purpose of the calling method applying a policy.
        /// </summary>
        public string MethodPurpose { get; }


        /// <summary>
        /// Default constructor. 
        /// </summary>
        /// <param name="methodName">String parameter to hold the name of the calling method.</param>
        /// <param name="methodPurpose">String parameter that describes the purpose of the action being executed.</param>
        public ConsulData(string methodName, string methodPurpose)
        {
            MethodName = methodName;
            MethodPurpose = methodPurpose;
        }

        /// <summary>
        /// Returns a dictionary of context data.
        /// </summary>
        /// <returns>IDictionary of string and object</returns>
        public IDictionary<string, object> ToDictionary()
        {
            return new Dictionary<string, object>()
            {
                { methodName, this.MethodName },
                { methodPurpose, this.MethodPurpose }
            };
        }

        /// <summary>
        /// Returns AdapterRegistration object using the input context.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>ConsulData</returns>
        public static ConsulData FromContext(Context context)
        {
            var method = context[methodName]?.ToString() ?? "No method defined";
            var purpose = context[methodPurpose]?.ToString() ?? "No purpose defined";
            return new ConsulData(method, purpose);
        }
    }
}
