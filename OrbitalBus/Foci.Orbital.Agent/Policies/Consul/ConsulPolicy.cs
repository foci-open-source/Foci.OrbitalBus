﻿using NLog;
using Polly;
using System;

namespace Foci.Orbital.Agent.Policies.Consul
{
    /// <summary>
    /// Provides access to excute various Consul policies.
    /// </summary>
    public static class ConsulPolicy
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Executes the intended action using the associated policy for registering services with Consul.
        /// </summary>
        /// <param name="action">Action to be executed</param>
        /// <param name="data">custom data to pass as Context.</param>
        /// <returns>bool</returns>
        public static bool Execute(Func<Context, bool> action, ConsulData data)
        {
            var finalPolicy = Scenarios.ConsulRegistration.Get();
            return finalPolicy.Execute(action, data.ToDictionary());

        }


        /// <summary>
        /// Executes the intended action using the associated policy for retrieving values from Consul.
        /// </summary>
        /// <param name="action">Action to be executed</param>
        /// <param name="data">custom data to pass as Context.</param>
        /// <returns>bool</returns>
        public static string Execute(Func<Context, string> action, ConsulData data)
        {
            var finalPolicy = Scenarios.ConsulRegistration.Get();
            return finalPolicy.Execute(action, data.ToDictionary());

        }
    }
}
