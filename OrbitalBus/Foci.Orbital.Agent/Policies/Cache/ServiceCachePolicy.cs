﻿using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using NLog;
using Polly;
using System;
using System.Collections.Generic;
using System.Text;

namespace Foci.Orbital.Agent.Policies.Cache
{
    /// <summary>
    ///  Executes the action within the policy
    /// </summary>
    public class ServiceCachePolicy
    {

        /// <summary>
        /// Executes the policy with the action defined with the custom data.
        /// </summary>
        /// <param name="action">Action to be executed</param>
        /// <param name="data">custom data to pass as Context.</param>
        /// <returns>OrbitalConfiguration object</returns>
        public static ServiceDefinition Put(Func<Context,ServiceDefinition> action, string ServiceName)
        {
            var finalPolicy = ServiceCache.Get();
            var result = finalPolicy.Execute(action, new Context(ServiceName));
            return result;
        }

        /// <summary>
        /// Executes the policy with the action defined with the custom data.
        /// </summary>
        /// <param name="action">Action to be executed</param>
        /// <param name="data">custom data to pass as Context.</param>
        /// <returns>OrbitalConfiguration object</returns>
        public static ServiceDefinition Get(Func<Context,ServiceDefinition> action, string ServiceName)
        {
            var finalPolicy = ServiceCache.Get();
            var result = finalPolicy.Execute(action, new Context(ServiceName));
            return result;
        }
    }
}
