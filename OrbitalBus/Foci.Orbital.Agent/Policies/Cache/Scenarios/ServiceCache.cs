﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Models.Service.ServiceContract;
using Foci.Orbital.Agent.Repositories;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interface;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using NLog;
using Polly;
using Polly.Caching;
using Polly.Caching.Memory;
using Polly.Registry;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Foci.Orbital.Agent.Policies.Cache.Scenarios
{
    [ExcludeFromCodeCoverage]
    /// <summary>
    /// Policy that handle the caching of services
    /// </summary>
    public class ServiceCache
    {
        private static IPolicyRegistry<string> policyRegistry;
        private const string policyKey = "LocalCachePolicy";
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        private static MemoryCacheProvider memoryCacheProvider;
        private static IServiceDefinitionRepository serviceDefinitionRepository;
        
        public ServiceCache(IServiceDefinitionRepository serviceDefinitionRepository)
        {
            ServiceCache.serviceDefinitionRepository = serviceDefinitionRepository ?? throw new ArgumentNullException(nameof(serviceDefinitionRepository));
        }

        /// <summary>
        /// Returns the Policy needed to be executed.
        /// </summary>
        /// <returns>Policy that returns a Payload, as a result, if it's triggered</returns>
        public static CachePolicy<ServiceDefinition> Get()
        {
            return policyRegistry.Get<CachePolicy<ServiceDefinition>>(policyKey);
        }

        /// <summary>
        /// Sets up the cache policy registry.
        /// </summary>
        public static void StartPolicyCache()
        {
            policyRegistry = new PolicyRegistry();
            var policy = SetCacheRegistry();
            policyRegistry.Add(policyKey, policy);
        }

        /// <summary>
        /// Initializes the Cache Policy.
        /// </summary>
        /// <returns>The Cache Policy.</returns>
        private static CachePolicy<ServiceDefinition> SetCacheRegistry()
        {
            MemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());
            memoryCacheProvider = new MemoryCacheProvider(memoryCache);
            return Policy.Cache<ServiceDefinition>(memoryCacheProvider, TimeSpan.FromDays(7),
                (context,key)=> {
                    logger.Info("Retrieving {0} from Cache", context.OperationKey);
                }, 
                (context, key) => {
                    logger.Info("Service Definition was not found in Cache {0}", context.OperationKey);
                },
                (context,key) => {
                    logger.Info("Caching new service {0}",key);
                    serviceDefinitionRepository.RegisterToServiceDiscovery(context.OperationKey);
                },
                (context, key, ex) => {
                    if (ex != null)
                    {
                        logger.Error("There was an error while retrieving {0} definition from the Cache: {1}", key, ex.Message);
                    }
                },
                (context, key, ex) => {
                    if (ex != null)
                    {
                        logger.Error("There was an error while putting {0} definition to the Cache: {1}", key, ex.Message);
                    }
                });
        }

    }
}
