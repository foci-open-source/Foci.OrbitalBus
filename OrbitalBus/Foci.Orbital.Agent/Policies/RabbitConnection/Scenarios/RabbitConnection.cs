﻿using NLog;
using Polly;
using Polly.Retry;
using System;
using System.Reflection;

namespace Foci.Orbital.Agent.Policies.RabbitConnection.Scenarios
{
    /// <summary>
    /// RabbitConnection provides a WaitAndRetry policy for establishing a connection with RabbitMQ.
    /// </summary>
    public static class RabbitConnection
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Get returns a policy that handles establishing a connection with RabbitMQ. 
        /// </summary>
        /// <returns>RetryPolicy</returns>
        public static RetryPolicy Get()
        {
            return Policy.Handle<TargetInvocationException>()
                            .OrInner<TimeoutException>()
                            .WaitAndRetry(new[]
                              {
                                TimeSpan.FromMilliseconds(500),
                                TimeSpan.FromSeconds(2),
                                TimeSpan.FromSeconds(3)
                              }, (exception, timeSpan, context) => {
                                  var data = RabbitConnectionData.FromContext(context);
                                  logger.Debug("Retrying method: {0} in {1} seconds", data.MethodName, timeSpan);
                                  logger.Warn("Unable to connect to Rabbit MQ. Trying again in {0} seconds.", timeSpan);
                              });
        }
    }
}
