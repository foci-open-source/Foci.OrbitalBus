﻿using NLog;
using System;

namespace Foci.Orbital.Agent.Policies.RabbitConnection
{
    /// <summary>
    /// Provides a means to establish a connection with a configured messaging queuing system.
    /// </summary>
    public static class RabbitConnectionPolicy
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Executes the intended action using the associated policy for establishing a connection with RabbitMQ.
        /// </summary>
        /// <param name="action"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static void Execute(Action<Polly.Context> action, RabbitConnectionData data)
        {
                var finalPolicy = Scenarios.RabbitConnection.Get();
                finalPolicy.Execute(action, data.ToDictionary());
        }
    }
}
