﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// An initialization exception has occurred.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalInitializationException : OrbitalException
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OrbitalInitializationException()
        { }

        /// <inheritdoc />
        public OrbitalInitializationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalInitializationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalInitializationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
