﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// An unhandled exception has occurred.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalUnhandledException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalUnhandledException()
        { }

        /// <inheritdoc />
        public OrbitalUnhandledException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalUnhandledException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalUnhandledException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
