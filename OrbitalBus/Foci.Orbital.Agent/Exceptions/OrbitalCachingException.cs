﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// This exception is thrown when caching exceptions occur.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalCachingException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalCachingException()
        { }

        /// <inheritdoc />
        public OrbitalCachingException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalCachingException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalCachingException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
