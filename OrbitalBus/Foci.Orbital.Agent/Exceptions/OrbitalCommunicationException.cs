﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// Orbital communication exception
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalCommunicationException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalCommunicationException()
        { }

        /// <inheritdoc />
        public OrbitalCommunicationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalCommunicationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalCommunicationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
