﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// An file exception has occurred.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalOperationHandlerException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalOperationHandlerException()
        { }

        /// <inheritdoc />
        public OrbitalOperationHandlerException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalOperationHandlerException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalOperationHandlerException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
