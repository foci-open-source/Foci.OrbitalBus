﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// This exception is thrown when business exceptions occur.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalBusinessException : OrbitalException
    {
        public OrbitalFaultCode FaultCode { get; set; }
        public Dictionary<string, string> Headers { get; set; }

        /// <summary>
        /// Override to default constructor.
        /// </summary>
        public OrbitalBusinessException()
        { }

        /// <inheritdoc />
        public OrbitalBusinessException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalBusinessException(string message, Exception innerException)
        : base(message, innerException)
        { }

        //// <inheritdoc />
        protected OrbitalBusinessException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }

        /// <summary>
        /// Constructor to assign fault code and headers.
        /// Extends base constructor with message.
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        /// <param name="faultCode">Enum representing the business fault code.</param>
        /// <param name="headers">Dictionary representing the headers with additional information about the exception.</param>
        public OrbitalBusinessException(string message, OrbitalFaultCode faultCode, Dictionary<string, string> headers)
        : base(message)
        {
            FaultCode = faultCode;
            Headers = headers;
        }
    }
}
