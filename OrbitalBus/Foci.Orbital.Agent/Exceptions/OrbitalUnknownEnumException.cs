﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// Unknown enumeration exception.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalUnknownEnumException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalUnknownEnumException()
        { }

        /// <inheritdoc />
        public OrbitalUnknownEnumException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalUnknownEnumException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalUnknownEnumException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
