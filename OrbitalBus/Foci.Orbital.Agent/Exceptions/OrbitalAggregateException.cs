﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// A OrbitalException to handle Aggregate Exceptions.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalAggregateException : OrbitalException
    {
        private readonly IEnumerable<Exception> InnerExceptions = new List<Exception>();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public OrbitalAggregateException()
        { }

        /// <inheritdoc />
        public OrbitalAggregateException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalAggregateException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <summary>
        /// Unique aggregate constructor to build aggregate exceptions based on IEnumerable
        /// </summary>
        /// <param name="message">Additional information to include in the exception.</param>
        /// <param name="innerException">Exception object containing additional information regarding the exception occurred.</param>
        /// <param name="innerExceptions">Collection of additional exceptions.</param>
        public OrbitalAggregateException(string message, Exception innerException, IEnumerable<Exception> innerExceptions)
        : base(message, innerException)
        {
            InnerExceptions = innerExceptions;
        }

        /// <inheritdoc />
        protected OrbitalAggregateException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
