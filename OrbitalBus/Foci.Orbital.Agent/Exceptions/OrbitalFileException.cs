﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// An file exception has occurred.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalFileException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalFileException()
        { }

        /// <inheritdoc />
        public OrbitalFileException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalFileException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalFileException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
