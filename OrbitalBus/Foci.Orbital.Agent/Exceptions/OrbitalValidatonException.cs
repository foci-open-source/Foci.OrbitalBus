﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// validation exception occurred in the Orbital.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalValidationException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalValidationException()
        { }

        /// <inheritdoc />
        public OrbitalValidationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalValidationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalValidationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
