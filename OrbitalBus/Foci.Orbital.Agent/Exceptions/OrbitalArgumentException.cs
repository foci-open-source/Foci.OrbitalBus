﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// Orbital Argument Exception
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalArgumentException : OrbitalException
    {
        /// <summary>
        /// Overridden Exception constructor
        /// </summary>
        public OrbitalArgumentException()
        { }

        /// <inheritdoc />
        public OrbitalArgumentException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalArgumentException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalArgumentException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
