﻿using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Foci.Orbital.Agent.Exceptions
{
    /// <summary>
    /// Translation exception occurred in the Orbital.
    /// </summary>
    [Serializable]
    [ExcludeFromCodeCoverage]
    public class OrbitalTranslationException : OrbitalException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public OrbitalTranslationException()
        { }

        /// <inheritdoc />
        public OrbitalTranslationException(string message)
        : base(message)
        { }

        /// <inheritdoc />
        public OrbitalTranslationException(string message, Exception innerException)
        : base(message, innerException)
        { }

        /// <inheritdoc />
        protected OrbitalTranslationException(SerializationInfo info, StreamingContext context)
        : base(info, context)
        { }
    }
}
