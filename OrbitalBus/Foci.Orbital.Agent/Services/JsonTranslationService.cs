﻿using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Services.Interfaces;
using Jint;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Services
{
    /// <summary>
    /// The service that performs translations with Jint Javascript Engine.
    /// </summary>
    public class JsonTranslationService : IJsonTranslationService
    {
        public enum JsonTranslationFunction
        {
            None = 0,
            Inbound = 1,
            Outbound,
            GetParams
        }

        #region Constants
        //These are the function calls supported by the translation engine.
        //All the logic to be performed by the translation should be contained in these functions in the translation.
        private const string ServiceInboundFunction = "serviceInboundTranslation";
        private const string ServiceOutboundFunction = "serviceOutboundTranslation";
        private const string ServiceGetParametersFunction = "getDynamicParameters";
        #endregion

        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        private static readonly Dictionary<string, Delegate> EngineCommonFunctions = new Dictionary<string, Delegate>()
        {
            { "throwBusinessError", (Action<object, object>)ThrowBusinessError },
            { "logInfo", (Action<object>)LogInfo },
            { "logTrace", (Action<object>)LogTrace },
            { "logDebug", (Action<object>)LogDebug },
            { "logWarn", (Action<object>)LogWarn },
            { "logError", (Action<object>)LogError },
            { "logFatal", (Action<object>)LogFatal },
        };

        /// <summary>
        /// Default constructor.
        /// </summary>
        public JsonTranslationService()
        { }

        /// <inheritdoc />
        public string ExecuteTranslationFunction(string message, string translation, JsonTranslationFunction function)
        {
            if (message == null)
            {
                message = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(translation))
            {
                throw new OrbitalArgumentException("Translation cannot be null or empty");
            }

            return Translate(message, translation, GetFunctionName(function));
        }

        /// <inheritdoc />
        public bool FunctionExist(string translation, JsonTranslationFunction function)
        {
            if (string.IsNullOrWhiteSpace(translation))
            {
                throw new OrbitalArgumentException("Translation cannot be null or empty");
            }

            // Execute Javascript Translation
            // Since the engine isn't threadsafe, we need to make sure it doesn't cross streams.
            Engine engine = CreateEngine(translation);
            return CheckFunctionExist(engine, GetFunctionName(function));
        }

        #region Private Methods
        /// Initialize a Jint Engine with a script.
        /// </summary>
        /// <param name="javaScriptCode">The script to initialize in the engine.</param>
        /// <returns>The new engine.</returns>
        private static Engine CreateEngine(string javaScriptCode)
        {
            Engine engine = new Engine();

            foreach (KeyValuePair<string, Delegate> logFunction in EngineCommonFunctions)
            {
                engine.SetValue(logFunction.Key, logFunction.Value);
            }

            engine.Execute(javaScriptCode);

            return engine;
        }

        /// <summary>
        /// Translates a MessageDescription into a specified type.
        /// </summary>
        /// <param name="message">The message information to process.</param>
        /// <param name="translation">string representation of JavaScript translation</param>
        /// <param name="functionName">The name of the function to call in the translation javascript.</param>
        /// <returns>The json result of the translation function output.</returns>        
        private string Translate(string message, string translation, string functionName)
        {
            // Execute Javascript Translation
            // Since the engine isn't threadsafe, we need to make sure it doesn't cross streams.
            Engine engine = CreateEngine(translation);

            if (CheckFunctionExist(engine, functionName))
            {
                string jsonResult = CallFunction(message, engine, functionName);

                return jsonResult;
            }

            string exception = string.Format("The JavaScript function {0} was not found in the translation", functionName);
            throw new OrbitalTranslationException(exception);
        }

        /// <summary>
        /// Call a function using Jint's engine invoke function
        /// </summary>
        /// <param name="message">The message to pass into the function.</param>
        /// <param name="engine">The Engine to run the function. It contains the function that should already be loaded via Execute call.</param>
        /// <param name="functionName">The name of the function to call.</param>
        /// <returns>A serialized object of the function result.</returns>
        private static string CallFunction(string message, Engine engine, string functionName)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return null;
            }
            Jint.Native.JsValue result = TranslateMessageInJavaScriptFormat(message, engine, functionName);
            if (result.IsNull())
            {
                return null;
            }
            else if (result.IsString())
            {

                return result.AsString();
            }

            return JsonConvert.SerializeObject(result.ToObject());
        }

        /// <summary>
        /// Private method to make the actual translation in Javascript of the given function name and message and return a string that will be passed
        /// </summary>
        /// <param name="message"> string represenation of the message to be translated </param>
        /// <param name="engine"> The Engine to run the function. It contains the function that should already be loaded via Execute call</param>
        /// <param name="functionName"> The name of the function to call.</param>
        /// <returns> string representation of the result from translating the message</returns>
        private static Jint.Native.JsValue TranslateMessageInJavaScriptFormat(string message, Engine engine, string functionName)
        {
            var translate = "translate";
            var translatorFunction = string.Format("function {0}(n){{var r=JSON.parse(n),t={1}(r);return JSON.stringify(t)}}", translate, functionName);
            return engine.Execute(translatorFunction).GetValue(translate).Invoke(message);
        }

        //Check the given function exist in the translation
        private bool CheckFunctionExist(Engine engine, string functionName)
        {
            string checkFunction = string.Concat(@"typeof ", functionName, @" == 'function'");
            return engine.Execute(checkFunction).GetCompletionValue().AsBoolean();
        }

        //Get function name base on JsonTranslationFunction enum
        private static string GetFunctionName(JsonTranslationFunction function)
        {
            switch (function)
            {
                case JsonTranslationFunction.Inbound:
                    return ServiceInboundFunction;
                case JsonTranslationFunction.Outbound:
                    return ServiceOutboundFunction;
                case JsonTranslationFunction.GetParams:
                    return ServiceGetParametersFunction;
                default:
                    string exception = string.Format("The JavaScript function {0} has not been implemented", Enum.GetName(typeof(JsonTranslationFunction), function));
                    throw new OrbitalUnknownEnumException(exception);
            }
        }

        /// <summary>
        /// Logs information at the appropriate level with the following preface:
        /// "Logging called from translation: "
        /// </summary>
        /// <param name="level">The level to log the information at.</param>
        /// <param name="information">The information provided from the translation for logging.</param>
        private static void WriteToLog(LogLevel level, string information)
        {
            logger.Log(level, "Logging called from translation: {0}", information);
        }
        #endregion

        #region Functions available to translation

        /// <summary>
        /// A function to be called from within the translation.
        /// Throws a business fault with information passed in.
        /// </summary>
        /// <param name="data">The data to serialize and return.</param>
        /// <param name="info">The key-value information to return as headers.</param>
        /// <exception cref="OrbitalBusinessException">Success path of this function is to throw this exception.</exception>
        /// <exception cref="JsonSerializationException">If the header values provided in the javascript are not a Dictionary this exception will be thrown.</exception>
        private static void ThrowBusinessError(object data, object info)
        {
            string errorData = JsonConvert.SerializeObject(data);
            string headersInfo = JsonConvert.SerializeObject(info);

            //If JsonConvert.DeserializeObject fails it will throw a Serialization exception, which will then be caught and end up as a runtime fault.
            //We want a runtime fault because the translation is not properly assembled.
            Dictionary<string, string> headers = JsonConvert.DeserializeObject<Dictionary<string, string>>(headersInfo);
            throw new OrbitalBusinessException(errorData, OrbitalFaultCode.Orbital_BusinessFault_010, headers);
        }

        #region Logging functions
        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the INFO level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogInfo(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Info, infoToLog);
        }

        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the DEBUG level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogDebug(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Debug, infoToLog);
        }

        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the ERROR level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogError(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Error, infoToLog);
        }

        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the TRACE level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogTrace(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Trace, infoToLog);
        }

        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the WARN level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogWarn(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Warn, infoToLog);
        }

        /// <summary>
        /// A function to be called from within the translation.
        /// Logs information at the FATAL level.
        /// </summary>
        /// <param name="information">The data to log.</param>
        private static void LogFatal(object information)
        {
            string infoToLog = JsonConvert.SerializeObject(information);
            WriteToLog(LogLevel.Fatal, infoToLog);
        }
        #endregion

        #endregion
    }
}
