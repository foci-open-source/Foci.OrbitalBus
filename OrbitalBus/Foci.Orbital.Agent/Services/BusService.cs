﻿using EasyNetQ;
using EasyNetQ.FluentConfiguration;
using EasyNetQ.NonGeneric;
using EasyNetQ.Producer;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Policies.RabbitConnection;
using Foci.Orbital.Agent.Repositories;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using NLog;
using System;
using System.Threading.Tasks;

namespace Foci.Orbital.Agent.Services
{
    internal class BusService : IBusService
    {
        private readonly IBusManager manager;
        private readonly IEasyNetQRepository easyNetQRepository;
        private const string servicePublishTopic = "orbital.service.publish";
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BusService() : this(new BusManager(), new EasyNetQRepository()) { }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="manager">The IBusManager to consult.</param>
        /// <param name="easyNetQRepository">The IEasyNetQService to update buses in RabbitMQ</param>
        public BusService(IBusManager manager, IEasyNetQRepository easyNetQRepository)
        {
            this.manager = manager ?? throw new ArgumentNullException(nameof(manager));
            this.easyNetQRepository = easyNetQRepository ?? throw new ArgumentNullException(nameof(easyNetQRepository));
        }


        /// <inheritdoc />
        public IBus BootstrapService(BusConnectionDetails connectionDetails, params SubscriberDetails[] subscriptions)
        {
            if (connectionDetails == null)
            {
                logger.Error("BootstrapService: Connection details cannot be null");
                throw new ArgumentNullException(nameof(connectionDetails));
            }

            IBus bus;

            if (!this.manager.Exists(connectionDetails.Host))
            {
                logger.Debug("The bus for host {0} has not yet been created. Creating it now.", connectionDetails.Host);
                bus = easyNetQRepository.CreateBus(connectionDetails);
                this.manager.Add(connectionDetails.Host, bus);
            }
            else
            {
                logger.Debug("The bus for host {0} has already been created.", connectionDetails.Host);
                bus = this.manager.Get(connectionDetails.Host);
            }

            foreach (var subscription in subscriptions)
            {
                AddSubscription(bus, subscription);
            }

            return bus;
        }

        /// <inheritdoc />
        public IBus SubscribeToServicePublishExchange(BusConnectionDetails connectionDetails, Action<UpdateService> action)
        {
            IBus bus = null;
            if (!this.manager.Exists(connectionDetails.Host))
            {
                logger.Debug("The bus for host {0} has not yet been created. Creating it now.", connectionDetails.Host);
                bus = easyNetQRepository.CreateBus(connectionDetails);
                this.manager.Add(connectionDetails.Host, bus);
            }
            else
            {
                logger.Debug("The bus for host {0} has already been created.", connectionDetails.Host);
                bus = this.manager.Get(connectionDetails.Host);
            }
            
            RabbitConnectionPolicy.Execute(a => bus.Subscribe<UpdateService>(Guid.NewGuid().ToString(),action, t => t.WithTopic(servicePublishTopic)),
                 new RabbitConnectionData("RegisterServiceQueue", "Register Queue and subscribe"));
            return bus;

        }

        /// <inheritdoc />
        public IBus CreateBus(BusConnectionDetails connectionDetails)
        {
            if (connectionDetails == null)
            {
                logger.Error("CreateBus: Connection details cannot be null");
                throw new ArgumentNullException(nameof(connectionDetails));
            }

            return easyNetQRepository.CreateBus(connectionDetails);
        }

        /// <summary>
        /// Adds a subscription to the bus.
        /// </summary>
        /// <param name="bus">The bus to add the subscriber to.</param>
        /// <param name="details">All the info required to make a description.</param>
        /// <returns>The modified IBus with the new subscription.</returns>
        private IBus AddSubscription(IBus bus, SubscriberDetails details)
        {
            var typeDetails = details.GetType();
            return details.FlatMap(
                async => RegisterAsyncAction(bus, async, typeDetails),
                sync => RegisterSynchronousAction(bus, sync, typeDetails));
        }

        /// <summary>
        /// Calls the generic subscribe function of EasyNetQ using the details and type given.
        /// </summary>
        /// <param name="bus"> The easyNetQ bus</param>
        /// <param name="details"> The details of the subscriber function</param>
        /// <param name="typeDetails"> The type to register against the bus</param>
        /// <returns> The modified EastNetQ Bus</returns>
        private static IBus RegisterAsyncAction(IBus bus, SubscriberDetails details, Type typeDetails)
        {
            if (!details.IsSubscriberAsyncDetails())
            {
                var exception = new ArgumentException(
                    "The details parameter must be of generic type SubscriberAsyncDetails", "details");

                logger.Error("The details parameter must be of generic type SubscriberAsyncDetails");
                logger.Debug(exception, "The details parameter must be of generic type SubscriberAsyncDetails");
                return bus;
            }

            var messageType = typeDetails.GetProperty("Type").GetValue(details, null) as Type;
            var messageAction = typeDetails.GetProperty("GetMessageHandler").GetValue(details, null);
            var queueAction = typeDetails.GetProperty("GetQueueName").GetValue(details, null);

            var method = typeof(NonGenericExtensions).GetMethod("Subscribe", new[] {typeof (IBus),
                                                                                     typeof (Type),
                                                                                     typeof (string),
                                                                                     typeof (Action<object>),
                                                                                     typeof(Action<ISubscriptionConfiguration>)});

            RabbitConnectionPolicy.Execute(c => method.Invoke(null, new[] { bus, messageType, string.Empty, messageAction, queueAction }),
                            new RabbitConnectionData("RegisterAsyncAction", "Register Queue and subscribe"));

            return bus;
        }

        /// <summary>
        /// Calls the EasyNetQ Respond function with the type details contained in the SubscriberDetails.
        /// </summary>
        /// <param name="bus"> The EasyNetQ bus</param>
        /// <param name="details"> The subscriber details</param>
        /// <param name="typeDetails"> The </param>
        /// <returns>The EasyNetQ bus</returns>
        private static IBus RegisterSynchronousAction(IBus bus, SubscriberDetails details, Type typeDetails)
        {

            if (details.IsSubscriberAsyncDetails())
            {
                var exception = new ArgumentException(
                    "The details parameter must be of generic type SubscriberSyncronousDetails", "details");

                logger.Error("The details parameter must be of generic type SubscriberSyncronousDetails");
                logger.Debug(exception, "The details parameter must be of generic type SubscriberSyncronousDetails");
                return bus;
            }

            var requestType = typeDetails.GetProperty("GetRequestType").GetValue(details, null) as Type;
            var responseType = typeDetails.GetProperty("GetResponseType").GetValue(details, null) as Type;
            var messageAction = typeDetails.GetProperty("GetMessageHandler").GetValue(details, null);
            var queueAction = typeDetails.GetProperty("GetQueueResponse").GetValue(details, null);

            var method = new Func<Func<object, object>, Action<IResponderConfiguration>, IDisposable>(bus.Respond);
            var methodInfo = method.Method.GetGenericMethodDefinition().MakeGenericMethod(requestType, responseType);

            RabbitConnectionPolicy.Execute(c => methodInfo.Invoke(bus, new[] { messageAction, queueAction }),
                                           new RabbitConnectionData("RegisterSynchronousAction", "Register Queue and subscribe"));

            return bus;
        }

        #region IDisposable
        // Flag: Has Dispose already been called?
        private bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                this.manager.Dispose();
            }

            // Free any unmanaged objects here.

            disposed = true;
        }

        ~BusService()
        {
            Dispose(false);
        }
        #endregion
    }
}
