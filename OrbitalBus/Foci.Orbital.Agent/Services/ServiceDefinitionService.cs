﻿using EasyNetQ;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Models.Service.ServiceContract;
using Foci.Orbital.Agent.OperationHandler;
using Foci.Orbital.Agent.Policies.Cache;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Exceptions;

namespace Foci.Orbital.Agent.Services
{
    /// <summary>
    /// This service handles all logic related to service definitions.
    /// </summary>
    internal class ServiceDefinitionService : IServiceDefinitionService
    {

        private readonly IConfigurationService configurationService;
        private readonly IBusService busService;
        private readonly IOperationsHandler operationsHandler;
        private readonly ILogger log = LogManager.GetCurrentClassLogger();
        private readonly IServiceDefinitionRepository serviceDefinitionRepository;
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ServiceDefinitionService(IConfigurationService configurationService,
                            IBusService busService,
                            IServiceDefinitionRepository serviceDefinitionRepository,
                            IOperationsHandler operationsHandler)
        {
            this.configurationService = configurationService ?? throw new ArgumentNullException(nameof(configurationService));
            this.busService = busService ?? throw new ArgumentNullException(nameof(busService));
            this.operationsHandler = operationsHandler ?? throw new ArgumentNullException(nameof(operationsHandler));
            this.serviceDefinitionRepository = serviceDefinitionRepository ?? throw new ArgumentNullException(nameof(serviceDefinitionRepository));
        }

        /// <inheritdoc />  
        public IEnumerable<RegisteredServiceDetails> RegisterAndBootsrapServices()
        {
            var services = RegisterServices();
            services.Select(d => BootStrapSubscriberDetails(d.Key.ServiceName, d.Value)).ToList();

            return GetRegisteredServiceDetails(services);
        }

        /// <inheritdoc /> 
        public IDictionary<ServiceDefinition, IEnumerable<SubscriberDetails>> RegisterServices()
        {
            this.log.Info("Started to load services");
            var serviceNames = this.configurationService.GetConfigurationProperties().RegisteredServiceNames;
            var services = new List<ServiceDefinition>();
            foreach (var serviceName in serviceNames)
            {
                services.Add(ServiceCachePolicy.Get((context) => serviceDefinitionRepository.GetServiceDefinitionByName(serviceName), serviceName));
            }
            return GetSubscriberDetails(services);
        }

        /// <inheritdoc /> 
        public IBus BootStrapSubscriberDetails(string serviceName, IEnumerable<SubscriberDetails> details)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                throw new ArgumentException("Service Name cannot be null or empty", nameof(serviceName));
            }

            var connectionDetails = new BusConnectionDetails(this.configurationService.GetConfigurationProperties().BrokerConfiguration);
            var bus = this.busService.BootstrapService(connectionDetails, details == null ? new SubscriberDetails[0] : details.ToArray());
            this.log.Info("Service {0} has been bootstrapped and is listening to incoming messages", serviceName);
            return bus;
        }

        #region PrivateMethods

        /// <inheritdoc /> 
        public IBus SubscribeToServiceUpdate()
        {
            var connectionDetails = new BusConnectionDetails(this.configurationService.GetConfigurationProperties().BrokerConfiguration);
            var bus = this.busService.SubscribeToServicePublishExchange(connectionDetails, (message) =>
            {

                var result = ServiceCachePolicy.Put( (context) => serviceDefinitionRepository.GetServiceDefinitionByName(message.ServiceName), message.ServiceName);
                var subscriberDetails = default(IEnumerable<SubscriberDetails>);
                try
                {
                    subscriberDetails = this.operationsHandler.GetSubscriberDetails(result);
                }
                catch (ResolutionFailedException e)
                {
                    this.log.Debug(e, "Failed to load the service with service id {0}. Most likely the corresponding adapter was not loaded", result.ServiceName);
                    this.log.Error("Failed to load the service with service id {0}. Most likely the corresponding adapter was not loaded", result.ServiceName);
                }
                this.log.Info("Successfully bound the service with service id {0}", result.ServiceName);

                this.busService.BootstrapService(connectionDetails, subscriberDetails == null ? new SubscriberDetails[0] : subscriberDetails.ToArray());
                this.log.Info("Service {0} has been bootstrapped and is listening to incoming messages", result.ServiceName);
            });
            return bus;
        }
        /// <summary>
        /// Runs the logic to get subscriber details from a service
        /// </summary>
        /// <param name="services"> The details of the service</param>
        /// <returns>a key of service id and a list of the loaded subscriber details. If a service fails to load, it's not added to the dictionary.</returns>
        private IDictionary<ServiceDefinition, IEnumerable<SubscriberDetails>> GetSubscriberDetails(IEnumerable<ServiceDefinition> services)
        {
            var details = new Dictionary<ServiceDefinition, IEnumerable<SubscriberDetails>>();

            foreach (var service in services)
            {
                try
                {
                    var subscriberDetails = this.operationsHandler.GetSubscriberDetails(service);
                    details.Add(service, subscriberDetails);
                }
                catch (ResolutionFailedException e)
                {
                    this.log.Debug(e, "Failed to load the service with service id {0}. Most likely the corresponding adapter was not loaded", service.ServiceName);
                    this.log.Error("Failed to load the service with service id {0}. Most likely the corresponding adapter was not loaded", service.ServiceName);
                }
                this.log.Info("Successfully bound the service with service id {0}", service.ServiceName);
            }

            return details;
        }

        /// <summary>
        /// Goes to the files system to get the service definition for the provided services.
        /// </summary>
        /// <param name="details">The collection of service keys and details</param>
        /// <returns>A list of registered service details.</returns>
        private IEnumerable<RegisteredServiceDetails> GetRegisteredServiceDetails(IDictionary<ServiceDefinition, IEnumerable<SubscriberDetails>> details)
        {
            var results = new List<RegisteredServiceDetails>();

            foreach (var detail in details)
            {
                try
                {
                    this.log.Info("Trying to get the service definition for service name {0}", detail.Key);
                    results.Add(new RegisteredServiceDetails(detail.Key.ServiceName, detail.Value, ExtractServiceContract(detail.Key)));
                }
                catch (OrbitalFileException e)
                {
                    this.log.Debug(e, "Failed to get the Service Definition for service id {0}", detail.Key);
                    this.log.Error("Failed to get the Service Definition for service id {0}", detail.Key);
                }
            }

            return results;
        }

        /// <summary>
        /// This method extracts the service contract json string from the ServiceDefinition object
        /// </summary>
        /// <param name="service">The service definition object</param>
        /// <returns>The service contract json string</returns>
        private string ExtractServiceContract(ServiceDefinition service)
        {
            var contract = new ServiceContract
            {
                Operations = new List<OperationDescription>(),
                ServiceName = service.ServiceName
            };
            foreach (var operation in service.Operations)
            {
                contract.Operations.Add(new OperationDescription()
                {
                    Name = operation.Name,
                    IsAsync = operation.IsSync,
                    RequestSchema = operation.Schemas.Request,
                    ResponseSchema = operation.Schemas.Response
                });
            }

            return JsonConvert.SerializeObject(contract);
        }
        #endregion
    }
}
