﻿using EasyNetQ;
using System;

namespace Foci.Orbital.Agent.Services.Interface
{
    public interface IBusManager : IDisposable
    {
        /// <summary>
        /// Add a bus to the bus manager.
        /// </summary>
        /// <param name="busAddress">The address of a bus and the key value in the dictionary</param>
        /// <param name="bus">The bus whose address we are inserting into the dictionary.</param>
        /// <returns>The bus added.</returns>
        IBus Add(string busAddress, IBus bus);

        /// <summary>
        /// Destroy a bus address. Removes a bus from the dictionary and calls dispose on it.
        /// </summary>
        /// <param name="busAddress">The location of the bus address.</param>
        bool Destroy(string busAddress);

        /// <summary>
        /// Check if a bus address exists in the dictionary.
        /// </summary>
        /// <param name="busAddress"> The bus address whose existence we are checking. </param>
        /// <returns>True if it exists, false if not.</returns>
        bool Exists(string busAddress);

        /// <summary>
        /// Get a bus in the dictionary with it's key value, bus address.
        /// </summary>
        /// <param name="busAddress">key value related to the bus needed.</param>
        /// <returns>Bus related to the busAddress provided. </returns>
        IBus Get(string busAddress);

        /// <summary>
        /// Remove a bus based on its string bus address from the dictionary.
        /// </summary>
        /// <param name="busAddress"></param>
        /// <returns>The IBus to be removed.</returns>
        IBus Remove(string busAddress);
    }
}
