﻿
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Services.Interfaces
{
    public interface IJsonTranslationService
    {
        /// <summary>
        /// Executes a given translation function against the specified message and translation file.
        /// </summary>
        /// <param name="message"> The message information to process</param>
        /// <param name="translation">string representation of JavaScript translation</param>
        /// <param name="function"> The function to execute</param>
        /// <returns>The translated JSON string.</returns>
        string ExecuteTranslationFunction(string message, string translation, JsonTranslationFunction function);

        /// <summary>
        /// Check if the given function exist in the translation
        /// </summary>
        /// <param name="translation">string representation of JavaScript translation</param>
        /// <param name="function">The function to check</param>
        /// <returns>True if function exist in the translation, false otherwise</returns>
        bool FunctionExist(string translation, JsonTranslationFunction function);
    }
}
