﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul;
using Foci.Orbital.Agent.Models.Consul.Requests;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Services.Interface
{
    internal interface IConsulService
    {
        /// <summary>
        /// Register the service with the local consul agent.
        /// </summary>
        /// <param name="registerServiceRequest">The request to be sent to the agent.</param>
        bool RegisterService(RegisterServiceRequest registerServiceRequest);

        /// <summary>
        /// Registers service descriptions to consuls key value store, and registers the service
        /// </summary>
        /// <param name="services">The service information required for consul registration</param>
        /// <returns>The results of what services were successfully registered to consul</returns>
        IEnumerable<ConsulRegistrationResult> RegisterPublishedServices(IEnumerable<RegisteredServiceDetails> services);

        /// <summary>
        /// Set a key-value in the Consul store.
        /// </summary>
        /// <param name="key">The key against which to store the value.</param>
        /// <param name="value">The value to store.</param>
        /// <returns>True if successful.</returns>
        bool SetKeyValue(string key, string value);

        /// <summary>
        /// Get value from the Consul store by key.
        /// </summary>
        /// <param name="key">The key against which to query the store.</param>
        /// <returns>True string of the stored value.</returns>
        string GetKeyValue(string key);

        //Consul attributes
        /// <summary>
        /// If security is enabled or not, it will return a string with the appropriate protocol 
        /// </summary>
        /// <returns>String representation of the url for consul communication.</returns>
        string GetConsulHostAddress();
    }
}
