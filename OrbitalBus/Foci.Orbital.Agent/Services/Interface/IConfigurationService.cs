﻿using Foci.Orbital.Agent.Models.Configurations;

namespace Foci.Orbital.Agent.Services.Interfaces
{
    public interface IConfigurationService
    {
        /// <summary>
        /// Returns a OrbitalConfiguration object. If the private variable is null, it will read again from the fileRepository
        /// </summary>
        /// <returns>Returns a OrbitalConfiguration object.</returns>
        OrbitalConfiguration GetConfigurationProperties();

        /// <summary>
        /// Creates the OrbitalConfiguration object based on JSON string.
        /// </summary>
        /// <param name="json">JSON string that contains the properties to be populated to a OrbitalConfiguration object</param>
        void Initialize(string json);
    }
}
