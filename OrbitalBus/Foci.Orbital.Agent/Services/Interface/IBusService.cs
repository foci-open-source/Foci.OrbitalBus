﻿using EasyNetQ;
using Foci.Orbital.Agent.Models;
using System;

namespace Foci.Orbital.Agent.Services.Interface
{
    internal interface IBusService : IDisposable
    {
        /// <summary>
        /// Using a connection string and Subscription details this method will create and register a EasyNetQ
        /// bus and register the services describer in the Subscriber Details.
        /// </summary>
        /// <param name="connectionDetails"> The connection details of the service</param>
        /// <param name="types"> The details for the subscriptions.</param>
        /// <returns>The new IBus that we can use.</returns>
        IBus BootstrapService(BusConnectionDetails connectionDetails, params SubscriberDetails[] types);

        /// <summary>
        /// A method that returns an IBus with a specified connection string.
        /// </summary>
        /// <param name="connectionDetails">A connection string in EasyNetQ format.</param>
        /// <returns>The new IBus that we can use.</returns>
        IBus CreateBus(BusConnectionDetails connectionDetails);


        /// <summary>
        /// A method to subscribe to the Queue that will contain the name of the services update in Consul
        /// </summary>
        /// <param name="connectionDetails">Representation of the RabbitMQ broker connection details./</param>
        /// <param name="action">Binding action to perform when getting a message from the subscribed queue.</param>
        /// <returns>The new IBus that we can use.</returns>
        IBus SubscribeToServicePublishExchange(BusConnectionDetails connectionDetails, Action<UpdateService> action);

    }
}

