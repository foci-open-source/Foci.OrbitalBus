﻿using EasyNetQ;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Service;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Services.Interface
{
    /// <summary>
    /// Interface for ServiceDefinitionService that handles service registration.
    /// </summary>
    internal interface IServiceDefinitionService
    {
        /// <summary>
        /// Creates needed dependencies and bootstraps the given details.
        /// </summary>
        /// <param name="details">The subscriber details to be bootstrapped for listening to the queue.</param>
        /// <returns>Bus created with the details and service name specified </returns>

        /// <summary>
        /// Creates needed dependencies and bootstraps the given details.
        /// </summary>
        /// <param name="serviceName">The service name</param>
        /// <param name="details">The subscriber details to be bootstrapped for listening to the queue.</param>
        /// <returns>Bus created with the details and service name specified </returns>
        IBus BootStrapSubscriberDetails(string serviceName, IEnumerable<SubscriberDetails> details);

        /// <summary>
        /// Registers and bootstrap all service definitions
        /// </summary>
        /// <returns>A list of registered services details. </returns>
        IEnumerable<RegisteredServiceDetails> RegisterAndBootsrapServices();

        /// <summary>
        /// Registers all services found.
        /// </summary>
        /// <returns>A collection of services id's and Subscription Details</returns>
        IDictionary<ServiceDefinition, IEnumerable<SubscriberDetails>> RegisterServices();

        /// <summary>
        /// Subscribes to the queue to get notified when a service has been updated.
        /// </summary>
        /// <returns>Bus created with the details and service name specified</returns>
        IBus SubscribeToServiceUpdate();

    }
}
