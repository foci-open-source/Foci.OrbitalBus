﻿using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul;
using Foci.Orbital.Agent.Models.Consul.Requests;
using Foci.Orbital.Agent.Policies.Consul;
using Foci.Orbital.Agent.Repositories;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Foci.Orbital.Agent.Services
{
    internal class ConsulService : IConsulService
    {
        private const string Contract_Key = "orbital.{0}.contract";
        private const string BROKER_ID = "orbital.{0}.broker";
        private const string BROKER_PRIVATE_ID = "orbital.{0}.broker.private";
        private const string HTTP_ID = "orbital.{0}.http";

        private readonly IConsulRepository repository;
        private readonly IConfigurationService config;
        private readonly Maybe<X509Certificate> certificate;
        private ILogger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConsulService() : this(new ConsulRepository(), new ConfigurationService())
        {

        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConsulService(IConsulRepository ConsulRepository, IConfigurationService config)
        {
            this.repository = ConsulRepository ?? throw new ArgumentNullException(nameof(ConsulRepository));
            this.config = config ?? throw new ArgumentNullException(nameof(config));
            this.certificate = this.GetX509Certificate();
        }

        /// <inheritdoc />
        public bool RegisterService(RegisterServiceRequest registerServiceRequest)
        {
            if (registerServiceRequest == null)
            {
                log.Warn("RegisterService: Register service request shouldn't be null");
                return false;
            }

            return ConsulPolicy.Execute(c => this.repository.RegisterService(registerServiceRequest, GetConsulHostAddress(), this.certificate),
                                                                 new ConsulData("RegisterService", "Register a service with Consul"));
        }

        /// <inheritdoc />
        public bool SetKeyValue(string key, string value)
        {
            if (string.IsNullOrEmpty(key))
            {
                log.Warn("SetKeyValue: Key shouldn't be null or empty");
                return false;
            }

            if (string.IsNullOrEmpty(value))
            {
                log.Warn("SetKeyValue: Value shouldn't be null or empty");
                return false;
            }

            return ConsulPolicy.Execute(c => this.repository.SetKeyValue(key, value, GetConsulHostAddress(), this.certificate),
                                                                 new ConsulData("SetKeyValue", "Add an entry to Consul key value store"));
        }

        /// <inheritdoc />
        public string GetKeyValue(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                log.Warn("SetKeyValue: Key shouldn't be null or empty");
                return string.Empty;
            }

            return ConsulPolicy.Execute(c => this.repository.GetKeyValue(key, GetConsulHostAddress(), this.certificate),
                                                                 new ConsulData("GetKeyValue", "Get a value from Consul store"));
        }

        /// <inheritdoc />
        public IEnumerable<ConsulRegistrationResult> RegisterPublishedServices(IEnumerable<RegisteredServiceDetails> services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            var successfulServices = services.Select(s => new
            {
                id = string.Format(Contract_Key, s.ServiceName),
                success = this.SetKeyValue(string.Format(Contract_Key, s.ServiceName), s.ServiceContract)
            })
            .Where(r => r.success)
            .Select(r => r.id).ToList();

            var servicesToRegister = services.Where(s => successfulServices.Contains(string.Format(Contract_Key, s.ServiceName))).ToList();

            var registrationResults = RegisterServices(servicesToRegister);

            return registrationResults;
        }

        /// <inheritdoc />
        public string GetConsulHostAddress()
        {
            var configurationProperties = config.GetConfigurationProperties();

            return configurationProperties.ConsulConfiguration.SslEnabledConsul
                ? string.Format("https://{0}:{1}", configurationProperties.ConsulConfiguration.HostIp, configurationProperties.ConsulConfiguration.HostPort.ToString())
                : string.Format("http://{0}:{1}", configurationProperties.ConsulConfiguration.HostIp, configurationProperties.ConsulConfiguration.HostPort.ToString());
        }

        #region Private Methods
        /// <summary>
        /// If the string begins and ends with double quotes, it removes them.
        /// </summary>
        /// <param name="input">The string to check.</param>
        /// <returns>The string with quotes removed.  Will return just the string if it is less than 3 characters long.</returns>
        private string CleanupSerializedData(string input)
        {
            //If it's less than 3 characters long, just return the input.
            if (input == null || string.IsNullOrEmpty(input) || input.Length < 3)
            {
                return input;
            }

            //If there were single slashes for escaping on the way in, the encoding has added two extra slashes by now.
            var escapesRemoved = input.Replace("\\", "");

            //Only trim the first and last if they're both double quotes.
            if (escapesRemoved[0].Equals('"') && escapesRemoved.Last().Equals('"'))
            {
                return escapesRemoved.Substring(1, escapesRemoved.Length - 2);
            }
            return escapesRemoved;
        }

        /// <summary>
        /// Registers services to Consul
        /// </summary>
        /// <param name="servcies">services to register to consul</param>
        /// <returns>The consul registration results</returns>
        private IEnumerable<ConsulRegistrationResult> RegisterServices(IEnumerable<RegisteredServiceDetails> services)
        {
            var result = new List<ConsulRegistrationResult>();

            var publicConfig = config.GetConfigurationProperties().PublicConfiguration;
            var rabbitConfiguration = config.GetConfigurationProperties().BrokerConfiguration;

            var brokerIP = publicConfig.BrokerIp;
            var brokerPort = (ushort)publicConfig.BrokerPort;
            var privateBrokerIP = rabbitConfiguration.BusHostIp;
            var privateBrokerPort = (ushort)rabbitConfiguration.BusHostPort;
            var httpIP = publicConfig.HttpIp;
            var httpPort = (ushort)publicConfig.HttpPort;

            foreach (var service in services)
            {
                var serviceName = service.ServiceName;
                var registerBroker = RegisterService(BROKER_ID, serviceName, brokerIP, brokerPort);
                var registerPrivateBroker = RegisterService(BROKER_PRIVATE_ID, serviceName, privateBrokerIP, privateBrokerPort);
                var registerHttp = RegisterService(HTTP_ID, serviceName, httpIP, httpPort);

                result.Add(registerBroker);
                result.Add(registerPrivateBroker);
                result.Add(registerHttp);
            }

            return result;
        }

        /// <summary>
        /// Register provided service to Consul
        /// </summary>
        /// <param name="idFormat">Format string for the ID</param>
        /// <param name="serviceName">String representation of service name</param>
        /// <param name="ip">String representation of ip address</param>
        /// <param name="port">ushort representation of port</param>
        /// <returns>Consul registration result</returns>
        private ConsulRegistrationResult RegisterService(string idFormat, string serviceName, string ip, ushort port)
        {
            var id = string.Format(idFormat, serviceName);
            var registerService = new RegisterServiceRequest(id, serviceName, ip, port);
            var isSuccessful = this.RegisterService(registerService);
            if (!isSuccessful)
            {
                this.log.Warn("{0} was not registered with {1}:{2} on the Consul host {3}",
                registerService.Name,
                registerService.Address,
                registerService.Port,
                config.GetConfigurationProperties().ConsulConfiguration.HostIp);
            }
            else
            {
                this.log.Info("{0} was successfully registered with {1}:{2} on the Consul host {3}",
                registerService.Name,
                registerService.Address,
                registerService.Port,
                config.GetConfigurationProperties().ConsulConfiguration.HostIp);
            }

            return new ConsulRegistrationResult(id, isSuccessful);
        }

        private Maybe<X509Certificate> GetX509Certificate()
        {
            string certString = config.GetConfigurationProperties().ConsulConfiguration.Authorization != null ?
                config.GetConfigurationProperties().ConsulConfiguration.Authorization.PlainText :
                string.Empty;
            if (string.IsNullOrWhiteSpace(certString))
            {
                return Maybe<X509Certificate>.None;
            }

            try
            {
                byte[] rawCert = Encoding.ASCII.GetBytes(certString);
                X509Certificate cert = new X509Certificate(rawCert);
                return cert.ToMaybe();
            }
            catch (Exception)
            {
                return Maybe<X509Certificate>.None;
            }
        }
        #endregion
    }
}
