﻿using EasyNetQ;
using Foci.Orbital.Agent.Services.Interface;
using NLog;
using System;
using System.Collections.Concurrent;

namespace Foci.Orbital.Agent.Services
{
    /// <summary>
    /// Manages the EasyNetQ buses.
    /// Buses should only be created and accessed though the bus manager.
    /// </summary>
    public class BusManager : IBusManager
    {

        private readonly ConcurrentDictionary<string, IBus> currentBuses;
        private ILogger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BusManager()
        {
            this.currentBuses = new ConcurrentDictionary<string, IBus>();
        }

        /// <inheritdoc />
        public bool Exists(string busAddress)
        {
            if (string.IsNullOrWhiteSpace(busAddress))
            {
                logger.Warn("Exists: Bus address should not be null or empty");
                return false;
            }

            return this.currentBuses.ContainsKey(busAddress);
        }

        /// <inheritdoc />
        public IBus Get(string busAddress)
        {
            if (string.IsNullOrWhiteSpace(busAddress))
            {
                logger.Warn("Get: Bus address should not be null or empty");
                return default(IBus);
            }

            IBus bus;
            this.currentBuses.TryGetValue(busAddress, out bus);

            if (bus == default(IBus))
            {
                logger.Warn("Bus at given address does not exist");
            }

            return bus;
        }

        /// <inheritdoc />
        public IBus Add(string busAddress, IBus bus)
        {
            if (string.IsNullOrWhiteSpace(busAddress))
            {
                logger.Warn("Add: Bus address should not be null or empty");
                return default(IBus);
            }

            if (bus == null)
            {
                logger.Warn("Add: Bus should not be null");
                return default(IBus);
            }

            return this.currentBuses.GetOrAdd(busAddress, bus);
        }


        /// <inheritdoc />
        public IBus Remove(string busAddress)
        {
            if (string.IsNullOrWhiteSpace(busAddress))
            {
                logger.Warn("Remove: Bus address should not be null or empty");
                return default(IBus);
            }

            IBus bus;
            this.currentBuses.TryRemove(busAddress, out bus);

            if (bus == default(IBus))
            {
                logger.Warn("Bus has already been removed.");
            }

            return bus;
        }

        /// <inheritdoc />
        public bool Destroy(string busAddress)
        {
            if (string.IsNullOrWhiteSpace(busAddress))
            {
                logger.Warn("Destroy: Bus address cannot be null or empty");
                return true;
            }

            var bus = this.Remove(busAddress);
            if (bus == default(IBus))
            {
                return false;
            }
            bus.Dispose();
            return true;
        }


        #region IDisposible
        private bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                foreach (var bus in this.currentBuses)
                {
                    bus.Value.Dispose();
                }
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        /// <summary>
        /// Allows for checks in testing Disposal.
        /// </summary>
        /// <returns>If the BusManager is disposed.</returns>
        public bool IsDisposed()
        {
            return disposed;
        } 
        #endregion
    }
}
