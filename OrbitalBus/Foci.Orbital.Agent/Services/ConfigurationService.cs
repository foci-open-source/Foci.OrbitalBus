﻿using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Policies.Configuration;
using Foci.Orbital.Agent.Services.Interfaces;
using Newtonsoft.Json;
using NLog;
using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Foci.Orbital.Agent.Services
{
    /// <summary>
    /// Used as a singleton in the Dependency Injector for Agent and Dispatcher.
    /// Json file is loaded in the constructor.
    /// </summary>
    public class ConfigurationService : IConfigurationService
    {
        private readonly ILogger log = LogManager.GetCurrentClassLogger();

        private OrbitalConfiguration configurationProperties;
        private string ConfigJSON;


        /// <inheritdoc />
        public void Initialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                log.Warn("Unable to read Agent configuration file, using default configuration instead.");
                Initialize();
            }
            else
            {
                this.ConfigJSON = json;
                this.configurationProperties = this.LoadFromJSON(json);
            }

            SetCallback();
        }

        /// <inheritdoc />
        public OrbitalConfiguration GetConfigurationProperties()
        {
            if (configurationProperties == null)
            {
                this.configurationProperties = LoadFromJSON(ConfigJSON);
            }

            return configurationProperties;
        }

        /// <summary>
        /// Initialize OrbitalConfiguration using default value
        /// </summary>
        /// <returns>JSON representation of OrbitalConfiguration</returns>
        private string Initialize()
        {
            configurationProperties = new OrbitalConfiguration();
            configurationProperties.ServiceStaticParameters.GetType();//To force update on _serviceStaticParameters
            ConfigJSON = JsonConvert.SerializeObject(configurationProperties);

            return ConfigJSON;
        }

        //Set callback for certificate validation
        private void SetCallback()
        {
            if (configurationProperties.ConsulConfiguration.SslEnabledConsul)
            {
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationComparingIssuers;
            }
        }

        //Load configuration from json
        private OrbitalConfiguration LoadFromJSON(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                throw new ArgumentException("JSON string cannot be empty or null.");
            }

            return ConfigurationDeserializationPolicy.Execute(f => JsonConvert.DeserializeObject<OrbitalConfiguration>(json),
                                                                  new ConfigurationDeserializationData("LoadFromJSON", "Deserialize Json string to an OrbitalConfiguration object", json));
        }

        /// <summary>
        /// If the sslPolicyErrors matches RemoteCertificateNameMismatch enum return true. Development purposes. Do not implement in production.
        /// </summary>
        /// <param name="sender">Object type.</param>
        /// <param name="certificate">X509Certificate object.</param>
        /// <param name="chain">X509Chain object.</param>
        /// <param name="sslPolicyErrors">Enumerator SSL policy errors.</param>
        /// <returns> Result of key signing</returns>
        private static bool RemoteCertificateValidationComparingIssuers(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return sslPolicyErrors == SslPolicyErrors.RemoteCertificateNameMismatch ||
                   sslPolicyErrors == SslPolicyErrors.None;
        }
    }
}
