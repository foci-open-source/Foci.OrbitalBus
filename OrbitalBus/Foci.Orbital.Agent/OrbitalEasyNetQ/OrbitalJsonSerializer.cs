﻿using EasyNetQ;
using Foci.Orbital.Agent.Exceptions;
using Newtonsoft.Json;
using System;
using System.Text;

namespace Foci.Orbital.Agent.OrbitalEasyNetQ
{
    /// <summary>
    /// EasyNetQ replacement component. Replace ISerializer with our custom functionality.
    /// </summary>
    public class OrbitalJsonSerializer : ISerializer
    {
        private readonly ITypeNameSerializer typeNameSerializer;

        private readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            NullValueHandling = NullValueHandling.Ignore
        };

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="typeNameSerializer">Serializer to use for deserialize/serialize a message.</param>
        public OrbitalJsonSerializer(ITypeNameSerializer typeNameSerializer)
        {
            this.typeNameSerializer = typeNameSerializer ?? throw new System.ArgumentNullException(nameof(typeNameSerializer));
        }

        /// <summary>
        /// Convert generic message to bytes.
        /// </summary>
        /// <typeparam name="T">Type of message</typeparam>
        /// <param name="message">Message to be serialized.</param>
        /// <returns>byte array representing the serialized message.</returns>
        public byte[] MessageToBytes<T>(T message) where T : class
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message, serializerSettings));
        }

        /// <summary>
        /// Convert byte array to type.
        /// </summary>
        /// <typeparam name="T">Type of message</typeparam>
        /// <param name="bytes">byte array representing a message.</param>
        /// <returns>Type of message deserialized.</returns>
        public T BytesToMessage<T>(byte[] bytes)
        {
            if (bytes == null)
            {
                bytes = new byte[0];
            }

            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(bytes), serializerSettings);
        }

        /// <summary>
        /// Convert byte array to generic object.
        /// </summary>
        /// <param name="type">The type of object</param>
        /// <param name="bytes">Byte array representing a message</param>
        /// <returns>Deserialized message</returns>
        public object BytesToMessage(Type type, byte[] bytes)
        {
            if (type == null)
            {
                throw new OrbitalArgumentException("Type cannot be null");
            }

            if (bytes == null)
            {
                bytes = new byte[0];
            }

            return JsonConvert.DeserializeObject(Encoding.UTF8.GetString(bytes), type, serializerSettings);
        }
    }
}
