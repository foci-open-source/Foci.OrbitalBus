﻿using EasyNetQ;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Foci.Orbital.Agent.OrbitalEasyNetQ
{ /// <summary>
  /// Replace easynetq component with our custom type name serializer.
  /// </summary>
    public class OrbitalTypeNameSerializer : ITypeNameSerializer
    {
        private readonly ConcurrentDictionary<string, Type> deserializedTypes = new ConcurrentDictionary<string, Type>();
        private readonly ConcurrentDictionary<Type, string> serializedTypes = new ConcurrentDictionary<Type, string>();

        /// <summary>
        /// Deserialize a string that represents a type, to the C# type. If generics are used in the string name they must be in the "Serialize" method format of
        /// this class.
        /// </summary>
        /// <param name="typeName">String representation of Type of object name</param>
        /// <returns>Type of object.</returns>
        public Type DeSerialize(string typeName)
        {
            return deserializedTypes.GetOrAdd(typeName, t =>
            {
                var types = t.Split(',').ToList();
                if (types.Count == 1)
                {
                    var type = GetTypeWithOnePart(types[0]);

                    return type;
                }
                else
                {
                    var type = GetTypeWithMoreThanOnePart(types, t);

                    return type;
                }
            });
        }

        /// <summary>
        /// Serializes a C# type to a string representation of the type. Generic types are packaged in a way such that the base type is first, and
        /// subsequent generic types follow separated by commas. A type is represented by a types Namespace.Name:AssemblyName
        /// </summary>
        /// <param name="type">Type of object</param>
        /// <returns>String representation of Type of object name</returns>
        public string Serialize(Type type)
        {
            return serializedTypes.GetOrAdd(type, t =>
            {
                var typeName = t.Namespace + "." + t.Name + ":" + t.Assembly.GetName().Name;

                foreach (var genericType in t.GenericTypeArguments)
                {
                    typeName += "," + genericType.Namespace + "." + genericType.Name + ":" + genericType.Assembly.GetName().Name;
                }

                if (typeName.Length > 255)
                {
                    throw new EasyNetQException("The serialized name of type '{0}' exceeds the AMQP " +
                                                "maximum short string length of 255 characters.", t.Name);
                }
                return typeName;
            });
        }

        /// <summary>
        /// A helper method to perform work if the there is more than one type embedded in the string. Ie. type with generics.
        /// </summary>
        /// <param name="types">collection of strings representing the Type of object names.</param>
        /// <param name="t">String representation of Type of object name</param>
        /// <returns>Type of object</returns>
        private static Type GetTypeWithMoreThanOnePart(List<string> types, string t)
        {
            var nameParts = types[0].Split(':');

            if (nameParts.Length != 2)
            {
                throw new EasyNetQException(
                    "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
            }

            var baseType = Type.GetType(nameParts[0] + ", " + nameParts[1]);

            var genericTypes = new List<Type>();

            foreach (var genericTypeString in types.Skip(1))
            {
                var genericNameParts = genericTypeString.Split(':');
                if (genericNameParts.Length != 2)
                {
                    throw new EasyNetQException(
                        "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
                }

                var genericType = Type.GetType(genericNameParts[0] + ", " + genericNameParts[1]);
                genericTypes.Add(genericType);
            }

            var type = baseType.MakeGenericType(genericTypes.ToArray());

            if (type == null)
            {
                throw new EasyNetQException("Cannot find type {0}", t);
            }
            return type;
        }

        /// <summary>
        /// A helper method to perform work if the there is only one type embedded in the string. Ie. type with no generics.
        /// </summary>
        /// <param name="t">String representation of Type of object name</param>
        /// <returns>Type of object</returns>
        private static Type GetTypeWithOnePart(string t)
        {
            var nameParts = t.Split(':');
            if (nameParts.Length != 2)
            {
                throw new EasyNetQException(
                    "type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", t);
            }

            var type = Type.GetType(nameParts[0] + ", " + nameParts[1]);
            if (type == null)
            {
                throw new EasyNetQException("Cannot find type {0}", t);
            }
            return type;
        }
    }
}
