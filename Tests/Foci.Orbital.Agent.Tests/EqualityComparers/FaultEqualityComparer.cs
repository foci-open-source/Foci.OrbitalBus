using Foci.Orbital.Adapters.Contract.Faults;
using System.Collections.Generic;

namespace Foci.Orbital.Agent.Tests.EqualityComparers
{
    /// <summary>
    /// Comparer for two faults.
    /// </summary>
    /// <typeparam name="Fault">The type to compare</typeparam>
    public class FaultEqualityComparer : IEqualityComparer<Fault>
    {
        /// <summary>
        /// Gets equality for two provided faults
        /// </summary>
        /// <param name="x">First Fault</param>
        /// <param name="y">Second Fault</param>
        /// <returns>If the faults are equal or not</returns>
        public bool Equals(Fault x, Fault y)
        {
            return y != null && x != null &&
                   x.OrbitalFaultCode == y.OrbitalFaultCode &&
                   x.OrbitalFaultName == y.OrbitalFaultName;
        }

        /// <summary>
        /// Returns hash code for a single fault.
        /// </summary>
        /// <param name="obj">The object to return a hash code for</param>
        /// <returns>The hash code</returns>
        public int GetHashCode(Fault obj)
        {
            var hashCode = 1462544184;
            hashCode = hashCode * -1521134295 + obj.OrbitalFaultCode.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.OrbitalFaultName);
            return hashCode;
        }
    }
}