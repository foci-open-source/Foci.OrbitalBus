﻿using Bogus;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Policies.Cache;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Policies
{
    [ExcludeFromCodeCoverage]
    public class PoliciesTest
    {
        /// <summary>
        /// Test to check if Polly Cache will perform the correct action if the Cache doesn't have the servicedefinition.
        /// </summary>
        [Fact]
        [Trait("Category", "Policy")]
        public void CachePolicyTest()
        {
            #region Input
            var ServiceDefinitionRepo = Substitute.For<IServiceDefinitionRepository>();
            var consulService = Substitute.For<IConsulService>();
            var serviceName = new Guid();
            var operationDef = new Faker<OperationDefinition>()
                .RuleFor(r => r.Adapter, f => new AdapterConfiguration() { Configuration = f.Random.String(), Type = f.Random.String() })
                .RuleFor(r => r.IsSync, f=> false)
                .RuleFor(r => r.Name, f=> f.Random.String())
                .RuleFor(r => r.Schemas, f=> new OperationSchemas() {Request= f.Random.String(), Response = f.Random.String() })
                .RuleFor(r=> r.Translation, f => f.Random.String());
            var Input = new
            {
               
                servicedefinition = new ServiceDefinition() { ServiceName = serviceName.ToString(),
                    Operations = new List<OperationDefinition>() {  operationDef  }
                }
            };
            
            ServiceDefinitionRepo.GetServiceDefinitionByName(serviceName.ToString()).Returns(Input.servicedefinition);
            var cache = new ServiceCache(ServiceDefinitionRepo);
            ServiceCache.StartPolicyCache();
            #endregion
            var Actual = ServiceCachePolicy.Get((context) => ServiceDefinitionRepo.GetServiceDefinitionByName(serviceName.ToString()), serviceName.ToString());

            Assert.Equal(Input.servicedefinition, Actual);
        }
    }
}
