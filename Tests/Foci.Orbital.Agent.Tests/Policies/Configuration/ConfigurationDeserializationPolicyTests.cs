using Bogus;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Policies.Configuration;
using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Policies.Configuration
{
    /// <summary>
    /// Unit tests for the new Policies.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConfigurationDeserializationPolicyTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test to verify what is returned by the policy when it is triggered.
        /// This policy will be implemented when configuration service fails to deserialize the config file.
        /// </summary>
        [Fact]
        [Trait("Category", "Policy")]
        public void ConfigurationDeserializationPolicyTest()
        {

            #region Setup
            var inputException = new JsonSerializationException();
            var inputData = new ConfigurationDeserializationData("methodTest", "methodPurpose", "{\"testString\":\"TestString\"}");
            #endregion

            #region Expected
            var expectedresult = new OrbitalConfiguration();
            #endregion

            var Target = ConfigurationDeserializationPolicy.Execute(Func => throw inputException, inputData);

            Assert.NotNull(Target);
        }
    }
}
