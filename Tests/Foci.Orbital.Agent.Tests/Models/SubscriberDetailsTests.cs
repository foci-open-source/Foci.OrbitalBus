using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Models;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Models
{
    /// <summary>
    /// Unit tests for the SubscriberDetails
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SubscriberDetailsTests
    {
        /// <summary>
        /// Testing the successful creation of Async SubscriberDetails by testing its properties.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SubscriberDetailsAsyncTests()
        {
            var Input = new
            {
                asyncHandler = new Action<string>((t) => { })
            };

            #region Targets
            var Target = SubscriberDetails.Create<string>(Input.asyncHandler, string.Empty) as SubscriberAsyncDetails<string>;

            var Expected = true;
            var Actual = Target.IsSubscriberAsyncDetails();

            var ActualHandler = Target.GetMessageHandler;
            Action<object> ExpectedHandler = (o => Input.asyncHandler((string)o));
            #endregion

            #region Asserts
            Assert.Equal(Expected, Actual);
            // TODO: Perhaps not testing correct call. Need to make sure Input.asyncHandler is returned but the Target encapsulates the action in its own action.
            Assert.Equal(ExpectedHandler.GetType(), ActualHandler.GetType());
            #endregion

        }

        /// <summary>
        /// Testing the successful creation of Sync SubscriberDetails by testing its properties.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SubscriberDetailsSyncTests()
        {
            var Input = new
            {
                syncHandler = new Func<string, Payload>((t) => { return Payload.Create("test"); })
            };

            #region Targets

            var Target = SubscriberDetails.Create(Input.syncHandler, string.Empty) as SubscriberSynchronousDetails<string, Payload>;

            var Actual = Target.IsSubscriberAsyncDetails();
            var Expected = false;

            var ActualHandler = Target.GetMessageHandler;
            Func<string, Payload> ExpectedHandler = (o => Input.syncHandler(o));
            #endregion

            #region Asserts
            Assert.Equal(Expected, Actual);
            Assert.Equal(ExpectedHandler.GetType(), ActualHandler.GetType());
            #endregion
        }

        /// <summary>
        /// Testing the successful convert SubscriberDetails to string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SubscriberDetailsToStringTests()
        {
            var Input = new
            {
                syncHandler = new Func<string, Payload>((t) => { return Payload.Create("test"); })
            };

            var Expected = "SubscriberDetails-RequestType(System.String), ResponseType(Foci.Orbital.Adapters.Contract.Models.Payloads.Payload)";

            var Target = SubscriberDetails.Create(Input.syncHandler, string.Empty) as SubscriberSynchronousDetails<string, Payload>;

            var Actual = Target.ToString();

            Assert.Equal(Expected, Actual);
        }
    }
}
