﻿using Bogus;
using Foci.Orbital.Agent.Models.Consul.Responses;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Tests.Models.Consul
{
    /// <summary>
    /// Unit test for Consul GetServiceInfoResponse.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class GetServiceInfoResponseTests
    {

        /// <summary>
        /// Testing getting the service information details in Consul.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PropertiesTest()
        {
            var fixture = new Faker();

            var Target = new Faker<GetServiceInfoResponse>()
                .RuleFor(g => g.Address, f => f.Random.String())
                .RuleFor(g => g.Id, f=>f.Random.String())
                .RuleFor(g => g.Name, f => f.Random.String())
                .RuleFor(g => g.NodeName, f => f.Random.String())
                .RuleFor(g => g.Port, f => f.Random.Short())
                .RuleFor(g => g.ServiceAddress, f => f.Random.String())
                .RuleFor(g => g.Tags, f => new List<string>())
                .Generate();

            foreach (var item in Target.GetType().GetProperties())
            {
                Assert.NotNull(item.GetValue(Target));
            }
        }
    }
}
