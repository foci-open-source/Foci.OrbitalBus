﻿using Bogus;
using Foci.Orbital.Agent.Models.Consul.Common;
using Foci.Orbital.Agent.Models.Consul.Responses;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Tests.Models.Consul
{
    /// <summary>
    /// Unit test for Consul CheckResponses model.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class CheckResponseTests
    {

        /// <summary>
        /// Testing the success of getting Consul status response details.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PropertyTests(){

            var faker = new Faker();

            var Input = new
            {
                checkId = faker.Random.String(),
                name = faker.Random.String(),
                node = faker.Random.String(),
                notes = faker.Random.String(),
                output = faker.Random.String(),
                serviceId = faker.Random.String(),
                serviceName = faker.Random.String()
            };

            foreach (var checkValue in Enum.GetNames(typeof(CheckStatusValue)))
            {

                var Target = new CheckResponse()
                {
                    CheckId = Input.checkId,
                    Name = Input.name,
                    Node = Input.node,
                    Notes = Input.notes,
                    Output = Input.output,
                    ServiceID = Input.serviceId,
                    ServiceName = Input.serviceName,
                    Status = (CheckStatus) checkValue
                };

                Assert.Equal((CheckStatusValue)((CheckStatus) checkValue), (CheckStatusValue) Target.Status);
                Assert.Equal(Input.checkId, Target.CheckId);
                Assert.Equal(Input.name, Target.Name);
                Assert.Equal(Input.node, Target.Node);
                Assert.Equal(Input.notes, Target.Notes);
                Assert.Equal(Input.serviceId, Target.ServiceID);
                Assert.Equal(Input.serviceName, Target.ServiceName);
                Assert.Equal(string.Format("Node:{0}, CheckId:{1}, Name:{2}, Status:{3}, Notes:{4}, Output:{5}, ServiceID:{6}, ServiceName:{7}",
                    Input.node, Input.checkId, Input.name, (CheckStatus)checkValue, Input.notes, Input.output, Input.serviceId, Input.serviceName)
                    , Target.ToString());
            }
        }
    }
}
