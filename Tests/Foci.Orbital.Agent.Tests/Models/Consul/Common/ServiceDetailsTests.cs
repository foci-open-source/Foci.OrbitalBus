﻿using Bogus;
using Foci.Orbital.Agent.Models.Consul.Common;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Models.Consul.Common
{
    [ExcludeFromCodeCoverage]
    public class ServiceDetailsTests
    {


        /// <summary>
        /// Tests if the ToString override is outputting the necessary information.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToStringTest()
        {
            var faker = new Faker();
            var details = new ServiceDetails(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            {
                Tags = faker.Random.WordsArray(1, 20).ToList()
            };

            var builder = new StringBuilder();
            builder.AppendFormat("Name:{0}, Address:{1}, Port:{2}, ID:{3}, Tags:", details.Name, details.Address, details.Port, details.ID);
            details.Tags.ForEach(tag => builder.AppendFormat("{0};", tag));

            var Target = details;

            var Expected = builder.ToString();

            var Actual = Target.ToString();

            Assert.Equal(Expected, Actual);
        }
    }
}
