﻿using Bogus;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Services.Interfaces;
using NSubstitute;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Models
{

    /// <summary>
    ///  Unit tests for the Adapter BusConnectionDetails
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class BusConnectionDetailsTests
    {
        /// <summary>
        /// Testing for success route. Ensure proper host name is registered.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusconnectionDetailsHostTest()
        {
            var Input = new
            {
                host = "ArbitraryHostName"
            };

            var Target = new BusConnectionDetails(Input.host);
            var ExpectedHost = Input.host;

            var ActualHost = Target.Host;

            Assert.Equal(ActualHost, ExpectedHost);
        }

        /// <summary>
        /// Testing the creation of a BusConnectionDetails from a configuration service.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusConnectionDetailsConfigTest()
        {
            #region Substitutions
            var configSubstitute = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var rabbitConfigFake = new Faker<BrokerConfiguration>()
                    .RuleFor(o => o.BusHostIp, f => f.Internet.Ip())
                    .RuleFor(o => o.BusHostPort, f => f.Random.Number(65535))
                    .RuleFor(c => c.Username, f => f.Internet.UserName())
                    .RuleFor(c => c.Password, f => f.Internet.Password())
                    .RuleFor(o => o.SslEnabled, f => true);

            var Input = new
            {
                config = new OrbitalConfiguration(),
            };
            Input.config.BrokerConfiguration = rabbitConfigFake.Generate();

            configSubstitute.GetConfigurationProperties().Returns(Input.config);
            #endregion

            #region Expected
            var ExpectedHost = Input.config.BrokerConfiguration.BusHostIp;
            var ExpectedPort = Input.config.BrokerConfiguration.BusHostPort.ToString();
            var ExpectedUsername = Input.config.BrokerConfiguration.Username;
            var ExpectedPassword = Input.config.BrokerConfiguration.Password;
            #endregion

            var Target = new BusConnectionDetails(Input.config.BrokerConfiguration);

            #region Actuals
            var ActualHost = Target.Host;
            var ActualPort = Target.Port;
            var ActualSslEnabled = Target.SslEnabled;
            var ActualUsername = Target.Username;
            var ActualPassword = Target.Password;
            #endregion

            #region Asserts
            Assert.Equal(ExpectedHost, ActualHost);
            Assert.Equal(ExpectedPort, ActualPort);
            Assert.True(ActualSslEnabled);
            Assert.Equal(ExpectedUsername, ActualUsername);
            Assert.Equal(ExpectedPassword, ActualPassword);
            #endregion
        }
    }
}
