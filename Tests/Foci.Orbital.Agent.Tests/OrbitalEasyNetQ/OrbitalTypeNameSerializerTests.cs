﻿using EasyNetQ;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.OrbitalEasyNetQ;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.OrbitalEasyNetQ
{
    [ExcludeFromCodeCoverage]
    public class OrbitalTypeNameSerializerTests
    {
        /// <summary>
        /// Successful serialization of a type. Testing all possible types.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestSuccessSerialize()
        {
            var Target = new OrbitalTypeNameSerializer();
            var Expected = "System.String:System.Private.CoreLib";
            var Actual = Target.Serialize(typeof(string));

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the successful deserialization of a string to it's type. Tests all three possible success cases.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestSuccessDeSerialize()
        {
            var Target = new OrbitalTypeNameSerializer();

            var Expected = typeof(string);
            var Actual = Target.DeSerialize("System.String:System.Private.CoreLib");

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the successful deserialization of a maybe<string>
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void TestSuccessDeSerializeMultipart()
        {
            var Target = new OrbitalTypeNameSerializer();

            var Expected = typeof(Maybe<string>);
            var Actual = Target.DeSerialize("Foci.Orbital.Agent.Models.Maybe`1:Foci.Orbital.Agent,System.String:mscorlib");

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing deserialization of multi-part type with first invalid type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestDeSerializeInvalidFirstType()
        {
            var input = ",";
            var Expected = string.Format("type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", input);
            var Target = new OrbitalTypeNameSerializer();
            var Actual = Assert.Throws<EasyNetQException>(() => Target.DeSerialize(input));

            Assert.Equal(Expected, Actual.Message);
        }

        /// <summary>
        /// Testing deserialization of multi-part with invalid second type
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestDeSerializeInvalidSecondType()
        {
            var input = ":,";
            var Expected = string.Format("type name {0}, is not a valid EasyNetQ type name. Expected Type:Assembly", input);
            var Target = new OrbitalTypeNameSerializer();
            var Actual = Assert.Throws<EasyNetQException>(() => Target.DeSerialize(input));

            Assert.Equal(Expected, Actual.Message);
        }

        /// <summary>
        /// Testing fail to deserialize invalid type string
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestDeSerializeInvalidTypeString()
        {
            var Target = new OrbitalTypeNameSerializer();
            Assert.Throws<EasyNetQException>(() => Target.DeSerialize("System.String"));
        }

        /// <summary>
        /// Testing fail to deserialize null type string
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TestDeSerializeNullTypeString()
        {
            var Target = new OrbitalTypeNameSerializer();
            Assert.Throws<EasyNetQException>(() => Target.DeSerialize(":"));
        }
    }
}
