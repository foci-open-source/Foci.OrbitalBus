﻿using Bogus;
using EasyNetQ;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.OrbitalEasyNetQ;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Xunit;

namespace Foci.Orbital.Agent.Tests.OrbitalEasyNetQ
{
    /// <summary>
    /// Unit tests for the OrbitalJsonSerializer.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalJsonSerializerTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Tests the MessageToBytes method for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void MessageToBytesTest()
        {
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();

            var Input = new TestType { TestName = "TestNameValue", TestAge = 14 };
            var ExpectedAsString = "{\"TestName\":\"TestNameValue\",\"TestAge\":14}";

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            var Actual = Target.MessageToBytes<TestType>(Input);

            Assert.IsType<byte[]>(Actual);
            Assert.Equal(ExpectedAsString, Encoding.Default.GetString(Actual));
        }

        /// <summary>
        /// Tests the BytesToMessage method which accepts a byte[] for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BytesToMessageByteArray()
        {
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();

            var Input = Encoding.Default.GetBytes("{\"TestName\":\"TestNameValue\",\"TestAge\":14}");
            var Expected = new TestType { TestName = "TestNameValue", TestAge = 14 };

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            var Actual = Target.BytesToMessage<TestType>(Input);

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.TestName, Actual.TestName);
            Assert.Equal(Expected.TestAge, Actual.TestAge);
        }

        /// <summary>
        /// Tests the BytesToMessage method which accepts a type name for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BytesToMessageTypeName()
        {
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            TypeNameSerializer.DeSerialize("TestType").Returns(typeof(TestType));

            var InputBytes = Encoding.Default.GetBytes("{\"TestName\":\"TestNameValue\",\"TestAge\":14}");
            var Expected = new TestType { TestName = "TestNameValue", TestAge = 14 };

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            var Actual = Target.BytesToMessage(typeof(TestType), InputBytes);

            var ActualForAsserts = Actual as TestType;
            Assert.IsType(Expected.GetType(), ActualForAsserts);
            Assert.Equal(Expected.TestName, ActualForAsserts.TestName);
            Assert.Equal(Expected.TestAge, ActualForAsserts.TestAge);
        }

        /// <summary> 
        /// Test passing type name with null byte array to OrbitalJsonSerializer
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void OrbitalJsonSerializerNullSerializer()
        {
            Assert.Throws<ArgumentNullException>(() => new OrbitalJsonSerializer(null));
        }

        /// <summary> 
        /// Test passing null byte array to BytesToMessage method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BytesToMessageNullArray()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            Assert.Null(Target.BytesToMessage<TestType>(null));
        }

        /// <summary> 
        /// Test passing null byte array to BytesToMessage method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BytesToMessageNullTypeName()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            #region Setup
            var inputString = faker.Random.String();
            var inputNum = faker.Random.Number();
            var Input = new
            {
                bytes = Encoding.Default.GetBytes("{\"TestName\":\"" + inputString + "\",\"TestAge\":" + inputNum + "}")
            };
            #endregion

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            Assert.Throws<OrbitalArgumentException>(() => Target.BytesToMessage(null, Input.bytes));
        }

        /// <summary> 
        /// Test passing type name with null byte array to BytesToMessage method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BytesToMessageWithNullByteArray()
        {
            #region Substitutions
            var TypeNameSerializer = Substitute.For<ITypeNameSerializer>();
            #endregion

            var Target = new OrbitalJsonSerializer(TypeNameSerializer);

            Assert.Null(Target.BytesToMessage(typeof(TestType), null));
        }
    }

    //Test type for the serialization in this class.
    [ExcludeFromCodeCoverage]
    public class TestType
    {
        public string TestName { get; set; }
        public int TestAge { get; set; }
    }
}
