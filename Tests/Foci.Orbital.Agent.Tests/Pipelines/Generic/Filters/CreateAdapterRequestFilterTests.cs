﻿using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Tests.EqualityComparers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for CreateAdapterRequestFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class CreateAdapterRequestFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing port containing a valid adapter type to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInformation()
        {

            #region Setup
            var inputType = faker.Random.String();
            var @params = new Dictionary<string, string>();
            var readonlyparams = new ReadOnlyDictionary<string, string>(@params);
            var message = faker.Random.String();
            var config = faker.Random.String();
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    Parameters = @params,
                    TranslatedMessage = message,
                    AdapterConfiguration = config
                }
            };
            #endregion

            var Target = new CreateAdapterRequestFilter<TestPort>();

            var Expected = new AdapterRequest(config, message, readonlyparams);
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected.AdapterConfiguration, Actual.AdapterRequest.AdapterConfiguration);
            Assert.Equal(Expected.Message, Actual.AdapterRequest.Message);
        }

        /// <summary>
        /// Test passing port containing a valid adapter type to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithNullParameters()
        {

            #region Setup
            var inputType = faker.Random.String();
            var error = "Adapter parameters cannot be null";
            var exception = new OrbitalArgumentException(error);
            var inputFault = new AgentFaultFactory().CreateFault(exception);
            var message = faker.Random.String();
            var config = faker.Random.String();

            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    TranslatedMessage = message,
                    AdapterConfiguration = config,
                    Parameters = null
                }
            };
            #endregion
            var Expected = new List<Fault>() { inputFault };
            var Target = new CreateAdapterRequestFilter<TestPort>();

            var Actual = Target.Process(Input.port);
            Assert.True(Actual.IsFaulted);
            Assert.Equal(Expected, Actual.Faults, new FaultEqualityComparer());
        }

        #region MockClass
        private class TestPort : IFaultablePort, IAdapterRequestPort, ITranslatedMessagePort, IAdapterParametersPort, IAdapterDescriptionPort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public string AdapterType { get; set; }
            public string AdapterConfiguration { get; set; }
            public IAdapter Adapter { get; set; }
            public AdapterRequest AdapterRequest { get; set; }
            public string TranslatedMessage { get; set; }
            public IDictionary<string, string> Parameters { get; set; }

            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }

        }
        #endregion
    }
}
