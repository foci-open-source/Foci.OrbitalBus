using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Services.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for TranslateOrbitalRequestBodyFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class TranslateOrbitalRequestBodyFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing null service to TranslateOrbitalRequestBodyFilter 
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TranslateOrbitalRequestBodyFilterWithNullService()
        {
            Assert.Throws<ArgumentNullException>(() => new TranslateOrbitalRequestBodyFilter<TestPort>(null));
        }

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInputs()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Translation = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String()
                },
                translated = faker.Random.String()
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Inbound).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.Inbound)
                .Returns(Input.translated);
            #endregion

            var Target = new TranslateOrbitalRequestBodyFilter<TestPort>(translationService);

            var Expected = Input.translated;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual.TranslatedMessage);
        }

        /// <summary>
        /// Test passing a port with no request body to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithNullMessage()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Translation = faker.Random.String()
                }
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Inbound).Returns(true);
            #endregion

            var Target = new TranslateOrbitalRequestBodyFilter<TestPort>(translationService);

            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Empty(Actual.TranslatedMessage);
        }

        /// <summary>
        /// Test passing null port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPort()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            var Target = new TranslateOrbitalRequestBodyFilter<TestPort>(translationService);

            Assert.Null(Target.Process(null));
        }

        /// <summary>
        /// Test passing faulted port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithFaultedPort()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new ArgumentNullException());
            var Input = new
            {
                port = new TestPort()
                {
                    Faults = new List<Fault>()
                    {
                        inputFault
                    }
                }
            };
            #endregion

            var Target = new TranslateOrbitalRequestBodyFilter<TestPort>(translationService);

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(Expected.Faults, Actual.Faults);
        }

        /// <summary>
        /// Test passing a port with invalid translation to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidTranslation()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Translation = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String()
                }
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Inbound)
                .Returns(x => throw new OrbitalTranslationException());
            #endregion

            var Target = new TranslateOrbitalRequestBodyFilter<TestPort>(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_TranslationFault_002;

            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        #region MockClass
        private class TestPort : IFaultablePort, ITranslationPort, IOrbitalRequestBodyPort, ITranslatedMessagePort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public string Translation { get; set; }
            public string OrbitalRequestBody { get; set; }
            public string TranslatedMessage { get; set; }

            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }
        }
        #endregion
    }
}
