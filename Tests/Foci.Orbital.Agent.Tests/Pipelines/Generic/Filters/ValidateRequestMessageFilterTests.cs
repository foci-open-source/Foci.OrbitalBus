using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using NJsonSchema;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for ValidateRequestMessageFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ValidateRequestMessageFilterTests
    {
        private readonly Faker faker = new Faker();

        #region Fields
        private readonly JsonSchema4 jsonSchema = JsonSchema4.FromJsonAsync(
            @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}"
            ).Result;
        #endregion

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInputs()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    OrbitalRequestBody = "{\"id\":1}",
                    ValidateRequest = true,
                    RequestSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateRequestMessageFilter<TestPort>();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidateRequestFalse()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    ValidateRequest = false
                }
            };
            #endregion

            var Target = new ValidateRequestMessageFilter<TestPort>();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with null request to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullRequest()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    ValidateRequest = true,
                    RequestSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateRequestMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with null request schema to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullRequestSchema()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    OrbitalRequestBody = "{\"id\":1}",
                    ValidateRequest = true,
                    RequestSchema = null
                }
            };
            #endregion

            var Target = new ValidateRequestMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with invalid request message to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidMessage()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    OrbitalRequestBody = "{ }",
                    ValidateRequest = true,
                    RequestSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateRequestMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Business_Validation_Error_015;

            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        #region MockClass
        private class TestPort : IFaultablePort, ISchemaPort, IOrbitalRequestBodyPort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public string OrbitalRequestBody { get; set; }
            public bool ValidateRequest { get; set; }
            public bool ValidateResponse { get; set; }
            public JsonSchema4 RequestSchema { get; set; }
            public JsonSchema4 ResponseSchema { get; set; }


            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }
        }
        #endregion
    }
}
