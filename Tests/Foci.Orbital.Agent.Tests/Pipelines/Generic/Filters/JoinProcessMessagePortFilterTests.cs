using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using NJsonSchema;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for JoinProcessMessagePortFilter class
    /// </summary>v
    [ExcludeFromCodeCoverage]
    public class JoinProcessMessagePortFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing 3 valid ports to Process method
        /// 
        /// Note: ProcessThree method only have one test,
        ///   because all other conditions is covered by unit tests for Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessThreeAllValid()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>())),
                    ValidateRequest = true,
                    ValidateResponse = true,
                    RequestSchema = new JsonSchema4(),
                    ResponseSchema = new JsonSchema4()
                },

                port2 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                },

                port3 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                }
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2, input.port3);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = input.port1;
            var ExpectedParamSize = 3;

            var Actual = Target.ProcessThree(inputTuple);

            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Faults);
            Assert.Equal(Expected.Adapter, Actual.Adapter);
            Assert.Equal(Expected.AdapterType, Actual.AdapterType);
            Assert.Equal(Expected.AdapterConfiguration, Actual.AdapterConfiguration);
            Assert.Equal(Expected.OrbitalRequestBody, Actual.OrbitalRequestBody);
            Assert.Equal(ExpectedParamSize, Actual.Parameters.Count);
            Assert.Equal(Expected.TranslatedMessage, Actual.TranslatedMessage);
            Assert.Equal(Expected.Translation, Actual.Translation);
            Assert.Equal(Expected.Payload, Actual.Payload);
            Assert.Equal(Expected.AdapterRequest, Actual.AdapterRequest);
            Assert.True(Actual.ValidateRequest);
            Assert.True(Actual.ValidateResponse);
            Assert.Equal(Expected.RequestSchema, Actual.RequestSchema);
            Assert.Equal(Expected.ResponseSchema, Actual.ResponseSchema);
        }

        /// <summary>
        /// Test passing 2 valid ports to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessBothValid()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>())),
                    ValidateRequest = true,
                    ValidateResponse = true,
                    RequestSchema = new JsonSchema4(),
                    ResponseSchema = new JsonSchema4()
                },

                port2 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                }
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = input.port1;
            var ExpectedParamSize = 2;

            var Actual = Target.Process(inputTuple);

            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Faults);
            Assert.Equal(Expected.Adapter, Actual.Adapter);
            Assert.Equal(Expected.AdapterType, Actual.AdapterType);
            Assert.Equal(Expected.AdapterConfiguration, Actual.AdapterConfiguration);
            Assert.Equal(Expected.OrbitalRequestBody, Actual.OrbitalRequestBody);
            Assert.Equal(ExpectedParamSize, Actual.Parameters.Count);
            Assert.Equal(Expected.TranslatedMessage, Actual.TranslatedMessage);
            Assert.Equal(Expected.Translation, Actual.Translation);
            Assert.Equal(Expected.Payload, Actual.Payload);
            Assert.Equal(Expected.AdapterRequest, Actual.AdapterRequest);
            Assert.True(Actual.ValidateRequest);
            Assert.True(Actual.ValidateResponse);
            Assert.Equal(Expected.RequestSchema, Actual.RequestSchema);
            Assert.Equal(Expected.ResponseSchema, Actual.ResponseSchema);
        }

        /// <summary>
        /// Test passing 2 ports to Process method and one of the port is empty
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessOneEmpty()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var input = new
            {
                port2 = new ProcessMessagePort()
                {
                    AdapterType = faker.Random.String(),
                    Adapter = adapter,
                    AdapterConfiguration = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String(),
                    Parameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    },
                    TranslatedMessage = faker.Random.String(),
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String()),
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                        new ReadOnlyDictionary<string, string>(new Dictionary<string, string>())),
                    ValidateRequest = true,
                    ValidateResponse = true,
                    RequestSchema = new JsonSchema4(),
                    ResponseSchema = new JsonSchema4()
                },

                port1 = new ProcessMessagePort()
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = input.port2;

            var Actual = Target.Process(inputTuple);

            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Faults);
            Assert.Equal(Expected.Adapter, Actual.Adapter);
            Assert.Equal(Expected.AdapterType, Actual.AdapterType);
            Assert.Equal(Expected.AdapterConfiguration, Actual.AdapterConfiguration);
            Assert.Equal(Expected.OrbitalRequestBody, Actual.OrbitalRequestBody);
            Assert.Equal(Expected.Parameters, Actual.Parameters);
            Assert.Equal(Expected.TranslatedMessage, Actual.TranslatedMessage);
            Assert.Equal(Expected.Translation, Actual.Translation);
            Assert.Equal(Expected.Payload, Actual.Payload);
            Assert.Equal(Expected.AdapterRequest, Actual.AdapterRequest);
            Assert.True(Actual.ValidateRequest);
            Assert.True(Actual.ValidateResponse);
            Assert.Equal(Expected.RequestSchema, Actual.RequestSchema);
            Assert.Equal(Expected.ResponseSchema, Actual.ResponseSchema);
        }

        /// <summary>
        /// Test passing 2 empty ports to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessBothEmpty()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort(),
                port2 = new ProcessMessagePort()
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Actual = Target.Process(inputTuple);

            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Faults);
            Assert.Null(Actual.Adapter);
            Assert.Null(Actual.AdapterType);
            Assert.Null(Actual.AdapterConfiguration);
            Assert.Null(Actual.OrbitalRequestBody);
            Assert.Empty(Actual.Parameters);
            Assert.Null(Actual.TranslatedMessage);
            Assert.Null(Actual.Translation);
            Assert.Null(Actual.Payload);
            Assert.Null(Actual.AdapterRequest);
            Assert.False(Actual.ValidateRequest);
            Assert.False(Actual.ValidateResponse);
            Assert.Null(Actual.RequestSchema);
            Assert.Null(Actual.ResponseSchema);
        }

        /// <summary>
        /// Test passing 2 null ports to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessBothNull()
        {
            #region Setup
            var input = Tuple.Create<ProcessMessagePort, ProcessMessagePort>(null, null);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 2;

            var Actual = Target.Process(input);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.ElementAt(0).OrbitalFaultCode);
            Assert.Equal(Expected, Actual.Faults.ElementAt(1).OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing 2 ports to Process method and one of them is null
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessOneNull()
        {
            #region Setup
            var input = Tuple.Create<ProcessMessagePort, ProcessMessagePort>(new ProcessMessagePort(), null);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 1;

            var Actual = Target.Process(input);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing 2 faulted ports to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessBothFaulted()
        {
            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort()
                {
                    Faults = new List<Fault>() { new AgentFaultFactory().CreateFault(new OrbitalTranslationException()) }
                },
                port2 = new ProcessMessagePort()
                {
                    Faults = new List<Fault>() { new AgentFaultFactory().CreateFault(new OrbitalTranslationException()) }
                }
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = OrbitalFaultCode.Orbital_Runtime_TranslationFault_002;
            var ExpectedSize = 2;

            var Actual = Target.Process(inputTuple);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.ElementAt(0).OrbitalFaultCode);
            Assert.Equal(Expected, Actual.Faults.ElementAt(1).OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing 2 ports to Process method and one of them is faulted
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessOneFaulted()
        {
            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort()
                {
                    Faults = new List<Fault>() { new AgentFaultFactory().CreateFault(new OrbitalTranslationException()) }
                },
                port2 = new ProcessMessagePort()
            };
            ;
            var inputTuple = Tuple.Create(input.port1, input.port2);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var Expected = OrbitalFaultCode.Orbital_Runtime_TranslationFault_002;
            var ExpectedSize = 1;

            var Actual = Target.Process(inputTuple);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing 2 ports to Process method, where one is faulted and the other one is null
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessOneFaultedOneNull()
        {
            #region Setup
            var input = new
            {
                port1 = new ProcessMessagePort()
                {
                    Faults = new List<Fault>() { new AgentFaultFactory().CreateFault(new OrbitalTranslationException()) }
                }
            };
            ;
            var inputTuple = Tuple.Create<ProcessMessagePort, ProcessMessagePort>(input.port1, null);
            #endregion

            var Target = new JoinProcessMessagePortFilter();

            var ExpectedFault1 = OrbitalFaultCode.Orbital_Runtime_TranslationFault_002;
            var ExpectedFault2 = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 2;

            var Actual = Target.Process(inputTuple);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(ExpectedFault1, Actual.Faults.ElementAt(0).OrbitalFaultCode);
            Assert.Equal(ExpectedFault2, Actual.Faults.ElementAt(1).OrbitalFaultCode);
        }
    }
}
