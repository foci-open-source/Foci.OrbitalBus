using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Unity;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for GetAdapterFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class GetAdapterFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing null container to GetAdapterFilter 
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetAdapterFilterWithNullContainer()
        {
            Assert.Throws<ArgumentNullException>(() => new GetAdapterFilter<TestPort>(null));
        }

        /// <summary>
        /// Test passing port containing a valid adapter type to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidAdapterType()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            #endregion

            #region Setup
            var inputType = faker.Random.String();
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType
                }
            };
            container.IsRegistered<IAdapter>(inputType).Returns(true);
            container.Resolve<IAdapter>(inputType).Returns(adapter);
            #endregion

            var Target = new GetAdapterFilter<TestPort>(container);

            var Expected = adapter;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual.Adapter);
        }

        /// <summary>
        /// Test passing null port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPort()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            #endregion
            var Target = new GetAdapterFilter<TestPort>(container);

            Assert.Null(Target.Process(null));
        }

        /// <summary>
        /// Test passing faulted port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithFaultedPort()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            #endregion

            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new ArgumentNullException());
            var Input = new
            {
                port = new TestPort()
                {
                    Faults = new List<Fault>()
                    {
                        inputFault
                    }
                }
            };
            #endregion

            var Target = new GetAdapterFilter<TestPort>(container);

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(Expected.Faults, Actual.Faults);
        }

        /// <summary>
        /// Test passing port that does not contain an adapter type to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithoutAdapterType()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = string.Empty
                }
            };
            #endregion

            var Target = new GetAdapterFilter<TestPort>(container);

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 1;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing port that does not contain an adapter type to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidAdapterType()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            #endregion

            #region Setup
            var inputType = faker.Random.String();
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType
                }
            };
            container.IsRegistered<IAdapter>(inputType).Returns(false);
            #endregion

            var Target = new GetAdapterFilter<TestPort>(container);

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 1;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing port containing valid adapter type (but the adapter can't be resolved in container) to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessAdapterNotFound()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            #endregion

            #region Setup
            var inputType = faker.Random.String();
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType
                }
            };
            container.IsRegistered<IAdapter>(inputType).Returns(true);
            #endregion

            var Target = new GetAdapterFilter<TestPort>(container);

            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Adapter);
        }

        #region MockClass
        private class TestPort : IFaultablePort, IAdapterDescriptionPort, IAdapterPort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public string AdapterType { get; set; }
            public string AdapterConfiguration { get; set; }
            public IAdapter Adapter { get; set; }

            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }
        }
        #endregion
    }
}
