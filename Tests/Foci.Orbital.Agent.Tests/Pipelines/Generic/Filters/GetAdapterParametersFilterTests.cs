﻿using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Pipelines.Generic.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Services.Interfaces;
using Jint;
using Jint.Native.Error;
using Jint.Runtime;
using Newtonsoft.Json;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Pipelines.Generic.Filters
{
    /// <summary>
    /// Unit tests for GetAdapterParametersFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class GetAdapterParametersFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing port containing a valid information to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInformation()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            var configService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = faker.Random.String(),
                    Translation = faker.Random.String(),
                    OrbitalRequestBody = faker.Random.String()

                },
                dictionary = new Dictionary<string, string>() {
                    { faker.Random.String(), faker.Random.String() }
                },
                config = new OrbitalConfiguration()
                {
                    ServiceStaticParameters = new Dictionary<string, string>()
                    {
                        { faker.Random.String(), faker.Random.String() }
                    }
                }
            };

            var inputJson = JsonConvert.SerializeObject(Input.dictionary);

            configService.GetConfigurationProperties().Returns(Input.config);
            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.GetParams).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.GetParams)
                .Returns(inputJson);
            #endregion

            var Target = new GetAdapterParametersFilter<TestPort>(translationService, configService);

            var Expected = Input.dictionary.Union(Input.config.ServiceStaticParameters).ToDictionary(key => key.Key, value => value.Value);
            var Actual = Target.Process(Input.port).Parameters;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Throw null exception if the json translation service is null
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullTranslationService()
        {
            #region Substitutions
            IJsonTranslationService translationService = null;
            var configService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup            
            var Input = new
            {
                port = new TestPort()
            };
            #endregion            

            Assert.Throws<ArgumentNullException>(() => new GetAdapterParametersFilter<TestPort>(translationService, configService));
        }

        /// <summary>
        /// Throw null exception if the configuration service is null
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullConfigurationService()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            IConfigurationService configService = null;
            #endregion

            #region Setup            
            var Input = new
            {
                port = new TestPort()
            };
            #endregion            

            Assert.Throws<ArgumentNullException>(() => new GetAdapterParametersFilter<TestPort>(translationService, configService));
        }

        /// <summary>
        /// Test passing port containing a valid information to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWhenGetConfigurationPropertiesReturnsNull()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            var configService = Substitute.For<IConfigurationService>();
            var engine = Substitute.For<Engine>();
            #endregion

            #region Setup
            var inputType = this.faker.Random.String();
            var translation = this.faker.Random.String();
            var message = this.faker.Random.String();
            var exception = new JavaScriptException(new ErrorConstructor(engine));
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    Translation = translation,
                    OrbitalRequestBody = message

                },
                dictionary = new Dictionary<string, string>(),
                json = "{\"testString\": \"TestString\"}"
            };

            Input.dictionary.Add("testString", "TestString");

            configService.GetConfigurationProperties().Returns(a => null);
            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.GetParams).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.GetParams).Returns(Input.json);

            #endregion
            var Target = new GetAdapterParametersFilter<TestPort>(translationService, configService);

            var Actual = Target.Process(Input.port);

            Assert.Equal(Input.port, Actual, new TestPortEqualityComparer());
        }

        /// <summary>
        /// Test passing port when the GetDyanmicParameters doesn't return a dictionary.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWhenAdapterJavascriptDoesNotReturnDictionary()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            var configService = Substitute.For<IConfigurationService>();
            var engine = Substitute.For<Engine>();
            #endregion

            #region Setup
            var inputType = this.faker.Random.String();
            var translation = this.faker.Random.String();
            var message = this.faker.Random.String();
            var exception = new JavaScriptException(new ErrorConstructor(engine));
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    Translation = translation,
                    OrbitalRequestBody = message

                },
                dictionary = new Dictionary<string, string>(),
            };

            configService.GetConfigurationProperties().Returns(a => null);
            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.GetParams).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.GetParams).Returns(a => null);

            #endregion
            var Target = new GetAdapterParametersFilter<TestPort>(translationService, configService);

            var Actual = Target.Process(Input.port);

            Assert.Equal(Input.port, Actual, new TestPortEqualityComparer());
        }

        /// <summary>
        /// Test passing port containing a valid information to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessThrowsFault()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            var configService = Substitute.For<IConfigurationService>();
            var engine = Substitute.For<Engine>();
            #endregion

            #region Setup
            var inputType = this.faker.Random.String();
            var translation = this.faker.Random.String();
            var message = this.faker.Random.String();
            var exception = new JavaScriptException(new ErrorConstructor(engine));
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    Translation = translation,
                    OrbitalRequestBody = message

                },
                dictionary = new Dictionary<string, string>(),
                json = "{\"testString\": \"TestString\"}"
            };

            Input.dictionary.Add("testString", "TestString");

            configService.GetConfigurationProperties().Returns(new OrbitalConfiguration());
            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.GetParams).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.GetParams).Returns(x => { throw exception; });

            #endregion
            var Target = new GetAdapterParametersFilter<TestPort>(translationService, configService);
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.Faults.First().OrbitalFaultCode == OrbitalFaultCode.Orbital_Business_Translation_Error_013);
        }

        /// <summary>
        /// Test that shows non-expected exceptions are raised past the filter
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessThrowsException()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            var configService = Substitute.For<IConfigurationService>();
            var engine = Substitute.For<Engine>();
            #endregion

            #region Setup
            var inputType = this.faker.Random.String();
            var translation = this.faker.Random.String();
            var message = this.faker.Random.String();
            var exception = new OutOfMemoryException();
            var Input = new
            {
                port = new TestPort()
                {
                    AdapterType = inputType,
                    Translation = translation,
                    OrbitalRequestBody = message

                },
                dictionary = new Dictionary<string, string>(),
                json = "{\"testString\": \"TestString\"}"
            };

            Input.dictionary.Add("testString", "TestString");

            configService.GetConfigurationProperties().Returns(new OrbitalConfiguration());
            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.GetParams).Returns(true);
            translationService.ExecuteTranslationFunction(Input.port.OrbitalRequestBody, Input.port.Translation, JsonTranslationFunction.GetParams).Returns(x => { throw exception; });

            #endregion
            var Target = new GetAdapterParametersFilter<TestPort>(translationService, configService);
            Assert.Throws<OutOfMemoryException>(() => Target.Process(Input.port));
        }

        #region MockClass
        private class TestPort : IFaultablePort, ITranslationPort, IOrbitalRequestBodyPort, IAdapterParametersPort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public string AdapterType { get; set; }
            public string AdapterConfiguration { get; set; }
            public IAdapter Adapter { get; set; }
            public string Translation { get; set; }
            public string OrbitalRequestBody { get; set; }
            public IDictionary<string, string> Parameters { get; set; }

            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }
        }

        /// <summary>
        /// States the rules of equality for the TestPort
        /// </summary>
        private class TestPortEqualityComparer : IEqualityComparer<TestPort>
        {
            public bool Equals(TestPort x, TestPort y)
            {
                return y != null && x != null &&
                       EqualityComparer<ICollection<Fault>>.Default.Equals(x.Faults, y.Faults) &&
                       x.IsFaulted == y.IsFaulted &&
                       x.AdapterType == y.AdapterType &&
                       x.AdapterConfiguration == y.AdapterConfiguration &&
                       EqualityComparer<IAdapter>.Default.Equals(x.Adapter, y.Adapter) &&
                       x.Translation == y.Translation &&
                       x.OrbitalRequestBody == y.OrbitalRequestBody &&
                       EqualityComparer<IDictionary<string, string>>.Default.Equals(x.Parameters, y.Parameters);
            }

            public int GetHashCode(TestPort obj)
            {
                return HashCode.Combine(obj.Faults, obj.IsFaulted, obj.AdapterType, obj.AdapterConfiguration, obj.Adapter, obj.Translation, obj.OrbitalRequestBody, obj.Parameters);
            }
        }
        #endregion
    }
}
