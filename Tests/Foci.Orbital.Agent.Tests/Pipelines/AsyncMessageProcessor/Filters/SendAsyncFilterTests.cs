using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.AsyncMessageProcessor.Filters;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Tests.TheoryData.ProcessWithInvalidAdapter;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.AsyncMessageProcessor.Filters
{
    /// <summary>
    /// Unit tests for SendAsyncFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SendAsyncFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing null port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPort()
        {
            var Target = new SendAsyncFilter();

            Assert.Null(Target.Process(null));
        }

        /// <summary>
        /// Test passing port with valid adapter request to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidAdapterRequest()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Adapter = adapter,
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                    new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                }
            };
            #endregion

            var Target = new SendAsyncFilter();

            var Actual = Target.Process(Input.port);

            adapter.Received().SendAsync(Input.port.AdapterRequest);
            Assert.False(Actual.IsFaulted);
            Assert.Null(Actual.Payload);
        }

        /// <summary>
        /// Test passing faulted port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithFaultedPort()
        {
            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new ArgumentNullException());
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Faults = new List<Fault>()
                    {
                        inputFault
                    }
                }
            };
            #endregion

            var Target = new SendAsyncFilter();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(Expected.Faults, Actual.Faults);
        }

        /// <summary>
        /// Test passing port without a valid adapter or AdapterRequest to Process method
        /// </summary>
        [Theory]
        [ClassData(typeof(InputProcessWithInvalidAdapter))]
        [Trait("Category", "Failure")]
        internal void ProcessWithInvalidAdapter(ProcessMessagePort port)
        {

            var Target = new SendAsyncFilter();

            var Expected = OrbitalFaultCode.Orbital_Business_Adapter_Error_012;
            var ExpectedSize = 1;
            var Actual = Target.Process(port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

    }
}
