using Bogus;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Services.Interfaces;
using Newtonsoft.Json;
using NSubstitute;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Unity;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Pipelines.AsyncMessageProcessor
{
    /// <summary>
    /// Unit tests for AsyncMessageProcessor class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class AsyncMessageProcessorTests
    {
        private readonly Faker faker = new Faker();

        private const string schema = @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}";

        /// <summary>
        /// Test running stop method after running start
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void StopAfterStart()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            var Target = new Orbital.Agent.Pipelines.AsyncMessageProcessor.AsyncMessageProcessor(container, translationService, configurationService);

            Target.Start();
            var Actual = Target.Stop();

            Assert.True(Actual);
        }

        /// <summary>
        /// Test running stop method before running start
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void StopBeforeStart()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            var Target = new Orbital.Agent.Pipelines.AsyncMessageProcessor.AsyncMessageProcessor(container, translationService, configurationService);

            var Actual = Target.Stop();

            Assert.True(Actual);
        }

        /// <summary>
        /// Test Push method with valid inputs
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PushWithValidInput()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                adapterType = faker.Lorem.Word(),
                adapterConfig = faker.Random.String(),
                requestBody = "{\"id\":1}",
                translation = faker.Random.String(),
                translatedBody = faker.Random.String(),
                param = new Dictionary<string, string>()
                {
                    {faker.Random.String(), faker.Random.String() }
                },
                adapterMsg = faker.Random.String(),
                translatedAdapterMsg = faker.Random.String()
            };
            var inputOperation = new OperationDefinition()
            {
                Translation = Input.translation,
                Adapter = new AdapterConfiguration()
                {
                    Type = Input.adapterType,
                    Configuration = Input.adapterConfig
                },
                Schemas = new OperationSchemas()
                {
                    Request = schema
                },
                Validate = new OperationValidation()
                {
                    Request = true
                }
            };
            var inputConfig = new OrbitalConfiguration();
            var inputRequest = new MessageProcessorInput(inputOperation, Input.requestBody);

            //For getting adapter
            container.IsRegistered<IAdapter>(Input.adapterType).Returns(true);
            container.Resolve<IAdapter>(Input.adapterType).Returns(adapter);

            //For static parameters
            configurationService.GetConfigurationProperties().Returns(inputConfig);

            //For translations and dynamic parameters
            translationService.FunctionExist(Input.translation, Arg.Any<JsonTranslationFunction>()).Returns(true);
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.GetParams)
                .Returns(JsonConvert.SerializeObject(Input.param));
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.Inbound)
                .Returns(Input.translatedBody);
            translationService.ExecuteTranslationFunction(Input.adapterMsg, Input.translation, JsonTranslationFunction.Outbound)
                .Returns(Input.translatedAdapterMsg);
            #endregion

            var Target = new Orbital.Agent.Pipelines.AsyncMessageProcessor.AsyncMessageProcessor(container, translationService, configurationService);

            Target.Start();
            Target.Push(inputRequest);
            Target.Stop();

            adapter.Received().SendAsync(Arg.Any<AdapterRequest>());
        }

        /// <summary>
        /// Test Push method with null inputs
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void PushWithNullInput()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                adapterType = faker.Lorem.Word(),
                adapterConfig = faker.Random.String(),
                requestBody = faker.Random.String(),
                translation = faker.Random.String(),
                translatedBody = faker.Random.String(),
                param = new Dictionary<string, string>()
                {
                    {faker.Random.String(), faker.Random.String() }
                },
                adapterMsg = faker.Random.String(),
                translatedAdapterMsg = faker.Random.String()
            };
            var inputConfig = new OrbitalConfiguration();

            //For getting adapter
            container.IsRegistered<IAdapter>(Input.adapterType).Returns(true);
            container.Resolve<IAdapter>(Input.adapterType).Returns(adapter);

            //For static parameters
            configurationService.GetConfigurationProperties().Returns(inputConfig);

            //For translations and dynamic parameters
            translationService.FunctionExist(Input.translation, Arg.Any<JsonTranslationFunction>()).Returns(true);
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.GetParams)
                .Returns(JsonConvert.SerializeObject(Input.param));
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.Inbound)
                .Returns(Input.translatedBody);
            translationService.ExecuteTranslationFunction(Input.adapterMsg, Input.translation, JsonTranslationFunction.Outbound)
                .Returns(Input.translatedAdapterMsg);
            #endregion

            var Target = new Orbital.Agent.Pipelines.AsyncMessageProcessor.AsyncMessageProcessor(container, translationService, configurationService);

            Target.Start();
            Target.Push(null);
            Target.Stop();

            adapter.DidNotReceive().SendAsync(Arg.Any<AdapterRequest>());
        }

        /// <summary>
        /// Test Push method with invalid adapter type and translation
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void PushWithInvalidValues()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                adapterType = faker.Lorem.Word(),
                adapterConfig = faker.Random.String(),
                requestBody = faker.Random.String(),
                translation = faker.Random.String(),
                translatedBody = faker.Random.String(),
                param = new Dictionary<string, string>()
                {
                    {faker.Random.String(), faker.Random.String() }
                },
                adapterMsg = faker.Random.String(),
                translatedAdapterMsg = faker.Random.String()
            };
            var inputPayload = Payload.Create(Input.adapterMsg);
            var inputOperation = new OperationDefinition()
            {
                Translation = Input.translation,
                Adapter = new AdapterConfiguration()
                {
                    Type = Input.adapterType,
                    Configuration = Input.adapterConfig
                }
            };
            var inputConfig = new OrbitalConfiguration();
            var inputRequest = new MessageProcessorInput(inputOperation, Input.requestBody);

            //For getting adapter
            container.IsRegistered<IAdapter>(Input.adapterType).Returns(false);

            //For static parameters
            configurationService.GetConfigurationProperties().Returns(inputConfig);

            //For translations and dynamic parameters
            translationService.FunctionExist(Input.translation, Arg.Any<JsonTranslationFunction>()).Returns(false);
            #endregion

            var Target = new Orbital.Agent.Pipelines.AsyncMessageProcessor.AsyncMessageProcessor(container, translationService, configurationService);

            Target.Start();
            Target.Push(inputRequest);
            Target.Stop();

            adapter.DidNotReceive().SendAsync(Arg.Any<AdapterRequest>());
        }
    }
}
