using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Ports.Interfaces;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters;
using NJsonSchema;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.SyncMessageProcessor.Filters
{
    /// <summary>
    /// Unit tests for ValidateResponseMessageFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ValidateResponseMessageFilterTests
    {
        private readonly Faker faker = new Faker();

        #region Fields
        private readonly JsonSchema4 jsonSchema = JsonSchema4.FromJsonAsync(
            @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}"
            ).Result;
        #endregion

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInputs()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Payload = Payload.Create("{\"id\":1}"),
                    ValidateResponse = true,
                    ResponseSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with payload containing faults to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithFaultedPayload()
        {
            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new OrbitalArgumentException());
            var Input = new
            {
                port = new TestPort()
                {
                    Payload = Payload.Create(new List<Fault>() { inputFault }),
                    ValidateResponse = true,
                    ResponseSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidateRequestFalse()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    ValidateResponse = false
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with null response to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullRequest()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    ValidateResponse = true,
                    ResponseSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with null response schema to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullRequestSchema()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Payload = Payload.Create("{\"id\":1}"),
                    ValidateResponse = true,
                    ResponseSchema = null
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with invalid response message to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidMessage()
        {
            #region Setup
            var Input = new
            {
                port = new TestPort()
                {
                    Payload = Payload.Create("{ }"),
                    ValidateResponse = true,
                    ResponseSchema = jsonSchema
                }
            };
            #endregion

            var Target = new ValidateResponseMessageFilter<TestPort>();

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Business_Validation_Error_015;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        #region MockClass
        private class TestPort : IFaultablePort, ISchemaPort, IPayloadPort
        {
            public ICollection<Fault> Faults { get; set; }

            public bool IsFaulted => Faults != null && Faults.Any();

            public Payload Payload { get; set; }
            public bool ValidateRequest { get; set; }
            public bool ValidateResponse { get; set; }
            public JsonSchema4 RequestSchema { get; set; }
            public JsonSchema4 ResponseSchema { get; set; }


            public IFaultablePort AppendFault(Exception e)
            {
                var fault = new AgentFaultFactory().CreateFault(e);

                Faults = Faults ?? new List<Fault>();
                Faults.Add(fault);

                return this;
            }
        }
        #endregion
    }
}
