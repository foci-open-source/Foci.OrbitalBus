using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters;
using Foci.Orbital.Agent.Services.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Pipelines.SyncMessageProcessor.Filters
{
    /// <summary>
    /// Unit tests for TranslateResponseMessageFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class TranslateResponseMessageFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing null service to TranslateResponseMessageFilter 
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void TranslateResponseMessageFilterWithNullService()
        {
            Assert.Throws<ArgumentNullException>(() => new TranslateResponseMessageFilter(null));
        }

        /// <summary>
        /// Test passing a port with valid inputs to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidInputs()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var inputMsg = faker.Random.String();
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(inputMsg)
                },
                translated = faker.Random.String()
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Outbound).Returns(true);
            translationService.ExecuteTranslationFunction(inputMsg, Input.port.Translation, JsonTranslationFunction.Outbound)
                .Returns(Input.translated);
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var Expected = Input.translated;
            var Actual = Target.Process(Input.port).Payload.Match<string>(v => v, f => null);

            Assert.NotNull(Actual);
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test passing a port with no request body to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithNullMessage()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String())
                }
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Outbound).Returns(true);
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var Actual = Target.Process(Input.port).Payload.Match<string>(v => v, f => null);

            Assert.NotNull(Actual);
            Assert.Empty(Actual);
        }

        /// <summary>
        /// Test passing a port with payload containing fault to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithFaultedPayload()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new OrbitalArgumentException());
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(new List<Fault>() { inputFault })
                }
            };
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(null).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing null port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPort()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(null).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing faulted port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithFaultedPort()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new OrbitalArgumentException());
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Faults = new List<Fault>()
                    {
                        inputFault
                    }
                }
            };
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with null payload to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPayload()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Translation = faker.Random.String()
                }
            };
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Business_Adapter_Error_012;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing a port with invalid translation to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidTranslation()
        {
            #region Substitutions
            var translationService = Substitute.For<IJsonTranslationService>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Translation = faker.Random.String(),
                    Payload = Payload.Create(faker.Random.String())
                }
            };

            translationService.FunctionExist(Input.port.Translation, JsonTranslationFunction.Outbound)
                .Returns(x => throw new OrbitalTranslationException());
            #endregion

            var Target = new TranslateResponseMessageFilter(translationService);

            var ExpectedSize = 1;
            var Expected = OrbitalFaultCode.Orbital_Runtime_TranslationFault_002;

            var Actual = Target.Process(Input.port).Payload.Match<IEnumerable<Fault>>(v => null, f => f);

            Assert.NotNull(Actual);
            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }
    }
}
