using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using Foci.Orbital.Agent.Pipelines.SyncMessageProcessor.Filters;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Pipelines.SyncMessageProcessor.Filters
{
    /// <summary>
    /// Unit tests for SendSyncFilter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SendSyncFilterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test passing null port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullPort()
        {
            var Target = new SendSyncFilter();

            Assert.Null(Target.Process(null));
        }

        /// <summary>
        /// Test passing port with valid adapter request to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ProcessWithValidAdapterRequest()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Adapter = adapter,
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                    new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                },
                payload = Payload.Create(faker.Random.String())
            };

            adapter.SendSync(Input.port.AdapterRequest).Returns(Input.payload);
            #endregion

            var Target = new SendSyncFilter();

            var Exptected = Input.payload;
            var Actual = Target.Process(Input.port);

            Assert.False(Actual.IsFaulted);
            Assert.Equal(Exptected, Actual.Payload);
        }

        /// <summary>
        /// Test passing faulted port to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithFaultedPort()
        {
            #region Setup
            var inputFault = new AgentFaultFactory().CreateFault(new ArgumentNullException());
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Faults = new List<Fault>()
                    {
                        inputFault
                    }
                }
            };
            #endregion

            var Target = new SendSyncFilter();

            var Expected = Input.port;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(Expected.Faults, Actual.Faults);
        }

        /// <summary>
        /// Test passing port without adapter to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullAdapter()
        {
            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
            };
            #endregion

            var Target = new SendSyncFilter();

            var Expected = OrbitalFaultCode.Orbital_Business_Adapter_Error_012;
            var ExpectedSize = 1;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing port without adapter request to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithNullAdapterRequest()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Adapter = adapter
                }
            };
            #endregion

            var Target = new SendSyncFilter();

            var Expected = OrbitalFaultCode.Orbital_Business_Adapter_Error_012;
            var ExpectedSize = 1;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test passing port with invalid adapter request to Process method
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ProcessWithInvalidAdapterRequest()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            #endregion

            #region Setup
            var Input = new
            {
                port = new ProcessMessagePort()
                {
                    Adapter = adapter,
                    AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                    new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
                }
            };

            adapter.When(x => x.SendSync(Input.port.AdapterRequest))
                .Do(x => { throw new OrbitalAdapterException(); });
            #endregion

            var Target = new SendSyncFilter();

            var Expected = OrbitalFaultCode.Orbital_Business_Adapter_Error_012;
            var ExpectedSize = 1;
            var Actual = Target.Process(Input.port);

            Assert.True(Actual.IsFaulted);
            Assert.Equal(ExpectedSize, Actual.Faults.Count);
            Assert.Equal(Expected, Actual.Faults.FirstOrDefault().OrbitalFaultCode);
        }
    }
}
