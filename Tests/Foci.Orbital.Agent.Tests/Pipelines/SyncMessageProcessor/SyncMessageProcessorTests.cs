using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Pipelines.Generic.Models;
using Foci.Orbital.Agent.Services.Interfaces;
using Newtonsoft.Json;
using NSubstitute;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Unity;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Pipelines.SyncMessageProcessor
{
    /// <summary>
    /// Unit tests for SyncMessageProcessor class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SyncMessageProcessorTests
    {
        private readonly Faker faker = new Faker();

        private const string schema = @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}";

        /// <summary>
        /// Test running stop method after running start
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void StopAfterStart()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            var Target = new Orbital.Agent.Pipelines.SyncMessageProcessor.SyncMessageProcessor(container, translationService, configurationService);

            Target.Start();
            var Actual = Target.Stop();

            Assert.True(Actual);
        }

        /// <summary>
        /// Test running stop method before running start
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void StopBeforeStart()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            var Target = new Orbital.Agent.Pipelines.SyncMessageProcessor.SyncMessageProcessor(container, translationService, configurationService);

            var Actual = Target.Stop();

            Assert.True(Actual);
        }

        /// <summary>
        /// Test Push method with valid inputs
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PushWithValidInput()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                adapterType = faker.Lorem.Word(),
                adapterConfig = faker.Random.String(),
                requestBody = "{\"id\":1}",
                translation = faker.Random.String(),
                translatedBody = faker.Random.String(),
                param = new Dictionary<string, string>()
                {
                    {faker.Random.String(), faker.Random.String() }
                },
                adapterMsg = faker.Random.String(),
                translatedAdapterMsg = "{\"id\":1}"
            };
            var inputPayload = Payload.Create(Input.adapterMsg);
            var inputOperation = new OperationDefinition()
            {
                Translation = Input.translation,
                Adapter = new AdapterConfiguration()
                {
                    Type = Input.adapterType,
                    Configuration = Input.adapterConfig
                },
                Schemas = new OperationSchemas()
                {
                    Request = schema,
                    Response = schema
                },
                Validate = new OperationValidation()
                {
                    Request = true,
                    Response = true
                }
            };
            var inputConfig = new OrbitalConfiguration();
            var inputRequest = new MessageProcessorInput(inputOperation, Input.requestBody);

            //For getting adapter
            container.IsRegistered<IAdapter>(Input.adapterType).Returns(true);
            container.Resolve<IAdapter>(Input.adapterType).Returns(adapter);

            //For static parameters
            configurationService.GetConfigurationProperties().Returns(inputConfig);

            //For translations and dynamic parameters
            translationService.FunctionExist(Input.translation, Arg.Any<JsonTranslationFunction>()).Returns(true);
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.GetParams)
                .Returns(JsonConvert.SerializeObject(Input.param));
            translationService.ExecuteTranslationFunction(Input.requestBody, Input.translation, JsonTranslationFunction.Inbound)
                .Returns(Input.translatedBody);
            translationService.ExecuteTranslationFunction(Input.adapterMsg, Input.translation, JsonTranslationFunction.Outbound)
                .Returns(Input.translatedAdapterMsg);

            //For return payload
            adapter.SendSync(Arg.Any<AdapterRequest>()).Returns(inputPayload);
            #endregion

            var Target = new Orbital.Agent.Pipelines.SyncMessageProcessor.SyncMessageProcessor(container, translationService, configurationService);

            var Exptected = Input.translatedAdapterMsg;

            Target.Start();
            var Actual = Target.Push(inputRequest).Match<string>(v => v, f => null);
            Target.Stop();

            Assert.Equal(Exptected, Actual);
        }

        /// <summary>
        /// Test Push method with null inputs
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void PushWithNullInput()
        {
            #region Substitutions
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            var Target = new Orbital.Agent.Pipelines.SyncMessageProcessor.SyncMessageProcessor(container, translationService, configurationService);

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 1;

            Target.Start();
            var Actual = Target.Push(null).Match<IEnumerable<Fault>>(v => null, f => f);
            Target.Stop();

            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

        /// <summary>
        /// Test Push method with invalid inputs
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void PushWithInvalidInput()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var container = Substitute.For<IUnityContainer>();
            var translationService = Substitute.For<IJsonTranslationService>();
            var configurationService = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                adapterType = faker.Lorem.Word(),
                adapterConfig = faker.Random.String(),
                requestBody = faker.Random.String(),
                translation = faker.Random.String(),
                translatedBody = faker.Random.String(),
                param = new Dictionary<string, string>()
                {
                    {faker.Random.String(), faker.Random.String() }
                },
                adapterMsg = faker.Random.String(),
                translatedAdapterMsg = faker.Random.String()
            };
            var inputPayload = Payload.Create(Input.adapterMsg);
            var inputOperation = new OperationDefinition()
            {
                Translation = Input.translation,
                Adapter = new AdapterConfiguration()
                {
                    Type = Input.adapterType,
                    Configuration = Input.adapterConfig
                }
            };
            var inputConfig = new OrbitalConfiguration();
            var inputRequest = new MessageProcessorInput(inputOperation, Input.requestBody);

            //For getting adapter
            container.IsRegistered<IAdapter>(Input.adapterType).Returns(false);

            //For static parameters
            configurationService.GetConfigurationProperties().Returns(inputConfig);

            //For translations and dynamic parameters
            translationService.FunctionExist(Input.translation, Arg.Any<JsonTranslationFunction>()).Returns(false);

            //For return payload
            adapter.SendSync(Arg.Any<AdapterRequest>()).Returns(inputPayload);
            #endregion

            var Target = new Orbital.Agent.Pipelines.SyncMessageProcessor.SyncMessageProcessor(container, translationService, configurationService);

            var Expected = OrbitalFaultCode.Orbital_Runtime_ArgumentFault_005;
            var ExpectedSize = 1;

            Target.Start();
            var Actual = Target.Push(inputRequest).Match<IEnumerable<Fault>>(v => null, f => f);
            Target.Stop();

            Assert.Equal(ExpectedSize, Actual.Count());
            Assert.Equal(Expected, Actual.FirstOrDefault().OrbitalFaultCode);
        }

    }
}
