﻿using Bogus;
using Foci.Orbital.Agent.Models.Service;
using Microsoft.AspNetCore.Http;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.RequestPathServiceInvalidInputs
{
    [ExcludeFromCodeCoverage]
    public class InputRequestPathServiceInvalidInputs : IEnumerable<object[]>
    {
        private readonly Faker faker = new Faker();

        public IEnumerator<object[]> GetEnumerator()
        {
            var serviceName = faker.Lorem.Text();
            var operationName = faker.Lorem.Text();
            var operationName2 = faker.Lorem.Text();

            var verb = faker.Lorem.Text();

            var service = new ServiceDefinition()
            {
                ServiceName = serviceName,
                Operations = new List<OperationDefinition>()
                {
                    new OperationDefinition()
                    {
                        Name = operationName,
                        IsSync = true,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = true
                        }
                    },
                    new OperationDefinition()
                    {
                        Name = operationName2,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = true
                        }
                    }
                }
            };
            // Invalid input for service
            yield return new object[] { service, new PathString(""), verb };
            yield return new object[] { service, new PathString("/"), verb };
            yield return new object[] { service, new PathString($"/{serviceName}/{faker.Random.String()}/{faker.Random.String()}"), verb };
            yield return new object[] { service, new PathString($"/{faker.Random.String()}/{faker.Random.String()}"), verb };
            yield return new object[] { null, new PathString($"/{faker.Random.String()}/{faker.Random.String()}"), verb };
            // Invalid input for operation
            yield return new object[] { service, new PathString($"/{serviceName}/{faker.Random.String()}"), verb };
            yield return new object[] { service, new PathString($"/{serviceName}"), faker.Random.String() };
            yield return new object[] { service, new PathString($"/{serviceName}"), verb };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
