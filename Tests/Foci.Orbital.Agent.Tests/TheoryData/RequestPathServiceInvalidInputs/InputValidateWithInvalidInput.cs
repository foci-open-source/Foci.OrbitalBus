﻿using Bogus;
using Foci.Orbital.Agent.Models.Service;
using Microsoft.AspNetCore.Http;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.RequestPathServiceInvalidInputs
{
    [ExcludeFromCodeCoverage]
    public class InputValidateWithInvalidInput : IEnumerable<object[]>
    {
        private readonly Faker faker = new Faker();

        public IEnumerator<object[]> GetEnumerator()
        {
            var serviceName = faker.Lorem.Text();
            var operationName = faker.Lorem.Text();
            var operationName2 = faker.Lorem.Text();

            var verb = faker.Lorem.Text();

            var service = new ServiceDefinition()
            {
                ServiceName = serviceName,
                Operations = new List<OperationDefinition>()
                {
                    new OperationDefinition()
                    {
                        Name = operationName,
                        IsSync = true,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = true
                        }
                    },
                    new OperationDefinition()
                    {
                        Name = operationName2,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = true
                        }
                    }
                }
            };
            // Additional invalid input for operation
            yield return new object[] { service, new PathString($"/{serviceName}/{operationName}"), faker.Random.String() };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
