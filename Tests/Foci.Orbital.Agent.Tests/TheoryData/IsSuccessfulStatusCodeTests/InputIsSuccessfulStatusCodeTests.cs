﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace Foci.Orbital.Agent.Tests.TheoryData.IsSuccessfulStatusCodeTests
{
    [ExcludeFromCodeCoverage]
    public class InputIsSuccessfulStatusCodeTests : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { false, HttpStatusCode.NotFound };
            yield return new object[] { true, HttpStatusCode.OK };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
