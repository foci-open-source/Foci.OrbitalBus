﻿using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Models.Interfaces;
using Foci.Orbital.Adapters.Contract.Models.Requests;
using Foci.Orbital.Agent.Pipelines.Generic.Ports;
using NSubstitute;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.ProcessWithInvalidAdapter
{
    [ExcludeFromCodeCoverage]
    public class InputProcessWithInvalidAdapter : IEnumerable<object[]>
    {
        private readonly Faker faker = new Faker();

        public IEnumerator<object[]> GetEnumerator()
        {
            #region Substitutions
            var adapter = Substitute.For<IAdapter>();
            var adapter2 = Substitute.For<IAdapter>();

           
            #endregion

            #region Input
            var nullPort = new ProcessMessagePort();
            var nullAdapterRequestPort = new ProcessMessagePort()
            {
                Adapter = adapter
            };
            var invalidAdapterReuestPort = new ProcessMessagePort()
            {
                Adapter = adapter2,
                AdapterRequest = new AdapterRequest(faker.Random.String(), faker.Random.String(),
                    new ReadOnlyDictionary<string, string>(new Dictionary<string, string>()))
            };
            adapter2.When(x => x.SendAsync(invalidAdapterReuestPort.AdapterRequest))
              .Do(x => { throw new OrbitalAdapterException(); });
            #endregion

            yield return new object[] { nullPort };
            yield return new object[] { nullAdapterRequestPort};
            yield return new object[] {  invalidAdapterReuestPort};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
