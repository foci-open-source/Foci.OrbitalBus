﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.ValidateOrbitalJsonInvalidJsonString
{
    [ExcludeFromCodeCoverage]
    public class ValidateOrbitalJsonInvalidJsonStringData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { "{ }", 1, "id" };
            yield return new object[] { null, 1, "In order to perform validation, the message cannot be null" };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
