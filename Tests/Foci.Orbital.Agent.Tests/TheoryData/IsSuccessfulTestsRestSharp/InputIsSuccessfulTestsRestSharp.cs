﻿using RestSharp;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace Foci.Orbital.Agent.Tests.TheoryData.IsSuccessfulTests
{
    [ExcludeFromCodeCoverage]
    public class InputIsSuccessfulTestsRestSharp : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { true, HttpStatusCode.OK, ResponseStatus.Completed };
            yield return new object[] { false, HttpStatusCode.NotFound, ResponseStatus.Completed };
            yield return new object[] { false, HttpStatusCode.OK, ResponseStatus.Error };
            yield return new object[] { false, HttpStatusCode.NotFound, ResponseStatus.Error };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
