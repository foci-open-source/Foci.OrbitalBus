﻿using Bogus;
using Foci.Orbital.Agent.Models.Service;
using Microsoft.AspNetCore.Http;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.RequestPathServiceValidInputs
{
    [ExcludeFromCodeCoverage]
    public class InputRequestPathServiceValidInputs : IEnumerable<object[]>
    {
        private readonly Faker faker = new Faker();

        public IEnumerator<object[]> GetEnumerator()
        {
            var serviceName = faker.Lorem.Text();
            var operationName = faker.Lorem.Text();
            var operationName2 = faker.Lorem.Text();

            var verb = faker.Lorem.Text();

            var service = new ServiceDefinition()
            {
                ServiceName = serviceName,
                Operations = new List<OperationDefinition>()
                {
                    new OperationDefinition()
                    {
                        Name = operationName,
                        IsSync = true,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = true
                        }
                    },
                    new OperationDefinition()
                    {
                        Name = operationName2,
                        Http = new HttpConfiguration()
                        {
                            IsEnabled = true,
                            Verb = verb,
                            VerbOnly = false
                        }
                    }
                }
            };
            yield return new object[] { service, new PathString($"/{serviceName}/{operationName2}"), verb };
            yield return new object[] { service, new PathString($"/{serviceName}"), verb };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
