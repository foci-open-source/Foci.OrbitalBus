﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Agent.Tests.TheoryData.ExistsBusAddressNotFound
{
    [ExcludeFromCodeCoverage]
    public class InputExistsBusAddressNotFound : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { "localhost" };
            yield return new object[] { null };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
