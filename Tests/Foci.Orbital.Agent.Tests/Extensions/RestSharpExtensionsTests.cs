using Foci.Orbital.Agent.Repositories.Extensions;
using Foci.Orbital.Agent.Tests.TheoryData.IsSuccessfulStatusCodeTests;
using Foci.Orbital.Agent.Tests.TheoryData.IsSuccessfulTests;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Extensions
{
    [ExcludeFromCodeCoverage]
    public class RestSharpExtensionsTests
    {
        /// <summary>
        /// Tests the ToDictionary extension to make sure it returns a dictionary.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ToDictionaryTest()
        {
            var Input = new RestResponse();

            var Expected = new Dictionary<string, string>();
            var Actual = Input.ToDictionary();
            Assert.IsType(Expected.GetType(), Actual);
        }

        /// <summary>
        /// Test IsSuccessfulStatusCode if the status code is good or bad.
        /// </summary>
        [Theory]
        [ClassData(typeof(InputIsSuccessfulStatusCodeTests))]
        [Trait("Category", "Restsharp")]
        public void IsSuccessfulStatusCodeTests(bool expected, HttpStatusCode httpStatusCode)
        {

            var Input = new RestResponse { StatusCode = httpStatusCode };
            
            var Actual = Input.IsSuccessfulStatusCode();
            Assert.Equal(expected, Actual);
        }
        

        /// <summary>
        /// Test IsSuccessful if the status code and response are good or bad.
        /// </summary>
        [Theory]
        [ClassData(typeof(InputIsSuccessfulTestsRestSharp))]
        [Trait("Category", "Restsharp")]
        public void IsSuccessfulTests(bool expected, HttpStatusCode httpStatusCode, ResponseStatus responseStatus)
        {

            var Input = new RestResponse { StatusCode = httpStatusCode, ResponseStatus = responseStatus };
            
            var Actual = Input.IsSuccessful();
            Assert.Equal(expected, Actual);
        }
        

        /// <summary> 
        /// Test passing null response to ToDictionary method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ToDictionaryNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.ToDictionary(null));
        }

        /// <summary> 
        /// Test passing null response to IsSuccessfulStatusCode method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulStatusCodeNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.IsSuccessfulStatusCode(null));
        }

        /// <summary> 
        /// Test passing null response to IsSuccessful method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void IsSuccessfulNullResponse()
        {
            Assert.Throws<ArgumentNullException>(() => RestSharpExtensions.IsSuccessful(null));
        }
    }
}
