using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Factories.Faults;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Factories.Faults
{
    /// <summary>
    /// Unit tests for AgentFaultFactory class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class AgentFaultFactoryTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Testing the creation of all OrbitalException and then ensures the factory can create an exception for it.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void FaultCreationSuccessRoute()
        {
            var exceptionTypes = Assembly.GetAssembly(typeof(OrbitalException)).GetTypes().Where(t => t.IsSubclassOf(typeof(OrbitalException)));

            var Target = new AgentFaultFactory();

            foreach (var exceptionType in exceptionTypes)
            {
                var inputException = Activator.CreateInstance(exceptionType) as OrbitalException;

                var Expected = inputException.Message;
                Fault Actual = Target.CreateFault(inputException);

                Assert.Equal(Expected, Actual.Details);
            }
        }
    }
}
