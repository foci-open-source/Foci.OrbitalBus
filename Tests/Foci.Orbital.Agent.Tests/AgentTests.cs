﻿using Bogus;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Models.Consul;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using NSubstitute;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests
{
    [ExcludeFromCodeCoverage]
    public class AgentTests
    {
        /// <summary>
        /// Confirm Agent.Start() works properly.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void Agent()
        {
            #region Substitutions
            var config = Substitute.For<IConfigurationService>();
            var consul = Substitute.For<IConsulService>();
            var servicedefiniton = Substitute.For<IServiceDefinitionService>();
            var webhost = Substitute.For<IWebHost>();
            var configProperties = new OrbitalConfiguration() { BrokerConfiguration = new BrokerConfiguration() { BusHostIp = "localhost" } };

            var serviceDetails = new Faker<RegisteredServiceDetails>().Generate(10);
            var adapterDetails = new List<string>();

            var ExpectedConsulResults = new Faker<ConsulRegistrationResult>()
                .CustomInstantiator(f => new ConsulRegistrationResult(f.Random.String(), true))
                .Generate(10);
            servicedefiniton.RegisterAndBootsrapServices().Returns(serviceDetails);
            config.GetConfigurationProperties().Returns(configProperties);
            consul.RegisterPublishedServices(serviceDetails).Returns(ExpectedConsulResults);
            #endregion

            var Agent = new Agent(config, consul, servicedefiniton);
            var Actual = Agent.Start(webhost);

            Assert.NotNull(Actual.PluginRegistrationResults);
            Assert.NotNull(Actual.ConsulRegistrationResults);
        }
    }
}
