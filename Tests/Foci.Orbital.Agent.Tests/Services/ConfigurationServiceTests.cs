using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Services;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Services
{
    /// <summary>
    /// Unit tests for the ConfigurationService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConfigurationServiceTests
    {
        /// <summary>
        /// Testing the successful creation of the Orbital configuration service.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ConfigurationServiceConstructionTest()
        {
            #region Substitutes
            var input = new
            {
                json = jsonString
            };
            #endregion

            var Target = new ConfigurationService();
            Target.Initialize(input.json);

            #region Asserts
            Assert.Equal("1.2.3.4", Target.GetConfigurationProperties().BrokerConfiguration.BusHostIp);
            Assert.Equal("5672", Target.GetConfigurationProperties().BrokerConfiguration.BusHostPort.ToString());
            Assert.Equal("17.18.19.20", Target.GetConfigurationProperties().ServiceStaticParameters["ServiceIp"]);
            Assert.Equal("3580", Target.GetConfigurationProperties().ServiceStaticParameters["ServicePort"]);
            Assert.False(Target.GetConfigurationProperties().BrokerConfiguration.SslEnabled);
            Assert.False(Target.GetConfigurationProperties().ConsulConfiguration.SslEnabledConsul);
            Assert.False(Target.GetConfigurationProperties().ConsulConfiguration.SslPolicyCNError);
            #endregion
        }

        /// <summary>
        /// Testing the successful creation of the Orbital configuration service.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ConfigurationServicePolicyTest()
        {
            #region Substitutes
            var input = new
            {
                json = @"{""wrong"": ""json""}"
            };
            #endregion

            var Target = new ConfigurationService();
            Target.Initialize(input.json);
            var Actual = Target.GetConfigurationProperties();
            #region Asserts
            Assert.NotNull(Actual);
            #endregion
        }

        /// <summary>
        /// Testing the failure of the Orbital configuration service to load the configuration.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ConfigurationServiceEmptyJsonString()
        {
            #region Substitutes
            var input = new
            {
                json = ""
            };

            #endregion
            var Expected = new OrbitalInitializationException("Configuration properties are accessed before the ConfigurationService has been initialized with a JSON string.");
            var Target = new ConfigurationService();
            Target.Initialize(input.json);

            #region Asserts
            Assert.Equal("localhost", Target.GetConfigurationProperties().BrokerConfiguration.BusHostIp);
            Assert.Equal(5672, Target.GetConfigurationProperties().BrokerConfiguration.BusHostPort);
            Assert.Equal("localhost", Target.GetConfigurationProperties().ServiceStaticParameters["ServiceIp"]);
            Assert.Equal("3580", Target.GetConfigurationProperties().ServiceStaticParameters["ServicePort"]);
            Assert.False(Target.GetConfigurationProperties().BrokerConfiguration.SslEnabled);
            Assert.False(Target.GetConfigurationProperties().ConsulConfiguration.SslEnabledConsul);
            Assert.False(Target.GetConfigurationProperties().ConsulConfiguration.SslPolicyCNError);
            #endregion
        }

        /// <summary>
        /// Testing getting configuration without initialize
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ConfigurationServiceNotInitialized()
        {
            var Target = new ConfigurationService();
            var Actual = Assert.Throws<ArgumentException>(() => Target.GetConfigurationProperties());
        }

        #region JSON
        private readonly string jsonString = @"{
    ""ServiceStaticParameters"": {
                    ""ServiceIp"": ""17.18.19.20"",
      ""ServicePort"": ""3580""
    },
                ""BrokerConfiguration"": {
                    ""BusHostIp"": ""1.2.3.4"",
      ""BusHostPort"": 5672,
      ""SslEnabled"": ""false"",
      ""Alias"": ""rabbitCredentials""
                },
    ""ConsulConfiguration"": {
                    ""HostIp"": ""13.14.15.16"",
      ""HostPort"": 8500,
      ""Alias"": ""selfcertBase"",
      ""SslPolicyCNerror"": ""false"",
      ""SslEnabledConsul"":  ""false""
    }
        }";
        #endregion
    }
}
