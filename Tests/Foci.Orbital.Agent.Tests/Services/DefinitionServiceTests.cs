﻿using Bogus;
using EasyNetQ;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.OperationHandler;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Services.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Unity.Exceptions;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Services
{
    [ExcludeFromCodeCoverage]
    public class DefinitionServiceTests
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null configuration service to ServiceDefinitionService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void DefinitionServiceNullConfiguration()
        {
            #region Substitutions
            IBusService subBusService = Substitute.For<IBusService>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            IOperationsHandler operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new ServiceDefinitionService(null, subBusService, serviceDefinitionRepository, operationsHandler));
        }

        /// <summary> 
        /// Test passing null bus service to DefinitionService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void DefinitionServiceNullBusService()
        {
            #region Substitutions
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            IOperationsHandler operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new ServiceDefinitionService(subConfiguration, null, serviceDefinitionRepository, operationsHandler));
        }

        /// <summary> 
        /// Test passing null file repository to DefinitionService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void DefinitionServiceNullDefinitionRepository()
        {
            #region Substitutions
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler operationsHandler = Substitute.For<IOperationsHandler>();


            #endregion

            Assert.Throws<ArgumentNullException>(() => new ServiceDefinitionService(subConfiguration, subBusService, null, operationsHandler));
        }

        /// <summary>
        /// Bootstraps SubscriberDetails to Rabbit MQ.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BootstrapSubscripberDetailsSuccess()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            IBus subBus = Substitute.For<IBus>();

            var Input = new
            {
                RabbitConfiguration = new Faker<BrokerConfiguration>()
                .RuleFor(r => r.SslEnabled, f => false)
                .RuleFor(r => r.BusHostIp, f => f.Internet.Ip())
                .RuleFor(r => r.BusHostPort, f => f.Random.Int(1, 65535))
                .Generate(),
                ServiceId = "testid",
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s),string.Empty),
                    SubscriberDetails.Create<string, string>(s => s,string.Empty)
                }
            };
            var config = new OrbitalConfiguration
            {
                BrokerConfiguration = Input.RabbitConfiguration
            };
            subConfiguration.GetConfigurationProperties().Returns(config);
            subBusService.BootstrapService(Arg.Any<BusConnectionDetails>(), Arg.Any<SubscriberDetails[]>()).Returns(subBus);
            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = subBus;
            var Actual = Target.BootStrapSubscriberDetails(Input.ServiceId, Input.Details);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Bootstraps the same service Id. Expects the same bus to be returned.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BootstrapSubscripberDetailsSameBusSuccess()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();

            IBus subBus = Substitute.For<IBus>();

            var Input = new
            {
                RabbitConfiguration = new Faker<BrokerConfiguration>()
                .RuleFor(r => r.SslEnabled, f => false)
                .RuleFor(r => r.BusHostIp, f => f.Internet.Ip())
                .RuleFor(r => r.BusHostPort, f => f.Random.Int(1, 65535))
                .Generate(),
                ServiceId = "testid",
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s), string.Empty ),
                    SubscriberDetails.Create<string, string>(s => s, string.Empty)
                }
            };
            var config = new OrbitalConfiguration
            {
                BrokerConfiguration = Input.RabbitConfiguration
            };
            subConfiguration.GetConfigurationProperties().Returns(config);
            subBusService.BootstrapService(Arg.Any<BusConnectionDetails>(), Arg.Any<SubscriberDetails[]>()).Returns(subBus);
            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = subBus;
            var Actual1 = Target.BootStrapSubscriberDetails(Input.ServiceId, Input.Details);
            var Actual2 = Target.BootStrapSubscriberDetails(Input.ServiceId, Input.Details);

            Assert.Equal(Expected, Actual1);
            Assert.Equal(Actual1, Actual2);
        }

        /// <summary>
        /// Tests the connection with SSL Enabled.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BootstrapSubscripberDetailsSSLEnabledSuccess()
        {
            #region Substitutions
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();

            IBus subBus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                RabbitConfiguration = new Faker<BrokerConfiguration>()
                    .RuleFor(r => r.SslEnabled, f => true)
                    .RuleFor(r => r.Password, f => f.Internet.Password())
                    .RuleFor(r => r.Username, f => f.Internet.UserName())
                    .RuleFor(r => r.BusHostIp, f => f.Internet.Ip())
                    .RuleFor(r => r.BusHostPort, f => f.Random.Int(1, 65535))
                    .Generate(),
                ServiceId = "testid",
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s), string.Empty),
                    SubscriberDetails.Create<string, string>(s => s,string.Empty)
                }
            };
            var config = new OrbitalConfiguration
            {
                BrokerConfiguration = Input.RabbitConfiguration
            };

            subConfiguration.GetConfigurationProperties().Returns(config);
            subBusService.BootstrapService(Arg.Any<BusConnectionDetails>(), Arg.Any<SubscriberDetails[]>()).Returns(subBus);
            #endregion


            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = subBus;
            var Actual = Target.BootStrapSubscriberDetails(Input.ServiceId, Input.Details);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing null service ID to BootStrapSubscriberDetails method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BootStrapSubscriberDetailsNullServiceID()
        {
            #region Substitutions
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();

            IBus subBus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                RabbitConfiguration = new Faker<BrokerConfiguration>()
                    .RuleFor(r => r.SslEnabled, f => true)
                    .RuleFor(r => r.Password, f => f.Internet.Password())
                    .RuleFor(r => r.Username, f => f.Internet.UserName())
                    .RuleFor(r => r.BusHostIp, f => f.Internet.Ip())
                    .RuleFor(r => r.BusHostPort, f => f.Random.Int(1, 65535))
                    .Generate(),
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s), string.Empty),
                    SubscriberDetails.Create<string, string>(s => s,string.Empty)
                }
            };
            var config = new OrbitalConfiguration
            {
                BrokerConfiguration = Input.RabbitConfiguration
            };

            subConfiguration.GetConfigurationProperties().Returns(config);
            subBusService.BootstrapService(Arg.Any<BusConnectionDetails>(), Arg.Any<SubscriberDetails[]>()).Returns(subBus);
            #endregion


            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);

            Assert.Throws<ArgumentException>(() => Target.BootStrapSubscriberDetails(null, Input.Details));
        }

        /// <summary>
        /// Registers services with them all failing to load. Application continues to live.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceFailed()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            var services = new
            {
                service1 = faker.Random.String()
            };

            var Input = new
            {
                serviceNames = new List<string>()
                { services.service1
                 },
                mockServices = new List<ServiceDefinition>()
                {
                   Substitute.For<ServiceDefinition>()
                },
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s),"test"),
                    SubscriberDetails.Create<string, string>(s => s,services.service1)
                },
                config = new OrbitalConfiguration(),
                Exception = new ResolutionFailedException(
                                                    typeof(string),
                                                    "IConcreteService",
                                                    "Test Message",
                                                    new Exception())
            };

            Input.config.RegisteredServiceNames = Input.serviceNames;

            for (int i = 0; i < Input.mockServices.Count; i++)
            {
                Input.mockServices[i].ServiceName = Input.serviceNames[i];
                subOperationsHandler.GetSubscriberDetails(Input.mockServices[i]).Returns(Input.Details);
                subOperationsHandler.GetSubscriberDetails(Input.mockServices[i]).Returns(x => { throw Input.Exception; });

            }

            for (int i = 0; i < Input.serviceNames.Count(); i++)
            {
                serviceDefinitionRepository.GetServiceDefinitionByName(Input.serviceNames[i]).Returns(Input.mockServices[i]);
            }
            subConfiguration.GetConfigurationProperties().Returns(Input.config);

            subConfiguration.GetConfigurationProperties().Returns(Input.config);

            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = new List<RegisteredServiceDetails>()
            {
            };
            var Actual = Target.RegisterAndBootsrapServices();

            Assert.Equal(Expected, Actual);

        }



        /// <summary>
        /// Registers and Bootstraps adapters and services.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RegisterAndBootstrapMultipleSuccess()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            var services = new
            {
                service1 = faker.Random.String(),
                service2 = faker.Random.String(),
                service3 = faker.Random.String()
            };

            var Input = new
            {
                serviceNames = new List<string>()
                { services.service1,
                services.service2,
                services.service3
                 },
                mockServices = new List<ServiceDefinition>()
                {
                   Substitute.For<ServiceDefinition>(),
                   Substitute.For<ServiceDefinition>(),
                   Substitute.For<ServiceDefinition>()

                },
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s),"test"),
                    SubscriberDetails.Create<string, string>(s => s,services.service1)
                },
                config = new OrbitalConfiguration(),

            };

            Input.config.RegisteredServiceNames = Input.serviceNames;

            for (int i = 0; i < Input.mockServices.Count; i++)
            {
                Input.mockServices[i].ServiceName = Input.serviceNames[i];
                subOperationsHandler.GetSubscriberDetails(Input.mockServices[i]).Returns(Input.Details);
            }

            for (int i = 0; i < Input.serviceNames.Count(); i++)
            {
                serviceDefinitionRepository.GetServiceDefinitionByName(Input.serviceNames[i]).Returns(Input.mockServices[i]);
            }
            subConfiguration.GetConfigurationProperties().Returns(Input.config);

            subConfiguration.GetConfigurationProperties().Returns(Input.config);
            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = new List<RegisteredServiceDetails>()
                {
                    new RegisteredServiceDetails(services.service1, Input.Details, "{}"),
                    new RegisteredServiceDetails(services.service2, Input.Details, "{}"),
                    new RegisteredServiceDetails(services.service3, Input.Details, "{}")
                };
            var Actual = Target.RegisterAndBootsrapServices();

            Assert.Equal(Expected.Select(s => s.RegisteredServices.Count()),
                                                 Actual.Select(s => s.RegisteredServices.Count()));
            Assert.Equal(Expected.Select(s => s.ServiceName),
                         Actual.Select(s => s.ServiceName));
        }

        /// <summary>
        /// Registers and Bootstraps adapters and services.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RegisterAndBootstrapSuccess()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            var services = new
            {
                service1 = faker.Random.String()
            };

            var Input = new
            {
                serviceNames = new List<string>()
                { services.service1
                 },
                mockServices = new List<ServiceDefinition>()
                {
                   Substitute.For<ServiceDefinition>()
                },
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s),"test"),
                    SubscriberDetails.Create<string, string>(s => s,services.service1)
                },
                config = new OrbitalConfiguration(),

            };

            Input.config.RegisteredServiceNames = Input.serviceNames;

            for (int i = 0; i < Input.mockServices.Count; i++)
            {
                Input.mockServices[i].ServiceName = Input.serviceNames[i];
                subOperationsHandler.GetSubscriberDetails(Input.mockServices[i]).Returns(Input.Details);
            }

            for (int i = 0; i < Input.serviceNames.Count(); i++)
            {
                serviceDefinitionRepository.GetServiceDefinitionByName(Input.serviceNames[i]).Returns(Input.mockServices[i]);
            }
            subConfiguration.GetConfigurationProperties().Returns(Input.config);

            subConfiguration.GetConfigurationProperties().Returns(Input.config);

            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = new List<RegisteredServiceDetails>()
                {
                    new RegisteredServiceDetails(services.service1, Input.Details, "{}")
                };
            var Actual = Target.RegisterAndBootsrapServices();

            Assert.Equal(Expected.Select(s => s.RegisteredServices.Count()),
                                                 Actual.Select(s => s.RegisteredServices.Count()));
            Assert.Equal(Expected.Select(s => s.ServiceName),
                         Actual.Select(s => s.ServiceName));
        }

        /// <summary>
        /// Registers and Bootstraps services. Unable to find service definition file
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterAndBootstrapFileNotFound()
        {
            #region Substitutes
            IConfigurationService subConfiguration = Substitute.For<IConfigurationService>();
            IBusService subBusService = Substitute.For<IBusService>();
            IOperationsHandler subOperationsHandler = Substitute.For<IOperationsHandler>();
            IServiceDefinitionRepository serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();


            var baseconfig = new OrbitalConfiguration();
            subConfiguration.GetConfigurationProperties().Returns(baseconfig);

            var Input = new
            {
                Details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s),"test"),
                    SubscriberDetails.Create<string, string>(s => s,"testid")
                },
                ServiceId = "testid"
            };

            #endregion

            var Target = new ServiceDefinitionService(subConfiguration, subBusService, serviceDefinitionRepository, subOperationsHandler);
            var Expected = new List<RegisteredServiceDetails>();

            var Actual = Target.RegisterAndBootsrapServices();

            Assert.Equal(Expected.Count(), Actual.Count());
        }
    }
}
