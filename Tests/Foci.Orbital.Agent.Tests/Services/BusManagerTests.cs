﻿using Bogus;
using EasyNetQ;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Tests.TheoryData.ExistsBusAddressNotFound;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Services
{
    /// <summary>
    /// Unit tests for the BusManager.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class BusManagerTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Testing the success in finding an address that has been added. 
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ExistsBusAddressFound()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion
            
            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            }; 
            #endregion

            var Target = new BusManager();

            Target.Add(Input.busAddress, bus);

            Assert.True(Target.Exists(Input.busAddress));
        }

        /// <summary>
        /// Testing the failure to find an address that hasn't been added. 
        /// </summary>
        [Theory]
        [ClassData(typeof(InputExistsBusAddressNotFound))]
        [Trait("Category", "Failure")]
        public void ExistsBusAddressNotFound(string busAddress)
        {
         

            var Target = new BusManager();

            Assert.False(Target.Exists(busAddress));
        }

        /// <summary>
        /// Testing the success in getting a bus that exists.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetReturnBusByAddess()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            Target.Add(Input.busAddress, bus);

            var Expected = bus;
            var Actual = Target.Get(Input.busAddress);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing return null when Getting an address that doesn't exist.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetAddressDoesNotExist()
        {
            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();
            
            var Actual = Target.Get(Input.busAddress);

            Assert.Null(Actual);
        }

        /// <summary> 
        /// Test passing null bus IP to Get method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void GetNullBusAddress()
        {
            var Target = new BusManager();

            var Expected = default(IBus);
            var Actual = Target.Get(null);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the success in adding an address that has already been added. 
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddReturnSameBusIfAlreadyBeenAdded()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            Target.Add(Input.busAddress, bus);

            var Expected = bus;
            var Actual = Target.Add(Input.busAddress, bus);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the success in adding an address that doesn't exist in the manager yet.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AddReturnBusBeingAdded()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            var Expected = bus;
            var Actual = Target.Add(Input.busAddress, bus);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing null bus IP to Add method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void AddNullBusAddress()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            var Target = new BusManager();

            var Expected = default(IBus);
            var Actual = Target.Add(null, bus);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing null bus to Add method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void AddNullBus()
        {
            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            var Expected = default(IBus);
            var Actual = Target.Add(Input.busAddress, null);
            
            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the success in removing an address that has been added. 
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RemoveReturnBusRemoved()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();
            Target.Add(Input.busAddress, bus);

            var Expected = bus;
            var Actual = Target.Remove(Input.busAddress);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the failure of removing an address that doesn't exist.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RemoveAddressDoesNotExist()
        {
            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            Assert.Null(Target.Remove(Input.busAddress));
        }

        /// <summary> 
        /// Test passing null bus IP to Remove method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RemoveNullBusIP()
        {
            var Target = new BusManager();

            var Expected = default(IBus);
            var Actual = Target.Remove(null);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the success in calling Dispose when an address is destroyed.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DestroyCallsDisposeOnTheBus()
        {
            #region Substitutions
            var bus = Substitute.For<IBus>();
            #endregion

            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion
            
            var Target = new BusManager();
            Target.Add(Input.busAddress, bus);

            var Expected = bus;
            Target.Destroy(Input.busAddress);
            bus.Received().Dispose();//Ensure the Dispose method was called on the bus
        }

        /// <summary>
        /// Testing return false on Destroying a non existing address.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void DestroyAddressDoesNotExist()
        {
            #region Setup
            var Input = new
            {
                busAddress = faker.Internet.Ip()
            };
            #endregion

            var Target = new BusManager();

            Assert.False(Target.Destroy(Input.busAddress));
        }

        /// <summary> 
        /// Test passing null bus IP to Destroy method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void DestroyNullBusIP()
        {
            var Target = new BusManager();

            Assert.True(Target.Destroy(null));
        }
        
        /// <summary>
        /// Tests the Dispose method.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DisposeTest()
        {
            var busSubstitue = Substitute.For<IBus>();
            var Input = new
            {
                busAddress = "testaddress:8080",
                bus = busSubstitue
            };

            var Target = new BusManager();
            Target.Add(Input.busAddress, Input.bus);
            Target.Dispose();

            var Actual = Target.IsDisposed();

            //Ensure the Dispose method was called on the bus
            Assert.True(Actual);
        }

        /// <summary>
        /// Tests the Dispose method if already disposed.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void DisposeTwiceTest()
        {
            var Target = new BusManager();

            Target.Dispose();
            //The second call should return the disposed variable without doing any more work.
            Target.Dispose();

            var Actual = Target.IsDisposed();

            //Ensure the Dispose method was called on the bus
            Assert.True(Actual);
        }
    }
}
