﻿using Bogus;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Configurations;
using Foci.Orbital.Agent.Models.Consul.Requests;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interfaces;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Services
{
    /// <summary>
    /// Unit tests for the ConsulService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulServiceTests
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null Consul repository to ConsulService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ConsulServiceNullRepository()
        {
            #region Substitutions
            var config = Substitute.For<IConfigurationService>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new ConsulService(null, config));
        }

        /// <summary> 
        /// Test passing null Config service to ConsulService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ConsulServiceNullConfig()
        {
            #region Substitutions
            var repository = Substitute.For<IConsulRepository>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new ConsulService(repository, null));
        }

        /// <summary>
        /// Testing for the success of registering a new service with Consul.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RegisterServiceWithValidRequest()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            var configSub = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration(),
                request = new RegisterServiceRequest(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            };
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.RegisterService(Input.request, InputAddress, Arg.Any<Maybe<X509Certificate>>()).Returns(true);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            Assert.True(Target.RegisterService(Input.request));
        }

        /// <summary> 
        /// Test passing null register service request to GetServiceInfo method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceNullRequest()
        {
            #region Substitutions
            var repository = Substitute.For<IConsulRepository>();
            var config = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration()
            };

            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            config.GetConfigurationProperties().Returns(Input.config);
            #endregion

            var Target = new ConsulService(repository, config);

            Assert.False(Target.RegisterService(null));
        }

        /// <summary>
        /// Testing for the failure to registering a new service with Consul.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceInvalidRequest()
        {
            #region Substitutes
            var configSub = Substitute.For<IConfigurationService>();
            var repositorySub = Substitute.For<IConsulRepository>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration(),
                request = new RegisterServiceRequest(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            };
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.RegisterService(Input.request, InputAddress, Arg.Any<Maybe<X509Certificate>>()).Returns(false);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            Assert.False(Target.RegisterService(Input.request));
        }

        /// <summary>
        /// Testing for the success of setting a new key/value in Consul.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SetKeyValueValidInputs()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            var configSub = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                key = faker.Random.String(),
                value = faker.Random.String(),
                config = new OrbitalConfiguration()
            };
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.SetKeyValue(Input.key, Input.value, InputAddress, Arg.Any<Maybe<X509Certificate>>()).Returns(true);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            Assert.True(Target.SetKeyValue(Input.key, Input.value));
        }

        /// <summary> 
        /// Test passing null Key to SetKeyValue method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SetKeyValueNullKey()
        {
            #region Substitutions
            var repository = Substitute.For<IConsulRepository>();
            var config = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration(),
                value = faker.Random.String()
            };

            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            config.GetConfigurationProperties().Returns(Input.config);
            #endregion

            var Target = new ConsulService(repository, config);

            Assert.False(Target.SetKeyValue(null, Input.value));
        }

        /// <summary> 
        /// Test passing null value to SetKeyValue method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SetKeyValueNullValue()
        {
            #region Substitutions
            var repository = Substitute.For<IConsulRepository>();
            var config = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration(),
                value = faker.Random.String()
            };

            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            config.GetConfigurationProperties().Returns(Input.config);
            #endregion

            var Target = new ConsulService(repository, config);

            Assert.False(Target.SetKeyValue(Input.value, null));
        }

        /// <summary>
        /// Testing for the failure of setting a new key/value in Consul.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SetKeyValueInvalidInputs()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            var configSub = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                key = faker.Random.String(),
                value = faker.Random.String(),
                config = new OrbitalConfiguration()
            };
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.SetKeyValue(Input.key, Input.value, InputAddress, Arg.Any<Maybe<X509Certificate>>()).Returns(false);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            Assert.False(Target.SetKeyValue(Input.key, Input.value));
        }

        /// <summary>
        /// Testing for the success of registerService and set service description
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RegisterServicesAndSetServiceDescriptionValidService()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            var configSub = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                serviceId = faker.Random.String(),
                serviceDescription = faker.Random.String(),
                config = new OrbitalConfiguration(),
                details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s), faker.Random.String()),
                    SubscriberDetails.Create<string, string>(s => s, faker.Random.String())
                }
            };
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            var InputResult = new List<RegisteredServiceDetails>()
                {
                    new RegisteredServiceDetails(Input.serviceId, Input.details, Input.serviceDescription)
                };

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.SetKeyValue(string.Format("orbital.{0}.contract", Input.serviceId), Input.serviceDescription, InputAddress, Arg.Any<Maybe<X509Certificate>>())
                .Returns(true);
            repositorySub.RegisterService(Arg.Any<RegisterServiceRequest>(), InputAddress, Arg.Any<Maybe<X509Certificate>>())
                .Returns(true);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            var ExpectedCount = 3;
            var Actual = Target.RegisterPublishedServices(InputResult);

            Assert.NotEmpty(Actual);
            Assert.Equal(ExpectedCount, Actual.Count());
            Assert.All(Actual, e =>
            {
                Assert.True(e.RegistrationSuccessful);
            });
        }

        /// <summary> 
        /// Test passing null plugin to RegisterServicesAndSetServiceDescription method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServicesAndSetServiceDescriptionNull()
        {
            #region Substitutions
            var repository = Substitute.For<IConsulRepository>();
            var config = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                config = new OrbitalConfiguration()
            };

            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            config.GetConfigurationProperties().Returns(Input.config);
            #endregion

            var Target = new ConsulService(repository, config);

            Assert.Throws<ArgumentNullException>(() => Target.RegisterPublishedServices(null));
        }

        /// <summary>
        /// Testing for fail to registerService
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServicesAndSetServiceDescriptionInvalidService()
        {
            #region Substitutes
            var repositorySub = Substitute.For<IConsulRepository>();
            var configSub = Substitute.For<IConfigurationService>();
            #endregion

            #region Setup
            var Input = new
            {
                ip = faker.Internet.Ip(),
                port = faker.Random.UShort(),
                serviceId = faker.Random.String(),
                serviceDescription = faker.Random.String(),
                config = new OrbitalConfiguration(),
                details = new List<SubscriberDetails>()
                {
                    SubscriberDetails.Create<string>(s => string.IsNullOrEmpty(s), faker.Random.String()),
                    SubscriberDetails.Create<string, string>(s => s, faker.Random.String())
                }
            };
            Input.config.ConsulConfiguration.SslEnabledConsul = false;
            Input.config.ConsulConfiguration.HostIp = Input.ip;
            Input.config.ConsulConfiguration.HostPort = Input.port;

            var InputAddress = string.Format("http://{0}:{1}", Input.ip, Input.port);

            var InputResult = new List<RegisteredServiceDetails>()
                {
                    new RegisteredServiceDetails(Input.serviceId, Input.details, Input.serviceDescription)
                };

            configSub.GetConfigurationProperties().Returns(Input.config);
            repositorySub.SetKeyValue(string.Format("orbital.{0}.contract",Input.serviceId), Input.serviceDescription, InputAddress, Arg.Any<Maybe<X509Certificate>>())
                .Returns(true);
            repositorySub.RegisterService(Arg.Any<RegisterServiceRequest>(), InputAddress, Arg.Any<Maybe<X509Certificate>>())
                .Returns(false);
            #endregion

            var Target = new ConsulService(repositorySub, configSub);

            var ExpectedCount = 3;
            var Actual = Target.RegisterPublishedServices(InputResult);

            Assert.NotEmpty(Actual);
            Assert.Equal(ExpectedCount, Actual.Count());
            Assert.All(Actual, e =>
            {
                Assert.False(e.RegistrationSuccessful);
            });
        }
    }
}
