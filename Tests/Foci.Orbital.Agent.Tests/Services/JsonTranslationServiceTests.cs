using Bogus;
using Foci.Orbital.Agent.Exceptions;
using Foci.Orbital.Agent.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Xunit;
using static Foci.Orbital.Agent.Services.JsonTranslationService;

namespace Foci.Orbital.Agent.Tests.Services
{
    /// <summary>
    /// Unit tests for the JsonTranslationService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class JsonTranslationServiceTests
    {
        private readonly Faker faker = new Faker();

        #region Translations
        private const string throwBusinessErrorTranslation = "function serviceInboundTranslation(e){throwBusinessError('Test message.',{test1:'testvalue1',test2:'testvalue2'})}";
        private const string badHeaderTranslation = "function serviceInboundTranslation(s){throwBusinessError('Test message.','Bad Headers')}";
        private const string echoTranslation = "function serviceInboundTranslation(n){return n}function serviceOutboundTranslation(n){return n}function getDynamicParameters(n){return n}";
        #endregion

        /// <summary>
        /// Testing a successful Execute Translation Function for Inbound Translation
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceInboundTranslateTest()
        {
            var Input = new
            {
                originalMessage = "{\"testString\": \"TestString\"}",
                translationnnFunction = JsonTranslationFunction.Inbound
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = "{\"testString\":\"TestString\"}";

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a successful Execute Translation Function for Outbound Translation
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceOutboundTranslateTest()
        {
            var Input = new
            {
                originalMessage = "{\"testString\":\"TestString\"}",
                translationnnFunction = JsonTranslationFunction.Outbound
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = "{\"testString\":\"TestString\"}";

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a successful Execute Translation Function for Parameters Translation
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceParamsTranslateTest()
        {
            var Input = new
            {
                originalMessage = "{\"testString\":\"TestString\"}",
                translationnnFunction = JsonTranslationFunction.GetParams
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = "{\"testString\":\"TestString\"}";

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a successful Execute Translation Function for Inbound Translation with  string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceInboundTranslateTestWithString()
        {
            var TestObject = new TestObject() { TestString = "TestString" };
            var Input = new
            {
                originalMessage = JsonConvert.SerializeObject(TestObject),
                translationnnFunction = JsonTranslationFunction.Inbound
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = JsonConvert.SerializeObject(TestObject);

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a successful Execute Translation Function for Outbound Translation
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceOutboundTranslateTestWithString()
        {
            var Input = new
            {
                originalMessage = "{\"testString\":\"TestString\"}",
                translationnnFunction = JsonTranslationFunction.Outbound
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = "{\"testString\":\"TestString\"}";

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a successful Execute Translation Function for Outbound Translation
        /// </summary>
        [Theory]
        [InlineData("TranslationArray.js", "InputArray.json","ExpectedArray.json")]
        [InlineData("TranslationDoubleArrays.js", "InputDoubleArray.json", "ExpectedArray.json")]
        [Trait("Category", "Success")]
        public void ServiceOutboundTranslateTestWithStringArray(string translation,string inputMessage, string expected)
        {
            var Input = new
            {
                originalMessage = ReadJsonFile(inputMessage, "ServiceOutboundTranslateTestWithStringArray"),
                translationnnFunction = JsonTranslationFunction.Outbound,
                translation = ReadTranslationFile(translation, "ServiceOutboundTranslateTestWithStringArray")
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, Input.translation, Input.translationnnFunction);
            var Expected = ReadJsonFile(expected, "ServiceOutboundTranslateTestWithStringArray");

            Assert.Equal(Expected, Actual);

        }
        

        /// <summary>
        /// Testing a successful Execute Translation Function for Parameters Translation with string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ServiceParamsTranslateTestWithString()
        {
            var Input = new
            {
                originalMessage = "{\"testString\":\"TestString\"}",
                translationnnFunction = JsonTranslationFunction.GetParams
            };

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction);
            var Expected = "{\"testString\":\"TestString\"}";

            Assert.Equal(Expected, Actual);

        }

        /// <summary>
        /// Testing a failed Execute Translation of no Function 
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceNoneTranslateTest()
        {
            var Input = new
            {
                originalMessage = "{\"testString\":\"TestString\"}",
                translationnnFunction = JsonTranslationFunction.None
            };

            var Target = new JsonTranslationService();
            Assert.Throws<OrbitalUnknownEnumException>(() => Target.ExecuteTranslationFunction(Input.originalMessage, echoTranslation, Input.translationnnFunction));
        }

        /// <summary>
        /// Testing a failed Execute Translation Function for Inbound Translation
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceBusinessException()
        {
            var TestHeaders = new Dictionary<string, string>()
            {
                {"test1", "testvalue1" },
                {"test2", "testvalue2" }
            };
            var Input = new
            {
                originalMessage = "{\"testString\": \"TestString\"}",
                translationnnFunction = JsonTranslationFunction.Inbound
            };

            var Target = new JsonTranslationService();
            var Action = Assert.Throws<OrbitalBusinessException>(() => Target.ExecuteTranslationFunction(Input.originalMessage, throwBusinessErrorTranslation, Input.translationnnFunction));

            Assert.Equal(JsonConvert.SerializeObject("Test message."), Action.Message);
            Assert.Equal(TestHeaders, Action.Headers);
        }

        /// <summary>
        /// Testing a failed Execute Translation Function for Inbound Translation where the user returns invalid header information.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceBusinessExceptionWithBadHeaders()
        {
            var Input = new
            {
                originalMessage = "{\"testString\": \"TestString\"}",

                translationnnFunction = JsonTranslationFunction.Inbound
            };

            var Target = new JsonTranslationService();
            Assert.Throws<JsonSerializationException>(() => Target.ExecuteTranslationFunction(Input.originalMessage, badHeaderTranslation, Input.translationnnFunction));
        }

        /// <summary> 
        /// Test passing null message to ExecuteTranslationFunction method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ExecuteTranslationFunctionNullMessage()
        {
            #region Setup
            var Inputs = new
            {
                message = faker.Random.String(),

                function = JsonTranslationFunction.Inbound
            };
            #endregion

            var Target = new JsonTranslationService();

            var Actual = Target.ExecuteTranslationFunction(null, echoTranslation, Inputs.function);

            Assert.Null(Actual);
        }

        /// <summary> 
        /// Test passing null translation URI to ExecuteTranslationFunction method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ExecuteTranslationFunctionNullUri()
        {
            #region Setup
            var Inputs = new
            {
                message = faker.Random.String(),
                uri = faker.Random.String(),
                function = JsonTranslationFunction.Inbound
            };
            #endregion

            var Target = new JsonTranslationService();

            Assert.Throws<OrbitalArgumentException>(() => Target.ExecuteTranslationFunction(Inputs.message, null, Inputs.function));
        }

        /// <summary>
        /// Testing provide FunctionExist with a function that does exist in the translation
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void FunctionExistWithValidFunction()
        {
            var Input = new
            {
                translation = "function serviceInboundTranslation(n){return n}",
                translationnnFunction = JsonTranslationFunction.Inbound
            };

            var Target = new JsonTranslationService();
            var Actual = Target.FunctionExist(Input.translation, Input.translationnnFunction);

            Assert.True(Actual);
        }

        /// <summary>
        /// Testing provide FunctionExist with a function that does not exist in the translation
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void FunctionExistWithInvalidFunction()
        {
            var Input = new
            {
                translation = "function serviceInboundTranslation(n){return n}",
                translationnnFunction = JsonTranslationFunction.Outbound
            };

            var Target = new JsonTranslationService();
            var Actual = Target.FunctionExist(Input.translation, Input.translationnnFunction);

            Assert.False(Actual);
        }

        /// <summary>
        /// Testing provide FunctionExist with null translation
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void FunctionExistWithNullTranslation()
        {
            var Input = new
            {
                translationnnFunction = JsonTranslationFunction.Outbound
            };

            var Target = new JsonTranslationService();

            Assert.Throws<OrbitalArgumentException>(() => Target.FunctionExist(null, Input.translationnnFunction));
        }

        private class TestObject
        {
            public string TestString { get; set; }

            public TestObject()
            {

            }

        }

        private string FociAssemblyDirectory
        {
            get
            {
                UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
                return Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            }
        }

        private string ReadTranslationFile(string fileName, string testName)
        {
            var filePath = Path.Combine(FociAssemblyDirectory, "TheoryData", testName, fileName);

            var file = File.ReadAllText(filePath);
            return file;
        }
        private string ReadJsonFile(string fileName, string testName)
        {
            var filePath = Path.Combine(FociAssemblyDirectory, "TheoryData",testName, fileName);

            var file = File.ReadAllText(filePath);
            return file;
        }
    }
}
