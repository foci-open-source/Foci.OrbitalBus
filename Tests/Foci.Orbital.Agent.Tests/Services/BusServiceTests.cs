﻿using EasyNetQ;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services;
using Foci.Orbital.Agent.Services.Interface;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Unity;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Services
{
    /// <summary>
    /// Unit tests for the BusService.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class BusServiceTests
    {

        /// <summary>
        /// Testing the success of bootstrapping a service if it already exists in the bus manager.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusServiceBootstrapServiceTestIfExists()
        {
            var Input = new
            {
                connectionDetails = new BusConnectionDetails("Host Details"),
                subscription = SubscriberDetails.Create<string>((o) => { }, string.Empty)
            };
            #region Substitutes
            var Bus = Substitute.For<IBus>();
            var BusManager = Substitute.For<IBusManager>();
            BusManager.Exists("Host Details").ReturnsForAnyArgs(false);
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();
            EasyNetQService.CreateBus(Input.connectionDetails).ReturnsForAnyArgs(Bus);
            #endregion

            var Target = new BusService(BusManager, EasyNetQService);



            var Actual = Target.BootstrapService(Input.connectionDetails, Input.subscription);
            var Expected = Bus;
        }

        /// <summary>
        /// Testing the success of bootstrapping an async service if it does not exist in the bus manager.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusServiceBootstrapServiceAsynchronousTestIfDoesNotExists()
        {
            var Bus = Substitute.For<IBus>();
            var BusManager = Substitute.For<IBusManager>();
            BusManager.Exists("Host Details").ReturnsForAnyArgs(true);
            BusManager.Get("Host Details").ReturnsForAnyArgs(Bus);
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();

            var Input = new
            {
                connectionDetails = new BusConnectionDetails("Host Details"),
                subscription = SubscriberDetails.Create<string>((o) => { }, string.Empty)
            };

            var Target = new BusService(BusManager, EasyNetQService);

            var Actual = Target.BootstrapService(Input.connectionDetails, Input.subscription);
            var Expected = Bus;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing the success in bootstrapping a sync service if it does not already exist in the bus manager.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void BusServiceBootstrapServiceSynchronousTestIfDoesNotExists()
        {
            var Bus = Substitute.For<IBus>();
            var BusManager = Substitute.For<IBusManager>();
            BusManager.Exists("Host Details").ReturnsForAnyArgs(true);
            BusManager.Get("Host Details").ReturnsForAnyArgs(Bus);
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();

            var Input = new
            {
                connectionDetails = new BusConnectionDetails("Host Details"),
                subscription = SubscriberDetails.Create<string, Payload>((o) => { return Payload.Create("test"); }, string.Empty)
            };

            var Target = new BusService(BusManager, EasyNetQService);

            var Actual = Target.BootstrapService(Input.connectionDetails, Input.subscription);
            var Expected = Bus;

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing null bus manager to BusService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BusServiceNullManager()
        {
            #region Substitutions
            var BusManager = Substitute.For<IBusManager>();
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new BusService(null, EasyNetQService));
        }

        /// <summary> 
        /// Test passing null EasyNetQ service to BusService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BusServiceNullService()
        {
            #region Substitutions
            var BusManager = Substitute.For<IBusManager>();
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new BusService(BusManager, null));
        }

        /// <summary> 
        /// Test passing null connection detail to BootstrapService method
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void BootstrapServiceNullConnectionDetail()
        {
            #region Substitutions
            var BusManager = Substitute.For<IBusManager>();
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();
            #endregion

            #region Setup
            var Input = new
            {
                subscription = SubscriberDetails.Create<string>((o) => { }, string.Empty)
            };
            #endregion

            var Target = new BusService(BusManager, EasyNetQService);

            Assert.Throws<ArgumentNullException>(() => Target.BootstrapService(null, Input.subscription));
        }

        /// <summary> 
        /// Test passing null connection detail to CreateBus method
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateBusNullConnectionDetail()
        {
            #region Substitutions
            var BusManager = Substitute.For<IBusManager>();
            var EasyNetQService = Substitute.For<IEasyNetQRepository>();
            #endregion

            var Target = new BusService(BusManager, EasyNetQService);

            Assert.Throws<ArgumentNullException>(() => Target.CreateBus(null));
        }

        /// <summary>
        /// Verify that the dependency injector is managing the service as a singleton,
        /// Also verify that the dependency injector is calling dispose on the service if it is disposed as well
        /// </summary>
        [Fact]
        public void BusManagerIsDisposedWithContainer()
        {
            DependencyInjector.RegisterDependencies();

            var Target1 = DependencyInjector.Container.Resolve<IBusService>();
            var Target2 = DependencyInjector.Container.Resolve<IBusService>();

            var Expected = true;
            var IsSingleton = Target1.Equals(Target2);

            DependencyInjector.Container.Dispose();

            var Actual = Target1.GetType().GetField("disposed", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(Target1);

            Assert.Equal(Expected, Actual);
            Assert.Equal(Expected, IsSingleton);
        }
    }
}
