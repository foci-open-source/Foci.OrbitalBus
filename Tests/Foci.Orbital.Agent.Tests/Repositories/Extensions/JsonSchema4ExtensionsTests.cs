using Bogus;
using Foci.Orbital.Agent.Repositories.Extensions;
using Foci.Orbital.Agent.Tests.TheoryData.ValidateOrbitalJsonInvalidJsonString;
using NJsonSchema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Repositories.Extensions
{
    /// <summary>
    /// Unit tests for JsonSchema4Extensions class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class JsonSchema4ExtensionsTests
    {
        private readonly Faker faker = new Faker();

        #region Fields
        private readonly JsonSchema4 jsonSchema = JsonSchema4.FromJsonAsync(
            @"{ ""$id"": ""http://example.com/example.json"", ""type"": ""object"",""definitions"": {},""$schema"": ""http://json-schema.org/draft-07/schema#"",""properties"": {""id"": {""$id"": ""/properties/id"",""type"": ""integer"",""title"": ""The Id Schema "",""default"": 0}},""required"": [""id""]}"
            ).Result;
        #endregion

        /// <summary>
        /// Test ValidateOrbitalJson with valid json string
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ValidateOrbitalJsonValidJsonString()
        {
            #region Setup
            var Inputs = new
            {
                msg = "{\"id\":1}"
            };
            #endregion

            Assert.Empty(JsonSchema4Extensions.ValidateMessage(jsonSchema, Inputs.msg));
        }

        /// <summary>
        /// Test ValidateOrbitalJson with invalid json string and null value
        /// </summary>
        [Theory]
        [ClassData(typeof(ValidateOrbitalJsonInvalidJsonStringData))]
        [Trait("Category", "Failure")]
        public void ValidateOrbitalJsonInvalidJsonString(string message, int expectedCount, string expected)
        {

            var Actual = JsonSchema4Extensions.ValidateMessage(jsonSchema, message);

            Assert.Equal(expectedCount, Actual.Count());
            Assert.Contains(expected, Actual.First().Message);
        }
        

    }
}
