﻿using Bogus;
using Foci.Orbital.Agent.Models;
using Foci.Orbital.Agent.Models.Consul.Common;
using Foci.Orbital.Agent.Models.Consul.Requests;
using Foci.Orbital.Agent.Models.Consul.Responses;
using Foci.Orbital.Agent.Repositories;
using NSubstitute;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Xunit;

namespace Foci.Orbital.Agent.Tests.Repositories
{
    /// <summary>
    /// Unit test for consul repository
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsulRepositoryTest
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null client to ConsulRepository 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ConsulRepositoryNullClient()
        {
            Assert.Throws<ArgumentNullException>(() => new ConsulRepository(null));
        }

        /// <summary>
        /// Testing register service success
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void RegisterServiceValidRequest()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                request = new RegisterServiceRequest(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            };

            clientSub.Execute(Arg.Any<RestRequest>()).Returns(new RestResponse()
            {
                StatusCode = HttpStatusCode.OK,
                ResponseStatus = ResponseStatus.Completed
            });
            #endregion

            var Target = new ConsulRepository(clientSub);

            Assert.True(Target.RegisterService(Input.request, Input.address, Input.cert));
        }

        /// <summary> 
        /// Test passing null consul IP to RegisterService method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceNullAddress()
        {
            #region Substitutions
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                cert = Maybe<X509Certificate>.None,
                request = new RegisterServiceRequest(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            };
            #endregion

            var Target = new ConsulRepository(clientSub);

            Assert.Throws<ArgumentException>(() => Target.RegisterService(Input.request, null, Input.cert));
        }

        /// <summary> 
        /// Test passing null register service request to RegisterService method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceNullRequest()
        {
            #region Substitutions
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None
            };
            #endregion

            var Target = new ConsulRepository(clientSub);

            Assert.Throws<ArgumentNullException>(() => Target.RegisterService(null, Input.address, Input.cert));
        }

        /// <summary>
        /// Test failing to register service
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void RegisterServiceInvalidRequest()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();

            clientSub.Execute(Arg.Any<RestRequest>())
                .Returns(new RestResponse<Dictionary<string, ServiceDetails>>()
                {
                    ResponseStatus = ResponseStatus.Error,
                    ErrorException = new Exception("Test error")
                });
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                request = new RegisterServiceRequest(faker.Random.String(), faker.Random.String(), faker.Internet.Ip(), faker.Random.UShort())
            };

            clientSub.Execute(Arg.Any<RestRequest>()).Returns(new RestResponse<Dictionary<string, ServiceDetails>>()
            {
                ResponseStatus = ResponseStatus.Error,
                ErrorException = new Exception("Test error")
            });
            #endregion

            var Target = new ConsulRepository(clientSub);
            Assert.Throws<Exception>(() => Target.RegisterService(Input.request, Input.address, Input.cert));
        }

        /// <summary>
        /// Test successfully set key value pair
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void SetKeyValueValidRequest()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String(),
                value = faker.Random.String()
            };

            clientSub.Execute<bool>(Arg.Any<RestRequest>()).Returns(new RestResponse<bool>()
            {
                StatusCode = HttpStatusCode.OK,
                ResponseStatus = ResponseStatus.Completed
            });
            #endregion

            var Target = new ConsulRepository(clientSub);
            var Actual = Target.SetKeyValue(Input.key, Input.value, Input.address, Input.cert);

            Assert.True(Actual);
        }

        /// <summary> 
        /// Test passing null Consul IP address to SetKeyValue method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void SetKeyValueNullAddress()
        {
            #region Substitutions
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String(),
                value = faker.Random.String()
            };
            #endregion

            var Target = new ConsulRepository(clientSub);

            Assert.False(Target.SetKeyValue(Input.key, Input.value, null, Input.cert));
        }

        /// <summary>
        /// Test fail to set key value pair
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void SetKeyValueInvalidRequest()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String(),
                value = faker.Random.String()
            };

            clientSub.Execute<bool>(Arg.Any<RestRequest>()).Returns(new RestResponse<bool>()
            {
                ResponseStatus = ResponseStatus.Error,
                ErrorException = new Exception("Test error")
            });
            #endregion

            var Target = new ConsulRepository(clientSub);
            Assert.Throws<Exception>(() => Target.SetKeyValue(Input.key, Input.value, Input.address, Input.cert));
        }


        /// <summary>
        /// Test successfully set key value pair
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetKeyValueValidRequest()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String()
            };

            clientSub.Execute<List<GetKeyValueResponse>>(Arg.Any<RestRequest>()).Returns(new RestResponse<List<GetKeyValueResponse>>()
            {
                StatusCode = HttpStatusCode.OK,
                ResponseStatus = ResponseStatus.Completed,
                Content = "[{\"Value\":\"dGVzdFNlcnZpY2U=\"}]"
            });
            #endregion

            var Target = new ConsulRepository(clientSub);
            var Actual = Target.GetKeyValue(Input.key, Input.address, Input.cert);
            var Expected = "testService";

            Assert.Equal(Actual, Expected);
        }

        /// <summary> 
        /// Test passing null Consul IP address to SetKeyValue method 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void GetKeyValueNullAddress()
        {
            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String()
            };

            clientSub.Execute<List<GetKeyValueResponse>>(Arg.Any<RestRequest>()).Returns(new RestResponse<List<GetKeyValueResponse>>());
            #endregion

            var Target = new ConsulRepository(clientSub);
            var Actual = Target.GetKeyValue(Input.key, null, Input.cert);
            var Expected = string.Empty;

            Assert.Equal(Actual, Expected);
        }

        /// <summary>
        /// Test fail to set key value pair
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetKeyValueInvalidRequest()
        {

            #region Substitutes
            var clientSub = Substitute.For<IRestClient>();
            #endregion

            #region Setup
            var Input = new
            {
                address = string.Format("http://{0}:{1}", faker.Internet.Ip(), faker.Random.UShort()),
                cert = Maybe<X509Certificate>.None,
                key = faker.Random.String()
            };

            clientSub.Execute<List<GetKeyValueResponse>>(Arg.Any<RestRequest>()).Returns(new RestResponse<List<GetKeyValueResponse>>()
            {
                ResponseStatus = ResponseStatus.Error,
                ErrorException = new Exception("Test error")
            });
            #endregion

            var Target = new ConsulRepository(clientSub);
            Assert.Throws<Exception>(() => Target.GetKeyValue(Input.key, Input.address, Input.cert));
        }
    }
}
