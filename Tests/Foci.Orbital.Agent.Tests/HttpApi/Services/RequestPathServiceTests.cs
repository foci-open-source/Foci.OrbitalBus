using Bogus;
using Foci.Orbital.Agent.HttpApi.Services;
using Foci.Orbital.Agent.Models.Service;
using Foci.Orbital.Agent.Policies.Cache.Scenarios;
using Foci.Orbital.Agent.Repositories.Interface;
using Foci.Orbital.Agent.Services.Interface;
using Foci.Orbital.Agent.Tests.TheoryData.RequestPathServiceInvalidInputs;
using Foci.Orbital.Agent.Tests.TheoryData.RequestPathServiceValidInputs;
using Microsoft.AspNetCore.Http;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Agent.Tests.HttpApi.Services
{
    /// <summary>
    /// Unit tests for RequestPathService class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RequestPathServiceTests
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null memory cache to RequestPathService 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void RequestPathServiceNullMemCache()
        {
            Assert.Throws<ArgumentNullException>(() => new RequestPathService(null));
        }

        /// <summary> 
        /// Test passing various valid inputs to GetAdditionalPath 
        /// </summary> 
        [Theory]
        [ClassData(typeof(InputRequestPathServiceValidInputs))]
        [Trait("Category", "Success")]
        public void GetAdditionalPathWithValidPath(ServiceDefinition service, PathString path, string verb)
        {
            #region Substitutions
            var serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            var splits = path.Value.Split('/').Skip(1).ToArray();
            var operation = service.Operations
                   .Where(o => ((splits.Length > 1) ? (o.Name == splits[1]) : (o.Http.VerbOnly)) && o.Http.Verb.Equals(verb, StringComparison.InvariantCultureIgnoreCase))
                   .ToList().First();
            var result = (splits.Length > 1) ? new PathString($"/{operation.IsSync}") : new PathString($"/{operation.Name}/{operation.IsSync}");

            serviceDefinitionRepository.GetServiceDefinitionByName(service.ServiceName).Returns(service);
            #endregion

            var Target = new RequestPathService(serviceDefinitionRepository);

            var Expected = result;
            var Actual = Target.GetAdditionalPath(path, verb);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test passing various invalid inputs to GetAdditionalPath
        /// </summary> 
        [Theory]
        [ClassData(typeof(InputRequestPathServiceInvalidInputs))]
        [Trait("Category", "Failure")]
        public void GetAdditionalPathWithInvalidInputs(ServiceDefinition service, PathString path, string verb)
        {
            #region Substitutions
            var serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup

            var splits = path.Value.Split('/');
            var serviceName = (splits.Length < 2) ? "" : splits.Skip(1).ToArray()[0];

            serviceDefinitionRepository.GetServiceDefinitionByName(serviceName).Returns(x =>
            {
                if (service != null && !string.IsNullOrEmpty(serviceName) && service.ServiceName != serviceName)
                {
                    throw new ArgumentException();
                }
                return service;
            });
            #endregion

            var Target = new RequestPathService(serviceDefinitionRepository);

            Assert.Throws<ArgumentException>(() => Target.GetAdditionalPath(path, verb));
        }

        /// <summary> 
        /// Test passing various valid inputs to Validate 
        /// </summary> 
        [Theory]
        [ClassData(typeof(InputRequestPathServiceValidInputs))]
        [Trait("Category", "Success")]
        public void ValidateWithValidInput(ServiceDefinition service, PathString path, string verb)
        {
            #region Substitutions
            var serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            serviceDefinitionRepository.GetServiceDefinitionByName(service.ServiceName).Returns(service);
            #endregion

            var Target = new RequestPathService(serviceDefinitionRepository);

            Assert.Empty(Target.Validate(path, verb));
        }

        /// <summary> 
        /// Test passing various invalid inputs to Validate 
        /// </summary> 
        [Theory]
        [ClassData(typeof(InputRequestPathServiceInvalidInputs))]
        [ClassData(typeof(InputValidateWithInvalidInput))]
        [Trait("Category", "Failure")]
        public void ValidateWithInvalidInput(ServiceDefinition service, PathString path, string verb)
        {
            #region Substitutions
            var serviceDefinitionRepository = Substitute.For<IServiceDefinitionRepository>();
            var cache = new ServiceCache(serviceDefinitionRepository);
            ServiceCache.StartPolicyCache();
            #endregion

            #region Setup
            var splits = path.Value.Split('/');
            var serviceName = (splits.Length < 2) ? "" : splits.Skip(1).ToArray()[0];

            serviceDefinitionRepository.GetServiceDefinitionByName(serviceName).Returns(x =>
            {
                if (service != null && !string.IsNullOrEmpty(serviceName) && service.ServiceName != serviceName)
                {
                    throw new ArgumentException();
                }
                return service;
            });
            #endregion

            var Target = new RequestPathService(serviceDefinitionRepository);

            Assert.NotEmpty(Target.Validate(path, verb));
        }
    }
}
