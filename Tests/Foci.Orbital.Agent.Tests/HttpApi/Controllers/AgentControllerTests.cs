using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Agent.Factories.Faults;
using Foci.Orbital.Agent.HttpApi.Controllers;
using Foci.Orbital.Agent.Models.Requests;
using Foci.Orbital.Agent.OperationHandler;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Agent.Tests.HttpApi.Controllers
{
    /// <summary>
    /// Unit tests for AgentController class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class AgentControllerTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Test information for invoking sync operation to Get
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetWithSyncOperation()
        {
            #region Substitutions
            var operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            #region Setup
            var input = new
            {
                serviceName = faker.Random.String(),
                operationName = faker.Random.String(),
                body = faker.Random.String(),
                isSync = true,
                result = faker.Lorem.Sentence()
            };

            operationsHandler.InvokeSyncOperation(Arg.Any<OrbitalRequest>()).Returns(Payload.Create(input.result));
            #endregion

            var Target = new AgentController(operationsHandler);
            var result = ((ObjectResult)Target.Get(input.serviceName, input.operationName, input.isSync, input.body));

            var Expected = input.result;
            var ExpectedCode = 200;

            var Actual = ((Payload)result.Value).Match<string>(v => v, e => string.Empty);
            var ActualCode = result.StatusCode;

            Assert.Equal(Expected, Actual);
            Assert.Equal(ExpectedCode, ActualCode);
        }

        /// <summary>
        /// Test information for invoking async operation to Get
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetWithAsyncOperation()
        {
            #region Substitutions
            var operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            #region Setup
            var input = new
            {
                serviceName = faker.Random.String(),
                operationName = faker.Random.String(),
                body = faker.Random.String(),
                isSync = false
            };
            #endregion

            var Target = new AgentController(operationsHandler);
            var result = ((ObjectResult)Target.Get(input.serviceName, input.operationName, input.isSync, input.body));

            var Expected = 200;

            var Actual = result.StatusCode;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Test information for invoking sync operation with invalid request to Get
        /// Expecting to get status code 400 with faults
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void GetWithInvalidRequest()
        {
            #region Substitutions
            var operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            #region Setup
            var input = new
            {
                serviceName = faker.Random.String(),
                operationName = faker.Random.String(),
                body = faker.Random.String(),
                isSync = true,
                fault = new AgentFaultFactory().CreateFault(new ArgumentNullException())
            };

            operationsHandler.InvokeSyncOperation(Arg.Any<OrbitalRequest>()).Returns(Payload.Create(new List<Fault>() { input.fault }));
            #endregion

            var Target = new AgentController(operationsHandler);
            var result = ((ObjectResult)Target.Get(input.serviceName, input.operationName, input.isSync, input.body));

            var Expected = 400;
            var Actual = result.StatusCode;
            var ActualContent = ((Payload)result.Value).Match<bool>(v => false, e => true);

            Assert.Equal(Expected, Actual);
            Assert.True(ActualContent);
        }

        /// <summary>
        /// Test information for invoking operation to Post
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PostWithValidOperation()
        {
            #region Substitutions
            var operationsHandler = Substitute.For<IOperationsHandler>();
            #endregion

            #region Setup
            var input = new
            {
                serviceName = faker.Random.String(),
                operationName = faker.Random.String(),
                body = faker.Random.String(),
                isSync = true,
                result = faker.Lorem.Sentence()
            };

            operationsHandler.InvokeSyncOperation(Arg.Any<OrbitalRequest>()).Returns(Payload.Create(input.result));
            #endregion

            var Target = new AgentController(operationsHandler);
            var result = ((ObjectResult)Target.Post(input.serviceName, input.operationName, input.isSync, input.body));

            var Expected = input.result;
            var ExpectedCode = 200;

            var Actual = ((Payload)result.Value).Match<string>(v => v, e => string.Empty);
            var ActualCode = result.StatusCode;

            Assert.Equal(Expected, Actual);
            Assert.Equal(ExpectedCode, ActualCode);
        }
    }
}
