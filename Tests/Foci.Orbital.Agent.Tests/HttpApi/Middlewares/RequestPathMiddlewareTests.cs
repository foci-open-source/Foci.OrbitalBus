using Bogus;
using Foci.Orbital.Agent.HttpApi.Middlewares;
using Foci.Orbital.Agent.HttpApi.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace Foci.Orbital.Agent.Tests.HttpApi.Middlewares
{
    /// <summary>
    /// Unit tests for RequestPathMiddleware class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RequestPathMiddlewareTests
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null path creator to ServiceValidationMiddleware 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceValidationMiddlewareNullPathCreator()
        {
            #region Substitutions
            var serviceValidator = Substitute.For<IServiceValidator>();
            #endregion

            #region Setup
            async Task request(HttpContext innerHttpContext)
            {
                await innerHttpContext.Response.WriteAsync(faker.Random.String());
            }
            #endregion

            Assert.Throws<ArgumentNullException>(() => new RequestPathMiddleware(null, serviceValidator, request));
        }

        /// <summary> 
        /// Test passing null service validator to ServiceValidationMiddleware 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceValidationMiddlewareNullServiceValidator()
        {
            #region Substitutions
            var pathCreator = Substitute.For<IPathCreator>();
            #endregion

            #region Setup
            async Task request(HttpContext innerHttpContext)
            {
                await innerHttpContext.Response.WriteAsync(faker.Random.String());
            }
            #endregion

            Assert.Throws<ArgumentNullException>(() => new RequestPathMiddleware(pathCreator, null, request));
        }

        /// <summary> 
        /// Test passing null request delegate to ServiceValidationMiddleware 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void ServiceValidationMiddlewareNullRequest()
        {
            #region Substitutions
            var pathCreator = Substitute.For<IPathCreator>();
            var serviceValidator = Substitute.For<IServiceValidator>();
            #endregion

            Assert.Throws<ArgumentNullException>(() => new RequestPathMiddleware(pathCreator, serviceValidator, null));
        }

        /// <summary>
        /// Testing invoke using valid path
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public async void InvokeWithValidPath()
        {
            #region Substitutions
            var pathCreator = Substitute.For<IPathCreator>();
            var serviceValidator = Substitute.For<IServiceValidator>();
            #endregion

            #region Setup
            var input = new
            {
                serviceName = faker.Lorem.Word(),
                operationName = faker.Lorem.Word(),
                verb = "GET",
                isSync = false
            };

            var inputPath = new PathString($"/{input.serviceName}/{input.operationName}");

            var resultPath = new PathString($"/{input.isSync}");

            var inputHttpContext = new DefaultHttpContext();
            inputHttpContext.Request.Method = input.verb;
            inputHttpContext.Request.Path = inputPath;

            serviceValidator.Validate(inputPath, input.verb).Returns(new List<string>());

            pathCreator.GetAdditionalPath(inputPath, input.verb).Returns(resultPath);
            #endregion

            string Actual = string.Empty;

            Task request(HttpContext innerHttpContext)
            {
                return Task.Factory.StartNew(() =>
                {
                    Actual = innerHttpContext.Request.Path.ToString();
                });
            }

            var Target = new RequestPathMiddleware(pathCreator, serviceValidator, request);

            var Expected = inputPath.Add(resultPath).ToString();

            await Target.Invoke(inputHttpContext);

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test pass invalid path to invoke
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public async void InvokeWithInvalidPath()
        {
            #region Substitutions
            var pathCreator = Substitute.For<IPathCreator>();
            var serviceValidator = Substitute.For<IServiceValidator>();
            #endregion

            #region Setup
            async Task request(HttpContext innerHttpContext)
            {
                await innerHttpContext.Response.WriteAsync(faker.Random.String());
            }

            var input = new
            {
                serviceName = faker.Lorem.Text(),
                operationName = faker.Lorem.Text(),
                verb = "GET",
                isSync = false
            };

            var inputPath = new PathString($"/{input.serviceName}/{input.operationName}");

            var inputHttpContext = new DefaultHttpContext();
            inputHttpContext.Request.Method = input.verb;
            inputHttpContext.Request.Path = inputPath;

            serviceValidator.Validate(inputPath, input.verb).Returns(new List<string>() { faker.Random.String() });
            #endregion

            var Target = new RequestPathMiddleware(pathCreator, serviceValidator, request);

            var Expected = StatusCodes.Status400BadRequest;

            await Target.Invoke(inputHttpContext);

            var Actual = inputHttpContext.Response.StatusCode;

            Assert.Equal(Expected, Actual);
        }

        /// <summary> 
        /// Test pass invalid path to invoke
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public async void InvokeUnableToGetAdditionalPath()
        {
            #region Substitutions
            var pathCreator = Substitute.For<IPathCreator>();
            var serviceValidator = Substitute.For<IServiceValidator>();
            #endregion

            #region Setup
            async Task request(HttpContext innerHttpContext)
            {
                await innerHttpContext.Response.WriteAsync(faker.Random.String());
            }

            var input = new
            {
                serviceName = faker.Lorem.Text(),
                operationName = faker.Lorem.Text(),
                verb = "GET",
                isSync = false
            };

            var inputPath = new PathString($"/{input.serviceName}/{input.operationName}");

            var inputHttpContext = new DefaultHttpContext();
            inputHttpContext.Request.Method = input.verb;
            inputHttpContext.Request.Path = inputPath;

            serviceValidator.Validate(inputPath, input.verb).Returns(new List<string>());
            pathCreator.GetAdditionalPath(inputPath, input.verb).Returns(x => throw new ArgumentException());
            #endregion

            var Target = new RequestPathMiddleware(pathCreator, serviceValidator, request);

            var Expected = StatusCodes.Status400BadRequest;

            await Target.Invoke(inputHttpContext);

            var Actual = inputHttpContext.Response.StatusCode;

            Assert.Equal(Expected, Actual);
        }
    }
}
