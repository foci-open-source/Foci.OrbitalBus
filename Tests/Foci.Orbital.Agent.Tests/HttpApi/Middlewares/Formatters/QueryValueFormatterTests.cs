using Bogus;
using Foci.Orbital.Agent.HttpApi.Middlewares.Formatters;
using Foci.Orbital.Agent.Models.Service;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Xunit;

namespace Foci.Orbital.Agent.Tests.HttpApi.Middlewares.Formatters
{
    /// <summary>
    /// Unit tests for QueryValueFormatter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class QueryValueFormatterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary> 
        /// Test passing null operation to QueryValueFormatter 
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void QueryValueFormatterNullOperation()
        {
            #region Setup
            var input = new
            {
                bindingSource = BindingSource.Query,
                cultureInfo = CultureInfo.InvariantCulture,
                query = new Dictionary<string, StringValues>()
            };

            var inputQuery = new QueryCollection(input.query);
            #endregion

            Assert.Throws<ArgumentNullException>(() => new QueryValueFormatter(input.bindingSource, null, inputQuery, input.cultureInfo));
        }

        /// <summary>
        /// Testing provide request as key to GetValue
        /// </summary>
        [Theory]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"integer\"}},\"required\": [\"a\"]}", "{\"a\":\"1\"}", "{\"a\":1}")]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"number\"}},\"required\": [\"a\"]}", "{\"a\":\"1.1\"}", "{\"a\":1.1}")]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"string\"}},\"required\": [\"a\"]}", "{\"a\":\"1.1\"}", "{\"a\":\"1.1\"}")]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"boolean\"}},\"required\": [\"a\"]}", "{\"a\":\"true\"}", "{\"a\":true}")]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"array\"}},\"required\": [\"a\"]}", "{\"a\":\"[1, 1, 1]\"}", "{\"a\":\"[1, 1, 1]\"}")]
        [InlineData("{\"type\": \"object\",\"properties\": {\"a\": {\"type\": \"string\"}},\"required\": [\"a\"]}", "{\"b\":\"1\"}", "{\"b\":\"1\"}")]
        [Trait("Category", "Success")]
        public void GetValue(string schema, string query, string expected)
        {
            #region Setup
            var input = new
            {
                bindingSource = BindingSource.Query,
                cultureInfo = CultureInfo.InvariantCulture,
                query = JsonConvert.DeserializeObject<Dictionary<string, StringValues>>(query),
                key = "",
                operation = new OperationDefinition()
                {
                    Schemas = new OperationSchemas()
                    {
                        Request = schema
                    }
                }
            };

            var inputQuery = new QueryCollection(input.query);
            #endregion

            var Target = new QueryValueFormatter(input.bindingSource, input.operation, inputQuery, input.cultureInfo);

            var Expected = expected;
            var Actual = Target.GetValue(input.key);

            Assert.Equal(Expected, Actual.FirstValue);
        }
    }
}
