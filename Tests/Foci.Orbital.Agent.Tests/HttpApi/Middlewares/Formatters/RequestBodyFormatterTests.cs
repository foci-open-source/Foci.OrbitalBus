using Bogus;
using Foci.Orbital.Agent.HttpApi.Middlewares.Formatters;
using Foci.Orbital.Agent.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using Xunit;

namespace Foci.Orbital.Agent.Tests.HttpApi.Middlewares.Formatters
{
    /// <summary>
    /// Unit tests for RequestBodyFormatter class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class RequestBodyFormatterTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Testing provide valid content Types to CanRead
        /// </summary>
        [Theory]
        [InlineData("", true)]
        [InlineData(AgentConstants.MEDIA_TYPE_APP_JSON, true)]
        [InlineData(AgentConstants.MEDIA_TYPE_TEXT_PLAIN, true)]
        [Trait("Category", "Success")]
        public void CanReadWithValidContentType(string type, bool result)
        {
            #region Setup
            var input = new
            {
                httpContext = new DefaultHttpContext(),
                modelName = faker.Random.String(),
                dict = new ModelStateDictionary(),
                metadata = new EmptyModelMetadataProvider().GetMetadataForType(typeof(string))
            };
            input.httpContext.Request.ContentType = type;

            var inputContext = new InputFormatterContext(input.httpContext, input.modelName, input.dict, input.metadata, (s, e) => new StreamReader(s, e));
            #endregion

            var Target = new RequestBodyFormatter();

            var Expected = result;
            var Actual = Target.CanRead(inputContext);

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Testing providing null context to CanRead
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CanReadWithNullContext()
        {
            var Target = new RequestBodyFormatter();

            Assert.False(Target.CanRead(null));
        }

        /// <summary>
        /// Testing providing invalid content type to CanRead
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void CanReadWithInvalidContentType()
        {
            #region Setup
            var input = new
            {
                wrongType = faker.Random.String(),
                httpContext = new DefaultHttpContext(),
                modelName = faker.Random.String(),
                dict = new ModelStateDictionary(),
                metadata = new EmptyModelMetadataProvider().GetMetadataForType(typeof(string))
            };

            input.httpContext.Request.ContentType = input.wrongType;
            var inputContext = new InputFormatterContext(input.httpContext, input.modelName, input.dict, input.metadata, (s, e) => new StreamReader(s, e));
            #endregion

            var Target = new RequestBodyFormatter();

            Assert.False(Target.CanRead(inputContext));
        }

        /// <summary>
        /// Testing provide request body for reader to extract
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public async void ReadRequestBodyAsyncWithRequestBody()
        {
            #region Setup
            var input = new
            {
                body = faker.Lorem.Paragraph(),
                httpContext = new DefaultHttpContext(),
                modelName = faker.Random.String(),
                dict = new ModelStateDictionary(),
                metadata = new EmptyModelMetadataProvider().GetMetadataForType(typeof(string))
            };
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(input.body));
            input.httpContext.Request.Body = stream;

            var inputContext = new InputFormatterContext(input.httpContext, input.modelName, input.dict, input.metadata, (s, e) => new StreamReader(s, e));
            #endregion

            var Target = new RequestBodyFormatter();

            var Expected = input.body;
            var Actual = await Target.ReadRequestBodyAsync(inputContext);
            stream.Close();

            Assert.Equal(Expected, Actual.Model);
        }
    }
}
