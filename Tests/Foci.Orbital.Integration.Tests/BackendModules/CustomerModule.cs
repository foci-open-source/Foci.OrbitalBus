﻿using Foci.Orbital.Integration.Tests.Backend;
using Foci.Orbital.Integration.Tests.Models;
using Nancy;
using Nancy.ModelBinding;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Integration.Tests.BackendModules
{
    /// <summary>
    /// The Nancy module for Customer requests.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class CustomerModule : NancyModule
    {
        /// <summary>
        /// The backend client services. The HTTP verb is followed by the resource path. Values for the service are hard coded.
        /// </summary>
        public CustomerModule() : base("/customer")
        {

            // Receive customer and return success string
            Post("/", args =>
            {
                var customer = this.Bind<Customer>();

                ResultContainer.CustomerInformationReceived.Add(customer);

                return "Success!";
            });

            // Return a randomly generated customer 
            Get("/", args =>
            {
                var id = Request.Query.Id;
                ResultContainer.CustomerIdentifierReceived = new CustomerIdentifier { Id = id };

                //A default customer to return.
                var customer = new Customer()
                {
                    Id = id,
                    Name = new Name()
                    {
                        FirstName = "John",
                        MiddleName = "Jane",
                        LastName = "Doe"
                    },
                    Address = new Address()
                    {
                        AddressLine1 = "5 City Street",
                        AddressLine2 = "",
                        City = "Ottawa",
                        ProvinceState = "ON",
                        Country = "CA",
                        PostalZipCode = "5A5A5A"
                    }
                };

                return Response.AsJson(customer);
            });

            // Return http status code: NotFound
            Post("/StatusCodeError", args =>
            {
                return HttpStatusCode.NotFound;
            });

            // Return an unserializable type.
            Post("/WrongType", args =>
            {
                var wrongObject = new TestWrongClass() { TestString = "WrongDataType" };

                return Response.AsJson(wrongObject);
            });
        }

        /// <summary>
        /// The wrong class to be returned.
        /// </summary>
        public class TestWrongClass
        {
            /// <summary>
            /// A test string to check proper serialization.
            /// </summary>
            public string TestString { get; set; }
        }
    }
}
