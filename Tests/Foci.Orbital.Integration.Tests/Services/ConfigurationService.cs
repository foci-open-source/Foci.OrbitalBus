﻿using Foci.Orbital.Integration.Tests.Models.Configurations;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace Foci.Orbital.Integration.Tests.Services
{
    /// <summary>
    /// Service to load and access the configuration information for integration testing.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConfigurationService
    {
        private readonly ConsumerConfiguration configuration;

        /// <summary>
        /// Default constructor.  Loads the information from the config file.
        /// </summary>
        /// <param name="fileName">Name of the file in the relative Configs subfolder.</param>
        public ConfigurationService(string fileName)
        {
            try
            {
                var filePath = Path.Combine(FociAssemblyDirectory, "Configs", fileName);

                var file = File.ReadAllText(filePath);
                configuration = JsonConvert.DeserializeObject<ConsumerConfiguration>(file);
            }
            catch (Exception e)
            {
                throw new OrbitalConfigurationException(e.Message, e);
            }
        }

        /// <summary>
        /// Retrieve the configuration that has been loaded.
        /// </summary>
        /// <returns>The configuration information.</returns>
        public ConsumerConfiguration GetConfiguration()
        {
            return configuration;
        }

        private string FociAssemblyDirectory
        {
            get
            {
                UriBuilder uri = new UriBuilder(Assembly.GetExecutingAssembly().CodeBase);
                return Path.GetDirectoryName(Uri.UnescapeDataString(uri.Path));
            }
        }
    }
}
