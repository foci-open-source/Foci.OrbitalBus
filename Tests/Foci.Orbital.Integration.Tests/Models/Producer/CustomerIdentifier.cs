﻿
namespace Foci.Orbital.Integration.Tests.Models.Producer
{
    public class CustomerIdentifier
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CustomerIdentifier()
        {}

        /// <summary>
        /// Initialize Id during instantiation.
        /// </summary>
        /// <param name="id">The customer Id.</param>
        public CustomerIdentifier(int id)
        {
            this.id = id;
        }

        public int id { get; set; }
    }
}
