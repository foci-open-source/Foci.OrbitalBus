﻿using Newtonsoft.Json;

namespace Foci.Orbital.Integration.Tests.Models.Producer
{
    public class Name
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("middleName")]
        public string MiddleName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
