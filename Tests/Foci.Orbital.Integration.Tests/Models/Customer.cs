﻿namespace Foci.Orbital.Integration.Tests.Models
{
    /// <summary>
    /// Response type object for customer. 
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Customer()
        {
            this.Name = new Name();
            this.Address = new Address();
        }

        /// <summary>
        /// Int representing an ID for a customer
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Model that represents customer name
        /// </summary>
        public Name Name { get; set; }
        /// <summary>
        /// Model that represents customer address
        /// </summary>
        public Address Address { get; set; }
    }
}
