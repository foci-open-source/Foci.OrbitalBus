﻿namespace Foci.Orbital.Integration.Tests.Models
{
    /// <summary>
    /// Model for Customer's Address.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Integer representation of street number
        /// </summary>
        public int StreetNumber { get; set; }
        /// <summary>
        /// String representation of address
        /// </summary>
        public string AddressLine1 { get; set; }
        /// <summary>
        /// String representation of address
        /// </summary>
        public string AddressLine2 { get; set; }
        /// <summary>
        /// String representation of city
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// String representation of province/state
        /// </summary>
        public string ProvinceState { get; set; }
        /// <summary>
        /// String representation of country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// String representation of postal/zip code
        /// </summary>
        public string PostalZipCode { get; set; }
    }
}
