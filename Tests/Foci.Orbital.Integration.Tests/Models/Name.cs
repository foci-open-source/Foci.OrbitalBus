﻿namespace Foci.Orbital.Integration.Tests.Models
{
    /// <summary>
    /// Model for Customer's Name.
    /// </summary>
    public class Name
    {
        /// <summary>
        /// String representation of first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// String representation of middle name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// String representation of last name
        /// </summary>
        public string LastName { get; set; }
    }
}
