﻿namespace Foci.Orbital.Integration.Tests.Models
{
    /// <summary>
    /// Model containing an Int representing an ID for a customer.
    /// </summary>
    public class CustomerIdentifier
    {
        /// <summary>
        /// Int representing an ID for a customer
        /// </summary>
        public int Id { get; set; }
    }
}
