﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Foci.Orbital.Integration.Tests.Models.Configurations
{
    /// <summary>
    /// Public configuration class that stores configuration necessary for various public IPs
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PublicConfiguration
    {
        /// <summary>
        /// String representation of Agent's public IP address
        /// </summary>
        [JsonProperty("HttpIp")]
        public string HttpIp { get; set; } = "localhost";

        /// <summary>
        /// Int representation of Agent's public port
        /// </summary>
        [JsonProperty("HttpPort")]
        public int HttpPort { get; set; } = 3000;

        /// <summary>
        /// String representation of broker's public IP address
        /// </summary>
        [JsonProperty("BrokerIp")]
        public string BrokerIp { get; set; } = "localhost";

        /// <summary>
        /// Int representation of broker's public port
        /// </summary>
        [JsonProperty("BrokerPort")]
        public int BrokerPort { get; set; } = 5672;
    }
}
