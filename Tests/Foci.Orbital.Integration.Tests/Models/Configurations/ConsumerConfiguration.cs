﻿using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Integration.Tests.Models.Configurations
{
    /// <summary>
    /// A simple class to house the configuration parameters for the consumer.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ConsumerConfiguration
    {
        /// <summary>
        /// The ip address to register the Consumer at.
        /// </summary>
        public string NancyIp { get; set; }

        /// <summary>
        /// Port number to listen at for Nancy.
        /// </summary>
        public string NancyPort { get; set; }

        /// <summary>
        /// The IP address to access Consul
        /// </summary>
        public string ConsulIp { get; set; }
    }
}
