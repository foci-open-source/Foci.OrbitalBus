﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Xunit;

namespace Foci.Orbital.Integration.Tests.Attributes
{
    /// <summary>
    /// Attribute to only run a test in Linux
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class LinuxOnlyAttribute : FactAttribute
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public LinuxOnlyAttribute()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    if (Directory.Exists("/Applications")
                    & Directory.Exists("/System")
                    & Directory.Exists("/Users")
                    & Directory.Exists("/Volumes"))
                        Skip = "Not running on Linux system, ignored.";
                    break;
                default:
                    Skip = "Not running on Linux system, ignored.";
                    break;
            }
        }
    }
}
