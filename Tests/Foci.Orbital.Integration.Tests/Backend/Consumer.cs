﻿using Foci.Orbital.Integration.Tests.Models.Configurations;
using Nancy.Hosting.Self;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Foci.Orbital.Integration.Tests.Backend
{
    /// <summary>
    /// A consumer to serve as final destination of the integration tests.
    /// Allows for checking values getting to the far end of the pipeline.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Consumer
    {
        private readonly ConsumerConfiguration configuration;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="consumerConfiguration">The property values to configure the consumer service.</param>
        public Consumer(ConsumerConfiguration consumerConfiguration)
        {
            configuration = consumerConfiguration;
        }

        /// <summary>
        /// Starts the Nancy host to receive calls.
        /// </summary>
        /// <returns>The default task for a "void" async call.</returns>
        public async Task Run()
        {
            var uri = new Uri(string.Format("http://{0}:{1}", configuration.NancyIp, configuration.NancyPort));
            using (var host1 = new NancyHost(uri))
            {
                host1.Start();

                Console.WriteLine("Your application is running on " + uri);
                //This will keep the host running until the app is killed.
                await Task.Delay(Timeout.Infinite);
            }
        }
    }
}
