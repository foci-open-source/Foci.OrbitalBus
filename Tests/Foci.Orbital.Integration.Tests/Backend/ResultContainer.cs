﻿using Foci.Orbital.Integration.Tests.Models;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Foci.Orbital.Integration.Tests.Backend
{
    /// <summary>
    /// A class to house results received by the consumer.
    /// Properties are public static to allow the tests to access them.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ResultContainer
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        static ResultContainer()
        {
            //Prevents null values.
            CustomerInformationReceived = new List<Customer>();
        }

        /// <summary>
        /// A list of customers received at the Consumer.
        /// </summary>
        public static IList<Customer> CustomerInformationReceived { get; set; }

        /// <summary>
        /// A customer identifier that has been received at the Consumer.
        /// </summary>
        public static CustomerIdentifier CustomerIdentifierReceived { get; set; }

        /// <summary>
        /// Resets the contents of the variables to prevent cross-test results.
        /// </summary>
        public static void Initialize()
        {
            CustomerInformationReceived = new List<Customer>();
            CustomerIdentifierReceived = new CustomerIdentifier();
        }
    }
}
