using Foci.Orbital.Integration.Tests.Attributes;
using Foci.Orbital.Integration.Tests.Backend;
using Foci.Orbital.Integration.Tests.Models.Configurations;
using Foci.Orbital.Integration.Tests.Models.Producer;
using Foci.Orbital.Integration.Tests.Services;
using Foci.Orbital.OrbitalConnector.Factories;
using Foci.Orbital.OrbitalConnector.Models.Exceptions;
using Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions;
using Foci.Orbital.OrbitalConnector.Models.Payload;
using Foci.Orbital.OrbitalConnector.Models.Payload.Faults;
using Foci.Orbital.OrbitalConnector.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading;
using Xunit;

namespace Foci.Orbital.Integration.Tests
{
    /// <summary>
    /// The integration tests that are run are stored in this class.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class IntegrationTests
    {
        /// <summary>
        /// File names of configurations for integration testing.
        /// </summary>
        //private const string configFileName = "IntegrationConfig.json";
        private const string configFileName = "DockerIntegrationConfig.json"; //Config used for Docker

        private readonly Consumer TargetConsumer;
        private readonly ConsumerConfiguration configuration;
        private readonly PublicConfiguration publicConfiguration;


        /// <summary>
        /// Default constructor.  Instantiates the Configuration Service based on the runtime environment.
        /// </summary>
        public IntegrationTests()
        {
            var configurationService = new ConfigurationService(configFileName);
            configuration = configurationService.GetConfiguration();

            //publicConfiguration = new PublicConfiguration() { HttpIp = "localhost", HttpPort = 3000 };
            publicConfiguration = new PublicConfiguration() { HttpIp = "orbital", HttpPort = 80 }; //Config used for Docker

            TargetConsumer = new Consumer(configuration);
            TargetConsumer.Run();
        }

        /// <summary>
        /// Tests the basic asynchronous request using AddCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void AddCustomerIntegrationTest()
        {
            #region Setup
            ResultContainer.Initialize();
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .UseConsulPort(8500)
                .AddNewService("ConsumerService");

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "AddCustomer",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            #endregion

            var Expected = new Customer()
            {
                Id = 123,
                Name = new Name
                {
                    FirstName = "testFirst",
                    MiddleName = "testMiddle",
                    LastName = "testLast"
                },
                Address = new Address
                {
                    AddressLine1 = "testAddress1",
                    AddressLine2 = "testAddress2",
                    City = "testCity",
                    Country = "testCountry",
                    PostalZipCode = "testPostal",
                    ProvinceState = "testProvince"
                }
            };
            using (var Target = new OrbitalConnectionService())
            {

                Target.SendAsyncRaw(input.ServiceName, input.OperationName, input.Message);
                //Since it's async, we need to wait a bit to make sure the message has been received and the ResultContainer set.
                Thread.Sleep(2000);
                Assert.Single(ResultContainer.CustomerInformationReceived);
                var Actual = ResultContainer.CustomerInformationReceived.First();

                #region Asserts
                Assert.Equal(Expected.Id, Actual.Id);
                Assert.Equal(Expected.Name.FirstName, Actual.Name.FirstName);
                Assert.Equal(Expected.Name.MiddleName, Actual.Name.MiddleName);
                Assert.Equal(Expected.Name.LastName, Actual.Name.LastName);
                Assert.Equal(Expected.Address.AddressLine1, Actual.Address.AddressLine1);
                Assert.Equal(Expected.Address.AddressLine2, Actual.Address.AddressLine2);
                Assert.Equal(Expected.Address.City, Actual.Address.City);
                Assert.Equal(Expected.Address.ProvinceState, Actual.Address.ProvinceState);
                Assert.Equal(Expected.Address.Country, Actual.Address.Country);
                Assert.Equal(Expected.Address.PostalZipCode, Actual.Address.PostalZipCode);
                #endregion
            }
        }

        /// <summary>
        /// Tests the basic asynchronous request using AddCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void SendMessagetoNonexistantService()
        {
            #region Setup
            ResultContainer.Initialize();
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .UseConsulPort(8500)
                .AddNewService("ConsumerService");

            var input = new
            {
                ServiceName = "NonExistantService",
                OperationName = "AddCustomer",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            #endregion
            
            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalConsulException>(() => Target.SendAsyncRaw(input.ServiceName, input.OperationName, input.Message));
            }
        }

        /// <summary>
        /// Tests the basic asynchronous request using AddCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void SendMessagetoNonexistantOperation()
        {
            #region Setup
            ResultContainer.Initialize();
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .UseConsulPort(8500)
                .AddNewService("ConsumerService");

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "SomeOperation",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            #endregion

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalConfigurationException>(() => Target.SendAsyncRaw(input.ServiceName, input.OperationName, input.Message));

            }
        }

        #region Synchronous Tests
        /// <summary>
        /// Tests the basic synchronous request using GetCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void GetCustomerIntegrationTest()
        {
            #region Setup
            ResultContainer.Initialize();

            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService")
                .WithOperationTimeout("GetCustomer", TimeSpan.FromSeconds(30));

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomer",
                Message = JsonConvert.SerializeObject(new CustomerIdentifier(112))
            };
            #endregion

            #region Expected Values
            var ExpectedCustomer = new Customer()
            {
                Id = 112,
                Name = new Name
                {
                    FirstName = "John",
                    MiddleName = "Jane",
                    LastName = "Doe"
                },
                Address = new Address
                {
                    AddressLine1 = "5 City Street",
                    AddressLine2 = "",
                    City = "Ottawa",
                    ProvinceState = "ON",
                    Country = "CA",
                    PostalZipCode = "5A5A5A"
                }
            };
            var Expected = JsonConvert.SerializeObject(ExpectedCustomer);
            #endregion

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Target.SendRaw(input.ServiceName, input.OperationName, input.Message);
                Thread.Sleep(2000);
                var ActualCustomer = JsonConvert.DeserializeObject<Customer>(Actual);

                #region Asserts
                Assert.Equal(ExpectedCustomer.Id, ActualCustomer.Id);
                Assert.Equal(ExpectedCustomer.Name.FirstName, ActualCustomer.Name.FirstName);
                Assert.Equal(ExpectedCustomer.Name.MiddleName, ActualCustomer.Name.MiddleName);
                Assert.Equal(ExpectedCustomer.Name.LastName, ActualCustomer.Name.LastName);
                Assert.Equal(ExpectedCustomer.Address.AddressLine1, ActualCustomer.Address.AddressLine1);
                Assert.Equal(ExpectedCustomer.Address.AddressLine2, ActualCustomer.Address.AddressLine2);
                Assert.Equal(ExpectedCustomer.Address.City, ActualCustomer.Address.City);
                Assert.Equal(ExpectedCustomer.Address.ProvinceState, ActualCustomer.Address.ProvinceState);
                Assert.Equal(ExpectedCustomer.Address.Country, ActualCustomer.Address.Country);
                Assert.Equal(ExpectedCustomer.Address.PostalZipCode, ActualCustomer.Address.PostalZipCode);
                #endregion
            }
        }

        /// <summary>
        /// Tests the basic synchronous request for failure due to timeout.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void GetCustomerTimeoutIntegrationTest()
        {
            #region Setup
            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomer",
                Message = JsonConvert.SerializeObject(new CustomerIdentifier(112)),
                Timeout = TimeSpan.FromMilliseconds(10)
            };

            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService")
                .WithOperationTimeout("GetCustomer", input.Timeout);
            #endregion

            var Expected = new OrbitalTimeoutException("Operation took longer than " + input.Timeout + " to respond");

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalTimeoutException>(() => Target.SendRaw(input.ServiceName, input.OperationName, input.Message));
                Assert.Equal(Expected.Message, Actual.Message);
            }
        }

        /// <summary>
        /// Tests the basic synchronous request for failure due to a business fault in the translation layer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void GetCustomerTranslationBusinessFaultTest()
        {
            #region Setup
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService")
                .WithOperationTimeout("GetCustomer", TimeSpan.FromSeconds(30));

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomer",
                Message = JsonConvert.SerializeObject(new CustomerIdentifier(47))
            };
            #endregion

            #region Expected
            var Expected =
                   new OrbitalBusinessFaultException(
                       "Exception of type 'Foci.Orbital.OrbitalConnector.Models.Exceptions.FaultExceptions.OrbitalBusinessFaultException' was thrown.");
            //We just want to make sure the message got through.  The rest of the details is a pain to try and match against.
            var ExpectedFaultDetailsStart = "\"Bad first and last names\"";
            var ExpectedFaultErrorCode = OrbitalFaultCode.Orbital_BusinessFault_010;
            var ExpectedFaultTypeSource = "Orbital_BusinessFault_010";
            #endregion 

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalBusinessFaultException>(() => Target.SendRaw(input.ServiceName, input.OperationName, input.Message));

                #region Asserts
                Assert.Equal(Expected.Message, Actual.Message);
                Assert.Equal(ExpectedFaultDetailsStart, Actual.Fault.Details);
                Assert.Equal(ExpectedFaultErrorCode, Actual.Fault.OrbitalFaultCode);
                Assert.Equal(ExpectedFaultTypeSource, Actual.Fault.OrbitalFaultName);
                #endregion
            }
        }
        #endregion

        /// <summary>
        /// This test should return a business fault in the form of an HTTP Error Code. The error code is created from the Consumer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void HttpErrorTest()
        {

            #region Setup
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService");
            #endregion

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetHttpStatusCodeError",
                Message = JsonConvert.SerializeObject(new HttpStatusCodeErrorCustomer() { ArbitraryString = "Nada" })
            };
            //TODO change fault code to the correct one when it is fixed 
            var Expected = new OrbitalBusinessFaultException(
                new Fault(OrbitalFaultCode.Orbital_Business_Adapter_Error_012,
                                       "Orbital_Business_Adapter_Error_012",
                                       "There was an error in the adapter. See the details and headers for more information on the error.",
                                       "Https status code: NotFound Https status code description: Not Found"),
                new PayloadHeaders()
           );

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalBusinessFaultException>(() => Target.SendRaw(input.ServiceName, input.OperationName, input.Message));
                #region Asserts
                Assert.Equal(Expected.Fault.Details, Actual.Fault.Details);
                Assert.Equal(Expected.Fault.OrbitalFaultCode, Actual.Fault.OrbitalFaultCode);
                Assert.Equal(Expected.Fault.ErrorDescription, Actual.Fault.ErrorDescription);
                Assert.Equal(Expected.Fault.OrbitalFaultName, Actual.Fault.OrbitalFaultName);
                #endregion

            }
        }

        /// <summary>
        /// This test returns an empty object due to the Producer expecting the wrong object from the service.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "Integration")]
        public void EmptyObjectReturn()
        {
            #region Setup
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService");
            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetWrongObject",
                Message = JsonConvert.SerializeObject(new WrongObjectRequest() { ArbitraryString = "Arbitrary" })
            };
            #endregion

            using (var Target = new OrbitalConnectionService())
            {
                var Expected = JsonConvert.SerializeObject(new WrongObjectResponse() { testString = "WrongDataType" });
                var Actual = Target.SendRaw(input.ServiceName, input.OperationName, input.Message);

                Assert.Equal(Expected, Actual);
            }
        }
        #region HttpEndpoints
        /// <summary>
        /// The integration tests that are run are stored in this class.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "HttpEndpoints")]
        public void HttpAddCustomerIntegrationTest()
        {
            #region Input
            
            
            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "AddCustomer",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            var uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            #endregion
            #region Expected
            var Expected = new Customer()
            {
                Id = 123,
                Name = new Name
                {
                    FirstName = "testFirst",
                    MiddleName = "testMiddle",
                    LastName = "testLast"
                },
                Address = new Address
                {
                    AddressLine1 = "testAddress1",
                    AddressLine2 = "testAddress2",
                    City = "testCity",
                    Country = "testCountry",
                    PostalZipCode = "testPostal",
                    ProvinceState = "testProvince"
                }
            };
            var client = new RestClient(uri);
            var request = new RestRequest(uri,Method.POST)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };
            request.AddParameter("application/json",input.Message, ParameterType.RequestBody);
            request.AddHeader("Content-Type", "application/json");
           
            #endregion
            
                client.Execute(request);
                //Since it's async, we need to wait a bit to make sure the message has been received and the ResultContainer set.
                Thread.Sleep(2000);
                Assert.Single(ResultContainer.CustomerInformationReceived);
                var Actual = ResultContainer.CustomerInformationReceived.First();

                #region Asserts
                Assert.Equal(Expected.Id, Actual.Id);
                Assert.Equal(Expected.Name.FirstName, Actual.Name.FirstName);
                Assert.Equal(Expected.Name.MiddleName, Actual.Name.MiddleName);
                Assert.Equal(Expected.Name.LastName, Actual.Name.LastName);
                Assert.Equal(Expected.Address.AddressLine1, Actual.Address.AddressLine1);
                Assert.Equal(Expected.Address.AddressLine2, Actual.Address.AddressLine2);
                Assert.Equal(Expected.Address.City, Actual.Address.City);
                Assert.Equal(Expected.Address.ProvinceState, Actual.Address.ProvinceState);
                Assert.Equal(Expected.Address.Country, Actual.Address.Country);
                Assert.Equal(Expected.Address.PostalZipCode, Actual.Address.PostalZipCode);
            #endregion
        }

        /// <summary>
        /// Tests the basic synchronous request using GetCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "HttpEndpoints")]
        public void HttpGetCustomerIntegrationTest()
        {
            #region Setup
            ResultContainer.Initialize();

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomer",
                MessageId = "id",
                Message = 112
            };
            var uri = string.Format("http://{0}:{1}/{2}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };
            request.AddParameter(input.MessageId,112, ParameterType.QueryString);
            request.AddHeader("Content-Type", "application/json");
            #endregion

            #region Expected Values
            var ExpectedCustomer = new Customer()
            {
                Id = 112,
                Name = new Name
                {
                    FirstName = "John",
                    MiddleName = "Jane",
                    LastName = "Doe"
                },
                Address = new Address
                {
                    AddressLine1 = "5 City Street",
                    AddressLine2 = "",
                    City = "Ottawa",
                    ProvinceState = "ON",
                    Country = "CA",
                    PostalZipCode = "5A5A5A"
                }
            };
            var Expected = JsonConvert.SerializeObject(ExpectedCustomer);
            #endregion

            
                var Actual = client.Execute(request);
           
            Thread.Sleep(2000);
            var payloadresult = JsonConvert.DeserializeObject<Payload>(Actual.Content);
            var customer = payloadresult.Match<string>(value => value, faults => string.Empty);
                var ActualCustomer = JsonConvert.DeserializeObject<Customer>(customer);

                #region Asserts
                Assert.Equal(ExpectedCustomer.Id, ActualCustomer.Id);
                Assert.Equal(ExpectedCustomer.Name.FirstName, ActualCustomer.Name.FirstName);
                Assert.Equal(ExpectedCustomer.Name.MiddleName, ActualCustomer.Name.MiddleName);
                Assert.Equal(ExpectedCustomer.Name.LastName, ActualCustomer.Name.LastName);
                Assert.Equal(ExpectedCustomer.Address.AddressLine1, ActualCustomer.Address.AddressLine1);
                Assert.Equal(ExpectedCustomer.Address.AddressLine2, ActualCustomer.Address.AddressLine2);
                Assert.Equal(ExpectedCustomer.Address.City, ActualCustomer.Address.City);
                Assert.Equal(ExpectedCustomer.Address.ProvinceState, ActualCustomer.Address.ProvinceState);
                Assert.Equal(ExpectedCustomer.Address.Country, ActualCustomer.Address.Country);
                Assert.Equal(ExpectedCustomer.Address.PostalZipCode, ActualCustomer.Address.PostalZipCode);
            
                #endregion
            
        }

        /// <summary>
        /// Tests the basic synchronous request using GetCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "HttpEndpoints")]
        public void HttpoNonexistantServiceTest()
        {
            #region Setup
            ResultContainer.Initialize();

            var input = new
            {
                ServiceName = "NoConsumerService",
                MessageId = "id",
                Message = 112
            };
            var uri = string.Format("http://{0}:{1}/{2}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };
            request.AddParameter(input.MessageId, input.Message, ParameterType.QueryString);
            request.AddHeader("Content-Type", "application/json");
            #endregion

            #region Expected Values
            var expectedCode = OrbitalFaultCode.Orbital_Business_Validation_Error_015;
            var expectedDetails = "Unable to retrieve the service definition due to JSON string cannot be empty or null.";
            #endregion


            var Actual = client.Execute(request);
            Thread.Sleep(2000);
            var payloadresult = JsonConvert.DeserializeObject<Payload>(Actual.Content);
            var faultsList = payloadresult.Match<IEnumerable<Fault>>(value => default(IEnumerable<Fault>), faults => faults);

            Assert.False(Actual.IsSuccessful);
            Assert.Equal(HttpStatusCode.BadRequest, Actual.StatusCode);
            foreach (var fault in faultsList)
            {
                Assert.NotNull(fault);
                Assert.Equal(expectedCode, fault.OrbitalFaultCode);
                Assert.Equal(expectedDetails, fault.Details);
            }
        }

        /// <summary>
        /// Tests the basic synchronous request using GetCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "HttpEndpoints")]
        public void HttpoNonexistantOperationTest()
        {
            #region Setup
            ResultContainer.Initialize();

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "NonGetCustomer",
                MessageId = "id",
                Message = 112
            };
            var uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };
            request.AddParameter(input.MessageId, input.Message, ParameterType.QueryString);
            request.AddHeader("Content-Type", "application/json");
            #endregion

            #region Expected Values
            var expectedCode = OrbitalFaultCode.Orbital_Business_Validation_Error_015;
            var expectedDetails = "Unable to find any operation that matches the given conditions";
            #endregion


            var Actual = client.Execute(request);
            Thread.Sleep(2000);
            var payloadresult = JsonConvert.DeserializeObject<Payload>(Actual.Content);
            var faultsList = payloadresult.Match<IEnumerable<Fault>>(value => default(IEnumerable<Fault>), faults => faults);
            Assert.False(Actual.IsSuccessful);
            Assert.Equal(HttpStatusCode.BadRequest, Actual.StatusCode);
            foreach (var fault in faultsList)
            {
                Assert.NotNull(fault);
                Assert.Equal(expectedCode, fault.OrbitalFaultCode);
                Assert.Equal(expectedDetails, fault.Details);
            }

        }
        #endregion

        #region ConsulDown
        /// <summary>
        /// Tests the basic asynchronous request using AddCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "ConsulDown")]
        public void ConsulIsNotUp()
        {
            #region Setup
            ResultContainer.Initialize();
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .UseConsulPort(8500)
                .AddNewService("ConsumerService");

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "AddCustomer",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            #endregion

            using (var Target = new OrbitalConnectionService())
            {
                var Actual = Assert.Throws<OrbitalConsulException>(() => Target.SendAsyncRaw(input.ServiceName, input.OperationName, input.Message));
            }
        }
        #endregion

        #region RabbitMQDown

        /// <summary>
        /// Tests the basic asynchronous request using AddCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "RabbitMQDown")]
        public void RabbitMQAddCustomerIntegrationTest()
        {
            #region Setup
            ResultContainer.Initialize();
            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .UseConsulPort(8500)
                .AddNewService("ConsumerService");

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "AddCustomer",
                Message = JsonConvert.SerializeObject(new Customer
                {
                    Id = 123,
                    Name = new Name
                    {
                        FirstName = "testFirst",
                        MiddleName = "testMiddle",
                        LastName = "testLast"
                    },
                    Address = new Address
                    {
                        AddressLine1 = "testAddress1",
                        AddressLine2 = "testAddress2",
                        City = "testCity",
                        Country = "testCountry",
                        PostalZipCode = "testPostal",
                        ProvinceState = "testProvince"
                    }
                })
            };
            #endregion
            using (var Target = new OrbitalConnectionService())
            {

                Assert.Throws<TimeoutException>(()=>Target.SendAsyncRaw(input.ServiceName, input.OperationName, input.Message));
            }
        }

        /// <summary>
        /// Tests the basic synchronous request using GetCustomer.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "RabbitMQDown")]
        public void RabbitMQGetCustomerIntegrationTest()
        {
            #region Setup
            ResultContainer.Initialize();

            var launchPad = new OrbitalLaunchPad()
                .UseConsulAddress(configuration.ConsulIp)
                .AddNewService("ConsumerService")
                .WithOperationTimeout("GetCustomer", TimeSpan.FromSeconds(30));

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomer",
                Message = JsonConvert.SerializeObject(new CustomerIdentifier(112))
            };
            #endregion
            

            using (var Target = new OrbitalConnectionService())
            {
                Assert.Throws<TimeoutException>(()=>Target.SendRaw(input.ServiceName, input.OperationName, input.Message));
            }
        }

        #endregion


        #region DatabaseAdapter

        /// <summary>
        /// Tests the database adapter will set up table properly and return the expected result.
        /// </summary>
        [LinuxOnlyTheory]
        [InlineData("ConsumerService", "CreateCustomerTable", "0 rows affected")]
        [InlineData("ConsumerService", "InsertCustomer", "1 rows affected")]
        [Trait("Category", "TableInitialization")]
        public async void TableInitializationCustomerTest(string ServiceName, string OperationName, string Result)
        {
            #region Setup

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, ServiceName, OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var Actual = payloadresult.Match<string>(value => value, faults => string.Empty);

            var Expected = Result;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the database adapter will update data and field attribute properly and return the expected result.
        /// </summary>
        [LinuxOnlyTheory]
        [InlineData("ConsumerService", "UpdateCustomer", "1 rows affected")]
        [InlineData("ConsumerService", "SetCustomerIDPrimaryKey", "1 rows affected")]
        [Trait("Category", "UpdateData")]
        public async void UpdateCustomerDataTest(string ServiceName, string OperationName, string Result)
        {
            #region Setup

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, ServiceName, OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var Actual = payloadresult.Match<string>(value => value, faults => string.Empty);

            var Expected = Result;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the database adapter will get data based on primary key properly and return the expected result.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "DatabaseAdapter")]
        public async void GetCustomerByIDTest()
        {
            #region Setup

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomerByID",
                FirstName = "Jim",
                LastName = "Goodman"
            };

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var Actual = JsonConvert.DeserializeObject<List<CustomerTest>>(payloadresult.Match<string>(value => value, faults => string.Empty));

            var ExpectedFirstName = input.FirstName;
            var ExpectedLastName = input.LastName;

            Assert.Equal(ExpectedFirstName, Actual[0].FirstName);
            Assert.Equal(ExpectedLastName, Actual[0].LastName);

        }

        /// <summary>
        /// Tests the database adapter will return the expected result when get data not exist.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "DatabaseAdapter")]
        public async void GetCustomerByIDNotExistTest()
        {
            #region Setup

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "GetCustomerByIDNotExist",
            };

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var Actual = payloadresult.Match<string>(value => value, faults => string.Empty);

            var Expected = "[]";

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the database adapter will remove data properly and return the expected result.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "DatabaseAdapter")]
        public async void RemoveCustomerTest()
        {
            #region Setup

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "RemoveCustomer",
                Result = "1 rows affected"
            };

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var Actual = payloadresult.Match<string>(value => value, faults => string.Empty);

            var Expected = input.Result;

            Assert.Equal(Expected, Actual);
        }

        /// <summary>
        /// Tests the database adapter will return the expected fault result when using error connection string.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "DatabaseAdapter")]
        public async void ErrorConnectionStringTest()
        {
            #region Setup

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "ErrorConnectionString",
                expectedCode = OrbitalFaultCode.Orbital_Business_Adapter_Error_012,
                expectedDetails = "Unable to connect to any of the specified MySQL hosts."
            };

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var faultsList = payloadresult.Match<IEnumerable<Fault>>(value => default(IEnumerable<Fault>), faults => faults);

            foreach (var fault in faultsList)
            {
                Assert.NotNull(fault);
                Assert.Equal(input.expectedCode, fault.OrbitalFaultCode);
                Assert.Equal(input.expectedDetails, fault.Details);
            }

        }

        /// <summary>
        /// Tests the database adapter will return the expected result with table not exist.
        /// </summary>
        [LinuxOnly]
        [Trait("Category", "DatabaseAdapter")]
        public async void TableNotExist()
        {
            #region Setup

            var input = new
            {
                ServiceName = "ConsumerService",
                OperationName = "TableNotExist",
                expectedCode = OrbitalFaultCode.Orbital_Business_Adapter_Error_012,
                expectedDetails = "Table 'Customer.User' doesn't exist"
            };

            string uri = string.Format("http://{0}:{1}/{2}/{3}", publicConfiguration.HttpIp, publicConfiguration.HttpPort, input.ServiceName, input.OperationName);
            var client = new RestClient(uri);
            var request = new RestRequest(uri, Method.GET)
            {
                JsonSerializer = new RestSharp.Serializers.JsonSerializer()
            };

            #endregion

            var cancellationTokenSource = new CancellationToken();

            var response = await client.ExecuteGetTaskAsync(request, cancellationTokenSource);

            var payloadresult = JsonConvert.DeserializeObject<Payload>(response.Content);

            var faultsList = payloadresult.Match<IEnumerable<Fault>>(value => default(IEnumerable<Fault>), faults => faults);

            foreach (var fault in faultsList)
            {
                Assert.NotNull(fault);
                Assert.Equal(input.expectedCode, fault.OrbitalFaultCode);
                Assert.Equal(input.expectedDetails, fault.Details);
            }

        }

        internal class CustomerTest
        {
            private CustomerTest() { }
            public int CustomerId { get; set; }
            public string LastName { get; set; }
            public string FirstName { get; set; }
        }

        #endregion
    }
}
