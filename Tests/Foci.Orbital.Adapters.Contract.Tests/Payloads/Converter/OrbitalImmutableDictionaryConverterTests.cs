using Bogus;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Foci.Orbital.Adapters.Contract.Models.Payloads.Converter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Payloads.Converter
{
    /// <summary>
    ///  Unit tests for the Json.Net OrbitalImmutableDictionaryConverter.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalImmutableDictionaryConverterTest
    {
        private readonly Faker faker = new Faker();
        /// <summary>
        /// Test whether the Converter can read and can write. We expect true for both.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void CanReadIsTrue()
        {
            var Target = new OrbitalImmutableDictionaryConverter();

            Assert.True(Target.CanRead);
            Assert.True(Target.CanWrite);
        }

        /// <summary>
        /// Testing the boolean function CanConvert. Should be able to convert a PayloadHeaders but not any other type.
        /// </summary>
        [Fact]
        [Trait("Category", "SuccessAndFailure")]
        public void CanConvertSuccessAndFailure()
        {
            var Target = new OrbitalImmutableDictionaryConverter();

            var ActualTrue = Target.CanConvert(typeof(PayloadHeaders));
            var ActualFalse = Target.CanConvert(typeof(string));

            Assert.True(ActualTrue);
            Assert.False(ActualFalse);
        }

        /// <summary>
        /// Testing the read and write json for the DsbPayloadHeaders.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void ReadJsonWriteJsonSuccess()
        {
            var pair = new Faker<Tuple<string, string>>()
                .CustomInstantiator(f => new Tuple<string, string>(f.Random.String(), f.Random.String()));
            var dictionary = Enumerable.Range(1, 20).Select(x => pair.Generate()).ToDictionary(x => x.Item1, x => x.Item2);
            var Input = new
            {
                PayloadHeader = new PayloadHeaders(dictionary),
                JsonSerializer = new JsonSerializer()
            };

            var Target = new OrbitalImmutableDictionaryConverter();

            var HeaderJson = JsonConvert.SerializeObject(Input.PayloadHeader, Target);
            var JsonReader = JObject.Parse(HeaderJson).CreateReader();

            var Actual = Target.ReadJson(JsonReader, null, new Object(), Input.JsonSerializer) as PayloadHeaders;
            var Expected = Input.PayloadHeader;

            foreach (var key in Expected.GetAllKeys())
            {
                Assert.Equal(Expected.Value(key), Actual.Value(key));
            }

        }

        /// <summary>
        /// Testing the read json for failure.
        /// </summary>
        [Fact]
        [Trait("Category", "Failure")]
        public void ReadJsonFailure()
        {
            var Target = new OrbitalImmutableDictionaryConverter();

            //Passing in null values to trigger an exception.
            var Actual = Target.ReadJson(null, null, new Object(), null) as PayloadHeaders;
            var Expected = new PayloadHeaders();

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.Count(), Actual.Count());
        }
    }
}
