using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Models.Payloads;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Payloads
{
    /// <summary>
    ///  Unit tests for the Payload.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PayloadTests
    {
        /// <summary>
        /// Test the static Payload.Create methods. Ensure values are set properly.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PayloadNonGenericCreationTests()
        {
            var Input = new
            {
                TestObject = "ArbitraryString",
                faults = new List<Fault>()
                {
                    Fault.Create("ArbitraryDetails", OrbitalFaultCode.Orbital_Business_Common_011)
                },
                TestHeaders = new PayloadHeaders()
            };

            var Target = Payload.Create(Input.TestObject);
            var Target2 = Payload.Create(Input.TestObject, Input.TestHeaders);
            var Target3 = Payload.Create(Input.faults);
            var Target4 = Payload.Create(Input.faults, Input.TestHeaders);


            #region Asserts
            Assert.Equal(Target.Match<string>(x => x, x => null), Input.TestObject);

            Assert.Equal(Target2.Match<string>(x => x, x => null), Input.TestObject);
            Assert.Equal(Target2.Headers, Input.TestHeaders);

            Assert.Equal(Target3.Match<IEnumerable<Fault>>(x => null, x => x), Input.faults);

            Assert.Equal(Target4.Headers, Input.TestHeaders);
            Assert.Equal(Target4.Match<IEnumerable<Fault>>(x => null, x => x), Input.faults);
            #endregion
        }

        /// <summary>
        /// Test json deserializer/serializer of the Payload. THis will use the JsonConstructor.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void PayloadJsonConstructorTest()
        {
            var Input = new
            {
                TestObject = "ArbitraryString",
                faults = new List<Fault>()
                {
                    Fault.Create("ArbitraryDetails", OrbitalFaultCode.Orbital_Business_Common_011)
                },
                TestHeaders = new PayloadHeaders(),
                Serializer = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                }
            };

            var Target = Payload.Create(Input.TestObject);
            var Target2 = Payload.Create(Input.TestObject, Input.TestHeaders);
            var Target3 = Payload.Create(Input.faults);
            var Target4 = Payload.Create(Input.faults, Input.TestHeaders);

            #region Expected and Actuals
            var Expected = Target;
            var Actual = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Target));

            var Expected2 = Target2;
            var Actual2 = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Target2));

            var Expected3 = Target3;
            var Actual3 = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Target3, Input.Serializer), Input.Serializer);

            var Expected4 = Target4;
            var Actual4 = JsonConvert.DeserializeObject<Payload>(JsonConvert.SerializeObject(Target4, Input.Serializer), Input.Serializer);

            #endregion

            Assert.Equal(Expected.Match<string>(x => x, x => null), Actual.Match<string>(x => x, x => null));
            Assert.Equal(Expected2.Match<string>(x => x, x => null), Actual2.Match<string>(x => x, x => null));
            Assert.Equal(Expected3.Match(x => x, x => x).ToString(), Actual3.Match(x => x, x => x).ToString());
            Assert.Equal(Expected4.Match(x => x, x => x).ToString(), Actual4.Match(x => x, x => x).ToString());
        }

        internal class TestClass
        {
            public string TestString { get; set; }
        }
    }
}
