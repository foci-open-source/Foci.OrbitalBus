using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Exceptions
{
    /// <summary>
    /// Unit tests for OrbitalException class
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalExceptionTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// This test method tests whether all the user exceptions derive from OrbitalException.
        /// If an exception is added that does not derive from OrbitalException the test will fail.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void AllExceptionsAreOfTypeOrbitalException()
        {
            var exceptionTypes = Assembly.GetAssembly(typeof(OrbitalException)).GetTypes().Where(t => t.IsSubclassOf(typeof(Exception)));

            foreach (var exception in exceptionTypes)
            {
                if (exception == typeof(OrbitalException))
                {
                    continue;
                }

                if (exception.IsSubclassOf(typeof(OrbitalException)))
                {
                    continue;
                }
                else
                {
                    throw new Exception("Class does not inherit from OrbitalException as it should.");
                }
            }
        }

    }
}
