using Bogus;
using Foci.Orbital.Adapters.Contract.Exceptions;
//using Foci.Orbital.Adapters.Contract.Exceptions.Helpers;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Agent.Exceptions;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Exceptions.Helpers
{
    /// <summary>
    /// Unit tests for the OrbitalExceptionHelper.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class OrbitalExceptionHelperTests
    {
        private readonly Faker faker = new Faker();

        /// <summary>
        /// Testing HasHeaders for true success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void HasHeadersTrueTest()
        {
            var Input = new OrbitalBusinessException();

            var Actual = Input.HasHeaders();

            Assert.True(Actual);
        }

        /// <summary>
        /// Testing HasHeaders for false success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void HasHeadersFalseTest()
        {
            var Input = new OrbitalAdapterException();

            var Actual = Input.HasHeaders();

            Assert.False(Actual);
        }


        /// <summary>
        /// Testing GetHeaders for success.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void GetHeadersTest()
        {
            var headers = new Dictionary<string, string>
            {
                { "TestKey", "TestValue" }
            };
            var Input = new OrbitalBusinessException(faker.Random.String(), OrbitalFaultCode.Orbital_Business_Common_011, headers);

            var Expected = new Dictionary<string, string>
            {
                { "TestKey", "TestValue" }
            };

            var Actual = Input.GetHeaders();

            Assert.IsType(Expected.GetType(), Actual);
            Assert.Equal(Expected.Count, Actual.Count);
            foreach (var kvp in Expected)
            {
                Assert.Equal(kvp.Value, Actual[kvp.Key]);
            }
        }
    }
}
