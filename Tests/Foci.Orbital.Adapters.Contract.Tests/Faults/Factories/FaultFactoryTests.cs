using Foci.Orbital.Adapters.Contract.Exceptions;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Factories;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Faults.Factories
{
    /// <summary>
    ///  Unit test for  the runtime fault factory
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class FaultFactoryTests
    {
        /// <summary>
        /// Testing the creation of all OrbitalException and then ensures the factory can create an exception for it.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void FaultCreationSuccessRoute()
        {
            var exceptionTypes = Assembly.GetAssembly(typeof(OrbitalException)).GetTypes().Where(t => t.IsSubclassOf(typeof(OrbitalException)));

            var Target = new FaultFactory();

            foreach (var exceptionType in exceptionTypes)
            {
                var inputException = Activator.CreateInstance(exceptionType) as OrbitalException;

                var Expected = inputException.Message;
                Fault Actual = Target.CreateFault(inputException);

                Assert.Equal(Expected, Actual.Details);
            }
        }

        /// <summary> 
        /// Test CreateFault method with a null exception
        /// </summary> 
        [Fact]
        [Trait("Category", "Failure")]
        public void CreateFaultNullException()
        {
            var Target = new FaultFactory();
            Assert.Throws<ArgumentNullException>(() => Target.CreateFault(null));
        }
    }
}
