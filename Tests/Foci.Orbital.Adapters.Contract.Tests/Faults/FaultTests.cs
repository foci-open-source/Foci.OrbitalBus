using Bogus;
using Foci.Orbital.Adapters.Contract.Faults;
using Foci.Orbital.Adapters.Contract.Faults.Lookups;
using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Foci.Orbital.Adapters.Contract.Tests.Faults
{
    /// <summary>
    ///  Unit test for the for Runtime Faults.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class FaultTests
    {
        /// <summary>
        /// Create all Runtime Faults and check if created properly.
        /// </summary>
        [Fact]
        [Trait("Category", "Success")]
        public void FaultCreation()
        {
            #region Substitutes
            var faker = new Faker();

            var faultCodes = Enum.GetValues(typeof(OrbitalFaultCode));

            var inputDetails = faker.Random.String();
            #endregion

            foreach (OrbitalFaultCode inputCode in faultCodes)
            {
                var ExpectedFaultCode = inputCode;
                var ExpectedFaultName = Enum.GetName(typeof(OrbitalFaultCode), ExpectedFaultCode);
                var ExpectedErrorDescription = ErrorDescriptionLookUp.GetErrorDescription(ExpectedFaultCode);
                var ExpectedDetails = inputDetails;

                var Actual = Fault.Create(inputDetails, inputCode);

                #region Asserts
                Assert.Equal(ExpectedFaultCode, Actual.OrbitalFaultCode);
                Assert.Equal(ExpectedFaultName, Actual.OrbitalFaultName);
                Assert.Equal(ExpectedErrorDescription, Actual.ErrorDescription);
                Assert.Equal(ExpectedDetails, Actual.Details);
                #endregion
            }
        }

        /// <summary> 
        /// Create Fault by only providing it with fault code 
        /// </summary> 
        [Fact]
        [Trait("Category", "Success")]
        public void FaultWithOnlyFaultCode()
        {
            #region Setup 
            var Inputs = new
            {
                code = OrbitalFaultCode.Orbital_BusinessFault_010
            };
            #endregion

            var ExpectedFaultCode = Inputs.code;
            var ExpectedFaultName = Enum.GetName(typeof(OrbitalFaultCode), ExpectedFaultCode);
            var ExpectedErrorDescription = ErrorDescriptionLookUp.GetErrorDescription(ExpectedFaultCode);
            var ExpectedDetails = string.Empty;

            var Actual = new Fault(null, null, null, Inputs.code);

            #region Asserts 
            Assert.Equal(ExpectedFaultCode, Actual.OrbitalFaultCode);
            Assert.Equal(ExpectedFaultName, Actual.OrbitalFaultName);
            Assert.Equal(ExpectedErrorDescription, Actual.ErrorDescription);
            Assert.Equal(ExpectedDetails, Actual.Details);
            #endregion
        }
    }
}
